package com.gegosoft.demoschool;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gegosoft.demoschool.Fragments.HomeWorkPendingFragment;
import com.gegosoft.demoschool.Fragments.HomeWorkFinishedFragment;
import com.google.android.material.tabs.TabLayout;

public class HomeWorkActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homeworklayout);
        tabLayout=findViewById(R.id.tablayout);
        viewPager=findViewById(R.id.viewpager);
        tabLayout.addTab(tabLayout.newTab().setText("PENDING"));
        tabLayout.addTab(tabLayout.newTab().setText("FINISHED"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        toolbar=findViewById(R.id.homeworktoolbar);
        toolbar.setTitle("HOME WORK");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tabLayout.setTabTextColors(getResources().getColor(R.color.black),getResources().getColor(R.color.white));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//        ActionBar actionBar=getSupportActionBar();
//        actionBar.setTitle("Homework");
//        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        final TabAdapter adapter = new TabAdapter(getSupportFragmentManager(), HomeWorkActivity.this, tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
    public class TabAdapter extends FragmentPagerAdapter{

        private Context myContext;
        int totalTabs;
        public TabAdapter(@NonNull FragmentManager fm,Context context,int totalTabs) {
            super(fm);
            myContext = context;
            this.totalTabs = totalTabs;
    }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    HomeWorkPendingFragment homeWorkPendingFragment = new HomeWorkPendingFragment();
                    return homeWorkPendingFragment;
                case 1:
                    HomeWorkFinishedFragment homeWorkFinishedFragment = new HomeWorkFinishedFragment();
                    return homeWorkFinishedFragment;
                    default:
                return null;
            }
        }
        @Override
        public int getCount() {
            return totalTabs;
        }
    }


}
