package com.gegosoft.demoschool;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.gegosoft.demoschool.Fragments.AllNotificationFragment;
import com.gegosoft.demoschool.Fragments.Message_Fragment;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.MessageAndNotificationModel;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Map;

public class NotificationActivity extends AppCompatActivity {
    ArrayList<MessageAndNotificationModel> notificationModels;
    RecyclerView recyclerView;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Api api;
    Toolbar toolbar;
    //    Toolbar toolbar;
//    MyAdapter myAdapter;
    TextView textnodata;
    TabLayout tabLayout;
    ViewPager viewPager;
//    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notificationlayout);
        tabLayout=findViewById(R.id.tablayout);
        viewPager=findViewById(R.id.viewpager);
        tabLayout.addTab(tabLayout.newTab().setText("ALL NOTIFICATIONS"));
        tabLayout.addTab(tabLayout.newTab().setText("MESSAGES"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Notifications");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tabLayout.setTabTextColors(getResources().getColor(R.color.black),getResources().getColor(R.color.white));

//        toolbar.setTitle("Notifications");
//        toolbar.setTitleTextColor(Color.WHITE);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        ActionBar actionBar=getSupportActionBar();
//        actionBar.setTitle("Notifications");
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        tabLayout.setTabTextColors(ColorStateList.valueOf(getResources().getColor(R.color.white)));
//        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.white));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        final NotificationActivity.TabAdapter adapter = new NotificationActivity.TabAdapter(getSupportFragmentManager(), NotificationActivity.this, tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        userDetailsSharedPref = UserDetailsSharedPref.getInstance(this);
//        recyclerView=findViewById(R.id.recyclerview);
//        LinearLayoutManager linearLayoutManager =new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
//        recyclerView.setLayoutManager(linearLayoutManager);
//        api = ApiClient.getClient().create(Api.class);
//        headermap = new HashMap<>();
//        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
//        headermap.put("Accept","application/json");
//        if (CheckNetwork.isInternetAvailable(this)){
//            getdata();
//        }
//        textnodata=findViewById(R.id.textnodata);
//        toolbar=findViewById(R.id.notificationtoolbar);
//        toolbar.setTitle("Notifications");
//        toolbar.setTitleTextColor(Color.WHITE);
//
//
//        setSupportActionBar(toolbar);
//
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });

    }
    public class TabAdapter extends FragmentPagerAdapter {

        private Context myContext;
        int totalTabs;
        public TabAdapter(@NonNull FragmentManager fm, Context context, int totalTabs) {
            super(fm);
            myContext = context;
            this.totalTabs = totalTabs;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    AllNotificationFragment allNotificationFragment = new AllNotificationFragment();
                    return allNotificationFragment;
                case 1:
                    Message_Fragment message_fragment = new Message_Fragment();
                    return message_fragment;
                default:
                    return null;
            }
        }
        @Override
        public int getCount() {
            return totalTabs;
        }
    }
//    private void getdata(){
//        Call<MessageModel> messageModelCall = api.getMessageList(headermap);
//        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString("message","Loading");
//        progressDialogFragment.setArguments(bundle);
//
//        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
//        messageModelCall.enqueue(new Callback<MessageModel>() {
//            @Override
//            public void onResponse(Call<MessageModel> call, Response<MessageModel> response) {
//                if (response.isSuccessful()){
//                    List<MessageModel.Datum> data =response.body().getData();
//                    if (data.size()!=0){
//                        myAdapter=new MyAdapter(data);
//                        recyclerView.setAdapter(myAdapter);
//                    }
//                    else {
//                        textnodata.setVisibility(View.VISIBLE);
//                    }
//
//                }
//                else {
//                    if (response.code()==401){
//                        AppUtils.SessionExpired(NotificationActivity.this);
//                    }
//                }
//                progressDialogFragment.dismiss();
//            }
//
//            @Override
//            public void onFailure(Call<MessageModel> call, Throwable t) {
//                AppUtils.APIFails(NotificationActivity.this,t);
//                progressDialogFragment.dismiss();
//            }
//        });
//
//
//
//    }
//    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{
//        List<MessageModel.Datum> data;
//       // int[] drawable = {R.drawable.green,R.drawable.pink,R.drawable.red,R.drawable.skyblue,R.drawable.violet,R.drawable.yellow};
//        public MyAdapter(List<MessageModel.Datum> data){
//            this.data=data;
//        }
//
//        @NonNull
//        @Override
//        public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//
//            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.notificationrecyclerview_layout,parent,false);
//            return new MyAdapter.MyViewHolder(view);
//        }
//
//        @Override
//        public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
////            int background =new Random().nextInt(drawable.length);
////            holder.icons.setImageResource(drawable[background]);
//            holder.heading.setText(data.get(position).getSubject());
//            holder.subheading.setText(data.get(position).getMessage());
//            holder.time.setText(data.get(position).getSentAt());
////            if (data.get(position).getReadStatus()==0){
////                holder.indicator.setVisibility(View.VISIBLE);
////            }
////            else {
////                holder.indicator.setVisibility(View.GONE);
////            }
//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    holder.subheading.setText(data.get(position).getMessage());
//                    holder.subheading.setMaxLines(Integer.MAX_VALUE);
//                    holder.subheading.setTextIsSelectable(true);
////                    if (data.get(position).getReadStatus()==0){
////                        if (CheckNetwork.isInternetAvailable(NotificationActivity.this)){
////                            UpdateReadStatus(data.get(position).getId(),data.get(position));
////                        }
////
////                    }
//
//
//                }
//            });
////            holder.subheading.setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View view) {
////                    ClipboardManager cm = (ClipboardManager)getBaseContext().getSystemService(Context.CLIPBOARD_SERVICE);
////                    cm.setText(holder.subheading.getText());
////                    Toast.makeText(getBaseContext(), "Copied to clipboard", Toast.LENGTH_SHORT).show();
////                }
////            });
//        }
//
//        @Override
//        public int getItemCount() {
//            return data.size();
//        }
//
//        public class MyViewHolder extends RecyclerView.ViewHolder {
//            ImageView icons;
//            TextView heading,subheading,time,indicator;
//            public MyViewHolder(@NonNull View itemView) {
//                super(itemView);
//                icons=itemView.findViewById(R.id.circularimage);
//                heading=itemView.findViewById(R.id.heading);
//                subheading=itemView.findViewById(R.id.subheading);
//                time=itemView.findViewById(R.id.notificationtime);
//                indicator=itemView.findViewById(R.id.indicator);
//
//            }
//        }
//    }
//    private void UpdateReadStatus(Integer id, MessageModel.Datum datum){
//        Call<JSONObject> jsonObjectCall = api.updatereadstatus(headermap, String.valueOf(id));
//        jsonObjectCall.enqueue(new Callback<JSONObject>() {
//            @Override
//            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
////                if (response.isSuccessful()){
////                    datum.setReadStatus(1);
////                    myAdapter.notifyDataSetChanged();
////
////                }
//            }
//
//            @Override
//            public void onFailure(Call<JSONObject> call, Throwable t) {
//
//            }
//        });
//
//    }
}

