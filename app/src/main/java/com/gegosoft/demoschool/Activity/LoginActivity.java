package com.gegosoft.demoschool.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.MainActivity;
import com.gegosoft.demoschool.Model.LoginModel;
import com.gegosoft.demoschool.Model.SchoolInfoModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import retrofit2.Call;
//import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {
    EditText edtmobile, edtpassword;
    Button login;
    UserDetailsSharedPref userDetailsSharedPref;
    TextView forgotpassword;

    String email,password;
    Api api;
    Map<String,String> headermap;
    TextView contact_number;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginpage_layout);
        userDetailsSharedPref = userDetailsSharedPref.getInstance(this);
        TextView TV = (TextView)findViewById(R.id.appCompatTextView);

        Spannable wordtoSpan = new SpannableString("Powered by GegoK12");

        wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.button_tint)), 11,18, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        TV.setText(wordtoSpan);

        edtmobile = findViewById(R.id.edtmobile);
        edtpassword = findViewById(R.id.passwordedt);
        contact_number = findViewById(R.id.contact_number);
        login = findViewById(R.id.loginbtn);
        forgotpassword = findViewById(R.id.forgetpassword);
        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edtmobile.getText())){
                    Toast.makeText(LoginActivity.this,R.string.app_name,Toast.LENGTH_LONG).show();
                }
                else {
                    edtmobile.setError("Enter Number");
                }
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(LoginActivity.this)){
                    userlogin();
                }


            }
        });
//        if (userDetailsSharedPref.isLoggedIn()){
//            startActivity(new Intent(LoginActivity.this, MainActivity.class));
//            finish();
//
//        }
        api = ApiClient.getClient().create(Api.class);
     //   getSchoolDetails();


    }

    private void userlogin() {
        if (validate()){
//            login.setEnabled(false);

//            String Manufacturername = getDeviceName();
//            int version = Build.VERSION.SDK_INT;
//            String oSName =  Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();


//            Log.d("Device Name",Manufacturername +" " +version + " "+oSName );

            ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putString("message","Loading");
            progressDialogFragment.setArguments(bundle);
            progressDialogFragment.show(getSupportFragmentManager(),"loading screen");

            Call<LoginModel> jsonObjectCall = api.login(email,password,userDetailsSharedPref.getString("firebasetoken"));
//           Log.d("token",userDetailsSharedPref.getString("firebasetoken"));
            jsonObjectCall.enqueue(new Callback<LoginModel>() {
                @Override
                public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                    if (response.isSuccessful()){
                        progressDialogFragment.dismiss();
                        userDetailsSharedPref.saveString("token",response.body().getToken());
                        userDetailsSharedPref.saveString("parentId", String.valueOf(response.body().getParentId()));
                        userDetailsSharedPref.createLoginSession();
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();

                    }
                    else {
                        if (response.code()== 422){
                            try {
                                String s = response.errorBody().string();
                                JSONObject jsonObject = new JSONObject(s);
                                String error = jsonObject.getString("errors");
                                JSONObject jsonObject1 = new JSONObject(error);
                                Iterator keys = jsonObject1.keys();
                                while (keys.hasNext()) {
                                    Object key = keys.next();
                                    JSONArray value = jsonObject1.getJSONArray((String) key);
                                    String errormessage = value.getString(0);
                                    if (key.equals("email")) {
                                        edtmobile.setError(errormessage);
                                        edtmobile.requestFocus();
                                    }
                                    if (key.equals("password")) {
                                        edtpassword.setError(errormessage);
                                        edtpassword.requestFocus();
                                    }
                                }


                            } catch (IOException | JSONException e) {
                                e.printStackTrace();
                            }
//                            try {
//                                String errorJson = response.errorBody().string();
//                                JSONObject jsonObject = new JSONObject(errorJson);
//                                JSONObject errors = jsonObject.getJSONObject("errors");
//                                JSONArray jsonArray = errors.getJSONArray("email");
//                                String jsonMessage = jsonArray.getString(0);
//
//                                Toast.makeText(LoginActivity.this,jsonMessage,Toast.LENGTH_SHORT).show();
//                            } catch (IOException | JSONException e) {
//                                e.printStackTrace();
//                            }
                        }
                        if (response.code()==500){
                            //  Gson gson = new Gson();
                            login.setEnabled(true);
//                            login.setBackgroundColor(getResources().getColor(R.color.settingscolor));
                            String s  = null;
                            try {
                                s = response.errorBody().string();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            Log.d("LUNA",s);
                            try {
                                JSONObject jsonObject1 = new JSONObject(s);
                                String errors =   jsonObject1.getString("errors");
                                JSONObject jsonObject2 = new JSONObject(errors);
                                JSONArray jsonArray = jsonObject2.getJSONArray("email");
                                String errorstring = jsonArray.getString(0);
                                Toast.makeText(LoginActivity.this, errorstring,Toast.LENGTH_LONG).show();

                                if(errorstring.equalsIgnoreCase("Invalid User")){
                                    edtmobile.setError(errorstring);
                                }
                                else if (errorstring.equalsIgnoreCase("Incorrect Password")){
                                    edtpassword.setError(errorstring);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Toast.makeText(LoginActivity.this,s,Toast.LENGTH_SHORT).show();

                        }

                    }
                    progressDialogFragment.dismiss();
                }

                @Override
                public void onFailure(Call<LoginModel> call, Throwable t) {
                    AppUtils.APIFails(LoginActivity.this,t);
                    Toast.makeText(LoginActivity.this,t.getMessage(),Toast.LENGTH_SHORT).show();
                    login.setEnabled(true);
                    progressDialogFragment.dismiss();
                }
            });

        }

//        Api api = ApiClient.getClient().create(Api.class);

    }
    private boolean validate(){
         email = edtmobile.getText().toString().trim();
         password = edtpassword.getText().toString().trim();


        if (email.isEmpty()) {
            edtmobile.setError("Mobile Number is required");
            edtmobile.requestFocus();
            return false;
        }
//        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
//            edtemail.setError("Email a valid email");
//            edtemail.requestFocus();
//            return false;
//        }
        if (password.isEmpty()) {

            edtpassword.setError("Password Required");
            edtpassword.requestFocus();
            return false;
        }
        if (password.length() < 6) {

            edtpassword.setError("password should be atleast 6 character long");
            edtpassword.requestFocus();
            return false;
        }
        return true;
    }
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }
    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }
    private void getSchoolDetails(){
        Call<SchoolInfoModel> schoolInfoModelCall = api.getSchoolDetails(headermap);
        schoolInfoModelCall.enqueue(new Callback<SchoolInfoModel>() {
            @Override
            public void onResponse(Call<SchoolInfoModel> call, Response<SchoolInfoModel> response) {
                if (response.isSuccessful()){
                    SchoolInfoModel.Data data =response.body().getData();
                    contact_number.setText(data.getLandlineNo());


                }
            }

            @Override
            public void onFailure(Call<SchoolInfoModel> call, Throwable t) {
                Log.d("er",t.toString());

            }
        });
    }

}