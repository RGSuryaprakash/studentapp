package com.gegosoft.demoschool.Activity;

import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.anychart.AnyChartView;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.Map;

public class ExamResultActivity extends AppCompatActivity {
    RecyclerView recyclerView;

    Toolbar toolbar;
    Api api;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String, String> headermap;
    int examid;
    AnyChartView anyChartView;
    LinearLayout linearlayout;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        setContentView(R.layout.home_page);
//        linearlayout =  findViewById(R.id.linearlayout);
//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int height = displayMetrics.heightPixels;
//        int width = displayMetrics.widthPixels;
//        for (int i =0;i<5;i++){
//            View layout2 = LayoutInflater.from(this).inflate(R.layout.seekbar_layout, linearlayout, false);
//            linearlayout.addView(layout2);
//            ConstraintLayout constraintLayout = layout2.findViewById(R.id.constraintLayout);
//            SeekBar seekBar =layout2.findViewById(R.id.seekBar_pricing);
//            TextView examname = layout2.findViewById(R.id.examname);
//            TextView marks = layout2.findViewById(R.id.marks);
//            seekBar.setEnabled(false);
//         //   seekBar.getThumb().mutate().setAlpha(255);
//
//            if (i==0){
//
//                examname.setText("1st Mid term");
//                marks.setText("80");
//                seekBar.setProgress(Integer.parseInt(marks.getText().toString()));
////                int max = seekBar.getMax();
////                float progress = 80%max;
////
////                int c = (int) (width/progress);
//                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT, (float) 0.8);
//
//                constraintLayout.setLayoutParams(layoutParams);
//
//
//            }
//            else if (i==1){
//                examname.setText("Quarterly");
//                marks.setText("60");
//                seekBar.setProgress(Integer.parseInt(marks.getText().toString()));
//
//            }
//            else if (i==2){
//                examname.setText("2nd Mid term");
//                marks.setText("85");
//                seekBar.setProgress(Integer.parseInt(marks.getText().toString()));
//            }
//            else if (i==3){
//                examname.setText("Half-yearly");
//                marks.setText("70");
//                seekBar.setProgress(Integer.parseInt(marks.getText().toString()));
//            }
//
//            else if (i==4){
//                examname.setText("3rd Mid term");
//                marks.setText("95");
//                seekBar.setProgress(Integer.parseInt(marks.getText().toString()));
//            }
////
//        }


       // anyChartView = findViewById(R.id.anychart);
//        Cartesian cartesian = AnyChart.column();
////        cartesian.data()
//        for (int i=0;i<5;i++){
//            List<DataEntry> data = new ArrayList<>();
//            if (i==0){
//                data.add(new ValueDataEntry("1st Mid term", 100));
//                data.add(new ValueDataEntry("Quarterly", 82));
//                data.add(new ValueDataEntry("2nd Mid term", 45));
//                data.add(new ValueDataEntry("Half-Yearly", 55));
//            }
//            else if (i==1){
//                data.add(new ValueDataEntry("1st Mid term", 45));
//                data.add(new ValueDataEntry("Quarterly", 60));
//                data.add(new ValueDataEntry("2nd Mid term", 55));
//                data.add(new ValueDataEntry("Half-Yearly", 70));
//            }
//            else if (i==2){
//                data.add(new ValueDataEntry("1st Mid term", 72));
//                data.add(new ValueDataEntry("Quarterly", 80));
//                data.add(new ValueDataEntry("2nd Mid term", 70));
//                data.add(new ValueDataEntry("Half-Yearly", 85));
//            }
//            else if (i==3){
//                data.add(new ValueDataEntry("1st Mid term", 63));
//                data.add(new ValueDataEntry("Quarterly", 90));
//                data.add(new ValueDataEntry("2nd Mid term", 74));
//                data.add(new ValueDataEntry("Half-Yearly", 68));
//            }
//
//            else if (i==4){
//                data.add(new ValueDataEntry("1st Mid term", 78));
//                data.add(new ValueDataEntry("Quarterly", 95));
//                data.add(new ValueDataEntry("2nd Mid term", 45));
//                data.add(new ValueDataEntry("Half-Yearly", 68));
//            }
//
//
////            Set set = Set.instantiate();
////            set.data(data);
////            Mapping series1Data = set.mapAs("{ x: 'x', value: 'value' }");
////            Mapping series2Data = set.mapAs("{ x: 'x', value: 'value2' }");
////            Mapping series3Data = set.mapAs("{ x: 'x', value: 'value3' }");
////            Mapping series4Data = set.mapAs("{ x: 'x', value: 'value4' }");
////            Mapping series5Data = set.mapAs("{ x: 'x', value: 'value5' }");
////            Mapping series6Data = set.mapAs("{ x: 'x', value: 'value6' }");
//            Column column = cartesian.column(data);
//
//            column.tooltip()
//                    .titleFormat("{%X}")
//                    .position(Position.CENTER_BOTTOM)
//                    .anchor(Anchor.CENTER_BOTTOM)
//                    .offsetX(0d)
//                    .offsetY(5d)
//                    .fontColor(String.valueOf(Color.RED))
//                    .format("${%Value}{groupsSeparator: }");
//            if (i==0){
//                column.name("English");
//                column.fill(new SolidFill("#ff0000", 1d));
//                column.stroke("1 #f7f3f3");
//                column.hovered().stroke("3 #f7f3f3");
//            }
//           else if (i==1){
//                column.name("Maths");
//                column.fill(new SolidFill("#ffff00", 1d));
//                column.stroke("1 #f7f3f3");
//                column.hovered().stroke("3 #f7f3f3");
//            }
//            else if (i==2){
//                column.name("Tamil");
//                column.fill(new SolidFill("#000000", 1d));
//                column.stroke("1 #f7f3f3");
//                column.hovered().stroke("3 #f7f3f3");
//            }
//            else if (i==3){
//                column.name("Science");
//                column.fill(new SolidFill("#ededed", 1d));
//                column.stroke("1 #f7f3f3");
//                column.hovered().stroke("3 #f7f3f3");
//            }
//            else if (i==4){
//                column.name("Skills");
//                column.fill(new SolidFill(String.valueOf(Color.BLUE), 1d));
//                column.stroke("1 #f7f3f3");
//                column.hovered().stroke("3 #f7f3f3");
//            }
//
//        }
//
//
//
//        cartesian.legend(true);
//        cartesian.animation(true);
//        cartesian.title("Compare");
//
//        cartesian.yScale().minimum(0d);
//        cartesian.yScale().maximum(100);
//        cartesian.yAxis(0).labels().format("{%Value}{groupsSeparator: }");
//
//        cartesian.tooltip().positionMode(TooltipPositionMode.POINT);
//        cartesian.interactivity().hoverMode(HoverMode.BY_X);
//
//        cartesian.xAxis(0).title("Exams");
//        cartesian.yAxis(0).title("Marks");
//
//        anyChartView.setChart(cartesian);
    }
}
