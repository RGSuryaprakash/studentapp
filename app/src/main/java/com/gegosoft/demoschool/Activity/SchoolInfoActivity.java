package com.gegosoft.demoschool.Activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.SchoolInfoModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolInfoActivity extends AppCompatActivity {
    Toolbar toolbar;
    Api api;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    ImageView schoolimage;
    TextView school_name,school_moto,affiliated_by,affiliated_number,date,board,landlinenumber,address,aboutus;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schoolinfo_activity);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(this);
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        schoolimage= findViewById(R.id.schoolimage);
        school_name= findViewById(R.id.school_name);
        school_moto= findViewById(R.id.school_moto);
        affiliated_by= findViewById(R.id.affiliated_by);
        affiliated_number= findViewById(R.id.affiliated_number);
        date= findViewById(R.id.date);
        board= findViewById(R.id.board);
        landlinenumber= findViewById(R.id.landlinenumber);
        address= findViewById(R.id.address);
        aboutus= findViewById(R.id.aboutus);

        toolbar= findViewById(R.id.toolbar);
        toolbar.setTitle("School info");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (CheckNetwork.isInternetAvailable(this)){
            getSchoolDetails();
        }
    }

    private void getSchoolDetails(){
        Call<SchoolInfoModel> schoolInfoModelCall = api.getSchoolDetails(headermap);
        schoolInfoModelCall.enqueue(new Callback<SchoolInfoModel>() {
            @Override
            public void onResponse(Call<SchoolInfoModel> call, Response<SchoolInfoModel> response) {
                if (response.isSuccessful()){
                    SchoolInfoModel.Data data =response.body().getData();
                    Glide.with(SchoolInfoActivity.this).load(data.getSchoolLogo()).into(schoolimage);
                    school_name.setText(data.getSchoolName());
                    school_moto.setText(data.getMoto());
                    affiliated_by.setText(getString(R.string.affiliated_by)+data.getAffiliatedBy());
                    affiliated_number.setText(getString(R.string.affiliated_no)+data.getAffiliationNo());
                    date.setText(getString(R.string.established_on)+data.getDateOfEstablishment());
                    board.setText(getString(R.string.board)+data.getBoard());
                    landlinenumber.setText(getString(R.string.landline_no)+data.getLandlineNo());
                    address.setText(data.getAddress()+","+data.getCity()+"-"+data.getPincode()+" "+data.getState()+" "+data.getCountry());
                    aboutus.setText(data.getAboutUs());
                }
                else
                {
                }
            }

            @Override
            public void onFailure(Call<SchoolInfoModel> call, Throwable t) {
                Toast.makeText(SchoolInfoActivity.this,t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }
}
