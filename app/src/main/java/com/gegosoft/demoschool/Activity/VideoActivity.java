package com.gegosoft.demoschool.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.Helper.CameraCapturerCompat;
import com.gegosoft.demoschool.Helper.Dialog;
//import com.gegosoft.demoschool.Helper.ScreenCapturerManager;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Receiver.PhoneStateReceiver;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.twilio.audioswitch.AudioDevice;
import com.twilio.audioswitch.AudioSwitch;

import com.twilio.video.AspectRatio;
import com.twilio.video.AudioCodec;
import com.twilio.video.CameraCapturer;
import com.twilio.video.ConnectOptions;
import com.twilio.video.EncodingParameters;
import com.twilio.video.G722Codec;
import com.twilio.video.H264Codec;
import com.twilio.video.IsacCodec;
import com.twilio.video.LocalAudioTrack;
import com.twilio.video.LocalDataTrack;
import com.twilio.video.LocalParticipant;
import com.twilio.video.LocalVideoTrack;
import com.twilio.video.NetworkQualityConfiguration;
import com.twilio.video.NetworkQualityLevel;
import com.twilio.video.NetworkQualityVerbosity;
import com.twilio.video.OpusCodec;
import com.twilio.video.PcmaCodec;
import com.twilio.video.PcmuCodec;
import com.twilio.video.RemoteAudioTrack;
import com.twilio.video.RemoteAudioTrackPublication;
import com.twilio.video.RemoteDataTrack;
import com.twilio.video.RemoteDataTrackPublication;
import com.twilio.video.RemoteParticipant;
import com.twilio.video.RemoteVideoTrack;
import com.twilio.video.RemoteVideoTrackPublication;
import com.twilio.video.Room;
import com.twilio.video.ScreenCapturer;
import com.twilio.video.TwilioException;
import com.twilio.video.Video;
import com.twilio.video.VideoCodec;
import com.twilio.video.VideoConstraints;
import com.twilio.video.VideoDimensions;
import com.twilio.video.VideoRenderer;
import com.twilio.video.VideoTextureView;
import com.twilio.video.VideoView;
import com.twilio.video.Vp8Codec;
import com.twilio.video.Vp9Codec;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kotlin.Unit;
public class VideoActivity extends AppCompatActivity {
    private static final int CAMERA_MIC_PERMISSION_REQUEST_CODE = 1;
    private static final String TAG = "VideoActivity";
    byte[] bytes;
    /*
     * Audio and video tracks can be created with names. This feature is useful for categorizing
     * tracks of participants. For example, if one participant publishes a video track with
     * ScreenCapturer and CameraCapturer with the names "screen" and "camera" respectively then
     * other participants can use RemoteVideoTrack#getName to determine which video track is
     * produced from the other participant's screen or camera.
     */
    private static final String LOCAL_AUDIO_TRACK_NAME = "mic";
    private static final String LOCAL_VIDEO_TRACK_NAME = "camera";

    /*
     * You must provide a Twilio Access Token to connect to the Video service
     */
    //  private static final String TWILIO_ACCESS_TOKEN = BuildConfig.TWILIO_ACCESS_TOKEN;
    private static final String TWILIO_ACCESS_TOKEN = "AC9ed987a221fcfc958016b2b36ddab44d";
    private static final int PICKFILE_RESULT_CODE = 2;
//    private static final String ACCESS_TOKEN_SERVER = BuildConfig.TWILIO_ACCESS_TOKEN_SERVER;

    /*
     * Access token used to connect. This field will be set either from the console generated token
     * or the request to the token server.
     */
    private String accessToken,roomName;

    /*
     * A Room represents communication between a local participant and one or more participants.
     */
    private Room room;
    private LocalParticipant localParticipant;

    /*
     * AudioCodec and VideoCodec represent the preferred codec for encoding and decoding audio and
     * video.
     */
    private AudioCodec audioCodec;
    private VideoCodec videoCodec;

    /*
     * Encoding parameters represent the sender side bandwidth constraints.
     */
    private EncodingParameters encodingParameters;

    /*
     * A VideoView receives frames from a local or remote video track and renders them
     * to an associated view.
     */
    private VideoView primaryVideoView;
    // private VideoView thumbnailVideoView;

    /*
     * Android shared preferences used for settings
     */
    private SharedPreferences preferences;

    /*
     * Android application UI elements
     */
    private CameraCapturerCompat cameraCapturerCompat;
    private LocalAudioTrack localAudioTrack;
    private LocalVideoTrack localVideoTrack;
    private LocalDataTrack localDataTrack;
    private FloatingActionButton connectActionFab;
    private FloatingActionButton switchCameraActionFab;
    private FloatingActionButton localVideoActionFab;
    private FloatingActionButton muteActionFab;
    private ProgressBar reconnectingProgressBar;
    private AlertDialog connectDialog;
    private String remoteParticipantIdentity;
    RecyclerView thumbnail_recyclerview;
    ThumbnailAdapter thumbnailAdapter;

    /*
     * Audio management
     */
//    private AudioDeviceSelector audioDeviceSelector;
    private AudioSwitch audioSwitch;

    private int savedVolumeControlStream;
    private MenuItem audioDeviceMenuItem;

    private VideoRenderer localVideoView;
    private boolean disconnectedFromOnDestroy;
    private boolean enableAutomaticSubscription;
    List<RemoteParticipant> remoteParticipantList;
    int screenheight,screenwidth;

    private static final String DATA_TRACK_MESSAGE_THREAD_NAME = "DataTrackMessages";

    private final HandlerThread dataTrackMessageThread =
            new HandlerThread(DATA_TRACK_MESSAGE_THREAD_NAME);
    private Handler dataTrackMessageThreadHandler;
    private final Map<RemoteDataTrack, RemoteParticipant> dataTrackRemoteParticipantMap =
            new HashMap<>();
    boolean isOnCall=false;
    AudioManager audioManager;
    TextView video_status_textview;
    //    private LocalVideoTrack screenVideoTrack;



//    private final ScreenCapturer.Listener screenCapturerListener = new ScreenCapturer.Listener() {
//        @Override
//        public void onScreenCaptureError(String errorDescription) {
//            Log.e(TAG, "Screen capturer error: " + errorDescription);
//            stopScreenCapture();
//            Toast.makeText(VideoActivity.this, R.string.screen_capture_error,
//                    Toast.LENGTH_LONG).show();
//        }
//
//        @Override
//        public void onFirstFrameAvailable() {
//            Log.d(TAG, "First frame from screen capturer available");
//        }
//    };
    private PhoneStateReceiver phoneStateReceiver = new PhoneStateReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
            try {
                String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                if(state.equals(TelephonyManager.EXTRA_STATE_RINGING)){
                    //  Toast.makeText(context,"Ringing State Number is - " + incomingNumber, Toast.LENGTH_SHORT).show();
                }
                else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){

                    if (room.getRemoteParticipants().size()!=0){
                        if (localAudioTrack != null) {
//                                boolean enable = !localAudioTrack.isEnabled();
                            localAudioTrack.enable(false);
//                                int icon = enable ?
//                                        R.drawable.ic_mic_white_24dp : R.drawable.ic_mic_off_black_24dp;
                            muteActionFab.setImageDrawable(ContextCompat.getDrawable(
                                    VideoActivity.this, R.drawable.ic_mic_off_black_24dp));


                        }


                    }

                    isOnCall = true;
                }
                else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)){
                    if (localAudioTrack != null) {
                        //  boolean enable = !localAudioTrack.isEnabled();
                        localAudioTrack.enable(true);
//                        int icon = enable ?
//                                R.drawable.ic_mic_white_24dp : R.drawable.ic_mic_off_black_24dp;
                        muteActionFab.setImageDrawable(ContextCompat.getDrawable(
                                VideoActivity.this, R.drawable.ic_mic_white_24dp));
                    }

                    isOnCall = false;
                }
                //     Toast.makeText(context,"Ringing State Number is - " + incomingNumber, Toast.LENGTH_SHORT).show();

            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.videochat_activity);
        video_status_textview = findViewById(R.id.video_status_textview);
        primaryVideoView = findViewById(R.id.primary_video_view);
        //  thumbnailVideoView = findViewById(R.id.thumbnail_video_view);
        reconnectingProgressBar = findViewById(R.id.reconnecting_progress_bar);

        connectActionFab = findViewById(R.id.connect_action_fab);
        switchCameraActionFab = findViewById(R.id.switch_camera_action_fab);
        localVideoActionFab = findViewById(R.id.local_video_action_fab);
        muteActionFab = findViewById(R.id.mute_action_fab);
        thumbnail_recyclerview = findViewById(R.id.thumbnail_recyclerview);
        thumbnail_recyclerview.setLayoutManager(new GridLayoutManager(this,2));
        remoteParticipantList =new ArrayList<>();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenheight = displayMetrics.heightPixels;
        screenwidth = displayMetrics.widthPixels;
        /*
         * Get shared preferences to read settings
         */
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        /*
         * Setup audio management and set the volume control stream
         */
        audioSwitch = new AudioSwitch(VideoActivity.this);
        savedVolumeControlStream = getVolumeControlStream();
        setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);

        // Get AudioManager
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

// Route audio through speaker
        audioManager.setSpeakerphoneOn(true);

        /*
         * Check camera and microphone permissions. Needed in Android M.
         */
        if (!checkPermissionForCameraAndMicrophone()) {
            requestPermissionForCameraAndMicrophone();
        } else {
            createAudioAndVideoTracks();
            setAccessToken();
        }

        /*
         * Set the initial state of the UI
         */
        intializeUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_video_activity, menu);
        audioDeviceMenuItem = menu.findItem(R.id.menu_audio_device);

        /*
         * Start the audio device selector after the menu is created and update the icon when the
         * selected audio device changes.
         */
//        audioDeviceSelector.start((audioDevices, audioDevice) -> {
//            updateAudioDeviceIcon(audioDevice);
//            return Unit.INSTANCE;
//        });
        audioSwitch.start((audioDevices, audioDevice) -> {
            updateAudioDeviceIcon(audioDevice);
            return Unit.INSTANCE;
        });

        connectToRoom(roomName);

//        if (audioManager.isSpeakerphoneOn()){
//            AudioDevice audioDevice = new AudioDevice.Speakerphone();
//            audioSwitch.selectDevice(audioDevice);
//            updateAudioDeviceIcon(audioDevice);
//
//        }
//        else {
//            audioSwitch.start((audioDevices, audioDevice) -> {
//                updateAudioDeviceIcon(audioDevice);
//                return Unit.INSTANCE;
//            });
//        }


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                startActivity(new Intent(this, SettingsActivity.class));

                return true;

            case R.id.menu_audio_device:
                showAudioDevices();
                return true;
            case R.id.menu_mute_all:

                String title = item.getTitle().toString();
                if (title.equalsIgnoreCase("Mute all")){
                    item.setTitle("Unmute all");
                }
                else {
                    item.setTitle("Mute all");

                }

                for (int i = 0 ; i < remoteParticipantList.size();i++){
                    RemoteAudioTrackPublication remoteAudioTrackPublication = remoteParticipantList.get(i)
                            .getRemoteAudioTracks().get(0);
                    if(remoteAudioTrackPublication.getRemoteAudioTrack().isPlaybackEnabled()){
                        remoteAudioTrackPublication.getRemoteAudioTrack().enablePlayback(false);
                    }
                    else {
                        remoteAudioTrackPublication.getRemoteAudioTrack().enablePlayback(true);
                    }

                }
                thumbnailAdapter.notifyDataSetChanged();

                return true;
            default:
                return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == CAMERA_MIC_PERMISSION_REQUEST_CODE) {
            boolean cameraAndMicPermissionGranted = true;

            for (int grantResult : grantResults) {
                cameraAndMicPermissionGranted &= grantResult == PackageManager.PERMISSION_GRANTED;
            }

            if (cameraAndMicPermissionGranted) {
                createAudioAndVideoTracks();
                setAccessToken();
            } else {
                Toast.makeText(this,
                        R.string.permissions_needed,
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(phoneStateReceiver,new IntentFilter(PhoneStateReceiver.ACTION_PHONE));

        /*
         * Update preferred audio and video codec in case changed in settings
         */
        audioCodec = getAudioCodecPreference(SettingsActivity.PREF_AUDIO_CODEC,
                SettingsActivity.PREF_AUDIO_CODEC_DEFAULT);
        videoCodec = getVideoCodecPreference(SettingsActivity.PREF_VIDEO_CODEC,
                SettingsActivity.PREF_VIDEO_CODEC_DEFAULT);
        enableAutomaticSubscription = getAutomaticSubscriptionPreference(SettingsActivity.PREF_ENABLE_AUTOMATIC_SUBSCRIPTION,
                SettingsActivity.PREF_ENABLE_AUTOMATIC_SUBSCRIPTION_DEFAULT);
        /*
         * Get latest encoding parameters
         */
        final EncodingParameters newEncodingParameters = getEncodingParameters();

        /*
         * If the local video track was released when the app was put in the background, recreate.
         */
        if (localVideoTrack == null && checkPermissionForCameraAndMicrophone()) {

            VideoConstraints videoConstraints = new VideoConstraints.Builder()
                    .aspectRatio(AspectRatio.ASPECT_RATIO_16_9)
                    .minVideoDimensions(VideoDimensions.CIF_VIDEO_DIMENSIONS)
                    .maxVideoDimensions(VideoDimensions.HD_720P_VIDEO_DIMENSIONS)
                    .minFps(5)
                    .maxFps(24)
                    .build();

            localVideoTrack = LocalVideoTrack.create(this,
                    true,
                    cameraCapturerCompat.getVideoCapturer(),videoConstraints,
                    LOCAL_VIDEO_TRACK_NAME);
            localVideoTrack.addRenderer(localVideoView);

            /*
             * If connected to a Room then share the local video track.
             */
            if (localParticipant != null) {
                localParticipant.publishTrack(localVideoTrack);

                /*
                 * Update encoding parameters if they have changed.
                 */
                if (!newEncodingParameters.equals(encodingParameters)) {
                    localParticipant.setEncodingParameters(newEncodingParameters);
                }
            }
        }

        /*
         * Update encoding parameters
         */
        encodingParameters = newEncodingParameters;

        /*
         * Update reconnecting UI
         */
        if (room != null) {
            reconnectingProgressBar.setVisibility((room.getState() != Room.State.RECONNECTING) ?
                    View.GONE :
                    View.VISIBLE);
        }
    }

    @Override
    protected void onPause() {
        /*
         * Release the local video track before going in the background. This ensures that the
         * camera can be used by other applications while this app is in the background.
         */

            if (localVideoTrack != null) {
                /*
                 * If this local video track is being shared in a Room, unpublish from room before
                 * releasing the video track. Participants will be notified that the track has been
                 * unpublished.
                 */
                if (localParticipant != null) {
                    localParticipant.unpublishTrack(localVideoTrack);
                }

                localVideoTrack.release();
                localVideoTrack = null;
            }


        super.onPause();
    }

    @Override
    protected void onDestroy() {
        /*
         * Tear down audio management and restore previous volume stream
         */
        audioSwitch.stop();
        setVolumeControlStream(savedVolumeControlStream);

        /*
         * Always disconnect from the room before leaving the Activity to
         * ensure any memory allocated to the Room resource is freed.
         */
        if (room != null && room.getState() != Room.State.DISCONNECTED) {
            room.disconnect();
            disconnectedFromOnDestroy = true;
        }
        dataTrackMessageThread.quit();

        /*
         * Release the local audio and video tracks ensuring any memory allocated to audio
         * or video is freed.
         */
        if (localAudioTrack != null) {
            localAudioTrack.release();
            localAudioTrack = null;
        }
        if (localVideoTrack != null) {
            localVideoTrack.release();
            localVideoTrack = null;
        }
//        if (screenVideoTrack != null) {
//            screenVideoTrack.release();
//            screenVideoTrack = null;
//        }

        super.onDestroy();
    }

    private boolean checkPermissionForCameraAndMicrophone() {
        int resultCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int resultMic = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        int resultPhoneState= ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        return resultCamera == PackageManager.PERMISSION_GRANTED &&
                resultMic == PackageManager.PERMISSION_GRANTED && resultPhoneState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissionForCameraAndMicrophone() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.RECORD_AUDIO)||ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_PHONE_STATE)) {
//            Toast.makeText(this,
//                    R.string.permissions_needed,
//                    Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO,Manifest.permission.READ_PHONE_STATE},
                    CAMERA_MIC_PERMISSION_REQUEST_CODE);

        } else {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO,Manifest.permission.READ_PHONE_STATE},
                    CAMERA_MIC_PERMISSION_REQUEST_CODE);
        }
    }

    private void createAudioAndVideoTracks() {
        // Share your microphone
        localAudioTrack = LocalAudioTrack.create(this, true, LOCAL_AUDIO_TRACK_NAME);

        // Share your camera
        cameraCapturerCompat = new CameraCapturerCompat(this, getAvailableCameraSource());
        VideoConstraints videoConstraints = new VideoConstraints.Builder()
                .aspectRatio(AspectRatio.ASPECT_RATIO_16_9)
                .minVideoDimensions(VideoDimensions.CIF_VIDEO_DIMENSIONS)
                .maxVideoDimensions(VideoDimensions.HD_720P_VIDEO_DIMENSIONS)
                .minFps(5)
                .maxFps(24)
                .build();
        localVideoTrack = LocalVideoTrack.create(this,
                true,
                cameraCapturerCompat.getVideoCapturer(),videoConstraints,
                LOCAL_VIDEO_TRACK_NAME);
        primaryVideoView.setMirror(true);
        localVideoTrack.addRenderer(primaryVideoView);
        localVideoView = primaryVideoView;

    }

    private CameraCapturer.CameraSource getAvailableCameraSource() {
        return (CameraCapturer.isSourceAvailable(CameraCapturer.CameraSource.FRONT_CAMERA)) ?
                (CameraCapturer.CameraSource.FRONT_CAMERA) :
                (CameraCapturer.CameraSource.BACK_CAMERA);
    }

    private void setAccessToken() {
        if(getIntent().getExtras()!=null){
            accessToken = getIntent().getExtras().getString("accessToken");
            roomName = getIntent().getExtras().getString("roomName");
        }
        // accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzYxMzI1OGU3NWE5MDc3YTY4YzM0ZmI5ZmRkMGQwZWQwLTE1OTU5NDI1MjAiLCJpc3MiOiJTSzYxMzI1OGU3NWE5MDc3YTY4YzM0ZmI5ZmRkMGQwZWQwIiwic3ViIjoiQUM5ZWQ5ODdhMjIxZmNmYzk1ODAxNmIyYjM2ZGRhYjQ0ZCIsImV4cCI6MTU5NTk0NjEyMCwiZ3JhbnRzIjp7ImlkZW50aXR5IjoiRW11bGF0b3IiLCJ2aWRlbyI6eyJyb29tIjoiTUFJTCJ9fX0.qz_B99QvuWRPTKd1RetV3YPqul0vKuRwoT9noA1tUDg";
//        if (!BuildConfig.USE_TOKEN_SERVER) {
//            /*
//             * OPTION 1 - Generate an access token from the getting started portal
//             * https://www.twilio.com/console/video/dev-tools/testing-tools and add
//             * the variable TWILIO_ACCESS_TOKEN setting it equal to the access token
//             * string in your local.properties file.
//             */
//            this.accessToken = TWILIO_ACCESS_TOKEN;
//        } else {
//            /*
//             * OPTION 2 - Retrieve an access token from your own web app.
//             * Add the variable ACCESS_TOKEN_SERVER assigning it to the url of your
//             * token server and the variable USE_TOKEN_SERVER=true to your
//             * local.properties file.
//             */
//            retrieveAccessTokenfromServer();
//        }
    }

    private void connectToRoom(String roomName) {
        audioSwitch.activate();
        localDataTrack = LocalDataTrack.create(this);
        dataTrackMessageThread.start();
        dataTrackMessageThreadHandler = new Handler(dataTrackMessageThread.getLooper());
        NetworkQualityConfiguration configuration =
                new NetworkQualityConfiguration(
                        NetworkQualityVerbosity.NETWORK_QUALITY_VERBOSITY_MINIMAL,
                        NetworkQualityVerbosity.NETWORK_QUALITY_VERBOSITY_MINIMAL);
        ConnectOptions.Builder connectOptionsBuilder = new ConnectOptions.Builder(accessToken)
                .roomName(roomName)
                .enableNetworkQuality(true)
                .networkQualityConfiguration(configuration);

        /*
         * Add local audio track to connect options to share with participants.
         */
        if (localAudioTrack != null) {
            connectOptionsBuilder
                    .audioTracks(Collections.singletonList(localAudioTrack));
        }

        /*
         * Add local video track to connect options to share with participants.
         */
        if (localVideoTrack != null) {
            connectOptionsBuilder.videoTracks(Collections.singletonList(localVideoTrack));
        }

        /*
         * Set the preferred audio and video codec for media.
         */
        connectOptionsBuilder.preferAudioCodecs(Collections.singletonList(audioCodec));
        connectOptionsBuilder.preferVideoCodecs(Collections.singletonList(videoCodec));
        connectOptionsBuilder.dataTracks(Collections.singletonList(localDataTrack));
        /*
         * Set the sender side encoding parameters.
         */
        connectOptionsBuilder.encodingParameters(encodingParameters);

        /*
         * Toggles automatic track subscription. If set to false, the LocalParticipant will receive
         * notifications of track publish events, but will not automatically subscribe to them. If
         * set to true, the LocalParticipant will automatically subscribe to tracks as they are
         * published. If unset, the default is true. Note: This feature is only available for Group
         * Rooms. Toggling the flag in a P2P room does not modify subscription behavior.
         */
        connectOptionsBuilder.enableAutomaticSubscription(enableAutomaticSubscription);

        room = Video.connect(this, connectOptionsBuilder.build(), roomListener());


        setDisconnectAction();
    }

    /*
     * The initial state when there is no active room.
     */
    private void intializeUI() {
        connectActionFab.setImageDrawable(ContextCompat.getDrawable(this,
                R.drawable.ic_video_call_white_24dp));
        connectActionFab.show();
        connectActionFab.setOnClickListener(connectActionClickListener());
        switchCameraActionFab.show();
        switchCameraActionFab.setOnClickListener(switchCameraClickListener());
        localVideoActionFab.show();
        localVideoActionFab.setOnClickListener(localVideoClickListener());
        muteActionFab.show();
        muteActionFab.setOnClickListener(muteClickListener());
        remoteParticipantList.clear();
        thumbnailAdapter = new ThumbnailAdapter();
        thumbnail_recyclerview.setAdapter(thumbnailAdapter);
    }

    /*
     * Show the current available audio devices.
     */
    private void showAudioDevices() {
        AudioDevice selectedDevice = audioSwitch.getSelectedAudioDevice();
        List<AudioDevice> availableAudioDevices = audioSwitch.getAvailableAudioDevices();

        if (selectedDevice != null) {
            int selectedDeviceIndex = availableAudioDevices.indexOf(selectedDevice);

            ArrayList<String> audioDeviceNames = new ArrayList<>();
            for (AudioDevice a : availableAudioDevices) {
                audioDeviceNames.add(a.getName());
            }

            new AlertDialog.Builder(this)
                    .setTitle(R.string.room_screen_select_device)
                    .setSingleChoiceItems(
                            audioDeviceNames.toArray(new CharSequence[0]),
                            selectedDeviceIndex,
                            (dialog, index) -> {
                                dialog.dismiss();
                                AudioDevice selectedAudioDevice = availableAudioDevices.get(index);
                                updateAudioDeviceIcon(selectedAudioDevice);
                                audioSwitch.selectDevice(selectedAudioDevice);
                            }).create().show();
        }
    }

    /*
     * Update the menu icon based on the currently selected audio device.
     */
    private void updateAudioDeviceIcon(AudioDevice selectedAudioDevice) {
        int audioDeviceMenuIcon = R.drawable.ic_phonelink_ring_white_24dp;

        if (selectedAudioDevice instanceof AudioDevice.BluetoothHeadset) {
            audioDeviceMenuIcon = R.drawable.ic_bluetooth_white_24dp;
        } else if (selectedAudioDevice instanceof AudioDevice.WiredHeadset) {
            audioDeviceMenuIcon = R.drawable.ic_headset_mic_white_24dp;
        } else if (selectedAudioDevice instanceof AudioDevice.Earpiece) {
            audioDeviceMenuIcon = R.drawable.ic_phonelink_ring_white_24dp;
        } else if (selectedAudioDevice instanceof AudioDevice.Speakerphone) {
            audioDeviceMenuIcon = R.drawable.ic_volume_up_white_24dp;
        }

        audioDeviceMenuItem.setIcon(audioDeviceMenuIcon);
    }

    /*
     * Get the preferred audio codec from shared preferences
     */
    private AudioCodec getAudioCodecPreference(String key, String defaultValue) {
        final String audioCodecName = preferences.getString(key, defaultValue);

        switch (audioCodecName) {
            case IsacCodec.NAME:
                return new IsacCodec();
            case OpusCodec.NAME:
                return new OpusCodec();
            case PcmaCodec.NAME:
                return new PcmaCodec();
            case PcmuCodec.NAME:
                return new PcmuCodec();
            case G722Codec.NAME:
                return new G722Codec();
            default:
                return new OpusCodec();
        }
    }

    /*
     * Get the preferred video codec from shared preferences
     */
    private VideoCodec getVideoCodecPreference(String key, String defaultValue) {
        final String videoCodecName = preferences.getString(key, defaultValue);

        switch (videoCodecName) {
            case Vp8Codec.NAME:
                boolean simulcast = preferences.getBoolean(SettingsActivity.PREF_VP8_SIMULCAST,
                        SettingsActivity.PREF_VP8_SIMULCAST_DEFAULT);
                return new Vp8Codec(simulcast);
            case H264Codec.NAME:
                return new H264Codec();
            case Vp9Codec.NAME:
                return new Vp9Codec();
            default:
                return new Vp8Codec();
        }
    }

    private boolean getAutomaticSubscriptionPreference(String key, boolean defaultValue) {
        return preferences.getBoolean(key, defaultValue);
    }

    private EncodingParameters getEncodingParameters() {
        final int maxAudioBitrate = Integer.parseInt(
                preferences.getString(SettingsActivity.PREF_SENDER_MAX_AUDIO_BITRATE,
                        SettingsActivity.PREF_SENDER_MAX_AUDIO_BITRATE_DEFAULT));
        final int maxVideoBitrate = Integer.parseInt(
                preferences.getString(SettingsActivity.PREF_SENDER_MAX_VIDEO_BITRATE,
                        SettingsActivity.PREF_SENDER_MAX_VIDEO_BITRATE_DEFAULT));

        return new EncodingParameters(maxAudioBitrate, maxVideoBitrate);
    }

    /*
     * The actions performed during disconnect.
     */
    private void setDisconnectAction() {
        connectActionFab.setImageDrawable(ContextCompat.getDrawable(this,
                R.drawable.ic_call_end_white_24px));
        connectActionFab.show();
        connectActionFab.setOnClickListener(disconnectClickListener());
    }

    /*
     * Creates an connect UI dialog
     */
    private void showConnectDialog() {
        EditText roomEditText = new EditText(this);

        connectDialog = Dialog.createConnectDialog(roomEditText,
                connectClickListener(roomEditText),
                cancelConnectDialogClickListener(),
                this);
        connectDialog.show();
    }

    /*
     * Called when remote participant joins the room
     */
    @SuppressLint("SetTextI18n")
    private void addRemoteParticipant(RemoteParticipant remoteParticipant) {
        /*
         * This app only displays video for one additional participant per Room
         */
//        if (thumbnailVideoView.getVisibility() == View.VISIBLE) {
//            Snackbar.make(connectActionFab,
//                    "Multiple participants are not currently support in this UI",
//                    Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show();
//            return;
//        }
        remoteParticipantIdentity = remoteParticipant.getIdentity();



        /*
         * Add remote participant renderer
         */
        thumbnailAdapter.addThumbnail(remoteParticipant);


//        if (remoteParticipant.getRemoteVideoTracks().size() > 0) {
//
//            RemoteVideoTrackPublication remoteVideoTrackPublication =
//                    remoteParticipant.getRemoteVideoTracks().get(0);
//
//            /*
//             * Only render video tracks that are subscribed to
//             */
//            if (remoteVideoTrackPublication.isTrackSubscribed()) {
//                addRemoteParticipantVideo(remoteVideoTrackPublication.getRemoteVideoTrack());
//            }
//        }

        /*
         * Start listening for participant events
         */
        remoteParticipant.setListener(remoteParticipantListener());
        for (final RemoteDataTrackPublication remoteDataTrackPublication :
                remoteParticipant.getRemoteDataTracks()) {
            /*
             * Data track messages are received on the thread that calls setListener. Post the
             * invocation of setting the listener onto our dedicated data track message thread.
             */
            if (remoteDataTrackPublication.isTrackSubscribed()) {
                dataTrackMessageThreadHandler.post(() -> addRemoteDataTrack(remoteParticipant,
                        remoteDataTrackPublication.getRemoteDataTrack()));
            }
        }
        video_status_textview.setText(String.valueOf(room.getRemoteParticipants().size()));
    }
    private void addRemoteDataTrack(RemoteParticipant remoteParticipant,
                                    RemoteDataTrack remoteDataTrack) {
        dataTrackRemoteParticipantMap.put(remoteDataTrack, remoteParticipant);
        remoteDataTrack.setListener(remoteDataTrackListener());
    }

    /*
     * Set primary view as renderer for participant video track
     */
//    private void addRemoteParticipantVideo(VideoTrack videoTrack) {
//        moveLocalVideoToThumbnailView();
//        primaryVideoView.setMirror(false);
//        videoTrack.addRenderer(primaryVideoView);
//    }

    private void moveLocalVideoToThumbnailView() {
//        if (thumbnailVideoView.getVisibility() == View.GONE) {
//            thumbnailVideoView.setVisibility(View.VISIBLE);
//            localVideoTrack.removeRenderer(primaryVideoView);
//            localVideoTrack.addRenderer(thumbnailVideoView);
//            localVideoView = thumbnailVideoView;
//            thumbnailVideoView.setMirror(cameraCapturerCompat.getCameraSource() ==
//                    CameraCapturer.CameraSource.FRONT_CAMERA);
//        }
    }

    /*
     * Called when remote participant leaves the room
     */
    @SuppressLint("SetTextI18n")
    private void removeRemoteParticipant(RemoteParticipant remoteParticipant) {

        for (int i =0 ; i<remoteParticipantList.size();i++){

            if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
                if (!remoteParticipant.isConnected()){
                    remoteParticipantList.remove(remoteParticipantList.get(i));
                    thumbnailAdapter.notifyItemRemoved(i);
                }
            }
        }
//        if (!remoteParticipant.getIdentity().equals(remoteParticipantIdentity)) {
//            return;
//        }

        /*
         * Remove remote participant renderer
         */
//        if (!remoteParticipant.getRemoteVideoTracks().isEmpty()) {
//            RemoteVideoTrackPublication remoteVideoTrackPublication =
//                    remoteParticipant.getRemoteVideoTracks().get(0);
//
//            /*
//             * Remove video only if subscribed to participant track
//             */
//            if (remoteVideoTrackPublication.isTrackSubscribed()) {
//                removeParticipantVideo(remoteVideoTrackPublication.getRemoteVideoTrack());
//            }
//        }
        moveLocalVideoToPrimaryView();
    }

//    private void removeParticipantVideo(VideoTrack videoTrack) {
//        videoTrack.removeRenderer(primaryVideoView);
//    }

    private void moveLocalVideoToPrimaryView() {
//        if (thumbnailVideoView.getVisibility() == View.VISIBLE) {
//            thumbnailVideoView.setVisibility(View.GONE);
//            if (localVideoTrack != null) {
//                localVideoTrack.removeRenderer(thumbnailVideoView);
//                localVideoTrack.addRenderer(primaryVideoView);
//            }
//            localVideoView = primaryVideoView;
//            primaryVideoView.setMirror(cameraCapturerCompat.getCameraSource() ==
//                    CameraCapturer.CameraSource.FRONT_CAMERA);
//        }
        if (remoteParticipantList.size()==0){
            if (localVideoTrack != null) {
//                localVideoTrack.removeRenderer(thumbnailVideoView);
                thumbnailAdapter.notifyDataSetChanged();
                localVideoTrack.addRenderer(primaryVideoView);
            }
            localVideoView = primaryVideoView;
            primaryVideoView.setMirror(cameraCapturerCompat.getCameraSource() ==
                    CameraCapturer.CameraSource.FRONT_CAMERA);
        }

    }

    /*
     * Room events listener
     */
    @SuppressLint("SetTextI18n")
    private Room.Listener roomListener() {
        return new Room.Listener() {
            @Override
            public void onConnected(Room room) {
                localParticipant = room.getLocalParticipant();
                localParticipant.publishTrack(localDataTrack);
                setTitle(room.getName());

//                for (RemoteParticipant remoteParticipant : room.getRemoteParticipants()) {
//                    addRemoteParticipant(remoteParticipant);
//                    break;
//                }
                //    remoteParticipantList = room.getRemoteParticipants();
                for (RemoteParticipant remoteParticipant : room.getRemoteParticipants()) {
                    addRemoteParticipant(remoteParticipant);
                    // break;
                }
//                thumbnailAdapter.notifyDataSetChanged();



            }

            @Override
            public void onReconnecting(@NonNull Room room, @NonNull TwilioException twilioException) {
                reconnectingProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onReconnected(@NonNull Room room) {
                reconnectingProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onConnectFailure(Room room, TwilioException e) {
                Toast.makeText(VideoActivity.this,e.getExplanation(),Toast.LENGTH_LONG).show();
                audioSwitch.deactivate();
                intializeUI();
            }

            @Override
            public void onDisconnected(Room room, TwilioException e) {
                localParticipant = null;
                reconnectingProgressBar.setVisibility(View.GONE);
                VideoActivity.this.room = null;
                // Only reinitialize the UI if disconnect was not called from onDestroy()
                if (!disconnectedFromOnDestroy) {
                    audioSwitch.deactivate();
                    intializeUI();
                    moveLocalVideoToPrimaryView();
                    finish();
                }
            }

            @Override
            public void onParticipantConnected(Room room, RemoteParticipant remoteParticipant) {

                addRemoteParticipant(remoteParticipant);

            }

            @Override
            public void onParticipantDisconnected(Room room, RemoteParticipant remoteParticipant) {
                removeRemoteParticipant(remoteParticipant);
            }

            @Override
            public void onRecordingStarted(Room room) {
                /*
                 * Indicates when media shared to a Room is being recorded. Note that
                 * recording is only available in our Group Rooms developer preview.
                 */
                Log.d(TAG, "onRecordingStarted");
            }

            @Override
            public void onRecordingStopped(Room room) {
                /*
                 * Indicates when media shared to a Room is no longer being recorded. Note that
                 * recording is only available in our Group Rooms developer preview.
                 */
                Log.d(TAG, "onRecordingStopped");
            }
        };
    }

    @SuppressLint("SetTextI18n")
    private RemoteParticipant.Listener remoteParticipantListener() {
        return new RemoteParticipant.Listener() {
            @Override
            public void onAudioTrackPublished(RemoteParticipant remoteParticipant,
                                              RemoteAudioTrackPublication remoteAudioTrackPublication) {
                Log.i(TAG, String.format("onAudioTrackPublished: " +
                                "[RemoteParticipant: identity=%s], " +
                                "[RemoteAudioTrackPublication: sid=%s, enabled=%b, " +
                                "subscribed=%b, name=%s]",
                        remoteParticipant.getIdentity(),
                        remoteAudioTrackPublication.getTrackSid(),
                        remoteAudioTrackPublication.isTrackEnabled(),
                        remoteAudioTrackPublication.isTrackSubscribed(),
                        remoteAudioTrackPublication.getTrackName()));
            }

            @Override
            public void onAudioTrackUnpublished(RemoteParticipant remoteParticipant,
                                                RemoteAudioTrackPublication remoteAudioTrackPublication) {
                Log.i(TAG, String.format("onAudioTrackUnpublished: " +
                                "[RemoteParticipant: identity=%s], " +
                                "[RemoteAudioTrackPublication: sid=%s, enabled=%b, " +
                                "subscribed=%b, name=%s]",
                        remoteParticipant.getIdentity(),
                        remoteAudioTrackPublication.getTrackSid(),
                        remoteAudioTrackPublication.isTrackEnabled(),
                        remoteAudioTrackPublication.isTrackSubscribed(),
                        remoteAudioTrackPublication.getTrackName()));
            }

            @Override
            public void onDataTrackPublished(RemoteParticipant remoteParticipant,
                                             RemoteDataTrackPublication remoteDataTrackPublication) {
                Log.i(TAG, String.format("onDataTrackPublished: " +
                                "[RemoteParticipant: identity=%s], " +
                                "[RemoteDataTrackPublication: sid=%s, enabled=%b, " +
                                "subscribed=%b, name=%s]",
                        remoteParticipant.getIdentity(),
                        remoteDataTrackPublication.getTrackSid(),
                        remoteDataTrackPublication.isTrackEnabled(),
                        remoteDataTrackPublication.isTrackSubscribed(),
                        remoteDataTrackPublication.getTrackName()));
            }

            @Override
            public void onDataTrackUnpublished(RemoteParticipant remoteParticipant,
                                               RemoteDataTrackPublication remoteDataTrackPublication) {
                Log.i(TAG, String.format("onDataTrackUnpublished: " +
                                "[RemoteParticipant: identity=%s], " +
                                "[RemoteDataTrackPublication: sid=%s, enabled=%b, " +
                                "subscribed=%b, name=%s]",
                        remoteParticipant.getIdentity(),
                        remoteDataTrackPublication.getTrackSid(),
                        remoteDataTrackPublication.isTrackEnabled(),
                        remoteDataTrackPublication.isTrackSubscribed(),
                        remoteDataTrackPublication.getTrackName()));
            }

            @Override
            public void onVideoTrackPublished(RemoteParticipant remoteParticipant,
                                              RemoteVideoTrackPublication remoteVideoTrackPublication) {
                Log.i(TAG, String.format("onVideoTrackPublished: " +
                                "[RemoteParticipant: identity=%s], " +
                                "[RemoteVideoTrackPublication: sid=%s, enabled=%b, " +
                                "subscribed=%b, name=%s]",
                        remoteParticipant.getIdentity(),
                        remoteVideoTrackPublication.getTrackSid(),
                        remoteVideoTrackPublication.isTrackEnabled(),
                        remoteVideoTrackPublication.isTrackSubscribed(),
                        remoteVideoTrackPublication.getTrackName()));
            }

            @Override
            public void onVideoTrackUnpublished(RemoteParticipant remoteParticipant,
                                                RemoteVideoTrackPublication remoteVideoTrackPublication) {
                Log.i(TAG, String.format("onVideoTrackUnpublished: " +
                                "[RemoteParticipant: identity=%s], " +
                                "[RemoteVideoTrackPublication: sid=%s, enabled=%b, " +
                                "subscribed=%b, name=%s]",
                        remoteParticipant.getIdentity(),
                        remoteVideoTrackPublication.getTrackSid(),
                        remoteVideoTrackPublication.isTrackEnabled(),
                        remoteVideoTrackPublication.isTrackSubscribed(),
                        remoteVideoTrackPublication.getTrackName()));
            }

            @Override
            public void onAudioTrackSubscribed(RemoteParticipant remoteParticipant,
                                               RemoteAudioTrackPublication remoteAudioTrackPublication,
                                               RemoteAudioTrack remoteAudioTrack) {
                Log.i(TAG, String.format("onAudioTrackSubscribed: " +
                                "[RemoteParticipant: identity=%s], " +
                                "[RemoteAudioTrack: enabled=%b, playbackEnabled=%b, name=%s]",
                        remoteParticipant.getIdentity(),
                        remoteAudioTrack.isEnabled(),
                        remoteAudioTrack.isPlaybackEnabled(),
                        remoteAudioTrack.getName()));
            }

            @Override
            public void onAudioTrackUnsubscribed(RemoteParticipant remoteParticipant,
                                                 RemoteAudioTrackPublication remoteAudioTrackPublication,
                                                 RemoteAudioTrack remoteAudioTrack) {
                Log.i(TAG, String.format("onAudioTrackUnsubscribed: " +
                                "[RemoteParticipant: identity=%s], " +
                                "[RemoteAudioTrack: enabled=%b, playbackEnabled=%b, name=%s]",
                        remoteParticipant.getIdentity(),
                        remoteAudioTrack.isEnabled(),
                        remoteAudioTrack.isPlaybackEnabled(),
                        remoteAudioTrack.getName()));
            }

            @Override
            public void onAudioTrackSubscriptionFailed(RemoteParticipant remoteParticipant,
                                                       RemoteAudioTrackPublication remoteAudioTrackPublication,
                                                       TwilioException twilioException) {
                Log.i(TAG, String.format("onAudioTrackSubscriptionFailed: " +
                                "[RemoteParticipant: identity=%s], " +
                                "[RemoteAudioTrackPublication: sid=%b, name=%s]" +
                                "[TwilioException: code=%d, message=%s]",
                        remoteParticipant.getIdentity(),
                        remoteAudioTrackPublication.getTrackSid(),
                        remoteAudioTrackPublication.getTrackName(),
                        twilioException.getCode(),
                        twilioException.getMessage()));
            }

            @Override
            public void onDataTrackSubscribed(RemoteParticipant remoteParticipant,
                                              RemoteDataTrackPublication remoteDataTrackPublication,
                                              RemoteDataTrack remoteDataTrack) {
                Log.i(TAG, String.format("onDataTrackSubscribed: " +
                                "[RemoteParticipant: identity=%s], " +
                                "[RemoteDataTrack: enabled=%b, name=%s]",
                        remoteParticipant.getIdentity(),
                        remoteDataTrack.isEnabled(),
                        remoteDataTrack.getName()));
                dataTrackMessageThreadHandler.post(() -> addRemoteDataTrack(remoteParticipant, remoteDataTrack));

            }

            @Override
            public void onDataTrackUnsubscribed(RemoteParticipant remoteParticipant,
                                                RemoteDataTrackPublication remoteDataTrackPublication,
                                                RemoteDataTrack remoteDataTrack) {
                Log.i(TAG, String.format("onDataTrackUnsubscribed: " +
                                "[RemoteParticipant: identity=%s], " +
                                "[RemoteDataTrack: enabled=%b, name=%s]",
                        remoteParticipant.getIdentity(),
                        remoteDataTrack.isEnabled(),
                        remoteDataTrack.getName()));
            }

            @Override
            public void onDataTrackSubscriptionFailed(RemoteParticipant remoteParticipant,
                                                      RemoteDataTrackPublication remoteDataTrackPublication,
                                                      TwilioException twilioException) {
                Log.i(TAG, String.format("onDataTrackSubscriptionFailed: " +
                                "[RemoteParticipant: identity=%s], " +
                                "[RemoteDataTrackPublication: sid=%b, name=%s]" +
                                "[TwilioException: code=%d, message=%s]",
                        remoteParticipant.getIdentity(),
                        remoteDataTrackPublication.getTrackSid(),
                        remoteDataTrackPublication.getTrackName(),
                        twilioException.getCode(),
                        twilioException.getMessage()));
            }

            @Override
            public void onVideoTrackSubscribed(RemoteParticipant remoteParticipant,
                                               RemoteVideoTrackPublication remoteVideoTrackPublication,
                                               RemoteVideoTrack remoteVideoTrack) {
                Log.i(TAG, String.format("onVideoTrackSubscribed: " +
                                "[RemoteParticipant: identity=%s], " +
                                "[RemoteVideoTrack: enabled=%b, name=%s]",
                        remoteParticipant.getIdentity(),
                        remoteVideoTrack.isEnabled(),
                        remoteVideoTrack.getName()));
                //  addRemoteParticipantVideo(remoteVideoTrack);

//                if (remoteParticipant.isConnected()){
                if (remoteParticipantList.size()!=0){
                    for (int i =0 ; i<remoteParticipantList.size();i++){
                        if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
                            //remoteParticipantList.remove(remoteParticipantList.get(i));
                            thumbnailAdapter.notifyItemChanged(i);

                        }
                    }


                }



            }

            @Override
            public void onVideoTrackUnsubscribed(RemoteParticipant remoteParticipant,
                                                 RemoteVideoTrackPublication remoteVideoTrackPublication,
                                                 RemoteVideoTrack remoteVideoTrack) {
                Log.i(TAG, String.format("onVideoTrackUnsubscribed: " +
                                "[RemoteParticipant: identity=%s], " +
                                "[RemoteVideoTrack: enabled=%b, name=%s]",
                        remoteParticipant.getIdentity(),
                        remoteVideoTrack.isEnabled(),
                        remoteVideoTrack.getName()));
                for (int i =0 ; i<remoteParticipantList.size();i++){
                    if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
                        if (remoteParticipant.isConnected()){
                            thumbnailAdapter.notifyItemChanged(i);
                        }
                    }
                }



                //removeParticipantVideo(remoteVideoTrack);
            }

            @Override
            public void onVideoTrackSubscriptionFailed(RemoteParticipant remoteParticipant,
                                                       RemoteVideoTrackPublication remoteVideoTrackPublication,
                                                       TwilioException twilioException) {
                Log.i(TAG, String.format("onVideoTrackSubscriptionFailed: " +
                                "[RemoteParticipant: identity=%s], " +
                                "[RemoteVideoTrackPublication: sid=%b, name=%s]" +
                                "[TwilioException: code=%d, message=%s]",
                        remoteParticipant.getIdentity(),
                        remoteVideoTrackPublication.getTrackSid(),
                        remoteVideoTrackPublication.getTrackName(),
                        twilioException.getCode(),
                        twilioException.getMessage()));
                Snackbar.make(connectActionFab,
                        String.format("Failed to subscribe to %s video track",
                                remoteParticipant.getIdentity()),
                        Snackbar.LENGTH_LONG)
                        .show();
            }

            @Override
            public void onAudioTrackEnabled(RemoteParticipant remoteParticipant,
                                            RemoteAudioTrackPublication remoteAudioTrackPublication) {
                Log.d("Message ","Audio Enabled");
                for (int i =0 ; i<remoteParticipantList.size();i++){
                    if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
                        if (remoteParticipant.isConnected()){

                            thumbnailAdapter.notifyItemChanged(i);
                        }
                    }
                }

            }

            @Override
            public void onAudioTrackDisabled(RemoteParticipant remoteParticipant,
                                             RemoteAudioTrackPublication remoteAudioTrackPublication) {
                Log.d("Message ","Audio Disabled");
                for (int i =0 ; i<remoteParticipantList.size();i++){
                    if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
                        if (remoteParticipant.isConnected()){

                            thumbnailAdapter.notifyItemChanged(i);
                        }
                    }
                }

            }

            @Override
            public void onVideoTrackEnabled(RemoteParticipant remoteParticipant,
                                            RemoteVideoTrackPublication remoteVideoTrackPublication) {
                Log.d("Message ","Video Disabled");
                for (int i =0 ; i<remoteParticipantList.size();i++){
                    if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
                        if (remoteParticipant.isConnected()){

                            thumbnailAdapter.notifyItemChanged(i);
                        }
                    }
                }
            }

            @Override
            public void onVideoTrackDisabled(RemoteParticipant remoteParticipant,
                                             RemoteVideoTrackPublication remoteVideoTrackPublication) {
                Log.d("Message ","Video Enabled");
                for (int i =0 ; i<remoteParticipantList.size();i++){
                    if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
                        if (remoteParticipant.isConnected()){

                            thumbnailAdapter.notifyItemChanged(i);
                        }
                    }
                }
            }

            @Override
            public void onNetworkQualityLevelChanged(@NonNull RemoteParticipant remoteParticipant, @NonNull NetworkQualityLevel networkQualityLevel) {
//                for (int i =0 ; i<remoteParticipantList.size();i++){
//                    if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
//                        if (remoteParticipant.isConnected()){
//
//                            thumbnailAdapter.notifyItemChanged(i);
//                        }
//                    }
//                }
            }
        };


    }
    public void showPrimaryView(RemoteParticipant remoteParticipant)
    {

        // Create an alert builder
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        final View customLayout = getLayoutInflater().inflate(R.layout.participant_primary_view,
                null);
        builder.setView(customLayout);
        VideoTextureView video_view = customLayout.findViewById(R.id.video_view);
        if (remoteParticipant.getRemoteVideoTracks().size() > 0) {
            RemoteVideoTrackPublication remoteVideoTrackPublication =
                    remoteParticipant.getRemoteVideoTracks().get(0);
//            if (!remoteVideoTrackPublication.isTrackEnabled()){
//                holder.video_disabled.setVisibility(View.VISIBLE);
//
//            }
//            else {
//                holder.video_disabled.setVisibility(View.GONE);
//
//            }

            if (remoteVideoTrackPublication.isTrackSubscribed()){
                remoteVideoTrackPublication.getVideoTrack().addRenderer(video_view);


            }
            else {




            }

        }

        // add a button

        // create and show
        // the alert dialog
        AlertDialog dialog
                = builder.create();
        dialog.show();
    }

    private DialogInterface.OnClickListener connectClickListener(final EditText roomEditText) {
        return (dialog, which) -> {
            /*
             * Connect to room
             */
            // connectToRoom(roomEditText.getText().toString());
        };
    }

    private View.OnClickListener disconnectClickListener() {
        return v -> {
            /*
             * Disconnect from room
             */
            if (room != null) {
                room.disconnect();
            }
            intializeUI();
        };
    }

    private View.OnClickListener connectActionClickListener() {
        return v -> showConnectDialog();
    }

    private DialogInterface.OnClickListener cancelConnectDialogClickListener() {
        return (dialog, which) -> {
            intializeUI();
            connectDialog.dismiss();
        };
    }

    private View.OnClickListener switchCameraClickListener() {
        return v -> {
            if (cameraCapturerCompat != null) {
                CameraCapturer.CameraSource cameraSource = cameraCapturerCompat.getCameraSource();
                cameraCapturerCompat.switchCamera();
                thumbnailAdapter.notifyItemChanged(0);
                primaryVideoView.setMirror(cameraSource == CameraCapturer.CameraSource.BACK_CAMERA);

//                if (thumbnailVideoView.getVisibility() == View.VISIBLE) {
//                    thumbnailVideoView.setMirror(cameraSource == CameraCapturer.CameraSource.BACK_CAMERA);
//                } else {
//                    primaryVideoView.setMirror(cameraSource == CameraCapturer.CameraSource.BACK_CAMERA);
//                }
            }
        };
    }

    private View.OnClickListener localVideoClickListener() {
        return v -> {
            /*
             * Enable/disable the local video track
             */
            if (localVideoTrack != null) {
                boolean enable = !localVideoTrack.isEnabled();
                localVideoTrack.enable(enable);
                int icon;
                if (enable) {
                    icon = R.drawable.ic_videocam_white_24dp;
                    switchCameraActionFab.show();
                } else {
                    icon = R.drawable.ic_videocam_off_black_24dp;
                    switchCameraActionFab.hide();
                }
                localVideoActionFab.setImageDrawable(
                        ContextCompat.getDrawable(VideoActivity.this, icon));
            }
        };
    }

    private View.OnClickListener muteClickListener() {
        return v -> {
            /*
             * Enable/disable the local audio track. The results of this operation are
             * signaled to other Participants in the same Room. When an audio track is
             * disabled, the audio is muted.
             */
            if (localAudioTrack != null) {
                boolean enable = !localAudioTrack.isEnabled();
                localAudioTrack.enable(enable);
                int icon = enable ?
                        R.drawable.ic_mic_white_24dp : R.drawable.ic_mic_off_black_24dp;
                muteActionFab.setImageDrawable(ContextCompat.getDrawable(
                        VideoActivity.this, icon));
            }
        };
    }

    private class ThumbnailAdapter extends RecyclerView.Adapter<ThumbnailAdapter.MyViewHoler>{
        int  samePositionClicked = -1;
//        RemoteParticipant remoteParticipant;



        @NonNull
        @Override
        public ThumbnailAdapter.MyViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View  view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_thumbnail,parent,false);
            return new ThumbnailAdapter.MyViewHoler(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ThumbnailAdapter.MyViewHoler holder, int position) {
//            if (remoteParticipantList.get(position).getRemoteVideoTracks().size() > 0) {
            //  RemoteVideoTrackPublication remoteVideoTrackPublication =
//                        remoteParticipantList.get(position).getRemoteVideoTracks().get(0);

            /*
             * Only render video tracks that are subscribed to
             */
//            RemoteParticipant last = remoteParticipantList.get(remoteParticipantList.size() - 1);
//            if (remoteParticipantList.get(position).equals(last)){
//                if (remoteVideoTrackPublication.isTrackSubscribed()) {
//                    localVideoTrack.removeRenderer(primaryVideoView);
//                    localVideoTrack.addRenderer(holder.thumbnail_video_view);
//                    localVideoView = holder.thumbnail_video_view;
//                    holder.thumbnail_video_view.setMirror(cameraCapturerCompat.getCameraSource() ==
//                            CameraCapturer.CameraSource.FRONT_CAMERA);
//                    primaryVideoView.setMirror(false);
//                    remoteVideoTrackPublication.getRemoteVideoTrack().addRenderer(primaryVideoView);
//                }
//            }
//            else {
//                if(remoteVideoTrackPublication.isTrackSubscribed()){
//                    holder.thumbnail_video_view.setMirror(cameraCapturerCompat.getCameraSource() ==
//                            CameraCapturer.CameraSource.FRONT_CAMERA);
//                    remoteVideoTrackPublication.getRemoteVideoTrack().addRenderer(holder.thumbnail_video_view);
//                }
//
//            }
//            ConstraintLayout.LayoutParams cellParams = new ConstraintLayout.LayoutParams(screenwidth/2, (int) (screenheight/4.5));
//
//            holder.thumbnail_video_view.setLayoutParams(cellParams);


            if (remoteParticipantList.get(position).getRemoteVideoTracks().size() > 0) {
                RemoteVideoTrackPublication remoteVideoTrackPublication =
                        remoteParticipantList.get(position).getRemoteVideoTracks().get(0);
                if (!remoteVideoTrackPublication.isTrackEnabled()){
                    holder.video_disabled.setVisibility(View.VISIBLE);

                }
                else {
                    holder.video_disabled.setVisibility(View.GONE);

                }

                if (remoteVideoTrackPublication.isTrackSubscribed()){
                    remoteVideoTrackPublication.getVideoTrack().addRenderer(holder.thumbnail_video_view);
                    holder.progressBar.setVisibility(View.GONE);

                }
                else {


                    holder.progressBar.setVisibility(View.VISIBLE);

                }

            }
            RemoteAudioTrackPublication remoteAudioTrackPublication = remoteParticipantList.get(position).getRemoteAudioTracks().get(0);
            if (remoteAudioTrackPublication.getRemoteAudioTrack()!=null){
                if (remoteAudioTrackPublication.getAudioTrack().isEnabled()){
                    holder.text_onanothercall.setVisibility(View.GONE);

                }
                else {
                    holder.text_onanothercall.setVisibility(View.VISIBLE);

                }
                if(remoteAudioTrackPublication.getRemoteAudioTrack().isPlaybackEnabled()){

                    holder.mute_audio.setImageResource(R.drawable.ic_mic_white_24dp);
                }
                else {
                    holder.mute_audio.setImageResource(R.drawable.ic_mic_off_black_24dp);
                }
            }


            holder.mute_audio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(remoteAudioTrackPublication.getRemoteAudioTrack().isPlaybackEnabled()){
                        holder.mute_audio.setImageResource(R.drawable.ic_mic_off_black_24dp);

                        remoteAudioTrackPublication.getRemoteAudioTrack().enablePlayback(false);
                    }
                    else {

                        holder.mute_audio.setImageResource(R.drawable.ic_mic_white_24dp);
                        remoteAudioTrackPublication.getRemoteAudioTrack().enablePlayback(true);
                    }

                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPrimaryView(remoteParticipantList.get(position));
                }
            });
//            NetworkQualityLevel networkQualityLevel = remoteParticipantList.get(position).getNetworkQualityLevel();
//            if (networkQualityLevel == NetworkQualityLevel.NETWORK_QUALITY_LEVEL_UNKNOWN){
//
//                holder.text_strength.setText("Unknown");
//
//            }
//            else if (networkQualityLevel == NetworkQualityLevel.NETWORK_QUALITY_LEVEL_ZERO){
//                holder.text_strength.setText("0");
//            }
//            else if (networkQualityLevel == NetworkQualityLevel.NETWORK_QUALITY_LEVEL_ONE){
//                holder.text_strength.setText("1");
//            }
//            else if (networkQualityLevel == NetworkQualityLevel.NETWORK_QUALITY_LEVEL_TWO){
//                holder.text_strength.setText("2");
//            }
//            else if (networkQualityLevel == NetworkQualityLevel.NETWORK_QUALITY_LEVEL_THREE){
//                holder.text_strength.setText("3");
//            }
//            else if (networkQualityLevel == NetworkQualityLevel.NETWORK_QUALITY_LEVEL_FOUR){
//                holder.text_strength.setText("4");
//            }
//            else if (networkQualityLevel == NetworkQualityLevel.NETWORK_QUALITY_LEVEL_FIVE){
//                holder.text_strength.setText("5");
//            }

//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    RemoteVideoTrackPublication remoteVideoTrackPublication =
//                            remoteParticipantList.get(position).getRemoteVideoTracks().get(0);
//                    if(remoteVideoTrackPublication.isTrackSubscribed()){
//                        remoteVideoTrackPublication.getVideoTrack().removeRenderer(holder.thumbnail_video_view);
//
//                    }
//                    else {
//                        remoteVideoTrackPublication.getVideoTrack().addRenderer(holder.thumbnail_video_view);
//
//
//                    }
//
//                }
//            });

//                }

//            }

        }

        @Override
        public int getItemCount() {
            return remoteParticipantList.size();


        }

        public class MyViewHoler extends RecyclerView.ViewHolder {
            VideoTextureView thumbnail_video_view;
            ProgressBar progressBar;
            TextView text_onanothercall,text_strength;
            ImageView video_disabled,mute_audio;
            public MyViewHoler(@NonNull View itemView) {
                super(itemView);
                thumbnail_video_view= itemView.findViewById(R.id.thumbnail_video_view);
                progressBar = itemView.findViewById(R.id.progressBar);
                text_onanothercall = itemView.findViewById(R.id.text_onanothercall);
                video_disabled = itemView.findViewById(R.id.video_disabled);
                mute_audio = itemView.findViewById(R.id.mute_audio);
                text_strength = itemView.findViewById(R.id.text_strength);

            }
        }

        public void addThumbnail(RemoteParticipant remoteParticipant){
            remoteParticipantList.add(remoteParticipant);
            thumbnailAdapter.notifyDataSetChanged();

//            notifyItemInserted(thumbnailAdapter.getItemCount()+1);
//            if(remoteParticipantList.size()==1){
//                ConstraintLayout.LayoutParams cellParams = new ConstraintLayout.LayoutParams(screenwidth, (int) (screenheight/3));
//                primaryVideoView.setLayoutParams(cellParams);
//            }


        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }
    }


    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("")
                .setMessage("Do you want to quit")
                .setCancelable(true)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == PICKFILE_RESULT_CODE && resultCode == RESULT_OK) {
//
//            if (data.getClipData() != null) {
//                int count = data.getClipData().getItemCount(); //evaluate the count before the for loop --- otherwise, the count is evaluated every loop.
//                for (int i = 0; i < count; i++) {
//                    Uri uri = data.getClipData().getItemAt(i).getUri();
//
//                    // Uri uri = data.getData();
//                    String  filePath= FileUtils.getPath(this,uri);
//                    // filePath = uri.getPath();
//                    Log.d("path",filePath);
//
//
//
//
//                    bytes =    getByteArrayFromPath(filePath);
//
//
//
//                }
//
//            } else if (data.getData() != null) {
//                Uri uri = data.getData();
//                String  filePath= FileUtils.getPath(this,uri);
//                // filePath = uri.getPath();
//                Log.d("path",filePath);
//
//                bytes =    getByteArrayFromPath(filePath);
//
//
////
//
//
//
//            }
//            if (localDataTrack != null) {
//                ByteBuffer messageBuffer = ByteBuffer.wrap(bytes);
//
//                localDataTrack.send(messageBuffer);
//            } else {
//                Log.e(TAG, "Ignoring touch event because data track is release");
//            }
////            localParticipant.setListener(new LocalParticipant.Listener() {
////                @Override
////                public void onAudioTrackPublished(@NonNull LocalParticipant localParticipant, @NonNull LocalAudioTrackPublication localAudioTrackPublication) {
////
////                }
////
////                @Override
////                public void onAudioTrackPublicationFailed(@NonNull LocalParticipant localParticipant, @NonNull LocalAudioTrack localAudioTrack, @NonNull TwilioException twilioException) {
////
////                }
////
////                @Override
////                public void onVideoTrackPublished(@NonNull LocalParticipant localParticipant, @NonNull LocalVideoTrackPublication localVideoTrackPublication) {
////
////                }
////
////                @Override
////                public void onVideoTrackPublicationFailed(@NonNull LocalParticipant localParticipant, @NonNull LocalVideoTrack localVideoTrack, @NonNull TwilioException twilioException) {
////
////                }
////
////                @Override
////                public void onDataTrackPublished(@NonNull LocalParticipant localParticipant, @NonNull LocalDataTrackPublication localDataTrackPublication) {
//////                        String message = "hello DataTrack!";
//////                        localDataTrackPublication.getLocalDataTrack().send(message);
////
////                    if(bytes.length!=0){
////                        ByteBuffer messageBuffer = ByteBuffer.wrap(bytes);
////                        localDataTrackPublication.getLocalDataTrack().send(messageBuffer);
////                    }
////
////                }
////
////                @Override
////                public void onDataTrackPublicationFailed(@NonNull LocalParticipant localParticipant, @NonNull LocalDataTrack localDataTrack, @NonNull TwilioException twilioException) {
////
////                }
////            });
//
//
//
//        }

    }
    public static byte[] getByteArrayFromPath(String filePath) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filePath);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            for (int readNum; (readNum = fis.read(b)) != -1; ) {
                bos.write(b, 0, readNum);
            }
            return bos.toByteArray();
        } catch (Exception e) {
            Log.d("mylog", e.toString());
        }
        return null;
    }
    private RemoteDataTrack.Listener remoteDataTrackListener() {
        return new RemoteDataTrack.Listener() {
            @Override
            public void onMessage(RemoteDataTrack remoteDataTrack, ByteBuffer byteBuffer) {
                Log.d(TAG, "onMessage: " + byteBuffer);
            }

            @Override
            public void onMessage(RemoteDataTrack remoteDataTrack, String message) {
                Log.d(TAG, "onMessage: " + message);
//                MotionMessage motionMessage = MotionMessage.fromJson(message);
//
//                if (motionMessage != null) {
//                    RemoteParticipant remoteParticipant = dataTrackRemoteParticipantMap
//                            .get(remoteDataTrack);
//                    int actionEvent = (motionMessage.actionDown) ?
//                            (MotionEvent.ACTION_DOWN) :
//                            MotionEvent.ACTION_UP;
//
//                    // Process remote drawing event
//                    float projectedX = motionMessage.coordinates.first *
//                            (float) collaborativeDrawingView.getWidth();
//                    float projectedY = motionMessage.coordinates.second *
//                            (float) collaborativeDrawingView.getHeight();
//                    collaborativeDrawingView.onRemoteTouchEvent(remoteParticipant,
//                            actionEvent,
//                            projectedX,
//                            projectedY);
//                } else {
//                    Log.e(TAG, "Failed to deserialize message: " + message);
//                }
            }
        };
    }

}

// public class VideoActivity extends AppCompatActivity {
//    private static final int CAMERA_MIC_PERMISSION_REQUEST_CODE = 1;
//    private static final String TAG = "VideoActivity";
//
//    /*
//     * Audio and video tracks can be created with names. This feature is useful for categorizing
//     * tracks of participants. For example, if one participant publishes a video track with
//     * ScreenCapturer and CameraCapturer with the names "screen" and "camera" respectively then
//     * other participants can use RemoteVideoTrack#getName to determine which video track is
//     * produced from the other participant's screen or camera.
//     */
//    private static final String LOCAL_AUDIO_TRACK_NAME = "mic";
//    private static final String LOCAL_VIDEO_TRACK_NAME = "camera";
//
//    /*
//     * You must provide a Twilio Access Token to connect to the Video service
//     */
//    //  private static final String TWILIO_ACCESS_TOKEN = BuildConfig.TWILIO_ACCESS_TOKEN;
//    private static final String TWILIO_ACCESS_TOKEN = "AC9ed987a221fcfc958016b2b36ddab44d";
////    private static final String ACCESS_TOKEN_SERVER = BuildConfig.TWILIO_ACCESS_TOKEN_SERVER;
//
//    /*
//     * Access token used to connect. This field will be set either from the console generated token
//     * or the request to the token server.
//     */
//    private String accessToken,roomName;
//
//    /*
//     * A Room represents communication between a local participant and one or more participants.
//     */
//    private Room room;
//    private LocalParticipant localParticipant;
//
//    /*
//     * AudioCodec and VideoCodec represent the preferred codec for encoding and decoding audio and
//     * video.
//     */
//    private AudioCodec audioCodec;
//    private VideoCodec videoCodec;
//
//    /*
//     * Encoding parameters represent the sender side bandwidth constraints.
//     */
//    private EncodingParameters encodingParameters;
//
//    /*
//     * A VideoView receives frames from a local or remote video track and renders them
//     * to an associated view.
//     */
//    private VideoView primaryVideoView;
//    // private VideoView thumbnailVideoView;
//
//    /*
//     * Android shared preferences used for settings
//     */
//    private SharedPreferences preferences;
//
//    /*
//     * Android application UI elements
//     */
//    private CameraCapturerCompat cameraCapturerCompat;
//    private LocalAudioTrack localAudioTrack;
//    private LocalVideoTrack localVideoTrack;
//    private LocalDataTrack localDataTrack;
//    private FloatingActionButton connectActionFab;
//    private FloatingActionButton switchCameraActionFab;
//    private FloatingActionButton localVideoActionFab;
//    private FloatingActionButton muteActionFab;
//    private ProgressBar reconnectingProgressBar;
//    private AlertDialog connectDialog;
//    private String remoteParticipantIdentity;
//    RecyclerView thumbnail_recyclerview;
//    ThumbnailAdapter thumbnailAdapter;
//
//    /*
//     * Audio management
//     */
////    private AudioDeviceSelector audioDeviceSelector;
//    private AudioSwitch audioSwitch;
//
//    private int savedVolumeControlStream;
//    private MenuItem audioDeviceMenuItem;
//
//    private VideoRenderer localVideoView;
//    private boolean disconnectedFromOnDestroy;
//    private boolean enableAutomaticSubscription;
//    List<RemoteParticipant> remoteParticipantList;
//    int screenheight,screenwidth;
//
//    boolean isOnCall=false;
//    AudioManager audioManager;
//    TextView video_status_textview;
////    private LocalVideoTrack screenVideoTrack;
//    private ScreenCapturer screenCapturer;
//    private MenuItem screenCaptureMenuItem;
//    private ScreenCapturerManager screenCapturerManager;
//    private static final int REQUEST_MEDIA_PROJECTION = 100;
//    boolean isScreenshare = false;
//    private final ScreenCapturer.Listener screenCapturerListener = new ScreenCapturer.Listener() {
//        @Override
//        public void onScreenCaptureError(String errorDescription) {
//            Log.e(TAG, "Screen capturer error: " + errorDescription);
//            stopScreenCapture();
//            Toast.makeText(VideoActivity.this, R.string.screen_capture_error,
//                    Toast.LENGTH_LONG).show();
//        }
//
//        @Override
//        public void onFirstFrameAvailable() {
//            Log.d(TAG, "First frame from screen capturer available");
//        }
//    };
//    private PhoneStateReceiver phoneStateReceiver = new PhoneStateReceiver(){
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            super.onReceive(context, intent);
//            try {
//                String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
//                String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
//                if(state.equals(TelephonyManager.EXTRA_STATE_RINGING)){
//                    //  Toast.makeText(context,"Ringing State Number is - " + incomingNumber, Toast.LENGTH_SHORT).show();
//                }
//                else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
//
//                    if (room.getRemoteParticipants().size()!=0){
//                        if (localAudioTrack != null) {
////                                boolean enable = !localAudioTrack.isEnabled();
//                            localAudioTrack.enable(false);
////                                int icon = enable ?
////                                        R.drawable.ic_mic_white_24dp : R.drawable.ic_mic_off_black_24dp;
//                            muteActionFab.setImageDrawable(ContextCompat.getDrawable(
//                                    VideoActivity.this, R.drawable.ic_mic_off_black_24dp));
//
//
//                        }
//
//
//                    }
//
//                    isOnCall = true;
//                }
//                else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)){
//                    if (localAudioTrack != null) {
//                        //  boolean enable = !localAudioTrack.isEnabled();
//                        localAudioTrack.enable(true);
////                        int icon = enable ?
////                                R.drawable.ic_mic_white_24dp : R.drawable.ic_mic_off_black_24dp;
//                        muteActionFab.setImageDrawable(ContextCompat.getDrawable(
//                                VideoActivity.this, R.drawable.ic_mic_white_24dp));
//                    }
//
//                    isOnCall = false;
//                }
//                //     Toast.makeText(context,"Ringing State Number is - " + incomingNumber, Toast.LENGTH_SHORT).show();
//
//            }
//            catch (Exception e){
//                e.printStackTrace();
//            }
//
//        }
//    };
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//
//        setContentView(R.layout.videochat_activity);
//        video_status_textview = findViewById(R.id.video_status_textview);
//        primaryVideoView = findViewById(R.id.primary_video_view);
//        //  thumbnailVideoView = findViewById(R.id.thumbnail_video_view);
//        reconnectingProgressBar = findViewById(R.id.reconnecting_progress_bar);
//
//        connectActionFab = findViewById(R.id.connect_action_fab);
//        switchCameraActionFab = findViewById(R.id.switch_camera_action_fab);
//        localVideoActionFab = findViewById(R.id.local_video_action_fab);
//        muteActionFab = findViewById(R.id.mute_action_fab);
//        thumbnail_recyclerview = findViewById(R.id.thumbnail_recyclerview);
//        thumbnail_recyclerview.setLayoutManager(new GridLayoutManager(this,2));
//        remoteParticipantList =new ArrayList<>();
//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        screenheight = displayMetrics.heightPixels;
//        screenwidth = displayMetrics.widthPixels;
//        /*
//         * Get shared preferences to read settings
//         */
//        preferences = PreferenceManager.getDefaultSharedPreferences(this);
//
//        /*
//         * Setup audio management and set the volume control stream
//         */
//        audioSwitch = new AudioSwitch(VideoActivity.this);
//        savedVolumeControlStream = getVolumeControlStream();
//        setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
//
//        // Get AudioManager
//        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//
//// Route audio through speaker
//        audioManager.setSpeakerphoneOn(true);
//
//        /*
//         * Check camera and microphone permissions. Needed in Android M.
//         */
//        if (!checkPermissionForCameraAndMicrophone()) {
//            requestPermissionForCameraAndMicrophone();
//        } else {
//            createAudioAndVideoTracks();
//            setAccessToken();
//        }
//        if (Build.VERSION.SDK_INT >= 29) {
//            screenCapturerManager = new ScreenCapturerManager(this);
//        }
//        /*
//         * Set the initial state of the UI
//         */
//        intializeUI();
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_video_activity, menu);
//        audioDeviceMenuItem = menu.findItem(R.id.menu_audio_device);
//        screenCaptureMenuItem = menu.findItem(R.id.share_screen_menu_item);
//        /*
//         * Start the audio device selector after the menu is created and update the icon when the
//         * selected audio device changes.
//         */
////        audioDeviceSelector.start((audioDevices, audioDevice) -> {
////            updateAudioDeviceIcon(audioDevice);
////            return Unit.INSTANCE;
////        });
//        audioSwitch.start((audioDevices, audioDevice) -> {
//            updateAudioDeviceIcon(audioDevice);
//            return Unit.INSTANCE;
//        });
//
//        connectToRoom(roomName);
//
////        if (audioManager.isSpeakerphoneOn()){
////            AudioDevice audioDevice = new AudioDevice.Speakerphone();
////            audioSwitch.selectDevice(audioDevice);
////            updateAudioDeviceIcon(audioDevice);
////
////        }
////        else {
////            audioSwitch.start((audioDevices, audioDevice) -> {
////                updateAudioDeviceIcon(audioDevice);
////                return Unit.INSTANCE;
////            });
////        }
//
//
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.menu_settings:
//                startActivity(new Intent(this, SettingsActivity.class));
//
//                return true;
//            case R.id.share_screen_menu_item:
//                String shareScreen = getString(R.string.share_screen);
//                if (item.getTitle().equals(shareScreen)) {
//                    if (Build.VERSION.SDK_INT >= 29) {
//                        screenCapturerManager.startForeground();
//                    }
//                    if (screenCapturer == null) {
//                        requestScreenCapturePermission();
//                    } else {
//                        startScreenCapture();
//                    }
//                } else {
//                    if (Build.VERSION.SDK_INT >= 29) {
//                        screenCapturerManager.endForeground();
//                    }
//                    stopScreenCapture();
//                }
//
//                return true;
//            case R.id.menu_audio_device:
//                showAudioDevices();
//                return true;
//            case R.id.menu_mute_all:
//
//                String title = item.getTitle().toString();
//                if (title.equalsIgnoreCase("Mute all")){
//                    item.setTitle("Unmute all");
//                }
//                else {
//                    item.setTitle("Mute all");
//
//                }
//
//                for (int i = 0 ; i < remoteParticipantList.size();i++){
//                    RemoteAudioTrackPublication remoteAudioTrackPublication = remoteParticipantList.get(i)
//                            .getRemoteAudioTracks().get(0);
//                    if(remoteAudioTrackPublication.getRemoteAudioTrack().isPlaybackEnabled()){
//                        remoteAudioTrackPublication.getRemoteAudioTrack().enablePlayback(false);
//                    }
//                    else {
//                        remoteAudioTrackPublication.getRemoteAudioTrack().enablePlayback(true);
//                    }
//
//                }
//                thumbnailAdapter.notifyDataSetChanged();
//
//                return true;
//            default:
//                return false;
//        }
//    }
//    private void requestScreenCapturePermission() {
//        Log.d(TAG, "Requesting permission to capture screen");
//        MediaProjectionManager mediaProjectionManager = null;
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//            mediaProjectionManager = (MediaProjectionManager)
//                    getSystemService(Context.MEDIA_PROJECTION_SERVICE);
//            startActivityForResult(mediaProjectionManager.createScreenCaptureIntent(),
//                    REQUEST_MEDIA_PROJECTION);
//
//        }
////        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
////            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
////            String[] mimeTypes = {"image/*"};
////            chooseFile.setType("*/*");
////            chooseFile.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
////            chooseFile.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
////
////            chooseFile = Intent.createChooser(chooseFile, "Choose a file");
////            startActivityForResult(chooseFile, REQUEST_MEDIA_PROJECTION);
////
////        }
//
//
//
//        // This initiates a prompt dialog for the user to confirm screen projection.
//    }
//    private void startScreenCapture() {
//        isScreenshare = true;
//
//        if (localVideoTrack!=null){
//            localVideoTrack.removeRenderer(localVideoView);
//            if (localParticipant != null) {
//                localParticipant.unpublishTrack(localVideoTrack);
//
//            }
//            localVideoTrack.release();
//            localVideoTrack = null;
//        }
//
//        localVideoTrack = LocalVideoTrack.create(this, true, screenCapturer);
//        screenCaptureMenuItem.setIcon(R.drawable.ic_stop_screen_share_white_24dp);
//        screenCaptureMenuItem.setTitle(R.string.stop_screen_share);
//
////        primaryVideoView.setVisibility(View.VISIBLE);
//
//        localVideoTrack.addRenderer(primaryVideoView);
//        localVideoView = primaryVideoView;
//        if (localParticipant != null) {
//            localParticipant.publishTrack(localVideoTrack);
//
//            /*
//             * Update encoding parameters if they have changed.
//             */
////            if (!newEncodingParameters.equals(encodingParameters)) {
////                localParticipant.setEncodingParameters(newEncodingParameters);
////            }
//        }
//
//    }
//    private void stopScreenCapture() {
//        isScreenshare = false;
//
//        if (localVideoTrack != null) {
//            localVideoTrack.removeRenderer(localVideoView);
//
//            if (localParticipant != null) {
//                localParticipant.unpublishTrack(localVideoTrack);
//
//            }
//            localVideoTrack.release();
//            localVideoTrack = null;
//            screenCaptureMenuItem.setIcon(R.drawable.ic_screen_share_white_24dp);
//            screenCaptureMenuItem.setTitle(R.string.share_screen);
//            if (localVideoTrack == null && checkPermissionForCameraAndMicrophone()) {
//                localVideoTrack = LocalVideoTrack.create(this,
//                        true,
//                        cameraCapturerCompat.getVideoCapturer(),
//                        LOCAL_VIDEO_TRACK_NAME);
//                localVideoTrack.addRenderer(primaryVideoView);
//                localVideoView = primaryVideoView;
//
//                /*
//                 * If connected to a Room then share the local video track.
//                 */
//                if (localParticipant != null) {
//                    localParticipant.publishTrack(localVideoTrack);
//
//                }
//            }
//
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//        if (requestCode == CAMERA_MIC_PERMISSION_REQUEST_CODE) {
//            boolean cameraAndMicPermissionGranted = true;
//
//            for (int grantResult : grantResults) {
//                cameraAndMicPermissionGranted &= grantResult == PackageManager.PERMISSION_GRANTED;
//            }
//
//            if (cameraAndMicPermissionGranted) {
//                createAudioAndVideoTracks();
//                setAccessToken();
//            } else {
//                Toast.makeText(this,
//                        R.string.permissions_needed,
//                        Toast.LENGTH_LONG).show();
//            }
//        }
//    }
//
//    @SuppressLint("SetTextI18n")
//    @Override
//    protected void onResume() {
//        super.onResume();
//        registerReceiver(phoneStateReceiver,new IntentFilter(PhoneStateReceiver.ACTION_PHONE));
//
//        /*
//         * Update preferred audio and video codec in case changed in settings
//         */
//        audioCodec = getAudioCodecPreference(SettingsActivity.PREF_AUDIO_CODEC,
//                SettingsActivity.PREF_AUDIO_CODEC_DEFAULT);
//        videoCodec = getVideoCodecPreference(SettingsActivity.PREF_VIDEO_CODEC,
//                SettingsActivity.PREF_VIDEO_CODEC_DEFAULT);
//        enableAutomaticSubscription = getAutomaticSubscriptionPreference(SettingsActivity.PREF_ENABLE_AUTOMATIC_SUBSCRIPTION,
//                SettingsActivity.PREF_ENABLE_AUTOMATIC_SUBSCRIPTION_DEFAULT);
//        /*
//         * Get latest encoding parameters
//         */
//        final EncodingParameters newEncodingParameters = getEncodingParameters();
//
//        /*
//         * If the local video track was released when the app was put in the background, recreate.
//         */
//        if (localVideoTrack == null && checkPermissionForCameraAndMicrophone()) {
//            localVideoTrack = LocalVideoTrack.create(this,
//                    true,
//                    cameraCapturerCompat.getVideoCapturer(),
//                    LOCAL_VIDEO_TRACK_NAME);
//            localVideoTrack.addRenderer(localVideoView);
//
//            /*
//             * If connected to a Room then share the local video track.
//             */
//            if (localParticipant != null) {
//                localParticipant.publishTrack(localVideoTrack);
//
//                /*
//                 * Update encoding parameters if they have changed.
//                 */
//                if (!newEncodingParameters.equals(encodingParameters)) {
//                    localParticipant.setEncodingParameters(newEncodingParameters);
//                }
//            }
//        }
//
//        /*
//         * Update encoding parameters
//         */
//        encodingParameters = newEncodingParameters;
//
//        /*
//         * Update reconnecting UI
//         */
//        if (room != null) {
//            reconnectingProgressBar.setVisibility((room.getState() != Room.State.RECONNECTING) ?
//                    View.GONE :
//                    View.VISIBLE);
//        }
//    }
//
//    @Override
//    protected void onPause() {
//        /*
//         * Release the local video track before going in the background. This ensures that the
//         * camera can be used by other applications while this app is in the background.
//         */
//        if (!isScreenshare){
//            if (localVideoTrack != null) {
//                /*
//                 * If this local video track is being shared in a Room, unpublish from room before
//                 * releasing the video track. Participants will be notified that the track has been
//                 * unpublished.
//                 */
//                if (localParticipant != null) {
//                    localParticipant.unpublishTrack(localVideoTrack);
//                }
//
//                localVideoTrack.release();
//                localVideoTrack = null;
//            }
//
//        }
//        super.onPause();
//    }
//
//    @Override
//    protected void onDestroy() {
//        /*
//         * Tear down audio management and restore previous volume stream
//         */
//        audioSwitch.stop();
//        setVolumeControlStream(savedVolumeControlStream);
//
//        /*
//         * Always disconnect from the room before leaving the Activity to
//         * ensure any memory allocated to the Room resource is freed.
//         */
//        if (room != null && room.getState() != Room.State.DISCONNECTED) {
//            room.disconnect();
//            disconnectedFromOnDestroy = true;
//        }
//
//        /*
//         * Release the local audio and video tracks ensuring any memory allocated to audio
//         * or video is freed.
//         */
//        if (localAudioTrack != null) {
//            localAudioTrack.release();
//            localAudioTrack = null;
//        }
//        if (localVideoTrack != null) {
//            localVideoTrack.release();
//            localVideoTrack = null;
//        }
////        if (screenVideoTrack != null) {
////            screenVideoTrack.release();
////            screenVideoTrack = null;
////        }
//        if (Build.VERSION.SDK_INT >= 29) {
//            screenCapturerManager.unbindService();
//        }
//        super.onDestroy();
//    }
//
//    private boolean checkPermissionForCameraAndMicrophone() {
//        int resultCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
//        int resultMic = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
//        int resultPhoneState= ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
//        return resultCamera == PackageManager.PERMISSION_GRANTED &&
//                resultMic == PackageManager.PERMISSION_GRANTED && resultPhoneState == PackageManager.PERMISSION_GRANTED;
//    }
//
//    private void requestPermissionForCameraAndMicrophone() {
//        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
//                ActivityCompat.shouldShowRequestPermissionRationale(this,
//                        Manifest.permission.RECORD_AUDIO)||ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_PHONE_STATE)) {
////            Toast.makeText(this,
////                    R.string.permissions_needed,
////                    Toast.LENGTH_LONG).show();
//            ActivityCompat.requestPermissions(
//                    this,
//                    new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO,Manifest.permission.READ_PHONE_STATE},
//                    CAMERA_MIC_PERMISSION_REQUEST_CODE);
//
//        } else {
//            ActivityCompat.requestPermissions(
//                    this,
//                    new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO,Manifest.permission.READ_PHONE_STATE},
//                    CAMERA_MIC_PERMISSION_REQUEST_CODE);
//        }
//    }
//
//    private void createAudioAndVideoTracks() {
//        // Share your microphone
//        localAudioTrack = LocalAudioTrack.create(this, true, LOCAL_AUDIO_TRACK_NAME);
//
//        // Share your camera
//        cameraCapturerCompat = new CameraCapturerCompat(this, getAvailableCameraSource());
//        localVideoTrack = LocalVideoTrack.create(this,
//                true,
//                cameraCapturerCompat.getVideoCapturer(),
//                LOCAL_VIDEO_TRACK_NAME);
//        primaryVideoView.setMirror(true);
//        localVideoTrack.addRenderer(primaryVideoView);
//        localVideoView = primaryVideoView;
//
//    }
//
//    private CameraCapturer.CameraSource getAvailableCameraSource() {
//        return (CameraCapturer.isSourceAvailable(CameraCapturer.CameraSource.FRONT_CAMERA)) ?
//                (CameraCapturer.CameraSource.FRONT_CAMERA) :
//                (CameraCapturer.CameraSource.BACK_CAMERA);
//    }
//
//    private void setAccessToken() {
//        if(getIntent().getExtras()!=null){
//            accessToken = getIntent().getExtras().getString("accessToken");
//            roomName = getIntent().getExtras().getString("roomName");
//        }
//        // accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzYxMzI1OGU3NWE5MDc3YTY4YzM0ZmI5ZmRkMGQwZWQwLTE1OTU5NDI1MjAiLCJpc3MiOiJTSzYxMzI1OGU3NWE5MDc3YTY4YzM0ZmI5ZmRkMGQwZWQwIiwic3ViIjoiQUM5ZWQ5ODdhMjIxZmNmYzk1ODAxNmIyYjM2ZGRhYjQ0ZCIsImV4cCI6MTU5NTk0NjEyMCwiZ3JhbnRzIjp7ImlkZW50aXR5IjoiRW11bGF0b3IiLCJ2aWRlbyI6eyJyb29tIjoiTUFJTCJ9fX0.qz_B99QvuWRPTKd1RetV3YPqul0vKuRwoT9noA1tUDg";
////        if (!BuildConfig.USE_TOKEN_SERVER) {
////            /*
////             * OPTION 1 - Generate an access token from the getting started portal
////             * https://www.twilio.com/console/video/dev-tools/testing-tools and add
////             * the variable TWILIO_ACCESS_TOKEN setting it equal to the access token
////             * string in your local.properties file.
////             */
////            this.accessToken = TWILIO_ACCESS_TOKEN;
////        } else {
////            /*
////             * OPTION 2 - Retrieve an access token from your own web app.
////             * Add the variable ACCESS_TOKEN_SERVER assigning it to the url of your
////             * token server and the variable USE_TOKEN_SERVER=true to your
////             * local.properties file.
////             */
////            retrieveAccessTokenfromServer();
////        }
//    }
//
//    private void connectToRoom(String roomName) {
//        audioSwitch.activate();
//        localDataTrack = LocalDataTrack.create(this);
//        NetworkQualityConfiguration configuration =
//                new NetworkQualityConfiguration(
//                        NetworkQualityVerbosity.NETWORK_QUALITY_VERBOSITY_MINIMAL,
//                        NetworkQualityVerbosity.NETWORK_QUALITY_VERBOSITY_MINIMAL);
//        ConnectOptions.Builder connectOptionsBuilder = new ConnectOptions.Builder(accessToken)
//                .roomName(roomName)
//                .enableNetworkQuality(true)
//                .networkQualityConfiguration(configuration);
//
//        /*
//         * Add local audio track to connect options to share with participants.
//         */
//        if (localAudioTrack != null) {
//            connectOptionsBuilder
//                    .audioTracks(Collections.singletonList(localAudioTrack));
//        }
//
//        /*
//         * Add local video track to connect options to share with participants.
//         */
//        if (localVideoTrack != null) {
//            connectOptionsBuilder.videoTracks(Collections.singletonList(localVideoTrack));
//        }
//
//        /*
//         * Set the preferred audio and video codec for media.
//         */
//        connectOptionsBuilder.preferAudioCodecs(Collections.singletonList(audioCodec));
//        connectOptionsBuilder.preferVideoCodecs(Collections.singletonList(videoCodec));
//        connectOptionsBuilder.dataTracks(Collections.singletonList(localDataTrack));
//        /*
//         * Set the sender side encoding parameters.
//         */
//        connectOptionsBuilder.encodingParameters(encodingParameters);
//
//        /*
//         * Toggles automatic track subscription. If set to false, the LocalParticipant will receive
//         * notifications of track publish events, but will not automatically subscribe to them. If
//         * set to true, the LocalParticipant will automatically subscribe to tracks as they are
//         * published. If unset, the default is true. Note: This feature is only available for Group
//         * Rooms. Toggling the flag in a P2P room does not modify subscription behavior.
//         */
//        connectOptionsBuilder.enableAutomaticSubscription(enableAutomaticSubscription);
//
//        room = Video.connect(this, connectOptionsBuilder.build(), roomListener());
//
//
//        setDisconnectAction();
//    }
//
//    /*
//     * The initial state when there is no active room.
//     */
//    private void intializeUI() {
//        connectActionFab.setImageDrawable(ContextCompat.getDrawable(this,
//                R.drawable.ic_video_call_white_24dp));
//        connectActionFab.show();
//        connectActionFab.setOnClickListener(connectActionClickListener());
//        switchCameraActionFab.show();
//        switchCameraActionFab.setOnClickListener(switchCameraClickListener());
//        localVideoActionFab.show();
//        localVideoActionFab.setOnClickListener(localVideoClickListener());
//        muteActionFab.show();
//        muteActionFab.setOnClickListener(muteClickListener());
//        remoteParticipantList.clear();
//        thumbnailAdapter = new ThumbnailAdapter();
//        thumbnail_recyclerview.setAdapter(thumbnailAdapter);
//    }
//
//    /*
//     * Show the current available audio devices.
//     */
//    private void showAudioDevices() {
//        AudioDevice selectedDevice = audioSwitch.getSelectedAudioDevice();
//        List<AudioDevice> availableAudioDevices = audioSwitch.getAvailableAudioDevices();
//
//        if (selectedDevice != null) {
//            int selectedDeviceIndex = availableAudioDevices.indexOf(selectedDevice);
//
//            ArrayList<String> audioDeviceNames = new ArrayList<>();
//            for (AudioDevice a : availableAudioDevices) {
//                audioDeviceNames.add(a.getName());
//            }
//
//            new AlertDialog.Builder(this)
//                    .setTitle(R.string.room_screen_select_device)
//                    .setSingleChoiceItems(
//                            audioDeviceNames.toArray(new CharSequence[0]),
//                            selectedDeviceIndex,
//                            (dialog, index) -> {
//                                dialog.dismiss();
//                                AudioDevice selectedAudioDevice = availableAudioDevices.get(index);
//                                updateAudioDeviceIcon(selectedAudioDevice);
//                                audioSwitch.selectDevice(selectedAudioDevice);
//                            }).create().show();
//        }
//    }
//
//    /*
//     * Update the menu icon based on the currently selected audio device.
//     */
//    private void updateAudioDeviceIcon(AudioDevice selectedAudioDevice) {
//        int audioDeviceMenuIcon = R.drawable.ic_phonelink_ring_white_24dp;
//
//        if (selectedAudioDevice instanceof AudioDevice.BluetoothHeadset) {
//            audioDeviceMenuIcon = R.drawable.ic_bluetooth_white_24dp;
//        } else if (selectedAudioDevice instanceof AudioDevice.WiredHeadset) {
//            audioDeviceMenuIcon = R.drawable.ic_headset_mic_white_24dp;
//        } else if (selectedAudioDevice instanceof AudioDevice.Earpiece) {
//            audioDeviceMenuIcon = R.drawable.ic_phonelink_ring_white_24dp;
//        } else if (selectedAudioDevice instanceof AudioDevice.Speakerphone) {
//            audioDeviceMenuIcon = R.drawable.ic_volume_up_white_24dp;
//        }
//
//        audioDeviceMenuItem.setIcon(audioDeviceMenuIcon);
//    }
//
//    /*
//     * Get the preferred audio codec from shared preferences
//     */
//    private AudioCodec getAudioCodecPreference(String key, String defaultValue) {
//        final String audioCodecName = preferences.getString(key, defaultValue);
//
//        switch (audioCodecName) {
//            case IsacCodec.NAME:
//                return new IsacCodec();
//            case OpusCodec.NAME:
//                return new OpusCodec();
//            case PcmaCodec.NAME:
//                return new PcmaCodec();
//            case PcmuCodec.NAME:
//                return new PcmuCodec();
//            case G722Codec.NAME:
//                return new G722Codec();
//            default:
//                return new OpusCodec();
//        }
//    }
//
//    /*
//     * Get the preferred video codec from shared preferences
//     */
//    private VideoCodec getVideoCodecPreference(String key, String defaultValue) {
//        final String videoCodecName = preferences.getString(key, defaultValue);
//
//        switch (videoCodecName) {
//            case Vp8Codec.NAME:
//                boolean simulcast = preferences.getBoolean(SettingsActivity.PREF_VP8_SIMULCAST,
//                        SettingsActivity.PREF_VP8_SIMULCAST_DEFAULT);
//                return new Vp8Codec(simulcast);
//            case H264Codec.NAME:
//                return new H264Codec();
//            case Vp9Codec.NAME:
//                return new Vp9Codec();
//            default:
//                return new Vp8Codec();
//        }
//    }
//
//    private boolean getAutomaticSubscriptionPreference(String key, boolean defaultValue) {
//        return preferences.getBoolean(key, defaultValue);
//    }
//
//    private EncodingParameters getEncodingParameters() {
//        final int maxAudioBitrate = Integer.parseInt(
//                preferences.getString(SettingsActivity.PREF_SENDER_MAX_AUDIO_BITRATE,
//                        SettingsActivity.PREF_SENDER_MAX_AUDIO_BITRATE_DEFAULT));
//        final int maxVideoBitrate = Integer.parseInt(
//                preferences.getString(SettingsActivity.PREF_SENDER_MAX_VIDEO_BITRATE,
//                        SettingsActivity.PREF_SENDER_MAX_VIDEO_BITRATE_DEFAULT));
//
//        return new EncodingParameters(maxAudioBitrate, maxVideoBitrate);
//    }
//
//    /*
//     * The actions performed during disconnect.
//     */
//    private void setDisconnectAction() {
//        connectActionFab.setImageDrawable(ContextCompat.getDrawable(this,
//                R.drawable.ic_call_end_white_24px));
//        connectActionFab.show();
//        connectActionFab.setOnClickListener(disconnectClickListener());
//    }
//
//    /*
//     * Creates an connect UI dialog
//     */
//    private void showConnectDialog() {
//        EditText roomEditText = new EditText(this);
//
//        connectDialog = Dialog.createConnectDialog(roomEditText,
//                connectClickListener(roomEditText),
//                cancelConnectDialogClickListener(),
//                this);
//        connectDialog.show();
//    }
//
//    /*
//     * Called when remote participant joins the room
//     */
//    @SuppressLint("SetTextI18n")
//    private void addRemoteParticipant(RemoteParticipant remoteParticipant) {
//        /*
//         * This app only displays video for one additional participant per Room
//         */
////        if (thumbnailVideoView.getVisibility() == View.VISIBLE) {
////            Snackbar.make(connectActionFab,
////                    "Multiple participants are not currently support in this UI",
////                    Snackbar.LENGTH_LONG)
////                    .setAction("Action", null).show();
////            return;
////        }
//        remoteParticipantIdentity = remoteParticipant.getIdentity();
//
//
//
//        /*
//         * Add remote participant renderer
//         */
//        thumbnailAdapter.addThumbnail(remoteParticipant);
//
//
////        if (remoteParticipant.getRemoteVideoTracks().size() > 0) {
////
////            RemoteVideoTrackPublication remoteVideoTrackPublication =
////                    remoteParticipant.getRemoteVideoTracks().get(0);
////
////            /*
////             * Only render video tracks that are subscribed to
////             */
////            if (remoteVideoTrackPublication.isTrackSubscribed()) {
////                addRemoteParticipantVideo(remoteVideoTrackPublication.getRemoteVideoTrack());
////            }
////        }
//
//        /*
//         * Start listening for participant events
//         */
//        remoteParticipant.setListener(remoteParticipantListener());
//        video_status_textview.setText(String.valueOf(room.getRemoteParticipants().size()));
//    }
//
//    /*
//     * Set primary view as renderer for participant video track
//     */
////    private void addRemoteParticipantVideo(VideoTrack videoTrack) {
////        moveLocalVideoToThumbnailView();
////        primaryVideoView.setMirror(false);
////        videoTrack.addRenderer(primaryVideoView);
////    }
//
//    private void moveLocalVideoToThumbnailView() {
////        if (thumbnailVideoView.getVisibility() == View.GONE) {
////            thumbnailVideoView.setVisibility(View.VISIBLE);
////            localVideoTrack.removeRenderer(primaryVideoView);
////            localVideoTrack.addRenderer(thumbnailVideoView);
////            localVideoView = thumbnailVideoView;
////            thumbnailVideoView.setMirror(cameraCapturerCompat.getCameraSource() ==
////                    CameraCapturer.CameraSource.FRONT_CAMERA);
////        }
//    }
//
//    /*
//     * Called when remote participant leaves the room
//     */
//    @SuppressLint("SetTextI18n")
//    private void removeRemoteParticipant(RemoteParticipant remoteParticipant) {
//
//        for (int i =0 ; i<remoteParticipantList.size();i++){
//
//            if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
//                if (!remoteParticipant.isConnected()){
//                    remoteParticipantList.remove(remoteParticipantList.get(i));
//                    thumbnailAdapter.notifyItemRemoved(i);
//                }
//            }
//        }
////        if (!remoteParticipant.getIdentity().equals(remoteParticipantIdentity)) {
////            return;
////        }
//
//        /*
//         * Remove remote participant renderer
//         */
////        if (!remoteParticipant.getRemoteVideoTracks().isEmpty()) {
////            RemoteVideoTrackPublication remoteVideoTrackPublication =
////                    remoteParticipant.getRemoteVideoTracks().get(0);
////
////            /*
////             * Remove video only if subscribed to participant track
////             */
////            if (remoteVideoTrackPublication.isTrackSubscribed()) {
////                removeParticipantVideo(remoteVideoTrackPublication.getRemoteVideoTrack());
////            }
////        }
//        moveLocalVideoToPrimaryView();
//    }
//
////    private void removeParticipantVideo(VideoTrack videoTrack) {
////        videoTrack.removeRenderer(primaryVideoView);
////    }
//
//    private void moveLocalVideoToPrimaryView() {
////        if (thumbnailVideoView.getVisibility() == View.VISIBLE) {
////            thumbnailVideoView.setVisibility(View.GONE);
////            if (localVideoTrack != null) {
////                localVideoTrack.removeRenderer(thumbnailVideoView);
////                localVideoTrack.addRenderer(primaryVideoView);
////            }
////            localVideoView = primaryVideoView;
////            primaryVideoView.setMirror(cameraCapturerCompat.getCameraSource() ==
////                    CameraCapturer.CameraSource.FRONT_CAMERA);
////        }
//        if (remoteParticipantList.size()==0){
//            if (localVideoTrack != null) {
////                localVideoTrack.removeRenderer(thumbnailVideoView);
//                thumbnailAdapter.notifyDataSetChanged();
//                localVideoTrack.addRenderer(primaryVideoView);
//            }
//            localVideoView = primaryVideoView;
//            primaryVideoView.setMirror(cameraCapturerCompat.getCameraSource() ==
//                    CameraCapturer.CameraSource.FRONT_CAMERA);
//        }
//
//    }
//
//    /*
//     * Room events listener
//     */
//    @SuppressLint("SetTextI18n")
//    private Room.Listener roomListener() {
//        return new Room.Listener() {
//            @Override
//            public void onConnected(Room room) {
//                localParticipant = room.getLocalParticipant();
//                localParticipant.publishTrack(localDataTrack);
//                setTitle(room.getName());
//
////                for (RemoteParticipant remoteParticipant : room.getRemoteParticipants()) {
////                    addRemoteParticipant(remoteParticipant);
////                    break;
////                }
//                //    remoteParticipantList = room.getRemoteParticipants();
//                for (RemoteParticipant remoteParticipant : room.getRemoteParticipants()) {
//                    addRemoteParticipant(remoteParticipant);
//                    // break;
//                }
////                thumbnailAdapter.notifyDataSetChanged();
//
//
//
//            }
//
//            @Override
//            public void onReconnecting(@NonNull Room room, @NonNull TwilioException twilioException) {
//                reconnectingProgressBar.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onReconnected(@NonNull Room room) {
//                reconnectingProgressBar.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onConnectFailure(Room room, TwilioException e) {
//                Toast.makeText(VideoActivity.this,e.getExplanation(),Toast.LENGTH_LONG).show();
//                audioSwitch.deactivate();
//                intializeUI();
//            }
//
//            @Override
//            public void onDisconnected(Room room, TwilioException e) {
//                localParticipant = null;
//                reconnectingProgressBar.setVisibility(View.GONE);
//                VideoActivity.this.room = null;
//                // Only reinitialize the UI if disconnect was not called from onDestroy()
//                if (!disconnectedFromOnDestroy) {
//                    audioSwitch.deactivate();
//                    intializeUI();
//                    moveLocalVideoToPrimaryView();
//                    finish();
//                }
//            }
//
//            @Override
//            public void onParticipantConnected(Room room, RemoteParticipant remoteParticipant) {
//
//                addRemoteParticipant(remoteParticipant);
//
//            }
//
//            @Override
//            public void onParticipantDisconnected(Room room, RemoteParticipant remoteParticipant) {
//                removeRemoteParticipant(remoteParticipant);
//            }
//
//            @Override
//            public void onRecordingStarted(Room room) {
//                /*
//                 * Indicates when media shared to a Room is being recorded. Note that
//                 * recording is only available in our Group Rooms developer preview.
//                 */
//                Log.d(TAG, "onRecordingStarted");
//            }
//
//            @Override
//            public void onRecordingStopped(Room room) {
//                /*
//                 * Indicates when media shared to a Room is no longer being recorded. Note that
//                 * recording is only available in our Group Rooms developer preview.
//                 */
//                Log.d(TAG, "onRecordingStopped");
//            }
//        };
//    }
//
//    @SuppressLint("SetTextI18n")
//    private RemoteParticipant.Listener remoteParticipantListener() {
//        return new RemoteParticipant.Listener() {
//            @Override
//            public void onAudioTrackPublished(RemoteParticipant remoteParticipant,
//                                              RemoteAudioTrackPublication remoteAudioTrackPublication) {
//                Log.i(TAG, String.format("onAudioTrackPublished: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteAudioTrackPublication: sid=%s, enabled=%b, " +
//                                "subscribed=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteAudioTrackPublication.getTrackSid(),
//                        remoteAudioTrackPublication.isTrackEnabled(),
//                        remoteAudioTrackPublication.isTrackSubscribed(),
//                        remoteAudioTrackPublication.getTrackName()));
//            }
//
//            @Override
//            public void onAudioTrackUnpublished(RemoteParticipant remoteParticipant,
//                                                RemoteAudioTrackPublication remoteAudioTrackPublication) {
//                Log.i(TAG, String.format("onAudioTrackUnpublished: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteAudioTrackPublication: sid=%s, enabled=%b, " +
//                                "subscribed=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteAudioTrackPublication.getTrackSid(),
//                        remoteAudioTrackPublication.isTrackEnabled(),
//                        remoteAudioTrackPublication.isTrackSubscribed(),
//                        remoteAudioTrackPublication.getTrackName()));
//            }
//
//            @Override
//            public void onDataTrackPublished(RemoteParticipant remoteParticipant,
//                                             RemoteDataTrackPublication remoteDataTrackPublication) {
//                Log.i(TAG, String.format("onDataTrackPublished: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteDataTrackPublication: sid=%s, enabled=%b, " +
//                                "subscribed=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteDataTrackPublication.getTrackSid(),
//                        remoteDataTrackPublication.isTrackEnabled(),
//                        remoteDataTrackPublication.isTrackSubscribed(),
//                        remoteDataTrackPublication.getTrackName()));
//            }
//
//            @Override
//            public void onDataTrackUnpublished(RemoteParticipant remoteParticipant,
//                                               RemoteDataTrackPublication remoteDataTrackPublication) {
//                Log.i(TAG, String.format("onDataTrackUnpublished: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteDataTrackPublication: sid=%s, enabled=%b, " +
//                                "subscribed=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteDataTrackPublication.getTrackSid(),
//                        remoteDataTrackPublication.isTrackEnabled(),
//                        remoteDataTrackPublication.isTrackSubscribed(),
//                        remoteDataTrackPublication.getTrackName()));
//            }
//
//            @Override
//            public void onVideoTrackPublished(RemoteParticipant remoteParticipant,
//                                              RemoteVideoTrackPublication remoteVideoTrackPublication) {
//                Log.i(TAG, String.format("onVideoTrackPublished: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteVideoTrackPublication: sid=%s, enabled=%b, " +
//                                "subscribed=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteVideoTrackPublication.getTrackSid(),
//                        remoteVideoTrackPublication.isTrackEnabled(),
//                        remoteVideoTrackPublication.isTrackSubscribed(),
//                        remoteVideoTrackPublication.getTrackName()));
//            }
//
//            @Override
//            public void onVideoTrackUnpublished(RemoteParticipant remoteParticipant,
//                                                RemoteVideoTrackPublication remoteVideoTrackPublication) {
//                Log.i(TAG, String.format("onVideoTrackUnpublished: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteVideoTrackPublication: sid=%s, enabled=%b, " +
//                                "subscribed=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteVideoTrackPublication.getTrackSid(),
//                        remoteVideoTrackPublication.isTrackEnabled(),
//                        remoteVideoTrackPublication.isTrackSubscribed(),
//                        remoteVideoTrackPublication.getTrackName()));
//            }
//
//            @Override
//            public void onAudioTrackSubscribed(RemoteParticipant remoteParticipant,
//                                               RemoteAudioTrackPublication remoteAudioTrackPublication,
//                                               RemoteAudioTrack remoteAudioTrack) {
//                Log.i(TAG, String.format("onAudioTrackSubscribed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteAudioTrack: enabled=%b, playbackEnabled=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteAudioTrack.isEnabled(),
//                        remoteAudioTrack.isPlaybackEnabled(),
//                        remoteAudioTrack.getName()));
//            }
//
//            @Override
//            public void onAudioTrackUnsubscribed(RemoteParticipant remoteParticipant,
//                                                 RemoteAudioTrackPublication remoteAudioTrackPublication,
//                                                 RemoteAudioTrack remoteAudioTrack) {
//                Log.i(TAG, String.format("onAudioTrackUnsubscribed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteAudioTrack: enabled=%b, playbackEnabled=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteAudioTrack.isEnabled(),
//                        remoteAudioTrack.isPlaybackEnabled(),
//                        remoteAudioTrack.getName()));
//            }
//
//            @Override
//            public void onAudioTrackSubscriptionFailed(RemoteParticipant remoteParticipant,
//                                                       RemoteAudioTrackPublication remoteAudioTrackPublication,
//                                                       TwilioException twilioException) {
//                Log.i(TAG, String.format("onAudioTrackSubscriptionFailed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteAudioTrackPublication: sid=%b, name=%s]" +
//                                "[TwilioException: code=%d, message=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteAudioTrackPublication.getTrackSid(),
//                        remoteAudioTrackPublication.getTrackName(),
//                        twilioException.getCode(),
//                        twilioException.getMessage()));
//            }
//
//            @Override
//            public void onDataTrackSubscribed(RemoteParticipant remoteParticipant,
//                                              RemoteDataTrackPublication remoteDataTrackPublication,
//                                              RemoteDataTrack remoteDataTrack) {
//                Log.i(TAG, String.format("onDataTrackSubscribed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteDataTrack: enabled=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteDataTrack.isEnabled(),
//                        remoteDataTrack.getName()));
//            }
//
//            @Override
//            public void onDataTrackUnsubscribed(RemoteParticipant remoteParticipant,
//                                                RemoteDataTrackPublication remoteDataTrackPublication,
//                                                RemoteDataTrack remoteDataTrack) {
//                Log.i(TAG, String.format("onDataTrackUnsubscribed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteDataTrack: enabled=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteDataTrack.isEnabled(),
//                        remoteDataTrack.getName()));
//            }
//
//            @Override
//            public void onDataTrackSubscriptionFailed(RemoteParticipant remoteParticipant,
//                                                      RemoteDataTrackPublication remoteDataTrackPublication,
//                                                      TwilioException twilioException) {
//                Log.i(TAG, String.format("onDataTrackSubscriptionFailed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteDataTrackPublication: sid=%b, name=%s]" +
//                                "[TwilioException: code=%d, message=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteDataTrackPublication.getTrackSid(),
//                        remoteDataTrackPublication.getTrackName(),
//                        twilioException.getCode(),
//                        twilioException.getMessage()));
//            }
//
//            @Override
//            public void onVideoTrackSubscribed(RemoteParticipant remoteParticipant,
//                                               RemoteVideoTrackPublication remoteVideoTrackPublication,
//                                               RemoteVideoTrack remoteVideoTrack) {
//                Log.i(TAG, String.format("onVideoTrackSubscribed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteVideoTrack: enabled=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteVideoTrack.isEnabled(),
//                        remoteVideoTrack.getName()));
//                //  addRemoteParticipantVideo(remoteVideoTrack);
//
////                if (remoteParticipant.isConnected()){
//                if (remoteParticipantList.size()!=0){
//                    for (int i =0 ; i<remoteParticipantList.size();i++){
//                        if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
//                            //remoteParticipantList.remove(remoteParticipantList.get(i));
//                            thumbnailAdapter.notifyItemChanged(i);
//
//                        }
//                    }
//
//
//                }
//
//
//
//            }
//
//            @Override
//            public void onVideoTrackUnsubscribed(RemoteParticipant remoteParticipant,
//                                                 RemoteVideoTrackPublication remoteVideoTrackPublication,
//                                                 RemoteVideoTrack remoteVideoTrack) {
//                Log.i(TAG, String.format("onVideoTrackUnsubscribed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteVideoTrack: enabled=%b, name=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteVideoTrack.isEnabled(),
//                        remoteVideoTrack.getName()));
//                for (int i =0 ; i<remoteParticipantList.size();i++){
//                    if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
//                        if (remoteParticipant.isConnected()){
//                            thumbnailAdapter.notifyItemChanged(i);
//                        }
//                    }
//                }
//
//
//
//                //removeParticipantVideo(remoteVideoTrack);
//            }
//
//            @Override
//            public void onVideoTrackSubscriptionFailed(RemoteParticipant remoteParticipant,
//                                                       RemoteVideoTrackPublication remoteVideoTrackPublication,
//                                                       TwilioException twilioException) {
//                Log.i(TAG, String.format("onVideoTrackSubscriptionFailed: " +
//                                "[RemoteParticipant: identity=%s], " +
//                                "[RemoteVideoTrackPublication: sid=%b, name=%s]" +
//                                "[TwilioException: code=%d, message=%s]",
//                        remoteParticipant.getIdentity(),
//                        remoteVideoTrackPublication.getTrackSid(),
//                        remoteVideoTrackPublication.getTrackName(),
//                        twilioException.getCode(),
//                        twilioException.getMessage()));
//                Snackbar.make(connectActionFab,
//                        String.format("Failed to subscribe to %s video track",
//                                remoteParticipant.getIdentity()),
//                        Snackbar.LENGTH_LONG)
//                        .show();
//            }
//
//            @Override
//            public void onAudioTrackEnabled(RemoteParticipant remoteParticipant,
//                                            RemoteAudioTrackPublication remoteAudioTrackPublication) {
//                Log.d("Message ","Audio Enabled");
//                for (int i =0 ; i<remoteParticipantList.size();i++){
//                    if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
//                        if (remoteParticipant.isConnected()){
//
//                            thumbnailAdapter.notifyItemChanged(i);
//                        }
//                    }
//                }
//
//            }
//
//            @Override
//            public void onAudioTrackDisabled(RemoteParticipant remoteParticipant,
//                                             RemoteAudioTrackPublication remoteAudioTrackPublication) {
//                Log.d("Message ","Audio Disabled");
//                for (int i =0 ; i<remoteParticipantList.size();i++){
//                    if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
//                        if (remoteParticipant.isConnected()){
//
//                            thumbnailAdapter.notifyItemChanged(i);
//                        }
//                    }
//                }
//
//            }
//
//            @Override
//            public void onVideoTrackEnabled(RemoteParticipant remoteParticipant,
//                                            RemoteVideoTrackPublication remoteVideoTrackPublication) {
//                Log.d("Message ","Video Disabled");
//                for (int i =0 ; i<remoteParticipantList.size();i++){
//                    if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
//                        if (remoteParticipant.isConnected()){
//
//                            thumbnailAdapter.notifyItemChanged(i);
//                        }
//                    }
//                }
//            }
//
//            @Override
//            public void onVideoTrackDisabled(RemoteParticipant remoteParticipant,
//                                             RemoteVideoTrackPublication remoteVideoTrackPublication) {
//                Log.d("Message ","Video Enabled");
//                for (int i =0 ; i<remoteParticipantList.size();i++){
//                    if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
//                        if (remoteParticipant.isConnected()){
//
//                            thumbnailAdapter.notifyItemChanged(i);
//                        }
//                    }
//                }
//            }
//
//            @Override
//            public void onNetworkQualityLevelChanged(@NonNull RemoteParticipant remoteParticipant, @NonNull NetworkQualityLevel networkQualityLevel) {
////                for (int i =0 ; i<remoteParticipantList.size();i++){
////                    if (remoteParticipant.getIdentity().equals(remoteParticipantList.get(i).getIdentity())){
////                        if (remoteParticipant.isConnected()){
////
////                            thumbnailAdapter.notifyItemChanged(i);
////                        }
////                    }
////                }
//            }
//        };
//
//
//    }
//
//    private DialogInterface.OnClickListener connectClickListener(final EditText roomEditText) {
//        return (dialog, which) -> {
//            /*
//             * Connect to room
//             */
//            // connectToRoom(roomEditText.getText().toString());
//        };
//    }
//
//    private View.OnClickListener disconnectClickListener() {
//        return v -> {
//            /*
//             * Disconnect from room
//             */
//            if (room != null) {
//                room.disconnect();
//            }
//            intializeUI();
//        };
//    }
//
//    private View.OnClickListener connectActionClickListener() {
//        return v -> showConnectDialog();
//    }
//
//    private DialogInterface.OnClickListener cancelConnectDialogClickListener() {
//        return (dialog, which) -> {
//            intializeUI();
//            connectDialog.dismiss();
//        };
//    }
//
//    private View.OnClickListener switchCameraClickListener() {
//        return v -> {
//            if (cameraCapturerCompat != null) {
//                CameraCapturer.CameraSource cameraSource = cameraCapturerCompat.getCameraSource();
//                cameraCapturerCompat.switchCamera();
//                thumbnailAdapter.notifyItemChanged(0);
//                primaryVideoView.setMirror(cameraSource == CameraCapturer.CameraSource.BACK_CAMERA);
//
////                if (thumbnailVideoView.getVisibility() == View.VISIBLE) {
////                    thumbnailVideoView.setMirror(cameraSource == CameraCapturer.CameraSource.BACK_CAMERA);
////                } else {
////                    primaryVideoView.setMirror(cameraSource == CameraCapturer.CameraSource.BACK_CAMERA);
////                }
//            }
//        };
//    }
//
//    private View.OnClickListener localVideoClickListener() {
//        return v -> {
//            /*
//             * Enable/disable the local video track
//             */
//            if (localVideoTrack != null) {
//                boolean enable = !localVideoTrack.isEnabled();
//                localVideoTrack.enable(enable);
//                int icon;
//                if (enable) {
//                    icon = R.drawable.ic_videocam_white_24dp;
//                    switchCameraActionFab.show();
//                } else {
//                    icon = R.drawable.ic_videocam_off_black_24dp;
//                    switchCameraActionFab.hide();
//                }
//                localVideoActionFab.setImageDrawable(
//                        ContextCompat.getDrawable(VideoActivity.this, icon));
//            }
//        };
//    }
//
//    private View.OnClickListener muteClickListener() {
//        return v -> {
//            /*
//             * Enable/disable the local audio track. The results of this operation are
//             * signaled to other Participants in the same Room. When an audio track is
//             * disabled, the audio is muted.
//             */
//            if (localAudioTrack != null) {
//                boolean enable = !localAudioTrack.isEnabled();
//                localAudioTrack.enable(enable);
//                int icon = enable ?
//                        R.drawable.ic_mic_white_24dp : R.drawable.ic_mic_off_black_24dp;
//                muteActionFab.setImageDrawable(ContextCompat.getDrawable(
//                        VideoActivity.this, icon));
//            }
//        };
//    }
//
//    private class ThumbnailAdapter extends RecyclerView.Adapter<ThumbnailAdapter.MyViewHoler>{
//        int  samePositionClicked = -1;
////        RemoteParticipant remoteParticipant;
//
//
//
//        @NonNull
//        @Override
//        public ThumbnailAdapter.MyViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//            View  view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_thumbnail,parent,false);
//            return new ThumbnailAdapter.MyViewHoler(view);
//        }
//
//        @Override
//        public void onBindViewHolder(@NonNull ThumbnailAdapter.MyViewHoler holder, int position) {
////            if (remoteParticipantList.get(position).getRemoteVideoTracks().size() > 0) {
//            //  RemoteVideoTrackPublication remoteVideoTrackPublication =
////                        remoteParticipantList.get(position).getRemoteVideoTracks().get(0);
//
//            /*
//             * Only render video tracks that are subscribed to
//             */
////            RemoteParticipant last = remoteParticipantList.get(remoteParticipantList.size() - 1);
////            if (remoteParticipantList.get(position).equals(last)){
////                if (remoteVideoTrackPublication.isTrackSubscribed()) {
////                    localVideoTrack.removeRenderer(primaryVideoView);
////                    localVideoTrack.addRenderer(holder.thumbnail_video_view);
////                    localVideoView = holder.thumbnail_video_view;
////                    holder.thumbnail_video_view.setMirror(cameraCapturerCompat.getCameraSource() ==
////                            CameraCapturer.CameraSource.FRONT_CAMERA);
////                    primaryVideoView.setMirror(false);
////                    remoteVideoTrackPublication.getRemoteVideoTrack().addRenderer(primaryVideoView);
////                }
////            }
////            else {
////                if(remoteVideoTrackPublication.isTrackSubscribed()){
////                    holder.thumbnail_video_view.setMirror(cameraCapturerCompat.getCameraSource() ==
////                            CameraCapturer.CameraSource.FRONT_CAMERA);
////                    remoteVideoTrackPublication.getRemoteVideoTrack().addRenderer(holder.thumbnail_video_view);
////                }
////
////            }
//            ConstraintLayout.LayoutParams cellParams = new ConstraintLayout.LayoutParams(screenwidth/2, (int) (screenheight/4.5));
//
//            holder.thumbnail_video_view.setLayoutParams(cellParams);
//
//
//            if (remoteParticipantList.get(position).getRemoteVideoTracks().size() > 0) {
//                RemoteVideoTrackPublication remoteVideoTrackPublication =
//                        remoteParticipantList.get(position).getRemoteVideoTracks().get(0);
//                if (!remoteVideoTrackPublication.isTrackEnabled()){
//                    holder.video_disabled.setVisibility(View.VISIBLE);
//
//                }
//                else {
//                    holder.video_disabled.setVisibility(View.GONE);
//
//                }
//
//                if (remoteVideoTrackPublication.isTrackSubscribed()){
//                    remoteVideoTrackPublication.getVideoTrack().addRenderer(holder.thumbnail_video_view);
//                    holder.progressBar.setVisibility(View.GONE);
//
//                }
//                else {
//
//
//                    holder.progressBar.setVisibility(View.VISIBLE);
//
//                }
//
//            }
//            RemoteAudioTrackPublication remoteAudioTrackPublication = remoteParticipantList.get(position).getRemoteAudioTracks().get(0);
//            if (remoteAudioTrackPublication.getRemoteAudioTrack()!=null){
//                if (remoteAudioTrackPublication.getAudioTrack().isEnabled()){
//                    holder.text_onanothercall.setVisibility(View.GONE);
//
//                }
//                else {
//                    holder.text_onanothercall.setVisibility(View.VISIBLE);
//
//                }
//                if(remoteAudioTrackPublication.getRemoteAudioTrack().isPlaybackEnabled()){
//
//                    holder.mute_audio.setImageResource(R.drawable.ic_mic_white_24dp);
//                }
//                else {
//                    holder.mute_audio.setImageResource(R.drawable.ic_mic_off_black_24dp);
//                }
//            }
//
//
//            holder.mute_audio.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(remoteAudioTrackPublication.getRemoteAudioTrack().isPlaybackEnabled()){
//                        holder.mute_audio.setImageResource(R.drawable.ic_mic_off_black_24dp);
//
//                        remoteAudioTrackPublication.getRemoteAudioTrack().enablePlayback(false);
//                    }
//                    else {
//
//                        holder.mute_audio.setImageResource(R.drawable.ic_mic_white_24dp);
//                        remoteAudioTrackPublication.getRemoteAudioTrack().enablePlayback(true);
//                    }
//
//                }
//            });
////            NetworkQualityLevel networkQualityLevel = remoteParticipantList.get(position).getNetworkQualityLevel();
////            if (networkQualityLevel == NetworkQualityLevel.NETWORK_QUALITY_LEVEL_UNKNOWN){
////
////                holder.text_strength.setText("Unknown");
////
////            }
////            else if (networkQualityLevel == NetworkQualityLevel.NETWORK_QUALITY_LEVEL_ZERO){
////                holder.text_strength.setText("0");
////            }
////            else if (networkQualityLevel == NetworkQualityLevel.NETWORK_QUALITY_LEVEL_ONE){
////                holder.text_strength.setText("1");
////            }
////            else if (networkQualityLevel == NetworkQualityLevel.NETWORK_QUALITY_LEVEL_TWO){
////                holder.text_strength.setText("2");
////            }
////            else if (networkQualityLevel == NetworkQualityLevel.NETWORK_QUALITY_LEVEL_THREE){
////                holder.text_strength.setText("3");
////            }
////            else if (networkQualityLevel == NetworkQualityLevel.NETWORK_QUALITY_LEVEL_FOUR){
////                holder.text_strength.setText("4");
////            }
////            else if (networkQualityLevel == NetworkQualityLevel.NETWORK_QUALITY_LEVEL_FIVE){
////                holder.text_strength.setText("5");
////            }
//
////            holder.itemView.setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View v) {
////                    RemoteVideoTrackPublication remoteVideoTrackPublication =
////                            remoteParticipantList.get(position).getRemoteVideoTracks().get(0);
////                    if(remoteVideoTrackPublication.isTrackSubscribed()){
////                        remoteVideoTrackPublication.getVideoTrack().removeRenderer(holder.thumbnail_video_view);
////
////                    }
////                    else {
////                        remoteVideoTrackPublication.getVideoTrack().addRenderer(holder.thumbnail_video_view);
////
////
////                    }
////
////                }
////            });
//
////                }
//
////            }
//
//        }
//
//        @Override
//        public int getItemCount() {
//            return remoteParticipantList.size();
//
//
//        }
//
//        public class MyViewHoler extends RecyclerView.ViewHolder {
//            VideoTextureView thumbnail_video_view;
//            ProgressBar progressBar;
//            TextView text_onanothercall,text_strength;
//            ImageView video_disabled,mute_audio;
//            public MyViewHoler(@NonNull View itemView) {
//                super(itemView);
//                thumbnail_video_view= itemView.findViewById(R.id.thumbnail_video_view);
//                progressBar = itemView.findViewById(R.id.progressBar);
//                text_onanothercall = itemView.findViewById(R.id.text_onanothercall);
//                video_disabled = itemView.findViewById(R.id.video_disabled);
//                mute_audio = itemView.findViewById(R.id.mute_audio);
//                text_strength = itemView.findViewById(R.id.text_strength);
//
//            }
//        }
//
//        public void addThumbnail(RemoteParticipant remoteParticipant){
//            remoteParticipantList.add(remoteParticipant);
//            thumbnailAdapter.notifyDataSetChanged();
//
////            notifyItemInserted(thumbnailAdapter.getItemCount()+1);
////            if(remoteParticipantList.size()==1){
////                ConstraintLayout.LayoutParams cellParams = new ConstraintLayout.LayoutParams(screenwidth, (int) (screenheight/3));
////                primaryVideoView.setLayoutParams(cellParams);
////            }
//
//
//        }
//
//        @Override
//        public int getItemViewType(int position) {
//            return position;
//        }
//    }
//
//
//    @Override
//    public void onBackPressed() {
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("")
//                .setMessage("Do you want to quit")
//                .setCancelable(true)
//                .setPositiveButton("Yes",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                finish();
//                            }
//                        })
//                .setNegativeButton("No",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.dismiss();
//                            }
//                        });
//        AlertDialog alert = builder.create();
//        alert.setCanceledOnTouchOutside(false);
//        alert.show();
//    }
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == REQUEST_MEDIA_PROJECTION) {
//            if (resultCode != AppCompatActivity.RESULT_OK) {
//                Toast.makeText(this, R.string.screen_capture_permission_not_granted,
//                        Toast.LENGTH_LONG).show();
//                return;
//            }
//            screenCapturer = new ScreenCapturer(this, resultCode, data, screenCapturerListener);
//            startScreenCapture();
//        }
//    }
//
//}

