package com.gegosoft.demoschool.Activity;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Fragments.TimeTableFragment;

import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.TimeTableModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;
import com.google.android.material.tabs.TabLayout;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimeTableActivity extends AppCompatActivity {
    TabLayout timetabletabLayout;
    ViewPager timetableviewPager;
    Api api;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    List<TimeTableModel.Datum> timeTableModels;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timetable_layout);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(this);
        toolbar=findViewById(R.id.timetabletoolbar);
        toolbar.setTitle("Time Table");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        timetabletabLayout = findViewById(R.id.timetabletablayout);
        timetableviewPager = findViewById(R.id.timetableviewpager);
//        timetabletabLayout.addTab(timetabletabLayout.newTab().setText("MON"));
//        timetabletabLayout.addTab(timetabletabLayout.newTab().setText("TUE"));
//        timetabletabLayout.addTab(timetabletabLayout.newTab().setText("WED"));
//        timetabletabLayout.addTab(timetabletabLayout.newTab().setText("THU"));
//        timetabletabLayout.addTab(timetabletabLayout.newTab().setText("FRI"));
        timetabletabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        timetabletabLayout.setTabTextColors(getResources().getColor(R.color.black),getResources().getColor(R.color.white));
        ActionBar actionBar = getSupportActionBar();
//        actionBar.setTitle("TIME TABLE");
        if (CheckNetwork.isInternetAvailable(this)){
            getTimeTable();
        }


        timetableviewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(timetabletabLayout));
        timetabletabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                timetableviewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }


    private void getTimeTable(){
        api = ApiClient.getClient().create(Api.class);
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        Call<TimeTableModel>  tableModelCall = api.getTimeTable(headermap,ApiClient.getClient().baseUrl()+"api/v2/timetable/"+userDetailsSharedPref.getString("userId"));
        tableModelCall.enqueue(new Callback<TimeTableModel>() {
            @Override
            public void onResponse(Call<TimeTableModel> call, Response<TimeTableModel> response) {
                if (response.isSuccessful()){
                    timeTableModels = response.body().getData();
                    if (timeTableModels.size()!=0){

                        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
                        Date d = new Date();
                        String dayOfTheWeek = sdf.format(d);
                        for (int i = 0 ; i< timeTableModels.size();i++){
                            timetabletabLayout.addTab(timetabletabLayout.newTab().setText(timeTableModels.get(i).getDayName().substring(0,3)));
                        }
                        final TimetableAdapter timetableAdapter =new TimetableAdapter(getSupportFragmentManager(),TimeTableActivity.this,timetabletabLayout.getTabCount());
                        timetableviewPager.setAdapter(timetableAdapter);
                        for (int i = 0 ; i< timeTableModels.size();i++){
                            if (dayOfTheWeek.equalsIgnoreCase(timeTableModels.get(i).getDayName())){
                                timetableviewPager.setCurrentItem(i);
                            }
                        }

                    }

                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(TimeTableActivity.this);
                    }
                }
                progressDialogFragment.dismiss();

            }

            @Override
            public void onFailure(Call<TimeTableModel> call, Throwable t) {
                AppUtils.APIFails(TimeTableActivity.this,t);
                progressDialogFragment.dismiss();
            }
        });
    }
    public class TimetableAdapter extends FragmentPagerAdapter {

        private Context myContext;
        int totalTabs;

        public TimetableAdapter(@NonNull FragmentManager fm, Context context, int totalTabs) {
            super(fm);
            myContext = context;
            this.totalTabs = totalTabs;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
//            switch (position) {
//                case 0:
//                    TimeTable_MON_Fragment timeTable_mon_fragment = new TimeTable_MON_Fragment();
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable(timeTableModels.get(position).getDayName(), (Serializable) timeTableModels.get(position).getArray());
//                    timeTable_mon_fragment.setArguments(bundle);
//                    return timeTable_mon_fragment;
//                case 1:
//                    TimeTable_TUE_Fragment timeTable_tue_fragment = new TimeTable_TUE_Fragment();
//                    Bundle bundle1 = new Bundle();
//                    bundle1.putSerializable("Monday", (Serializable) timeTableModels.get(position).getArray());
//                    timeTable_tue_fragment.setArguments(bundle1);
//
//                    return timeTable_tue_fragment;
//                case 2:
//                    TimeTable_WED_Fragment timeTable_wed_fragment = new TimeTable_WED_Fragment();
//                    Bundle bundle2 = new Bundle();
//                    bundle2.putSerializable("Monday", (Serializable) timeTableModels.get(position).getArray());
//                    timeTable_wed_fragment.setArguments(bundle2);
//                    return timeTable_wed_fragment;
//                case 4:
//                    TimeTable_THU_Fragment timeTable_thu_fragment = new TimeTable_THU_Fragment();
//                    return timeTable_thu_fragment;
//                case 5:
//                    TimeTable_FRI_Fragment timeTable_fri_fragment = new TimeTable_FRI_Fragment();
//                    return timeTable_fri_fragment;
//                case 6:
//                    TimeTable_SAT_Fragment timeTable_sat_fragment=new TimeTable_SAT_Fragment();
//                    return timeTable_sat_fragment;
//                default:
//                    return null;
//            }
            TimeTableFragment timeTable_mon_fragment = new TimeTableFragment();
            Bundle bundle = new Bundle();
            bundle.putString("dayname", timeTableModels.get(position).getDayName());
            bundle.putSerializable(timeTableModels.get(position).getDayName(), (Serializable) timeTableModels.get(position).getArray());
            timeTable_mon_fragment.setArguments(bundle);
            return timeTable_mon_fragment;
        }
            @Override
            public int getCount () {
                return totalTabs;
            }
        }


}