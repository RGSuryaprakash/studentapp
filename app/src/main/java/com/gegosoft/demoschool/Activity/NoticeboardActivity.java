package com.gegosoft.demoschool.Activity;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ClassNoticefragment;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Fragments.SchoolNoticefragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.SchoolNoticeBoardModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;
import com.google.android.material.tabs.TabLayout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NoticeboardActivity extends AppCompatActivity {
    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticeboard);
        tabLayout=findViewById(R.id.tablayout);
        viewPager=findViewById(R.id.viewpager);
        tabLayout.addTab(tabLayout.newTab().setText("School"));
        tabLayout.addTab(tabLayout.newTab().setText("Class"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Noticeboard");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tabLayout.setTabTextColors(getResources().getColor(R.color.black),getResources().getColor(R.color.white));
//        toolbar.setTitle("Notifications");
//        toolbar.setTitleTextColor(Color.WHITE);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        ActionBar actionBar=getSupportActionBar();
//        actionBar.setTitle("Notifications");
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        tabLayout.setTabTextColors(ColorStateList.valueOf(getResources().getColor(R.color.white)));
//        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.white));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        final NoticeboardActivity.TabAdapter adapter = new NoticeboardActivity.TabAdapter(getSupportFragmentManager(), NoticeboardActivity.this, tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    public class TabAdapter extends FragmentPagerAdapter {

        private Context myContext;
        int totalTabs;
        public TabAdapter(@NonNull FragmentManager fm, Context context, int totalTabs) {
            super(fm);
            myContext = context;
            this.totalTabs = totalTabs;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    SchoolNoticefragment schoolNoticefragment = new SchoolNoticefragment();
                    return schoolNoticefragment;
                case 1:
                    ClassNoticefragment classNoticefragment = new ClassNoticefragment();
                    return classNoticefragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return totalTabs;
        }
    }
}
