package com.gegosoft.demoschool.Activity;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gegosoft.demoschool.Fragments.CompletedAssignment;
import com.gegosoft.demoschool.Fragments.OngoingAssignmentFragment;
import com.gegosoft.demoschool.R;
import com.google.android.material.tabs.TabLayout;

public class AssignmentActivity extends AppCompatActivity {

    Toolbar toolbar;
    TabLayout assignmenttablayout;
    ViewPager assignmentviewpager;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.assignmentactivity);
        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Assignment");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        assignmenttablayout = findViewById(R.id.assignmenttablayout);
        assignmentviewpager = findViewById(R.id.assignmentviewpager);
        assignmenttablayout.addTab(assignmenttablayout.newTab().setText("Ongoing"));
        assignmenttablayout.addTab(assignmenttablayout.newTab().setText("Completed"));
        assignmenttablayout.setTabTextColors(getResources().getColor(R.color.black),getResources().getColor(R.color.white));
        assignmenttablayout.setTabGravity(TabLayout.GRAVITY_FILL);



        assignmentviewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(assignmenttablayout));
        assignmenttablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                assignmentviewpager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        AssigmentPagerAdapter assigmentPagerAdapter = new AssigmentPagerAdapter(getSupportFragmentManager(), AssignmentActivity.this, assignmenttablayout.getTabCount());
        assignmentviewpager.setAdapter(assigmentPagerAdapter);
    }
    public class AssigmentPagerAdapter extends FragmentPagerAdapter {

        private Context myContext;
        int totalTabs;

        public AssigmentPagerAdapter(@NonNull FragmentManager fm, Context context, int totalTabs) {
            super(fm);
            myContext = context;
            this.totalTabs = totalTabs;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    OngoingAssignmentFragment ongoingAssignmentFragment = new OngoingAssignmentFragment();
                    return ongoingAssignmentFragment;
                case 1:
                    CompletedAssignment completedAssignment = new CompletedAssignment();
                    return completedAssignment;

                default:
                    return null;
            }

        }
        @Override
        public int getCount () {
            return totalTabs;
        }
    }
}
