package com.gegosoft.demoschool.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.JoinRoomModel;
import com.gegosoft.demoschool.Model.RoomListModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoomActivity extends AppCompatActivity {
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Toolbar toolbar;
    Api api;
    RecyclerView roomlist;
    TextView textnodata;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.roomactivity);
        roomlist = findViewById(R.id.recyclerview);
        api = ApiClient.getClient().create(Api.class);
        LinearLayoutManager linearLayoutManager =new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        roomlist.setLayoutManager(linearLayoutManager);
        roomlist.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(this);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        textnodata=findViewById(R.id.textnodata);
        toolbar=findViewById(R.id.toolbar);

        toolbar.setTitle("Online Class");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (CheckNetwork.isInternetAvailable(this)){
            getRoomlist();
        }
    }
    private void getRoomlist(){

        Call<RoomListModel> listModelCall = api.getListofRooms(headermap,userDetailsSharedPref.getString("userId"));
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        listModelCall.enqueue(new Callback<RoomListModel>() {
            @Override
            public void onResponse(Call<RoomListModel> call, Response<RoomListModel> response) {
                if (response.isSuccessful()){
                    List<RoomListModel.Datum> data = response.body().getData();
                    if (data.size()!=0){
                        RoomAdapter roomAdapter = new RoomAdapter(data);
                        roomlist.setAdapter(roomAdapter);


                    }
                    else {
                        textnodata.setVisibility(View.VISIBLE);
                    }

                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(RoomActivity.this);

                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<RoomListModel> call, Throwable t) {
                AppUtils.APIFails(RoomActivity.this,t);
                progressDialogFragment.dismiss();
            }
        });

    }

    private void getAccessToken(String roomName, String slug){
        Call<JoinRoomModel> joinRoomModelCall = api.getTokentoJoinRoom(headermap,slug,userDetailsSharedPref.getString("userId"));
        joinRoomModelCall.enqueue(new Callback<JoinRoomModel>() {
            @Override
            public void onResponse(Call<JoinRoomModel> call, Response<JoinRoomModel> response) {
                if (response.isSuccessful()){
                    String accessToken =  response.body().getData().getAccessToken();
                    startActivity(new Intent(RoomActivity.this,VideoActivity.class).putExtra("roomName",roomName).putExtra("accessToken",accessToken));
                }
            }

            @Override
            public void onFailure(Call<JoinRoomModel> call, Throwable t) {

            }
        });



    }

    public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.MyViewHolder>{
        List<RoomListModel.Datum> data;
        public RoomAdapter(List<RoomListModel.Datum> data) {
            this.data= data;

        }

        @NonNull
        @Override
        public RoomAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customroomlist,parent,false);

            return new RoomAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RoomAdapter.MyViewHolder holder, int position) {
            holder.roomname.setText(data.get(position).getTitle());
            //   holder.roomdescription.setText(data.get(position).getDescription());

            String roomstatus = data.get(position).getStatus();
            holder.subject_teacher_name.setText(data.get(position).getSubject()+" - " + data.get(position).getCreated_by());
            holder.date.setText(data.get(position).getJoining_date());
            holder.timing.setText(data.get(position).getStart_time() + " to "+data.get(position).getEnd_time());
            holder.status.setText(roomstatus);
            if (roomstatus.equalsIgnoreCase("Scheduled")){
                holder.joinroombtn.setBackground(getResources().getDrawable(R.drawable.round_button2));
                holder.joinroombtn.setTextColor(Color.GRAY);
                holder.status.setTextColor(Color.GRAY);
                holder.joinroombtn.setText("Join");
            }
            else if (roomstatus.equalsIgnoreCase("Class Live")){
                holder.joinroombtn.setBackground(getResources().getDrawable(R.drawable.round_button));
                holder.joinroombtn.setTextColor(Color.GREEN);
                holder.status.setTextColor(Color.GREEN);
                holder.joinroombtn.setText("Join");
            }
            else if (roomstatus.equalsIgnoreCase("Class Over")){
                holder.joinroombtn.setBackground(getResources().getDrawable(R.drawable.round_button3));
                holder.joinroombtn.setTextColor(Color.RED);
                holder.status.setTextColor(Color.RED);
                holder.joinroombtn.setText("Ended");
            }
            holder.joinroombtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (roomstatus.equalsIgnoreCase("Class Live")){
//                        getAccessToken(data.get(position).getTitle(),data.get(position).getSlug());
//                    }
                    getAccessToken(data.get(position).getTitle(),data.get(position).getSlug());
                }
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView roomname,status,roomdescription,subject_teacher_name,date,timing;
            Button joinroombtn;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                roomname = itemView.findViewById(R.id.roomname);
                status = itemView.findViewById(R.id.status);
//                roomdescription = itemView.findViewById(R.id.roomdescription);
                joinroombtn = itemView.findViewById(R.id.joinroombtn);
                subject_teacher_name = itemView.findViewById(R.id.subject_teacher_name);
                date = itemView.findViewById(R.id.date);
                timing = itemView.findViewById(R.id.timing);
            }
        }
    }
}
