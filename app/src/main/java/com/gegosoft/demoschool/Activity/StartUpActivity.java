package com.gegosoft.demoschool.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.gegosoft.demoschool.Fragments.SplashScreen;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

public class StartUpActivity extends AppCompatActivity {
    LinearLayout connection_error;
    ImageView refresh;
    UserDetailsSharedPref userDetailsSharedPref;
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.startupactivity);
        userDetailsSharedPref = userDetailsSharedPref.getInstance(this);
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            Bundle extras = intent.getExtras();
            String someData= extras.getString("message");
            String typeofnotification = extras.getString("type");
            Log.d("Virumaandi",someData+" "+typeofnotification);
            if (typeofnotification!=null){
                if (typeofnotification.equalsIgnoreCase("event")){
                    userDetailsSharedPref.saveBoolean("isEvent",true);

                }
                else if(typeofnotification.equalsIgnoreCase("assignment")){
                    userDetailsSharedPref.saveBoolean("isAssignment",true);
                }
                else if(typeofnotification.equalsIgnoreCase("discipline")){
                    userDetailsSharedPref.saveBoolean("isDiscipline",true);

                }
                else if(typeofnotification.equalsIgnoreCase("private message")){
                    Log.d("Mithun","Goyyala");
                    userDetailsSharedPref.saveBoolean("isPrivateMessage",true);

                }
                else if(typeofnotification.equalsIgnoreCase("homework")){
                    userDetailsSharedPref.saveBoolean("isHomeWork",true);

                }
                else if(typeofnotification.equalsIgnoreCase("bulletin")){
                    userDetailsSharedPref.saveBoolean("isMagazine",true);

                }
                 else if(typeofnotification.equalsIgnoreCase("conversation")){
                    userDetailsSharedPref.saveBoolean("isFeedback",true);
                }
            }


        }

        connection_error = findViewById(R.id.connection_error);
        refresh= (ImageView)findViewById(R.id.refresh);
        if(CheckNetwork.isInternetAvailable(StartUpActivity.this)){


            SplashScreen splashScreen = new SplashScreen();
            getSupportFragmentManager().beginTransaction().add(R.id.frame,splashScreen).addToBackStack(null).commit();
//      if (isTablet(getApplicationContext())){
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//      }
//
//      else{
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//      }
        }
        else {
            connection_error.setVisibility(View.VISIBLE);
            refresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(CheckNetwork.isInternetAvailable(StartUpActivity.this)){
                        connection_error.setVisibility(View.GONE);
                        Intent in = new Intent(StartUpActivity.this, StartUpActivity.class);
                        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(in);
                        StartUpActivity.this.finish();
                    }
                    else {
                        connection_error.setVisibility(View.VISIBLE);
                        Toast.makeText(StartUpActivity.this,"No Internet Connection",Toast.LENGTH_LONG).show();
                    }


                }
            });
            AlertDialog.Builder builder =new AlertDialog.Builder(this);
            builder.setTitle("No internet Connection");
            builder.setMessage("Please turn on internet connection to continue");
//        builder.setPositiveButton("TurnOn Wifi", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
////                Intent intent = new Intent(Intent.ACTION_MAIN);
////                intent.setClassName("com.android.phone", "com.android.phone.NetworkSetting");
////                startActivity(intent);
//                startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
//            }
//        });
            builder.setPositiveButton("TurnOn Data", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
//                    Intent intent = new Intent();
//                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.SettingsActivity$DataUsageSummaryActivity"));
//                    startActivity(intent);
                    Intent intent=new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
                    startActivity(intent);
//
                }
            });

            builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
//    SplashScreen splashScreen = new SplashScreen();
//    getSupportFragmentManager().beginTransaction().add(R.id.frame,splashScreen).addToBackStack(null).commit();

    }

}
