package com.gegosoft.demoschool.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Helper.FileUtils;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.FeedbackCategoryModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Receiver.NotificationReceiver;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.intentservice.chatui.ChatView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackActivity extends AppCompatActivity {
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static final int PICKFILE_RESULT_CODE = 2;
    Api apiInterface;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    ChatView chatView;
    int feedbackId = 0;
    Toolbar toolbar;
    LinearLayout imageviewlayout;
    EditText edt_message;
    String title,Msg;
    ImageView attach_file;
    Button btn_submit;
    List<FeedbackCategoryModel.Datum> feedbackCategory;
    RadioGroup radiogroup;
    String categoryId,message;
    ProgressDialogFragment progressDialogFragment;
    List<MultipartBody.Part> parts = new ArrayList<>();
   AppCompatTextView txt_myfeedback;
    private NotificationReceiver notificationReceiver = new NotificationReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
            RemoteMessage message = intent.getParcelableExtra("MessageReceived");
            JSONObject jsonObject = new JSONObject(message.getData());
            Log.d("datanotification",message.getData().toString());
            try {
                title =  jsonObject.getString("message");
                Msg=  jsonObject.getString("type");
                // String celebration =  jsonObject.getString("celebration");
                Log.d("Mera Nam",title+Msg);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            userDetailsSharedPref = userDetailsSharedPref.getInstance(context);
            if (Msg.equalsIgnoreCase("conversation")){

              //  getAllMessages();
            }

        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_form);
        userDetailsSharedPref = userDetailsSharedPref.getInstance(this);
        apiInterface = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        txt_myfeedback= findViewById(R.id.txt_myfeedback);
        btn_submit= findViewById(R.id.btn_submit);
        edt_message= findViewById(R.id.edt_message);
//        attach_file= findViewById(R.id.attach_file);
        toolbar= findViewById(R.id.toolbar);
//        radiogroup = findViewById(R.id.radiogroup);
        //imageviewlayout =  findViewById(R.id.imageviewlayout);
        toolbar.setTitle("Feedback");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        attach_file.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(!checkPermissionExternalStorage()){
//                    requestPermissionForExternalStorage();
//                }
//                else {
//                    seletFile();
//                }
//
//            }
//        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //    startActivity(new Intent(FeedbackActivity.this,FeebackListActivity.class));
                if (validate()){
                    submitFeedback();
                }




            }
        });
        txt_myfeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(FeedbackActivity.this,FeebackListActivity.class));
                Toast.makeText(FeedbackActivity.this, "Under Development...!!!!", Toast.LENGTH_SHORT).show();
            }
        });
//        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//               int id =  group.getCheckedRadioButtonId();
//                for (int i =0 ; i<feedbackCategory.size();i++){
//                    if (id==i){
//                        categoryId = feedbackCategory.get(i).getId();
//                    }
//
//                }
//            }
//        });
//        if (CheckNetwork.isInternetAvailable(this)){
//            getCategory();
//        }
//        getAllMessages();
//
//
//        chatView = (ChatView) findViewById(R.id.chat_view);
//        chatView.setBackgroundColor(getResources().getColor(R.color.white));
////        chatView.addMessage(new ChatMessage("Message received", System.currentTimeMillis(), ChatMessage.Type.RECEIVED));
//        //  chatView.addMessage(new ChatMessage("A message with a sender name",
//        //    System.currentTimeMillis(), ChatMessage.Type.RECEIVED, "Ryan Java"));
//        chatView.setOnSentMessageListener(new ChatView.OnSentMessageListener() {
//            @Override
//            public boolean sendMessage(ChatMessage chatMessage) {
//
//                if (feedbackId !=0){
//                    continueconvo(chatMessage.getMessage());
//
//                }
//                else {
//                    startConverstaion(chatMessage.getMessage());
//                }
//                return true;
//            }
//        });
//
//        chatView.setTypingListener(new ChatView.TypingListener() {
//            @Override
//            public void userStartedTyping() {
//
//
//
//            }
//
//            @Override
//            public void userStoppedTyping() {
//
//            }
//        });
//



    }

    private void submitFeedback(){
//        RequestBody reqcategory =
//                RequestBody.create(categoryId,MultipartBody.FORM);
//        RequestBody reqmessage =
//                RequestBody.create(message,MultipartBody.FORM);
        Call<ResponseBody> requestBodyCall = apiInterface.sendFeedback(headermap,edt_message.getText().toString(),userDetailsSharedPref.getString("userId"));
        Log.d("stdntid",userDetailsSharedPref.getString("userId"));
        progressDialogFragment = new ProgressDialogFragment();
        progressDialogFragment.setCancelable(false);
        Bundle bundle = new Bundle();
        bundle.putString("message","Uploading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"Please wait...");
        requestBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    ResponseBody responseBody = response.body();
                    Log.d("df",responseBody.toString());
                    Toast.makeText(getApplicationContext(),"Feedback added Successfully",Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("error",t.getMessage());

            }
        });


    }

    private boolean validate() {
        message = edt_message.getText().toString();

//        if (categoryId.isEmpty()){
//
//            return false;
//        }
        if (message.isEmpty()){
            edt_message.setError("Enter message");
            edt_message.requestFocus();
            return false;
        }
        return true;
    }

    private void getCategory() {
        Call<FeedbackCategoryModel>  categoryModelCall = apiInterface.getFeedbackCategorylist();
        progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        categoryModelCall.enqueue(new Callback<FeedbackCategoryModel>() {
            @Override
            public void onResponse(Call<FeedbackCategoryModel> call, Response<FeedbackCategoryModel> response) {
                if (response.isSuccessful()){
                    feedbackCategory = response.body().getData();
                    for (FeedbackCategoryModel.Datum datum :feedbackCategory){
                        RadioButton radioButton = (RadioButton) getLayoutInflater().inflate(R.layout.radiobutton,null,false);
                        radioButton.setId(radiogroup.getChildCount());

                        radioButton.setText(datum.getName());
                        radiogroup.addView(radioButton);

                    }

                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<FeedbackCategoryModel> call, Throwable t) {

                progressDialogFragment.dismiss();
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(notificationReceiver,new IntentFilter(NotificationReceiver.ACTION_NOTIFICATION));

    }

//    private void getAllMessages(){
//        Call<FeedBackModel> modelCall = apiInterface.getAllMessages(headermap);
//        modelCall.enqueue(new Callback<FeedBackModel>() {
//            @Override
//            public void onResponse(Call<FeedBackModel> call, Response<FeedBackModel> response) {
//                if (response.isSuccessful()){
//                    chatView.clearMessages();
//                    List<FeedBackModel.Datum> conversationDetails = response.body().getData();
//                    if (conversationDetails.size()!=0){
//                        for(int i = 0; i< conversationDetails.size();i++){
//                            feedbackId = conversationDetails.get(i).getFeedbackId();
//                            List<FeedBackModel.Datum.Message> messagesList = conversationDetails.get(i).getMessages();
//                            for (int pos = 0; pos<messagesList.size();pos++){
//                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
////                            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
//
//                                String inputString = messagesList.get(pos).getTime();
//
//                                Date date = null;
//                                Calendar calendar = Calendar.getInstance();
//                                try {
//                                    date = sdf.parse( inputString);
//
//                                    //Setting the Calendar date and time to the given date and time
//                                  //  calendar.set(1900+date.getYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes());
//
//
//                                    calendar.setTime(date);
//                                } catch (ParseException e) {
//                                    e.printStackTrace();
//                                }
//
//                                System.out.println("in milliseconds: " + calendar.getTime());
//                                if (messagesList.get(pos).getType().equalsIgnoreCase("send")){
//                                    // long time = Long.parseLong(messagesList.get(pos).getMessage().getTime());
//                                    chatView.addMessage(new ChatMessage(messagesList.get(pos).getMessage(),date.getTime(), ChatMessage.Type.SENT));
//
//                                }
//                                else {
//                                    // long time = Long.parseLong(messagesList.get(pos).getMessage().getTime());
//                                    chatView.addMessage(new ChatMessage(messagesList.get(pos).getMessage(), date.getTime(), ChatMessage.Type.RECEIVED));
//
//                                }
//                            }
//
//                        }
//
//                    }
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<FeedBackModel> call, Throwable t) {
//
//            }
//        });
//    }
    private void startConverstaion(final String message){

        Call<JsonObject> jsonObjectCall = apiInterface.startConvo(headermap,message);
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful()){
                    if (response.code() == 200) {


                        JsonObject jsonObject = response.body();
                        Log.d("ASWIN", jsonObject.toString());
                        String s = jsonObject.toString();




//                        getAllMessages();
                        userDetailsSharedPref.saveBoolean("convo",true);





                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });


    }
    private void continueconvo(String message){
        Call<JsonObject> jsonObjectCall = apiInterface.continueConvo(headermap, String.valueOf(feedbackId),message);
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful()){
                    if (response.code() == 200) {


                        JsonObject jsonObject = response.body();
                        Log.d("ASWIN", jsonObject.toString());
                        String s = jsonObject.toString();


                        userDetailsSharedPref.saveBoolean("convo",true);






                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });

    }
    private boolean checkPermissionExternalStorage() {
        int externalStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        return externalStorage == PackageManager.PERMISSION_GRANTED ;
    }

    private void requestPermissionForExternalStorage() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ) {
//            Toast.makeText(this,
//                    R.string.permissions_needed,
//                    Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_EXTERNAL_STORAGE);


        } else {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            // check whether storage permission granted or not.
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do what you want;
                seletFile();
            }
            else {
                requestPermissionForExternalStorage();
            }
        }
    }

    public void seletFile() {
        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
        String[] mimeTypes = {"image/*"};
        chooseFile.setType("*/*");
        chooseFile.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        chooseFile = Intent.createChooser(chooseFile, "Choose a file");
        startActivityForResult(chooseFile, PICKFILE_RESULT_CODE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICKFILE_RESULT_CODE && resultCode == RESULT_OK) {


            Uri uri = data.getData();
            String  filePath= FileUtils.getPath(this,uri);
            // filePath = uri.getPath();
            Log.d("path",filePath);
            ImageView imageView = new ImageView(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(128,
                    128);
            imageView.setLayoutParams(params);
            imageView.setPadding(8,8,8,8);



            imageviewlayout.addView(imageView);
            imageView.setImageURI(uri);
            File file = new File(filePath);
            RequestBody requestFile = RequestBody.create(file, MultipartBody.FORM);



           MultipartBody.Part body = MultipartBody.Part.createFormData("files[]",file.getName(), requestFile);
           parts.add(body);

//
//            RequestBody reqpriorityid =
//                    RequestBody.create(priorityid,MultipartBody.FORM);
//            RequestBody reqcategoryid =
//                    RequestBody.create(categoryid,MultipartBody.FORM);
        }
    }
}
