package com.gegosoft.demoschool.Activity;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gegosoft.demoschool.Fragments.PaidFeesFragment;
import com.gegosoft.demoschool.Fragments.UnPaidFeesFragment;
import com.gegosoft.demoschool.R;
import com.google.android.material.tabs.TabLayout;

public class FeeActivity extends AppCompatActivity {

    Toolbar toolbar;
    TabLayout feetablayout;
    ViewPager feeviewpager;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fee_activity);
        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Fee");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        feetablayout = findViewById(R.id.feetablayout);
        feeviewpager = findViewById(R.id.feeviewpager);
        feetablayout.addTab(feetablayout.newTab().setText("Paid"));
        feetablayout.addTab(feetablayout.newTab().setText("UnPaid"));
        feetablayout.setTabTextColors(getResources().getColor(R.color.black),getResources().getColor(R.color.white));
        feetablayout.setTabGravity(TabLayout.GRAVITY_FILL);



        feeviewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(feetablayout));
        feetablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                feeviewpager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        FeePagerAdapter feePagerAdapter = new FeePagerAdapter(getSupportFragmentManager(), FeeActivity.this, feetablayout.getTabCount());
        feeviewpager.setAdapter(feePagerAdapter);
    }
    public class FeePagerAdapter extends FragmentPagerAdapter {

        private Context myContext;
        int totalTabs;

        public FeePagerAdapter(@NonNull FragmentManager fm, Context context, int totalTabs) {
            super(fm);
            myContext = context;
            this.totalTabs = totalTabs;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    PaidFeesFragment paidFeesFragment = new PaidFeesFragment();

                    return paidFeesFragment;
                case 1:
                    UnPaidFeesFragment unPaidFeesFragment = new UnPaidFeesFragment();

                    return unPaidFeesFragment;

                default:
                    return null;
            }

        }
        @Override
        public int getCount () {
            return totalTabs;
        }
    }
}
