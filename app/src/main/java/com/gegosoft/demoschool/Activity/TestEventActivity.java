package com.gegosoft.demoschool.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.EventsModel;
import com.gegosoft.demoschool.Model.HomeworkModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestEventActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Api api;

    ConcatAdapter concatAdapter;
    EventsAdapter eventsAdapter;
    AllAdapter allAdapter;
    TextView today_date;
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.sample_event_activity);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(this);
        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Events");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
       // today_date = findViewById(R.id.today_date);
        /* starts before 1 month from now */
        final Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.DAY_OF_WEEK, 0);
        String dayOfTheWeek = (String) DateFormat.format("EEEE", startDate); // Thursday
        String day          = (String) DateFormat.format("dd",   startDate); // 20
        String monthString  = (String) DateFormat.format("MMM",  startDate); // Jun
        String monthNumber  = (String) DateFormat.format("MM",   startDate); // 06
        String year         = (String) DateFormat.format("yyyy", startDate); // 2013
        today_date.setText("Today "+dayOfTheWeek+" "+monthString+" "+day);
        /* ends after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 12);

        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(5)
                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                //do something
                String dayOfTheWeek = (String) DateFormat.format("EEEE", date); // Thursday
                String day          = (String) DateFormat.format("dd",   date); // 20
                String monthString  = (String) DateFormat.format("MMM",  date); // Jun
                String monthNumber  = (String) DateFormat.format("MM",   date); // 06
                String year         = (String) DateFormat.format("yyyy", date); // 2013

                event();
                today_date.setText("Today "+dayOfTheWeek+" "+monthString+" "+day);
            }

        });

        if (CheckNetwork.isInternetAvailable(this)){
            event();


        }

    }
    private void event(){

        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        Call<EventsModel> modelCall = api.getUpcomingevents(headermap);
        modelCall.enqueue(new Callback<EventsModel>() {
            @Override
            public void onResponse(Call<EventsModel> call, Response<EventsModel> response) {
                if (response.isSuccessful()){
                    List<EventsModel.Datum> eventsdata = response.body().getData();
                    if (eventsdata.size()!=0){

                        eventsAdapter = new EventsAdapter(eventsdata);


                        getdata();
                    }


                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(TestEventActivity.this);
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<EventsModel> call, Throwable t) {
                AppUtils.APIFails(TestEventActivity.this,t);
                progressDialogFragment.dismiss();
            }
        });



    }

    private void getdata(){

        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        Call<HomeworkModel> examsModelCall = api.getPendingHomeWork(headermap,userDetailsSharedPref.getString("userId"));
        examsModelCall.enqueue(new Callback<HomeworkModel>() {
            @Override
            public void onResponse(Call<HomeworkModel> call, Response<HomeworkModel> response) {
                if (response.isSuccessful()){
                    List<HomeworkModel.Datum> data = response.body().getData();
                    if (data.size()!=0){
                        allAdapter=new AllAdapter(data);
                        recyclerView.setAdapter(allAdapter);
                        concatAdapter = new ConcatAdapter(eventsAdapter,allAdapter);

                        recyclerView.setAdapter(concatAdapter);
                        recyclerView.addItemDecoration(new DividerItemDecoration(TestEventActivity.this,DividerItemDecoration.VERTICAL));

                    }

                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(TestEventActivity.this);
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<HomeworkModel> call, Throwable t) {
                AppUtils.APIFails(TestEventActivity.this,t);
                progressDialogFragment.dismiss();
            }
        });


    }
    public class AllAdapter extends RecyclerView.Adapter<AllAdapter.AllViewHolder>{
        List<HomeworkModel.Datum> homework_all_modelArrayList;
        public AllAdapter(List<HomeworkModel.Datum> homework_all_modelArrayList){
            this.homework_all_modelArrayList=homework_all_modelArrayList;
        }

        @NonNull
        @Override
        public AllAdapter.AllViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.pendinghomework,parent,false);

            return new AllAdapter.AllViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull AllAdapter.AllViewHolder holder, int position) {
            //holder.allheading.setText(homework_all_modelArrayList.get(position).getSubjectName());
            holder.allsubheading.setText(homework_all_modelArrayList.get(position).getDescription());
            holder.date.setText(homework_all_modelArrayList.get(position).getDate());
            if (!homework_all_modelArrayList.get(position).getAttachment().equalsIgnoreCase("")){
                holder.attachmentlayout.setVisibility(View.VISIBLE);
            }
            else {
                holder.attachmentlayout.setVisibility(View.GONE);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.allsubheading.setText(homework_all_modelArrayList.get(position).getDescription());
                    holder.allsubheading.setMaxLines(Integer.MAX_VALUE);
                    holder.allsubheading.setTextIsSelectable(true);



                }
            });
            holder.attachmentlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle extras = new Bundle();

                    extras.putString("url", String.valueOf(homework_all_modelArrayList.get(position).getAttachment()));
                    startActivity(new Intent(TestEventActivity.this, PDFWebViewActivity.class).putExtras(extras));
                }
            });
        }

        @Override
        public int getItemCount() {
            return homework_all_modelArrayList.size();
        }

        public class AllViewHolder extends RecyclerView.ViewHolder {
            TextView allheading,allsubheading;
            TextView date;
            LinearLayout attachmentlayout;

            public AllViewHolder(@NonNull View itemView) {
                super(itemView);
                allheading=itemView.findViewById(R.id.homeworkallheading);
                allsubheading=itemView.findViewById(R.id.homeworkallsubheading);
                date=itemView.findViewById(R.id.date);
                attachmentlayout=itemView.findViewById(R.id.attachmentlayout);
            }
        }
    }

    private class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventsViewHolder>{
        List<EventsModel.Datum> data;

        public EventsAdapter(List<EventsModel.Datum> data) {
            this.data = data;

        }

        @NonNull
        @Override
        public EventsAdapter.EventsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.events_recyclerview_layout,parent,false);

            return new EventsAdapter.EventsViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull EventsAdapter.EventsViewHolder holder, int position) {
            //  holder.locationimage.setImageResource(data.get(position).getLocation());


            SimpleDateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
            Date date = null;
            try {
                date = inputFormat.parse(data.get(position).getStartDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }


            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            String[] monthName = {"January", "February",
                    "March", "April", "May", "June", "July",
                    "August", "September", "October", "November",
                    "December"};
            String monthname = monthName[cal.get(Calendar.MONTH)];
//        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
//        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
//        int dayOfYear = cal.get(Calendar.DAY_OF_YEAR);
//        int month = cal.get(Calendar.MONTH);
//        int year = cal.get(Calendar.YEAR);

//        System.out.println("current date : " + date);
//        System.out.println("dayOfWeek from date in Java 6 : " + dayOfWeek);
//        System.out.println("dayOfMonth from a date in Java 7 : " + dayOfMonth);
//        System.out.println("dayOfYear from a date in Java 1.6 : " + dayOfYear);
//        System.out.println("month from a date in Java 1.7 : " + month);
//        System.out.println("year from date in JDK 6 : " + year);

            SimpleDateFormat f = new SimpleDateFormat("hh:mm a");
            String time = f.format(date.getTime());

            holder.eventsdate.setText(String.valueOf(date.getDate()));
            holder.eventsmon.setText(monthname);
            holder.eventslocationname.setText(data.get(position).getLocation());
            holder.eventstime.setText(time);
            holder.eventsname.setText(data.get(position).getTitle());

            if (data.get(position).getStandardId()!=null){
                if (data.get(position).getStandardId().toString().equalsIgnoreCase(userDetailsSharedPref.getString("standardId"))){
                    holder.itemView.setVisibility(View.VISIBLE);
                }
                else {
                    holder.itemView.setVisibility(View.VISIBLE);
                }
            }

            else {
                holder.itemView.setVisibility(View.VISIBLE);
            }

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class EventsViewHolder extends RecyclerView.ViewHolder {
            ImageView locationimage;
            TextView eventsdate,eventsmon,eventsname,eventstime,eventslocationname;
            public EventsViewHolder(@NonNull View itemView) {
                super(itemView);
                locationimage=itemView.findViewById(R.id.eventslocationimg);
                eventsdate=itemView.findViewById(R.id.eventsdate);
                eventsmon=itemView.findViewById(R.id.eventsmonth);
                eventsname=itemView.findViewById(R.id.eventsname);
                eventslocationname=itemView.findViewById(R.id.eventslocationname);
                eventstime=itemView.findViewById(R.id.eventstime);
            }
        }
    }
    public void removePlanetAdapter(View view) {
        //its removing no need to change
        concatAdapter.removeAdapter(eventsAdapter);
    }

}

