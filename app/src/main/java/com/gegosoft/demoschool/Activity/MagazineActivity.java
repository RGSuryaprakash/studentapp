package com.gegosoft.demoschool.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.MagazineModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MagazineActivity extends AppCompatActivity {
    RecyclerView recyclerview;
    Toolbar toolbar;

    Api api;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.magazineactivity);
        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        recyclerview.setHasFixedSize(true);
        userDetailsSharedPref = userDetailsSharedPref.getInstance(this);
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        if (CheckNetwork.isInternetAvailable(this)){
            getMagazine();
        }
        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Magazine");

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }
    private void getMagazine(){
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        Call<MagazineModel> magazineModelCall  = api.getSchoolMagazine(headermap);
        magazineModelCall.enqueue(new Callback<MagazineModel>() {
            @Override
            public void onResponse(Call<MagazineModel> call, Response<MagazineModel> response) {
                if (response.isSuccessful()){
                    List<MagazineModel.Datum> data = response.body().getData();
                    MagazineAdapter magazineAdapter = new MagazineAdapter(data);
                    recyclerview.setAdapter(magazineAdapter);

                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(MagazineActivity.this);
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<MagazineModel> call, Throwable t) {
                AppUtils.APIFails(MagazineActivity.this,t);
                progressDialogFragment.dismiss();
            }
        });

    }
    private class MagazineAdapter extends RecyclerView.Adapter<MagazineAdapter.MyViewHolder>{
        List<MagazineModel.Datum> data;
        public MagazineAdapter(List<MagazineModel.Datum> data) {
            this.data = data;

        }

        @NonNull
        @Override
        public MagazineAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custommagazinelist,parent,false);
            return new MagazineAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MagazineAdapter.MyViewHolder holder, int position) {
            Glide.with(getBaseContext()).load(data.get(position).getCoverImage())
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.bulletincoverimage);

            if (data.get(position).getType()!=null){
                if (data.get(position).getType().equals("week")){
                    holder.weekormonthtext.setText("Week :"+data.get(position).getWeek());

                }
                else if (data.get(position).getType().equals("month")){
                    holder.weekormonthtext.setText("Month :"+(data.get(position).getMonth()));
                }
            }

            holder.bulletinyear.setText("Year :"+String.valueOf(data.get(position).getYear()));
            holder.bulletinname.setText(data.get(position).getName());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle extras = new Bundle();

                    extras.putString("url", String.valueOf(data.get(position).getPath()));
                    startActivity(new Intent(MagazineActivity.this,PDFWebViewActivity.class).putExtras(extras));
                }
            });
            holder.viewdetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Bundle extras = new Bundle();
//
//                    extras.putString("url", String.valueOf(data.get(position).getPath()));
//                    startActivity(new Intent(BulletinActivity.this,PDFWebViewActivity.class).putExtras(extras));
                }
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView bulletincoverimage;
            TextView bulletinyear,weekormonthtext,bulletinname,viewdetails;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                bulletincoverimage = itemView.findViewById(R.id.bulletincoverimage);
                bulletinyear = itemView.findViewById(R.id.bulletinyear);
                weekormonthtext = itemView.findViewById(R.id.weekormonthtext);
                bulletinname = itemView.findViewById(R.id.bulletinname);
                viewdetails = itemView.findViewById(R.id.viewdetails);
            }
        }
    }
}
