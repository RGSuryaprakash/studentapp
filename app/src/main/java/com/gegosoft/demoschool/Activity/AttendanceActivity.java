package com.gegosoft.demoschool.Activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.AttendanceModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttendanceActivity extends AppCompatActivity {
    Toolbar toolbar;
    Api api;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    CalendarView calendarview;
    RecyclerView recyclerview;
    TextView attendancepercentage,total_leaves;
    ImageView attendanceimage;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendanceactivity);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(this);
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
       // calendarview = findViewById(R.id.calendarview);
        toolbar= findViewById(R.id.toolbar);
        attendanceimage = findViewById(R.id.attendanceimage);
        attendancepercentage = findViewById(R.id.attendancepercentage);
        total_leaves = findViewById(R.id.total_leaves);
        toolbar.setTitle("ATTENDANCE");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        attendanceimage.setColorFilter(getResources().getColor(R.color.colorPrimary));
        recyclerview=findViewById(R.id.recyclerview);
        LinearLayoutManager linearLayoutManager =new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        recyclerview.setLayoutManager(linearLayoutManager);
        recyclerview.setHasFixedSize(true);
        if (CheckNetwork.isInternetAvailable(this)){
            getAttendance();
        }

    }

    private void getAttendance(){
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        Call<AttendanceModel> attendanceModelCall  =api.getAttendanceList(headermap,ApiClient.getClient().baseUrl()+"api/v2/attendance/"+userDetailsSharedPref.getString("userId"));
        attendanceModelCall.enqueue(new Callback<AttendanceModel>() {
            @Override
            public void onResponse(Call<AttendanceModel> call, Response<AttendanceModel> response) {
                if (response.isSuccessful()){
                    List<AttendanceModel.Datum> data = response.body().getData();
                    if (data.size()!=0){

                       calendarview = findViewById(R.id.calendarview);
                        List<EventDay> events = new ArrayList<>();
                        for (int i =0;i<data.size();i++){
                            Calendar calendar = Calendar.getInstance();
                            SimpleDateFormat inputFormat = new SimpleDateFormat("dd MMM yyyy");
                            Date date = null;
                            try {
                                date = inputFormat.parse(data.get(i).getDate());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            calendar.setTime(date);

                            events.add(new EventDay(calendar, R.drawable.ic_absent, Color.parseColor("#ff0000")));
                        }


                        calendarview.setEvents(events);
                        AttendanceAdapter attendanceAdapter = new AttendanceAdapter(data);
                        recyclerview.setAdapter(attendanceAdapter);
                    }
                    total_leaves.setText("Leaves Taken : "+data.size());

                    attendancepercentage.setText(response.body().getAttendancePercentage());

                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(AttendanceActivity.this);
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<AttendanceModel> call, Throwable t) {
                AppUtils.APIFails(AttendanceActivity.this,t);
                progressDialogFragment.dismiss();
            }
        });
    }

    private class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.MyViewHolder>{
        List<AttendanceModel.Datum> data;
        public AttendanceAdapter(List<AttendanceModel.Datum> data) {
            this.data = data;
        }

        @NonNull
        @Override
        public AttendanceAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View  view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customattendancelist,parent,false);
            return  new AttendanceAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull AttendanceAdapter.MyViewHolder holder, int position) {
//            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
//            SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
//            Date date = null;
//            try {
//                date = inputFormat.parse(data.get(position).getDate());
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            String formattedDate = outputFormat.format(date);
            holder.date.setText(data.get(position).getDate());
            holder.reason.setText(""+data.get(position).getReason());
            holder.session.setText(data.get(position).getSession());
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView date,reason,session;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                date = itemView.findViewById(R.id.date);
                reason = itemView.findViewById(R.id.reason);
                session = itemView.findViewById(R.id.session);
            }
        }
    }


}
