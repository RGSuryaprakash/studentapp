package com.gegosoft.demoschool.Activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gegosoft.demoschool.Fragments.AddLeaveFragment;
import com.gegosoft.demoschool.Fragments.MyLeavesFragment;
import com.gegosoft.demoschool.R;
import com.google.android.material.tabs.TabLayout;

public class LeaveActivity extends AppCompatActivity implements MyLeavesFragment.SendMessage {

    Toolbar toolbar;
    TabLayout leavetablayout;
    ViewPager leaveviewPager;
    boolean isEdit ;
    Bundle bundle;
    LeavePagerAdapter leavePagerAdapter;
    public void editLeave(boolean b, Bundle bundle) {
        this.isEdit = b;
        this.bundle = bundle;
        leaveviewPager.setCurrentItem(0);

        leavePagerAdapter.notifyDataSetChanged();

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave);
        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Leaves");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        leavetablayout = findViewById(R.id.leavetablayout);
        leaveviewPager = findViewById(R.id.leaveviewPager);
        leavetablayout.addTab(leavetablayout.newTab().setText("ADD LEAVE"));
        leavetablayout.addTab(leavetablayout.newTab().setText("MY LEAVES"));

        leavetablayout.setTabGravity(TabLayout.GRAVITY_FILL);
        leavetablayout.setTabTextColors(getResources().getColor(R.color.black),getResources().getColor(R.color.white));



        leaveviewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(leavetablayout));
        leavetablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                leaveviewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        leavePagerAdapter = new LeavePagerAdapter(getSupportFragmentManager(), LeaveActivity.this, leavetablayout.getTabCount());
        leaveviewPager.setAdapter(leavePagerAdapter);


    }


    @Override
    public void sendData(Bundle bundle) {
        String tag = "android:switcher:" + R.id.leaveviewPager + ":" + 0;
        leaveviewPager.setCurrentItem(0);
        AddLeaveFragment f = (AddLeaveFragment) getSupportFragmentManager().findFragmentByTag(tag);
        f.displayReceivedData(bundle);


    }

    private class LeavePagerAdapter extends FragmentPagerAdapter {

        private Context myContext;
        int totalTabs;

        public LeavePagerAdapter(@NonNull FragmentManager fm, Context context, int totalTabs) {
            super(fm);
            myContext = context;
            this.totalTabs = totalTabs;
        }



        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    AddLeaveFragment addLeaveFragment = new AddLeaveFragment();
                    if (isEdit)
                    {
                        addLeaveFragment.setArguments(bundle);
                    }

                    return addLeaveFragment;
                case 1:
                    MyLeavesFragment myLeavesFragment = new MyLeavesFragment();

                    return myLeavesFragment;

                default:
                    return null;
            }

        }
        @Override
        public int getCount () {
            return totalTabs;
        }
    }


}
