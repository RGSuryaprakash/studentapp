package com.gegosoft.demoschool.Activity;

import android.content.Context;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Fragments.SlideshowDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.GalleryModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class GalleryActivity extends AppCompatActivity {
    String eventId,title;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Toolbar toolbar;
    Api api;
    RecyclerView gallerylist;
    StaggeredGridLayoutManager gridLayoutManager;
    List<GalleryModel.Datum.Eventgallery> data;
    TextView textnodata;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.galleryactivity);
        Bundle extras = getIntent().getExtras();
        eventId =extras.getString("eventid");
        title = extras.getString("title");
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(this);
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        toolbar=findViewById(R.id.toolbar);

        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        textnodata = findViewById(R.id.textnodata);
        gallerylist=findViewById(R.id.recyclerview);
        gridLayoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        gallerylist.setLayoutManager(gridLayoutManager);
        gallerylist.setHasFixedSize(true);
        gallerylist.setItemAnimator(new DefaultItemAnimator());
        if (CheckNetwork.isInternetAvailable(GalleryActivity.this)){
            getGallery();
        }

        gallerylist.addOnItemTouchListener(new RecyclerTouchListener(this, gallerylist, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", (Serializable) data);
                bundle.putInt("position", position);
                bundle.putString("from","EventGallery");
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                newFragment.setArguments(bundle);
                newFragment.show(ft, "galleryslideshow");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }
    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }
    private void getGallery(){
        Call<GalleryModel> modelCall = api.getGallery(headermap,eventId);
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        modelCall.enqueue(new Callback<GalleryModel>() {
            @Override
            public void onResponse(Call<GalleryModel> call, Response<GalleryModel> response) {
                if (response.isSuccessful()){
                       data = response.body().getData().get(0).getEventgallery();

                    if (data.size()!=0){
                        GalleryAdapter galleryAdapter = new GalleryAdapter(data);
                        gallerylist.setAdapter(galleryAdapter);
                    }
                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(GalleryActivity.this);
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<GalleryModel> call, Throwable t) {
                AppUtils.APIFails(GalleryActivity.this,t);
                progressDialogFragment.dismiss();

            }
        });

    }
    private class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder>{
        List<GalleryModel.Datum.Eventgallery> data;
        public GalleryAdapter(List<GalleryModel.Datum.Eventgallery> data) {
            this.data = data;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customgallery,parent,false);
            return new GalleryAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            holder.galleryname.setVisibility(GONE);
            holder.total.setVisibility(GONE);
            Glide.with(GalleryActivity.this).load(data.get(position).getPath())
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.galleryimage);
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView galleryimage;
            TextView galleryname,total;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                galleryname = (TextView) itemView.findViewById(R.id.galleryname);
                galleryimage = (ImageView) itemView.findViewById(R.id.galleryimage);
                total = (TextView) itemView.findViewById(R.id.total);
            }
        }
    }
}
