package com.gegosoft.demoschool.Activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.EventsModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventsActivity extends AppCompatActivity {
    RecyclerView recyclerView,todayeventsrecyclerview;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Toolbar toolbar;
    Api api;
    TextView textnodata;
    LinearLayout noevents;
    ImageView eventscalendar;
    List<EventsModel.Datum> eventsdata;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events_layout);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(this);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        textnodata=findViewById(R.id.textnodata);
        textnodata.setText("No Upcoming Events");
        noevents=findViewById(R.id.noevents);
        todayeventsrecyclerview=findViewById(R.id.todayeventsrecyclerview);
        eventscalendar =findViewById(R.id.eventscalendar);
        toolbar=findViewById(R.id.eventstoolbar);

        toolbar.setTitle("Events");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        recyclerView=findViewById(R.id.eventsrecyclerview);
        LinearLayoutManager linearLayoutManager =new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);

        todayeventsrecyclerview=findViewById(R.id.todayeventsrecyclerview);
        LinearLayoutManager linearLayoutManager1 =new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        todayeventsrecyclerview.setLayoutManager(linearLayoutManager1);
        if (CheckNetwork.isInternetAvailable(this)){
            getdata();
        }



        eventscalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EventsActivity.this);
                LayoutInflater inflater = getLayoutInflater();

                View dialogLayout = inflater.inflate(R.layout.eventscalendar, null);
                builder.setView(dialogLayout);
//                builder.setTitle("Responded for your prayer");
                CalendarView calendarView = dialogLayout.findViewById(R.id.calendarView);
                List<EventDay> events = new ArrayList<>();
                for (int i =0;i<eventsdata.size();i++){
                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
                    Date date = null;
                    try {
                        date = inputFormat.parse(eventsdata.get(i).getStartDate());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    calendar.setTime(date);
                    events.add(new EventDay(calendar, R.drawable.annouce));
                }


                calendarView.setEvents(events);


                builder.show();


            }
        });

    }

private void getdata(){
    api= ApiClient.getClient().create(Api.class);
    ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
    Bundle bundle = new Bundle();
    bundle.putString("message","Loading");
    progressDialogFragment.setArguments(bundle);
    progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
    Call<EventsModel>  modelCall = api.getUpcomingevents(headermap);
    modelCall.enqueue(new Callback<EventsModel>() {
        @Override
        public void onResponse(Call<EventsModel> call, Response<EventsModel> response) {
            if (response.isSuccessful()){
                eventsdata = response.body().getData();
                if (eventsdata.size()!=0){
                    List<EventsModel.Datum> upcomingevents=new ArrayList<>();
                    List<EventsModel.Datum> todayevents=new ArrayList<>();
                    textnodata.setVisibility(View.GONE);
                    for (int i=0; i<eventsdata.size();i++){
                        Date date = new Date();
                        SimpleDateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
                        Date date1 = null;
                        try {
                            date1 = inputFormat.parse(eventsdata.get(i).getStartDate());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if (date.getDate()==date1.getDate()){
                            todayevents.add(eventsdata.get(i));
                        }
                        else {
                            upcomingevents.add(eventsdata.get(i));
                        }

                    }
                    if (upcomingevents.size()!=0){
                        textnodata.setVisibility(View.GONE);
                        EventsAdapter eventsAdapter = new EventsAdapter(upcomingevents);
                        recyclerView.setAdapter(eventsAdapter);
                    }
                    else {
                        textnodata.setVisibility(View.VISIBLE);
                    }
                    if (todayevents.size()!=0){
                        noevents.setVisibility(View.GONE);
                        EventsAdapter todayeventsAdapter = new EventsAdapter(todayevents);
                        todayeventsrecyclerview.setAdapter(todayeventsAdapter);
                    }



                }
                else {
                    textnodata.setVisibility(View.VISIBLE);
                }

            }
            else {
                if (response.code()==401){
                    AppUtils.SessionExpired(EventsActivity.this);
                }
            }
            progressDialogFragment.dismiss();
        }

        @Override
        public void onFailure(Call<EventsModel> call, Throwable t) {
            AppUtils.APIFails(EventsActivity.this,t);
            progressDialogFragment.dismiss();
        }
    });



}
public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventsViewHolder>{
    List<EventsModel.Datum> data;

    public EventsAdapter(List<EventsModel.Datum> data) {
        this.data = data;

    }

    @NonNull
    @Override
    public EventsAdapter.EventsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.events_recyclerview_layout,parent,false);

        return new EventsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventsAdapter.EventsViewHolder holder, int position) {
      //  holder.locationimage.setImageResource(data.get(position).getLocation());


        SimpleDateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        Date date = null;
        try {
            date = inputFormat.parse(data.get(position).getStartDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        String[] monthName = {"January", "February",
                "March", "April", "May", "June", "July",
                "August", "September", "October", "November",
                "December"};
        String monthname = monthName[cal.get(Calendar.MONTH)];
//        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
//        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
//        int dayOfYear = cal.get(Calendar.DAY_OF_YEAR);
//        int month = cal.get(Calendar.MONTH);
//        int year = cal.get(Calendar.YEAR);

//        System.out.println("current date : " + date);
//        System.out.println("dayOfWeek from date in Java 6 : " + dayOfWeek);
//        System.out.println("dayOfMonth from a date in Java 7 : " + dayOfMonth);
//        System.out.println("dayOfYear from a date in Java 1.6 : " + dayOfYear);
//        System.out.println("month from a date in Java 1.7 : " + month);
//        System.out.println("year from date in JDK 6 : " + year);

        SimpleDateFormat f = new SimpleDateFormat("hh:mm a");
        String time = f.format(date.getTime());

        holder.eventsdate.setText(String.valueOf(date.getDate()));
        holder.eventsmon.setText(monthname);
        holder.eventslocationname.setText(data.get(position).getLocation());
        holder.eventstime.setText(time);
        holder.eventsname.setText(data.get(position).getTitle());

        if (data.get(position).getStandardId()!=null){
            if (data.get(position).getStandardId().toString().equalsIgnoreCase(userDetailsSharedPref.getString("standardId"))){
                holder.itemView.setVisibility(View.VISIBLE);
            }
            else {
                holder.itemView.setVisibility(View.VISIBLE);
            }
        }

        else {
            holder.itemView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class EventsViewHolder extends RecyclerView.ViewHolder {
            ImageView locationimage;
            TextView eventsdate,eventsmon,eventsname,eventstime,eventslocationname;
        public EventsViewHolder(@NonNull View itemView) {
            super(itemView);
            locationimage=itemView.findViewById(R.id.eventslocationimg);
            eventsdate=itemView.findViewById(R.id.eventsdate);
            eventsmon=itemView.findViewById(R.id.eventsmonth);
            eventsname=itemView.findViewById(R.id.eventsname);
            eventslocationname=itemView.findViewById(R.id.eventslocationname);
            eventstime=itemView.findViewById(R.id.eventstime);
        }
    }
}
//    public static String convertSecondsToHMmSs(long seconds) {
//
//        long s = seconds % 60;
//
//        long m = (seconds / 60) % 60;
//
//        long h = (seconds / (60 * 60)) % 24;
//
//        return String.format("%d:%02d:%02d", h,m,s);
//
//    }
}
