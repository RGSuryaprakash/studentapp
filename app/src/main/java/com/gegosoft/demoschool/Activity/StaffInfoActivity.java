package com.gegosoft.demoschool.Activity;

import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.StaffInfoModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StaffInfoActivity extends AppCompatActivity {

RecyclerView recyclerView;
Toolbar toolbar;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    StaffInfoModel.Data staffdata;
    Api api;
    EditText staffnamesearch;
    StaffInfoAdapter staffInfoAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.staffinfo_layout);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(this);
        recyclerView=findViewById(R.id.staffinforecyclerview);
        staffnamesearch = findViewById(R.id.staffnamesearch);
        LinearLayoutManager linearLayoutManager =new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        toolbar=findViewById(R.id.staffinfotoolbar);
        toolbar.setTitle("STAFF INFO");

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        if (CheckNetwork.isInternetAvailable(this)){
            getdata();
        }

        staffnamesearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());

            }
        });

    }

    private void getdata(){
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        Call<StaffInfoModel> staffInfoModelCall = api.getTeachers(headermap,ApiClient.getClient().baseUrl()+"api/v2/teachers/"+userDetailsSharedPref.getString("userId"));
        staffInfoModelCall.enqueue(new Callback<StaffInfoModel>() {
            @Override
            public void onResponse(Call<StaffInfoModel> call, Response<StaffInfoModel> response) {
                if (response.isSuccessful()){
                    staffdata= response.body().getData();
                    staffInfoAdapter=new StaffInfoAdapter(staffdata.getTeachers());
                    recyclerView.setAdapter(staffInfoAdapter);
                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(StaffInfoActivity.this);
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<StaffInfoModel> call, Throwable t) {
                AppUtils.APIFails(StaffInfoActivity.this,t);
                progressDialogFragment.dismiss();
            }
        });

    }
    void filter(String text){
        List<StaffInfoModel.Data.Teacher> temp = new ArrayList();
        for(StaffInfoModel.Data.Teacher d: staffdata.getTeachers()){
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if(d.getTeacherFullname().toLowerCase().contains(text.toLowerCase())||d.getSubjectName().toLowerCase().contains(text.toLowerCase())){
                temp.add(d);
            }
        }
        //update recyclerview
        staffInfoAdapter.Filterlist(temp);
    }

    private class StaffInfoAdapter extends RecyclerView.Adapter<StaffInfoAdapter.StaffInfoViewHolder>{
        List<StaffInfoModel.Data.Teacher> infoModelArrayList;
        public StaffInfoAdapter(List<StaffInfoModel.Data.Teacher> infoModelArrayList){

            this.infoModelArrayList=infoModelArrayList;
        }

        @NonNull
        @Override
        public StaffInfoAdapter.StaffInfoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.staffinfo_recyclerview_layout,parent,false);
            return new StaffInfoViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull StaffInfoViewHolder holder, int position) {
            if(staffdata.getClassTeacher().getFullname() .equalsIgnoreCase(infoModelArrayList.get(position).getTeacherFullname()) ){
                holder.classteacher.setVisibility(View.VISIBLE);

            }
            else {
                holder.classteacher.setVisibility(View.GONE);

            }
        holder.staffname.setText(infoModelArrayList.get(position).getTeacherFullname());
        holder.staffsubject.setText(infoModelArrayList.get(position).getSubjectName());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Glide.with(getBaseContext()).load(infoModelArrayList.get(position).getTeacherAvatar())
                        .thumbnail(0.5f)
                        .error(getDrawable(R.drawable.profile))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.profilepic);      //  holder.staffactive.setText(infoModelArrayList.get(position).getStaffactive());
            }
            else {
                holder.profilepic.setImageResource(R.drawable.profile);
            }
        }

        @Override
        public int getItemCount() {
            return infoModelArrayList.size();
        }

        public class StaffInfoViewHolder extends RecyclerView.ViewHolder {
    ImageView profilepic;
    TextView staffname,staffsubject,staffactive,classteacher;
            public StaffInfoViewHolder(@NonNull View itemView) {
                super(itemView);
                profilepic=itemView.findViewById(R.id.staffprofilepic);
                classteacher=itemView.findViewById(R.id.classteacher);
                staffname=itemView.findViewById(R.id.staffname);
                staffsubject=itemView.findViewById(R.id.staffsubject);
                staffactive=itemView.findViewById(R.id.staffactive);
            }
        }

        public void Filterlist(List<StaffInfoModel.Data.Teacher> list){
            infoModelArrayList = list;
            notifyDataSetChanged();
        }
    }
}
