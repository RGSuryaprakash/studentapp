package com.gegosoft.demoschool.Activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.DisciplineModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisciplineActivity extends AppCompatActivity {
    RecyclerView disciplinerecyclerview;
    Api api;
    Map<String,String> headermap;
    UserDetailsSharedPref userDetailsSharedPref;
    Toolbar toolbar;
    TextView textnodata;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discipilineactivity);
        userDetailsSharedPref = userDetailsSharedPref.getInstance(this);
        disciplinerecyclerview = findViewById(R.id.disciplinerecyclerview);
        disciplinerecyclerview.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        disciplinerecyclerview.setHasFixedSize(true);

        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        if (CheckNetwork.isInternetAvailable(this)){
            getData();
        }

        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Disciplinary Record");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        textnodata=findViewById(R.id.textnodata);


    }
    private void getData(){
        Call<DisciplineModel> disciplineModelCall =api.getDisciplines(headermap,userDetailsSharedPref.getString("userId"));
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        disciplineModelCall.enqueue(new Callback<DisciplineModel>() {
            @Override
            public void onResponse(Call<DisciplineModel> call, Response<DisciplineModel> response) {
                if (response.isSuccessful()){
                    List<DisciplineModel.Datum>  data = response.body().getData();
                    if (data.size()!=0){
                        DisciiplineAdapter disciiplineAdapter = new DisciiplineAdapter(data);
                        disciplinerecyclerview.setAdapter(disciiplineAdapter);
                        textnodata.setVisibility(View.GONE);
                    }
                    else {
                        textnodata.setVisibility(View.VISIBLE);
                    }

                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(DisciplineActivity.this);

                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<DisciplineModel> call, Throwable t) {
                AppUtils.APIFails(DisciplineActivity.this,t);
                progressDialogFragment.dismiss();
            }
        });

    }
    private class DisciiplineAdapter extends RecyclerView.Adapter<DisciiplineAdapter.MyViewHolder>{
        List<DisciplineModel.Datum> data;
        public DisciiplineAdapter(List<DisciplineModel.Datum> data) {
            this.data =data;

        }

        @NonNull
        @Override
        public DisciiplineAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customdiscipline,parent,false);

            return new DisciiplineAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull DisciiplineAdapter.MyViewHolder holder, int position) {
            holder.report.setText("A Report declared by "+data.get(position).getTeacher()+" on " +data.get(position).getIncidentDate());
            holder.reportdetail.setText(data.get(position).getIncidentDetail());

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView report,reportdetail;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                report = itemView.findViewById(R.id.report);
                reportdetail = itemView.findViewById(R.id.reportdetail);
            }
        }
    }
}
