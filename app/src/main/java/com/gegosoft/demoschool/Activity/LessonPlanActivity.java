package com.gegosoft.demoschool.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.LessonPlanAttachmentModel;
import com.gegosoft.demoschool.Model.LessonPlanModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LessonPlanActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView recyclerView;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Api api;
    TextView textnodata;
    ProgressDialogFragment progressDialogFragment;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lessonplan);
        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Lesson Plan");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        recyclerView=findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(this);
        textnodata=findViewById(R.id.textnodata);
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        if (CheckNetwork.isInternetAvailable(this)){
            getdata();
        }
    }

    private void getdata() {
        Call<LessonPlanModel> lessonPlanModelCall = api.getlessonplan(headermap,userDetailsSharedPref.getString("userId"));
         progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        lessonPlanModelCall.enqueue(new Callback<LessonPlanModel>() {
            @Override
            public void onResponse(Call<LessonPlanModel> call, Response<LessonPlanModel> response) {
                if (response.isSuccessful()){
                    List<LessonPlanModel.Datum> data = response.body().getData();
                    if (data!=null&&data.size()!=0){
                    LessonPlanAdapter lessonPlanAdapter = new LessonPlanAdapter(data);
                    recyclerView.setAdapter(lessonPlanAdapter);

                    }
                    else {
                        textnodata.setVisibility(View.VISIBLE);
                    }


                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(LessonPlanActivity.this);
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<LessonPlanModel> call, Throwable t) {
                progressDialogFragment.dismiss();
            }
        });


    }
    private class LessonPlanAdapter extends RecyclerView.Adapter<LessonPlanAdapter.MyViewHolder>{
        List<LessonPlanModel.Datum> data;
        public LessonPlanAdapter(List<LessonPlanModel.Datum> data) {
            this.data = data;
        }

        @NonNull
        @Override
        public LessonPlanAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_lesson_plan,parent,false);
            return new LessonPlanAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull LessonPlanAdapter.MyViewHolder holder, int position) {
            holder.name_textview.setText(data.get(position).getTeacherFullname());
            holder.lesson_title.setText("Subject : "+data.get(position).getSubject());
            holder.unit_name_no.setText("Unit "+data.get(position).getUnitNo()+" "+data.get(position).getUnitName());
            holder.title.setText(data.get(position).getTitle());
            holder.estimated_time.setText(data.get(position).getDuration());
            holder.attachment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewAttachment(data.get(position).getId());
                }
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView name_textview,lesson_title,unit_name_no,title,estimated_time,attachment;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                name_textview = itemView.findViewById(R.id.name_textview);
                lesson_title = itemView.findViewById(R.id.lesson_title);
                unit_name_no = itemView.findViewById(R.id.unit_name_no);
                title = itemView.findViewById(R.id.title);
                estimated_time = itemView.findViewById(R.id.estimated_time);
                attachment = itemView.findViewById(R.id.attachment);

            }
        }
    }

    private void viewAttachment(Integer id) {
        Call<LessonPlanAttachmentModel> modelCall = api.getlessonplanAttachment(headermap,id);
        progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        modelCall.enqueue(new Callback<LessonPlanAttachmentModel>() {
            @Override
            public void onResponse(Call<LessonPlanAttachmentModel> call, Response<LessonPlanAttachmentModel> response) {
                if (response.isSuccessful()){
                    LessonPlanAttachmentModel lessonPlanAttachmentModel = response.body();
                    if (!lessonPlanAttachmentModel.getData().isEmpty()){
                        Bundle extras = new Bundle();
                        extras.putString("url", lessonPlanAttachmentModel.getData());
                        startActivity(new Intent(LessonPlanActivity.this,PDFWebViewActivity.class).putExtras(extras));
                    }

                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<LessonPlanAttachmentModel> call, Throwable t) {
                progressDialogFragment.dismiss();
            }
        });


    }
}
