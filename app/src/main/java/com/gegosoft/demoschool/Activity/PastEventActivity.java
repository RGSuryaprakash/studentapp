package com.gegosoft.demoschool.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.EventsModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PastEventActivity extends AppCompatActivity {
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Toolbar toolbar;
    Api api;
    RecyclerView pasteventslist;
    GridLayoutManager gridLayoutManager;
    TextView textnodata;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pasteventactivity);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(this);
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Events Gallery");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        textnodata = findViewById(R.id.textnodata);
        pasteventslist=findViewById(R.id.recyclerview);
        gridLayoutManager = new GridLayoutManager(this,2);
        pasteventslist.setLayoutManager(gridLayoutManager);
        pasteventslist.setHasFixedSize(true);
        pasteventslist.setItemAnimator(new DefaultItemAnimator());

        if (CheckNetwork.isInternetAvailable(this)){
            getPastEvent();
        }
    }
    private void getPastEvent(){
        Call<EventsModel> pasteventscall = api.getPastevents(headermap);
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        pasteventscall.enqueue(new Callback<EventsModel>() {
            @Override
            public void onResponse(Call<EventsModel> call, Response<EventsModel> response) {
                if (response.isSuccessful()){
                    List<EventsModel.Datum> data = response.body().getData();
                    if (data.size()!=0){
                        PastEventAdapter pastEventAdapter = new PastEventAdapter(data);
                        pasteventslist.setAdapter(pastEventAdapter);
                    }
                    else {
                        textnodata.setVisibility(View.VISIBLE);
                    }



                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(PastEventActivity.this);

                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<EventsModel> call, Throwable t) {
                AppUtils.APIFails(PastEventActivity.this,t);
                progressDialogFragment.dismiss();
            }
        });
    }

    private class PastEventAdapter extends RecyclerView.Adapter<PastEventAdapter.MyViewHolder>{
        List<EventsModel.Datum> data;

        public PastEventAdapter(List<EventsModel.Datum> data) {
            this.data =data;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custompastevents,parent,false);
            return new PastEventAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            Glide.with(PastEventActivity.this).load(data.get(position).getImage())
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.galleryimage);

            holder.galleryname.setText(data.get(position).getTitle());
            holder.total.setText(data.get(position).getPhotos());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(PastEventActivity.this,GalleryActivity.class);
                    intent.putExtra("eventid",data.get(position).getEventId().toString());
                    intent.putExtra("title",data.get(position).getTitle());
                    startActivity(intent);
                }
            });
           // holder.total.setText(data.get(position).getNoofphotos().toString());
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView galleryimage;
            TextView galleryname,total;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                galleryimage = (ImageView) itemView.findViewById(R.id.galleryimage);
                galleryname  = itemView.findViewById(R.id.galleryname);
                total  = itemView.findViewById(R.id.total);
            }
        }
    }

}
