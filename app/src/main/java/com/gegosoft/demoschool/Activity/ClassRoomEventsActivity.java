package com.gegosoft.demoschool.Activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.EventsModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClassRoomEventsActivity extends AppCompatActivity {
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Toolbar toolbar;
    Api api;
    RecyclerView classeventlist;

    TextView textnodata;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.classroom_activity);
        classeventlist = findViewById(R.id.recyclerview);
        api = ApiClient.getClient().create(Api.class);
        LinearLayoutManager linearLayoutManager =new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        classeventlist.setLayoutManager(linearLayoutManager);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(this);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        textnodata=findViewById(R.id.textnodata);
        toolbar=findViewById(R.id.toolbar);

        toolbar.setTitle("Class Events");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (CheckNetwork.isInternetAvailable(this)){
            getClassEvent();
        }
    }
    private void getClassEvent(){
        Call<EventsModel> datumCall =  api.getClassEvent(headermap,userDetailsSharedPref.getString("userId"));
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        datumCall.enqueue(new Callback<EventsModel>() {
            @Override
            public void onResponse(Call<EventsModel> call, Response<EventsModel> response) {
                if (response.isSuccessful()){
                    List<EventsModel.Datum> data = response.body().getData();
                    if (data.size()!=0){
                        ClassEventsAdapter classEventsAdapter = new ClassEventsAdapter(data);
                        classeventlist.setAdapter(classEventsAdapter);
                    }
                    else {
                        textnodata.setVisibility(View.VISIBLE);
                    }

                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(ClassRoomEventsActivity.this);

                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<EventsModel> call, Throwable t) {
                AppUtils.APIFails(ClassRoomEventsActivity.this,t);
                progressDialogFragment.dismiss();

            }
        });


    }
    public class ClassEventsAdapter extends RecyclerView.Adapter<ClassEventsAdapter.EventsViewHolder>{
        List<EventsModel.Datum> data;

        public ClassEventsAdapter(List<EventsModel.Datum> data) {
            this.data = data;

        }

        @NonNull
        @Override
        public ClassRoomEventsActivity.ClassEventsAdapter.EventsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.events_recyclerview_layout,parent,false);

            return new EventsViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ClassRoomEventsActivity.ClassEventsAdapter.EventsViewHolder holder, int position) {
            //  holder.locationimage.setImageResource(data.get(position).getLocation());


            SimpleDateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
            Date date = null;
            try {
                date = inputFormat.parse(data.get(position).getStartDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }


            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            String[] monthName = {"January", "February",
                    "March", "April", "May", "June", "July",
                    "August", "September", "October", "November",
                    "December"};
            String monthname = monthName[cal.get(Calendar.MONTH)];
//        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
//        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
//        int dayOfYear = cal.get(Calendar.DAY_OF_YEAR);
//        int month = cal.get(Calendar.MONTH);
//        int year = cal.get(Calendar.YEAR);

//        System.out.println("current date : " + date);
//        System.out.println("dayOfWeek from date in Java 6 : " + dayOfWeek);
//        System.out.println("dayOfMonth from a date in Java 7 : " + dayOfMonth);
//        System.out.println("dayOfYear from a date in Java 1.6 : " + dayOfYear);
//        System.out.println("month from a date in Java 1.7 : " + month);
//        System.out.println("year from date in JDK 6 : " + year);

            SimpleDateFormat f = new SimpleDateFormat("hh:mm a");
            String time = f.format(date.getTime());

            holder.eventsdate.setText(String.valueOf(date.getDate()));
            holder.eventsmon.setText(monthname);
            holder.eventslocationname.setText(data.get(position).getLocation());
            holder.eventstime.setText(time);
            holder.eventsname.setText(data.get(position).getTitle());
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class EventsViewHolder extends RecyclerView.ViewHolder {
            ImageView locationimage;
            TextView eventsdate,eventsmon,eventsname,eventstime,eventslocationname;
            public EventsViewHolder(@NonNull View itemView) {
                super(itemView);
                locationimage=itemView.findViewById(R.id.eventslocationimg);
                eventsdate=itemView.findViewById(R.id.eventsdate);
                eventsmon=itemView.findViewById(R.id.eventsmonth);
                eventsname=itemView.findViewById(R.id.eventsname);
                eventslocationname=itemView.findViewById(R.id.eventslocationname);
                eventstime=itemView.findViewById(R.id.eventstime);
            }
        }
    }
}
