package com.gegosoft.demoschool.Activity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {
    ImageView back;
    Api api;
    Map<String,String> headermap;
    UserDetailsSharedPref userDetailsSharedPref;
    EditText edtoldpassword,edtnewpassword,edtconfirmpassword;
    String oldpassword,newpassword,confirmpassword;
    Button confirmbtn;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changepassword);
        userDetailsSharedPref = userDetailsSharedPref.getInstance(this);
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        edtoldpassword =findViewById(R.id.edtoldpassword);
        edtnewpassword =findViewById(R.id.edtnewpassword);
        edtconfirmpassword =findViewById(R.id.edtconfirmpassword);
        confirmbtn =findViewById(R.id.confirmbtn);
        confirmbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isInternetAvailable(ChangePasswordActivity.this)){
                    if (validate()) {
                        oldpassword = edtoldpassword.getText().toString();
                        newpassword = edtnewpassword.getText().toString();
                        confirmpassword = edtconfirmpassword.getText().toString();
                        changePassword();
                    }
                }
            }
        });


        back = findViewById(R.id.back);
        back.setColorFilter(Color.BLACK);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    private void changePassword() {

            ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putString("message","Loading");
            progressDialogFragment.setArguments(bundle);
            progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
            Call<JSONObject> jsonObjectCall = api.changepassword(headermap,oldpassword,newpassword,confirmpassword);
            jsonObjectCall.enqueue(new Callback<JSONObject>() {
                @Override
                public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                    if (response.isSuccessful()){

                        try {
                            JSONObject jsonObject = new JSONObject(response.body().toString());
                            String message = jsonObject.getString("message");
                           // Toast.makeText(ChangePasswordActivity.this,message,Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(ChangePasswordActivity.this,"Successfully Changed",Toast.LENGTH_LONG).show();
                        onBackPressed();

                    }
                    else {
                        if (response.code()==401){
                            AppUtils.SessionExpired(ChangePasswordActivity.this);
                        }
                        if (response.code()==422){
                            confirmbtn.setEnabled(true);
//                            submitbtn.setBackgroundColor(getResources().getColor(R.color.notificationscolor));
                            String errorstring = null;
                            try {
                                errorstring = response.errorBody().string();
                                JSONObject jsonObject = new JSONObject(errorstring);
                                String errorObject = jsonObject.getString("errors");
                                JSONObject jsonObject1 = new JSONObject(errorObject);
                                if (jsonObject1.has("newpassword")){
                                    JSONArray jsonArray = jsonObject1.getJSONArray("aadhar_number");
                                    edtnewpassword.setError("Enter Valid Aadhar number");
                                    edtnewpassword.requestFocus();
                                    Toast.makeText(ChangePasswordActivity.this,jsonArray.getString(0),Toast.LENGTH_LONG).show();

                                }
                                else if (jsonObject1.has("oldpassword")){
                                    JSONArray jsonArray = jsonObject1.getJSONArray("oldpassword");
                                    Toast.makeText(ChangePasswordActivity.this,jsonArray.getString(0),Toast.LENGTH_LONG).show();

                                }
                                else if (jsonObject1.has("confirmpassword")){
                                    JSONArray jsonArray = jsonObject1.getJSONArray("confirmpassword");
                                    Toast.makeText(ChangePasswordActivity.this,jsonArray.getString(0),Toast.LENGTH_LONG).show();

                                }


                                Log.d("errorbody",errorObject);

                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }
                    progressDialogFragment.dismiss();
                }

                @Override
                public void onFailure(Call<JSONObject> call, Throwable t) {
                    AppUtils.APIFails(ChangePasswordActivity.this,t);
                    progressDialogFragment.dismiss();
                }
            });

    }

    private boolean validate(){
        if (edtoldpassword.getText().length()==0){
            edtoldpassword.setError("Field cannot be left blank.");
            edtoldpassword.requestFocus();
            return false;
        }

        else if (edtnewpassword.getText().length()==0){
            edtnewpassword.setError("Field cannot be left blank.");
            edtnewpassword.requestFocus();
            return false;
        }
        else if (edtnewpassword.getText().length()<8){
            edtnewpassword.setError("Should be 8 characters.");
            edtnewpassword.requestFocus();
            return false;
        }


        else if (edtconfirmpassword.getText().length()==0){
            edtconfirmpassword.setError("Field cannot be left blank.");
            edtconfirmpassword.requestFocus();
            return false;
        }
        else if (edtnewpassword.getText().length()<8){
            edtconfirmpassword.setError("Should be 8 characters.");
            edtconfirmpassword.requestFocus();
            return false;
        }

        else if (!edtconfirmpassword.getText().toString().equalsIgnoreCase(edtnewpassword.getText().toString())){
            edtnewpassword.setError("Password Should be Same");
            edtnewpassword.requestFocus();
            return false;
        }



        return true;
    }
}
