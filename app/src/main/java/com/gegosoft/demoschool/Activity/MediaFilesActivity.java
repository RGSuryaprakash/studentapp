package com.gegosoft.demoschool.Activity;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gegosoft.demoschool.Fragments.CompletedAssignment;
import com.gegosoft.demoschool.Fragments.MediaAudioFragment;
import com.gegosoft.demoschool.Fragments.MediaImageFragment;
import com.gegosoft.demoschool.Fragments.MediaVideoFragment;
import com.gegosoft.demoschool.Fragments.OngoingAssignmentFragment;
import com.gegosoft.demoschool.R;
import com.google.android.material.tabs.TabLayout;

public class MediaFilesActivity extends AppCompatActivity {
    Toolbar toolbar;
    TabLayout mediatablayout;
    ViewPager mediaviewpager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mediafiles);
        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Media Files");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mediatablayout = findViewById(R.id.assignmenttablayout);
        mediaviewpager = findViewById(R.id.assignmentviewpager);
        mediatablayout.addTab(mediatablayout.newTab().setText("Images"));
        mediatablayout.addTab(mediatablayout.newTab().setText("Audios"));
        mediatablayout.addTab(mediatablayout.newTab().setText("Videos"));
//        leavetablayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mediatablayout.setTabTextColors(getResources().getColor(R.color.black),getResources().getColor(R.color.white));
        mediatablayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mediaviewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mediatablayout));
        mediatablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                mediaviewpager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        MediaPagerAdapter mediaPagerAdapter = new MediaPagerAdapter(getSupportFragmentManager(), MediaFilesActivity.this, mediatablayout.getTabCount());
        mediaviewpager.setAdapter(mediaPagerAdapter);
    }
    public class MediaPagerAdapter extends FragmentPagerAdapter {

        private Context myContext;
        int totalTabs;

        public MediaPagerAdapter(@NonNull FragmentManager fm, Context context, int totalTabs) {
            super(fm);
            myContext = context;
            this.totalTabs = totalTabs;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    MediaImageFragment mediaImageFragment = new MediaImageFragment();
                    return mediaImageFragment;
                case 1:
                    MediaAudioFragment mediaAudioFragment = new MediaAudioFragment();
                    return mediaAudioFragment;
                case 2:
                    MediaVideoFragment mediaVideoFragment = new MediaVideoFragment();
                    return mediaVideoFragment;
                default:
                    return null;
            }

        }
        @Override
        public int getCount () {
            return totalTabs;
        }
    }
}
