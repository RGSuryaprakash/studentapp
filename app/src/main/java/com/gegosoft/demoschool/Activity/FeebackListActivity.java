package com.gegosoft.demoschool.Activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.FeedBackModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeebackListActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView recyclerView;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String, String> headermap;
    Api api;
    TextView textnodata;
    ProgressDialogFragment progressDialogFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_feedback_list);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("My Feedbacks");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(this);
        textnodata = findViewById(R.id.textnodata);
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization", "Bearer " + userDetailsSharedPref.getString("token"));
        headermap.put("Accept", "application/json");
        if (CheckNetwork.isInternetAvailable(this)) {
            getFeedbackList();
        }

    }

    private void getFeedbackList() {
        Call<FeedBackModel> feedBackModelCall = api.getFeedbacklist(headermap);
        progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message", "Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(), "loading screen");
        feedBackModelCall.enqueue(new Callback<FeedBackModel>() {
            @Override
            public void onResponse(Call<FeedBackModel> call, Response<FeedBackModel> response) {
                if (response.isSuccessful()) {
                    List<FeedBackModel.Datum> data = response.body().getData();
                    if (data != null && data.size() != 0) {
                        FeedbackAdapter feedbackAdapter = new FeedbackAdapter(data);
                        recyclerView.setAdapter(feedbackAdapter);
                    } else {
                        if (response.code() == 401) {
                            AppUtils.SessionExpired(FeebackListActivity.this);
                        }
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<FeedBackModel> call, Throwable t) {

            }
        });
    }

    private class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.MyViewHolder> {
        List<FeedBackModel.Datum> data;

        public FeedbackAdapter(List<FeedBackModel.Datum> data) {
            this.data = data;

        }

        @NonNull
        @Override
        public FeedbackAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_feedback_list, parent, false);
            return new FeedbackAdapter.MyViewHolder(view);
        }


        @Override
        public void onBindViewHolder(@NonNull FeedbackAdapter.MyViewHolder holder, int position) {
            holder.txt_category.setText(data.get(position).getCategory());
            //  holder.status.setText("Status : "+data.get(position).getStatus());
            holder.txt_description.setText(data.get(position).getMessages().get(0).getMessage());
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SimpleDateFormat formatter = new SimpleDateFormat("MMM   dd hh:mm a");

        //    data.get(position).getMessages().get(1).getMessage().i
//                holder.textdescreply.setText("");
//            } else {
            holder.textdescreply.setText(data.get(position).getMessages().get(1).getMessage());
//            }


//            if (data.get(position).getMessages().size()>0)
//
//            {
//                holder.textdescreply.setVisibility(View.VISIBLE);
//                holder.textdescreply.setText(data.get(position).getMessages().get(position).getMessage());
//
//            }
//            holder.textdescreply.setVisibility(View.GONE);


//            if (data.get(position).getMessages().get(position).getMessage().isEmpty() &&
//            data.get(position).getMessages().get(position).getMessage() == null)
//            {
//                holder.textdescreply.setVisibility(View.GONE);
//            }
//
//            holder.textdescreply.setVisibility(View.VISIBLE);
//            holder.textdescreply.setText(data.get(position).getMessages().get(1).getMessage());
            //   holder.textdescreply.setText(data.get(position).getMessages().get(2).getMessage());
            //  holder.textdescreply.setText(data.get(position).getMessages().get(3).getMessage());


//            if (data.get(position).getMessages().get(position).getType().contains("receive"))
//            {
//                holder.textdescreply.setText(data.get(position).getMessages().get(1).getMessage());
//            }
//            else {
//                holder.textdescreply.setVisibility(View.GONE);
//            }


            try {
                Date date = dateFormat.parse(data.get(position).getMessages().get(0).getTime());
                String strDate = formatter.format(date);
                holder.txt_date.setText(strDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (data.get(position).getMessages().get(0).getFile() != null && data.get(position).getMessages().get(0).getFile().size() != 0) {
                List<String> imageurls = data.get(position).getMessages().get(0).getFile();

                for (String url : imageurls) {
                    ImageView imageView = new ImageView(getBaseContext());
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(128,
                            128);
                    imageView.setLayoutParams(params);
                    imageView.setPadding(8, 8, 8, 8);
                    Glide.with(getBaseContext()).load(url).into(imageView);


                    holder.imageviewlayout.addView(imageView);

                }

            }
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView txt_category, txt_date, status, txt_description;
            LinearLayout imageviewlayout;
            TextView dateandtimereply, textdescreply;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                txt_category = itemView.findViewById(R.id.txt_category);
                txt_description = itemView.findViewById(R.id.txt_description);
                txt_date = itemView.findViewById(R.id.txt_date);
                status = itemView.findViewById(R.id.status);
                dateandtimereply = itemView.findViewById(R.id.dateandtime_reply);
                textdescreply = itemView.findViewById(R.id.text_desc_reply);
                //    imageviewlayout = itemView.findViewById(R.id.imageviewlayout);
            }
        }
    }
}
