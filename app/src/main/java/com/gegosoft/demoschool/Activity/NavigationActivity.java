package com.gegosoft.demoschool.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.ExamsActivity;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.HomeWorkActivity;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.LogOutModel;
import com.gegosoft.demoschool.Model.NavigationModel;
import com.gegosoft.demoschool.MyProfileActivity;
import com.gegosoft.demoschool.NotificationActivity;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NavigationActivity extends AppCompatActivity {
    RecyclerView navigationlistview;
    Toolbar toolbar;
    Api api;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigationview);
        userDetailsSharedPref = userDetailsSharedPref.getInstance(this);

        navigationlistview = (RecyclerView) findViewById(R.id.recyclerview);

        toolbar= findViewById(R.id.toolbar);
        toolbar.setTitle("DASHBOARD");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //Data
        ArrayList<NavigationModel> nav_item = new ArrayList<>();
        nav_item.add(new NavigationModel("Attendance",R.drawable.ic_attendance));
        nav_item.add(new NavigationModel("Message And Notification",R.drawable.ic_notification));
        nav_item.add(new NavigationModel("Home Work",R.drawable.ic_homework));
        nav_item.add(new NavigationModel("Profile",R.drawable.profile));
        nav_item.add(new NavigationModel("Events",R.drawable.ic_events));
        nav_item.add(new NavigationModel("Exams",R.drawable.ic_alert));
        nav_item.add(new NavigationModel("Time Table",R.drawable.ic_timetable));
        nav_item.add(new NavigationModel("Magazine",R.drawable.ic_magazine));
        nav_item.add(new NavigationModel("Sign Out",R.drawable.ic_logout));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        navigationlistview.setLayoutManager(layoutManager);
        NavigationAdapter recyclerAdapter = new NavigationAdapter(NavigationActivity.this, nav_item);
        navigationlistview.setAdapter(recyclerAdapter);
        navigationlistview.setHasFixedSize(false);
    }
    private class NavigationAdapter extends RecyclerView.Adapter<NavigationAdapter.MyViewHolder>{
        ArrayList<NavigationModel> nav_item;
        public NavigationAdapter(NavigationActivity mainActivity, ArrayList<NavigationModel> nav_item) {
            this.nav_item = nav_item;
        }

        @NonNull
        @Override
        public NavigationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.navigationitemlayout,parent,false);
            return new NavigationAdapter.MyViewHolder(view);}

        @Override
        public void onBindViewHolder(@NonNull NavigationAdapter.MyViewHolder holder, final int position) {
            holder.navitemname.setText(nav_item.get(position).getTitle());
            holder.navitemicon.setColorFilter(getResources().getColor(R.color.colorPrimary));
            Glide.with(getBaseContext()).load(nav_item.get(position).getDrawable()).into(holder.navitemicon);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (position==0){
                        startActivity(new Intent(getBaseContext(), AttendanceActivity.class));
                    }
                    else if (position==1){
                        userDetailsSharedPref.saveBoolean("isPrivateMessage",false);
                        startActivity(new Intent(getBaseContext(), NotificationActivity.class));
                    }
                    else if (position==2){
                        userDetailsSharedPref.saveBoolean("isHomeWork",false);
                        startActivity(new Intent(getBaseContext(), HomeWorkActivity.class));
                    }

                    else if (position==3){
                        startActivity(new Intent(getBaseContext(), MyProfileActivity.class));
                    }
                    else if (position==4){
                        startActivity(new Intent(getBaseContext(),EventsActivity.class));
                    }
                    else if (position==5){
                        startActivity(new Intent(getBaseContext(), ExamsActivity.class));
                    }
                    else if (position==6){
                        startActivity(new Intent(getBaseContext(),TimeTableActivity.class));
                    }

                    else if (position==7){
                        startActivity(new Intent(getBaseContext(),MagazineActivity.class));
                    }
                    else if (position==8){
                        if (CheckNetwork.isInternetAvailable(NavigationActivity.this)){
                            signOut();
                        }
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return nav_item.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView navitemicon;
            TextView navitemname;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                navitemname =  itemView.findViewById(R.id.navitemname);
                navitemicon =  itemView.findViewById(R.id.navitemicon);
            }
        }
    }

    private void signOut(){
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        Call<LogOutModel> logOutModelCall = api.logOut(headermap);
        logOutModelCall.enqueue(new Callback<LogOutModel>() {
            @Override
            public void onResponse(Call<LogOutModel> call, Response<LogOutModel> response) {
                if(response.isSuccessful()){
                    new  AsyncTask<Void,Void,Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            try
                            {
                                FirebaseInstanceId.getInstance().deleteInstanceId();
                            } catch (IOException e)
                            {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void result) {
                            // Call your Activity where you want to land after log out
                           // userDetailsSharedPref.saveString("login",null);
                            userDetailsSharedPref.logout();
                            userDetailsSharedPref.clearAllEntries();
//                            profileDataBase.deleteUserTable();
//                            eventDataBase.deleteEventsTable();
                            Intent i = new Intent(NavigationActivity.this, StartUpActivity.class);
// set the new task and clear flags
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                    }.execute();

                }
                else {
                    userDetailsSharedPref.logout();
                    userDetailsSharedPref.clearAllEntries();
//                    profileDataBase.deleteUserTable();
//                    eventDataBase.deleteEventsTable();
                    Intent i = new Intent(NavigationActivity.this, StartUpActivity.class);
// set the new task and clear flags
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();

                }
                }


            @Override
            public void onFailure(Call<LogOutModel> call, Throwable t) {
                AppUtils.APIFails(NavigationActivity.this,t);
            }
        });
    }

}
