package com.gegosoft.demoschool.Services;

import android.annotation.SuppressLint;

import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import java.io.IOException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;

public class SSLCertificates {
    @SuppressLint("TrulyRandom")
    public static void handleSSLHandshake() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    for( int i = 0; i < certs.length; i++) {
                        System.out.println(" Cert " + certs[i].getSigAlgName() + " Type " + certs[i].getType());
                    }
                }
            }};
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            //sc.init(null, trustAllCerts, SecureRandom.getInstance("SHA256WithRSA"));
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    if(arg0.equals("nkkr.gegok12.com")) {
                        return true;
                    }
//                    else if(arg0.equals("preprod.xtrachef.com")) {
//                        return true;
//                    } else if(arg0.equals("int1.xtrachef.com")) {
//                        return true;
//                    } else if(arg0.equals("preprodapi.xtrachef.com")) {
//                        return true;
//                    } else if(arg0.equals("prodapi.xtrachef.com")) {
//                        return true;
//                    } else if(arg0.equals("demo.xtrachef.com")) {
//                        return true;
//                    } else if(arg0.equals("demoapi.xtrachef.com")) {
//                        return true;
//                    }
                    else {
                        return false;
                    }
                }
            });
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }
    public static HttpClientBuilder handleSSLHandshake(HttpClientBuilder builder) {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            builder.setSslcontext(sc);
            X509HostnameVerifier hostnameVerifier = new X509HostnameVerifier() {
                @Override
                public void verify(String s, SSLSocket sslSocket) throws IOException {
                }
                @Override
                public void verify(String s, X509Certificate x509Certificate) throws SSLException {
                }
                @Override
                public void verify(String s, String[] strings, String[] strings1) throws SSLException {
                }
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    if(hostname.equals("nkkr.gegok12.com")) {
                        return true;
                    }
//                    else if(hostname.equals("preprod.xtrachef.com")) {
//                        return true;
//                    } else if(hostname.equals("int1.xtrachef.com")) {
//                        return true;
//                    } else if(hostname.equals("preprodapi.xtrachef.com")) {
//                        return true;
//                    } else if(hostname.equals("prodapi.xtrachef.com")) {
//                        return true;
//                    } else if(hostname.equals("demo.xtrachef.com")) {
//                        return true;
//                    } else if(hostname.equals("demoapi.xtrachef.com")) {
//                        return true;
//                    }
                    else {
                        return false;
                    }
                }
            };
            SSLConnectionSocketFactory factory = new SSLConnectionSocketFactory(sc, hostnameVerifier);
            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("https", factory)
                    .build();
            PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            builder.setConnectionManager(connectionManager);
        } catch (Exception ignored) {
        }
        return builder;
    }
    public static OkHttpClient getBuilder() {
        OkHttpClient.Builder builder = null;
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            builder = new OkHttpClient.Builder().sslSocketFactory(sc.getSocketFactory(), (X509TrustManager) trustAllCerts[0]).connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS);
            X509HostnameVerifier hostnameVerifier = new X509HostnameVerifier() {
                @Override
                public void verify(String s, SSLSocket sslSocket) throws IOException {
                }
                @Override
                public void verify(String s, X509Certificate x509Certificate) throws SSLException {
                }
                @Override
                public void verify(String s, String[] strings, String[] strings1) throws SSLException {
                }
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    if(hostname.equals("nkkr.gegok12.com")) {
                        return true;
                    }
//                    else if(hostname.equals("preprod.xtrachef.com")) {
//                        return true;
//                    } else if(hostname.equals("int1.xtrachef.com")) {
//                        return true;
//                    } else if(hostname.equals("preprodapi.xtrachef.com")) {
//                        return true;
//                    } else if(hostname.equals("prodapi.xtrachef.com")) {
//                        return true;
//                    } else if(hostname.equals("demo.xtrachef.com")) {
//                        return true;
//                    } else if(hostname.equals("demoapi.xtrachef.com")) {
//                        return true;
//                    }
                    else {
                        return false;
                    }
                }
            };
            builder.hostnameVerifier(hostnameVerifier);
        } catch (Exception ignored) {
        }
        OkHttpClient client = null;
        if(builder != null) {
            client = builder.build();
        } else {
            client = (new OkHttpClient.Builder()).build();
        }
        return client;
    }
}
