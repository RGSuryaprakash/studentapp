package com.gegosoft.demoschool.Interface;

import com.gegosoft.demoschool.Model.AddLeaveModel;
import com.gegosoft.demoschool.Model.AllNotificationModel;
import com.gegosoft.demoschool.Model.AppliedLeaveListModel;
import com.gegosoft.demoschool.Model.AssignmentModel;
import com.gegosoft.demoschool.Model.AttendanceModel;
import com.gegosoft.demoschool.Model.ClassNoticeBoardModel;
import com.gegosoft.demoschool.Model.DeleteLeaveModel;
import com.gegosoft.demoschool.Model.FeedBackModel;
import com.gegosoft.demoschool.Model.DisciplineModel;
import com.gegosoft.demoschool.Model.ExamResultMarksModel;
import com.gegosoft.demoschool.Model.ExamsModel;
import com.gegosoft.demoschool.Model.FeedbackCategoryModel;
import com.gegosoft.demoschool.Model.LessonPlanAttachmentModel;
import com.gegosoft.demoschool.Model.LessonPlanModel;
import com.gegosoft.demoschool.Model.MediaAudioModel;
import com.gegosoft.demoschool.Model.MediaImageModel;
import com.gegosoft.demoschool.Model.MediaVideoModel;
import com.gegosoft.demoschool.Model.PaidFeesModel;
import com.gegosoft.demoschool.Model.GalleryModel;
import com.gegosoft.demoschool.Model.HomeworkModel;
import com.gegosoft.demoschool.Model.JoinRoomModel;
import com.gegosoft.demoschool.Model.LogOutModel;
import com.gegosoft.demoschool.Model.LoginModel;
import com.gegosoft.demoschool.Model.MagazineModel;
import com.gegosoft.demoschool.Model.MessageModel;
import com.gegosoft.demoschool.Model.MyChildrenModel;
import com.gegosoft.demoschool.Model.MyChildrenDetailsModel;
import com.gegosoft.demoschool.Model.ReasonListModel;
import com.gegosoft.demoschool.Model.RoomListModel;
import com.gegosoft.demoschool.Model.SchoolEventsmodel;
import com.gegosoft.demoschool.Model.SchoolInfoModel;
import com.gegosoft.demoschool.Model.SchoolNoticeBoardModel;
import com.gegosoft.demoschool.Model.StaffInfoModel;
import com.gegosoft.demoschool.Model.TimeTableModel;
import com.gegosoft.demoschool.Model.EventsModel;
import com.gegosoft.demoschool.Model.UnPaidFeesModel;
import com.gegosoft.demoschool.Model.UpdateFCMTokenModel;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface Api {
    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("api/parent/login")
    Call<LoginModel> login(
            @Field("email")String email,
            @Field("password")String password,
            @Field("device_name")String platform_token);

    @POST("api/v2/logout")
    Call<LogOutModel> logOut(@HeaderMap Map<String,String> headerMap);

    @FormUrlEncoded
    @POST("update/platformtoken")
    Call<UpdateFCMTokenModel> updatefcmtoken(@HeaderMap Map<String,String> headerMap, @Field("platform_token") String token);

    @GET("api/v2/my-children")
    Call<MyChildrenModel> getMychildren(@HeaderMap Map<String,String> stringStringMap);

//    @GET
//    Observable<MyChildrenDetails> getMychildrenDetails(@HeaderMap Map<String,String> stringStringMap,@Url String url);

    @GET("api/v2/school/details")
    Call<SchoolInfoModel> getSchoolDetails(@HeaderMap Map<String, String> stringStringMap);

    @GET("api/v2/messages")
    Call<MessageModel> getMessageList(@HeaderMap Map<String,String> stringStringMap);

    @GET("api/v2/notifications/{studentid}")
    Call<AllNotificationModel> getallnotification(@HeaderMap Map<String,String> stringStringMap,@Path("studentid") String studentid);

    @GET("api/v2/my-school/notices")
    Call<SchoolNoticeBoardModel> getSchoolNotice(@HeaderMap Map<String,String> stringStringMap);

    @GET("api/v2/my-events/school")
    Observable<SchoolEventsmodel> getSchoolEvents(@HeaderMap Map<String,String> stringStringMap);

    @GET("api/v2/my-events/upcoming")
    Call<EventsModel> getUpcomingevents(@HeaderMap Map<String,String> stringStringMap);

    @GET("api/v2/my-events/past")
    Call<EventsModel> getPastevents(@HeaderMap Map<String,String> stringStringMap);

    @GET("api/v2/my-events/{userid}/class")
    Call<EventsModel> getClassEvent(@HeaderMap Map<String,String> stringStringMap, @Path("userid") String userid);

    @GET("api/v2/my-events/gallery/show/{id}")
    Call<GalleryModel> getGallery(@HeaderMap Map<String,String> stringStringMap, @Path("id") String id);

    @GET
    Call<TimeTableModel> getTimeTable(@HeaderMap Map<String,String> stringStringMap,@Url String url);

    @GET
    Call<StaffInfoModel> getTeachers(@HeaderMap Map<String,String> stringStringMap,@Url String url);

    @GET
    Call<ExamsModel> getExamModel(@HeaderMap Map<String,String> stringStringMap, @Url String url);

    @GET
    Observable<ExamsModel> getPastExamModel(@HeaderMap Map<String,String> stringStringMap, @Url String url);

    @GET("api/v2/marks/{userid}/{examid}")
    Observable<ExamResultMarksModel> getExamMarksList(@HeaderMap Map<String,String> stringStringMap, @Path("userid") String userid,@Path("examid") String examid);

    @GET
    Call<AttendanceModel> getAttendanceList(@HeaderMap Map<String,String> stringStringMap, @Url String url);

    @GET("api/v2/my-school/magazine")
    Call<MagazineModel> getSchoolMagazine(@HeaderMap Map<String,String> stringStringMap);

    @GET("api/v2/homeworks/pending/{userid}")
    Call<HomeworkModel> getPendingHomeWork(@HeaderMap Map<String,String> stringStringMap, @Path("userid") String userid);


    @GET("api/v2/homeworks/finished/{userid}")
    Call<HomeworkModel> getFinishedHomeWork(@HeaderMap Map<String,String> stringStringMap, @Path("userid") String userid);

    @GET("api/v2/my-children/{userid}/details")
    Call<MyChildrenDetailsModel> getChildrendetails(@HeaderMap Map<String,String> stringStringMap, @Path("userid") String userid);

    @GET("api/v2/assignments/{userid}")
    Call<AssignmentModel> getOngoingAssignment(@HeaderMap Map<String,String> stringStringMap, @Path("userid") String userid);

    @GET("api/v2/assignments/completed/{userid}")
    Call<AssignmentModel> getCompletedAssignment(@HeaderMap Map<String,String> stringStringMap, @Path("userid") String userid);

    @POST("api/v2/message/read/{id}")
    Call<JSONObject> updatereadstatus(@HeaderMap Map<String,String> headerMap, @Path("id") String messageid);

    @GET("api/v2/disciplines/{userid}")
    Call<DisciplineModel> getDisciplines(@HeaderMap Map<String,String> stringStringMap, @Path("userid") String userid);

    @GET("api/v2/feedbacks")
    Call<FeedBackModel> getFeedbacklist(@HeaderMap Map<String,String> headerMap);

    @GET("api/feedbackCategory/list")
    Call<FeedbackCategoryModel> getFeedbackCategorylist();

    @FormUrlEncoded
    @POST("api/v2/feedback/send")
    Call<JsonObject> startConvo(@HeaderMap Map<String,String> headerMap,@Field("message")String message);

    @FormUrlEncoded
    @POST("api/v2/feedback/save/{feedback_id}")
    Call<JsonObject> continueConvo(@HeaderMap Map<String,String> headerMap,@Path("feedback_id")String feedback_id, @Field("message")String message);

    @FormUrlEncoded
    @POST("api/v2/password/change")
    Call<JSONObject> changepassword(@HeaderMap Map<String, String> headermap,@Field("oldpassword") String oldpassword,@Field("newpassword") String newpassword,@Field("confirmpassword") String confirmpassword);

    @GET("api/v2/video-conference/{student_id}")
    Call<RoomListModel> getListofRooms(@HeaderMap Map<String,String> headerMap, @Path("student_id") String student_id);

    @GET("api/v2/video-conference/{slug}/{student_id}")
    Call<JoinRoomModel> getTokentoJoinRoom(@HeaderMap Map<String,String> headerMap, @Path("slug") String slug, @Path("student_id") String student_id);

    @GET("api/v2/fees/paid/{userid}")
    Call<PaidFeesModel> getPaidFeeList(@HeaderMap Map<String,String> stringStringMap, @Path("userid") String userid);

    @GET("api/v2/fees/unpaid/{userid}")
    Call<UnPaidFeesModel> getUnpaidFeeList(@HeaderMap Map<String,String> stringStringMap, @Path("userid") String userid);

    @GET("api/v2/lessonplan/{userid}")
    Call<LessonPlanModel> getlessonplan(@HeaderMap Map<String,String> stringStringMap, @Path("userid") String userid);

    @GET("api/v2/lessonplan/print/{lesson_id}")
    Call<LessonPlanAttachmentModel> getlessonplanAttachment(@HeaderMap Map<String,String> stringStringMap, @Path("lesson_id") Integer lesson_id);

    @GET("api/v2/leave/list")
    Call<ReasonListModel> getReason(@HeaderMap Map<String, String> headermap);

    @FormUrlEncoded
    @POST("api/v2/leave/add/{student_id}")
    Call<AddLeaveModel> AddLeave(@HeaderMap Map<String, String> headermap, @Field("from_date") String fromdate,@Field("to_date") String todate, @Field("reason_id")String reasonid,@Field("remarks") String remarks, @Path("student_id") String student_id);

    @FormUrlEncoded
    @POST("api/v2/leave/edit/{leave_id}")
    Call<ResponseBody> updateleave(@HeaderMap Map<String, String> headermap, @Field("from_date") String fromdate,@Field("to_date") String todate, @Field("reason_id")String reasonid,@Field("remarks") String remarks, @Path("leave_id") Integer leave_id);

    @GET("api/v2/leaves/{student_id}")
    Call<AppliedLeaveListModel> getAppliedLeaveListModelCall(@HeaderMap Map<String, String> headermap,  @Path("student_id")String userId);

    @GET("api/v2/leave/delete/{leave_id}")
    Call<DeleteLeaveModel> getdDeleteLeaveModelCall(@HeaderMap Map<String, String> headermap, @Path("leave_id") int leaveid);

    @FormUrlEncoded
    @POST("api/v2/feedback/send/{student_id}")
    Call<ResponseBody> sendFeedback(@HeaderMap Map<String, String> headermap, @Field("message") String message,@Path ("student_id") String student_id);

    @GET("api/v2/notices/{student_id}")
    Call<ClassNoticeBoardModel> getClassNotice(@HeaderMap Map<String,String> stringStringMap, @Path("student_id") String student_id);

    @GET("api/v2/media-files/audio/{student_id}")
    Call<MediaAudioModel> getmediaaudio(@HeaderMap Map<String, String> headermap, @Path("student_id") String student_id);

    @GET("api/v2/media-files/image/{student_id}")
    Call<MediaImageModel> getmediaimage(@HeaderMap Map<String,String> stringStringMap, @Path("student_id") String student_id);


    @GET("api/v2/media-files/video/{student_id}")
    Call<MediaVideoModel> getmediavideo(@HeaderMap Map<String,String> stringStringMap, @Path("student_id") String student_id);

    @Multipart
    @POST("api/v2/homework/submit/{home_work_id}/{student_id}")
    Call<ResponseBody> uploadHomeWorkFiles(@HeaderMap Map<String,String> stringStringMap,
                                           @Path("home_work_id") String home_work_id,
                                           @Path("student_id") String student_id,
                                           @Part List<MultipartBody.Part> file);

    @GET("api/v2/homework/delete/{studentHomeworkId}/{student_id}")
    Call<ResponseBody> deleteHomeworkfiles(@HeaderMap Map<String, String> headermap, @Path("studentHomeworkId")Integer studentHomeworkId,@Path("student_id") String student_id);


    @Multipart
    @POST("api/v2/assignment/submit/{assignment_id}/{student_id}")
    Call<ResponseBody> addassignmentbystudent(@HeaderMap Map<String,String> stringStringMap,
                                              @Path("assignment_id") String assignment_id,
                                              @Path("student_id") String student_id,
                                              @Part MultipartBody.Part file);


    @GET("api/v2/assignment/delete/{assignment_id}/{student_id}}")
    Call<ResponseBody> deleteassignmentbystudent(@HeaderMap Map<String, String> headermap, @Path("assignment_id")Integer assignment_id,@Path("student_id") String student_id);



}
