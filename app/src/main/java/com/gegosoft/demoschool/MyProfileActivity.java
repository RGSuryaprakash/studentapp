package com.gegosoft.demoschool;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.MyChildrenDetailsModel;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyProfileActivity extends AppCompatActivity {
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Api api;
    Toolbar toolbar;
    CircleImageView profilepic;
    TextView myprofilename,rollno,phonenum,classname,address,bloodgroup,dateofbirth;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myprofilelayout);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(this);
        profilepic = findViewById(R.id.myprofilepic);
        myprofilename = findViewById(R.id.myprofilename);
        rollno = findViewById(R.id.rollno);
        phonenum = findViewById(R.id.phonenum);
        classname = findViewById(R.id.classname);
        address = findViewById(R.id.address);
        bloodgroup = findViewById(R.id.bloodgroup);
        dateofbirth = findViewById(R.id.dateofbirth);

        toolbar=findViewById(R.id.myprofiletoolbar);
        toolbar.setTitle("My Profile");
        toolbar.setTitleTextColor(Color.WHITE);


        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        if (CheckNetwork.isInternetAvailable(this)){
            getdata();
        }


    }
    private void getdata(){
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        Call<MyChildrenDetailsModel> profileModelCall = api.getChildrendetails(headermap,userDetailsSharedPref.getString("userId"));
        profileModelCall.enqueue(new Callback<MyChildrenDetailsModel>() {
            @Override
            public void onResponse(Call<MyChildrenDetailsModel> call, Response<MyChildrenDetailsModel> response) {
                if (response.isSuccessful()){
                    MyChildrenDetailsModel.Data data = response.body().getData();
                    Glide.with(getBaseContext()).load(data.getAvatar()).into(profilepic);
                    myprofilename.setText(data.getFullName());
                    rollno.setText("ROLL NO : "+data.getRollNumber());
                    phonenum.setText(data.getMobileNo());
                    classname.setText(data.getClass_());
                    address.setText(data.getAddress());
                    dateofbirth.setText(data.getDateOfBirth());
                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(MyProfileActivity.this);
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<MyChildrenDetailsModel> call, Throwable t) {
                AppUtils.APIFails(MyProfileActivity.this,t);
                progressDialogFragment.dismiss();

            }
        });


    }

}
