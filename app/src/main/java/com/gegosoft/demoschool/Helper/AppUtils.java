package com.gegosoft.demoschool.Helper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.gegosoft.demoschool.Activity.LoginActivity;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.net.SocketTimeoutException;

public class AppUtils {
    public static Dialog getDialog(Context mContext, int mLayout) {
        Dialog mDialog = new Dialog(mContext, R.style.AppTheme);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        mDialog.setContentView(mLayout);
        mDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        mDialog.getWindow().setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(true);
        return mDialog;
    }
    public static void APIFails(Context context, Throwable t){
        if (t instanceof SocketTimeoutException)
        {
            Toast.makeText(context,"Connection Time Out",Toast.LENGTH_LONG).show();
            // "Connection Timeout";
        }
        else if (t instanceof IOException)
        {
            // "Timeout";
        }
        else
        {
//            //Call was cancelled by user
//            if(call.isCanceled())
//            {
//                System.out.println("Call was cancelled forcefully");
//            }
//            else
//            {
            //Generic error handling
            System.out.println("Network Error :: " + t.getLocalizedMessage());
//            }
        }
    }
    public static void SessionExpired(final Activity context) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Session Expired");
        alertDialog.setMessage("Please Log in Again");
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        refreshToken(context);
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
    private static void refreshToken(final Activity context){
        final UserDetailsSharedPref userDetailsSharedPref = UserDetailsSharedPref.getInstance(context);
        new  AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try
                {
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                    String token =  FirebaseInstanceId.getInstance().getToken();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
                return null;
//                    FirebaseInstanceId.getInstance().deleteInstanceId();
            }
            @Override
            protected void onPostExecute(Void result) {
                userDetailsSharedPref.logout();
                userDetailsSharedPref.clearAllEntries();
                context.startActivity(new Intent(context, LoginActivity.class));
                context.finish();
            }
        }.execute();
    }
}