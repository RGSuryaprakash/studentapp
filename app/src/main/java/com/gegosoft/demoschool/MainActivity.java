package com.gegosoft.demoschool;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gegosoft.demoschool.Activity.AssignmentActivity;
import com.gegosoft.demoschool.Activity.AttendanceActivity;
import com.gegosoft.demoschool.Activity.ChangePasswordActivity;
import com.gegosoft.demoschool.Activity.ClassRoomEventsActivity;
import com.gegosoft.demoschool.Activity.DisciplineActivity;
import com.gegosoft.demoschool.Activity.EventsActivity;
import com.gegosoft.demoschool.Activity.FeeActivity;
import com.gegosoft.demoschool.Activity.FeedbackActivity;
import com.gegosoft.demoschool.Activity.GalleryActivity;
import com.gegosoft.demoschool.Activity.LeaveActivity;
import com.gegosoft.demoschool.Activity.LessonPlanActivity;
import com.gegosoft.demoschool.Activity.MagazineActivity;
import com.gegosoft.demoschool.Activity.MediaFilesActivity;
import com.gegosoft.demoschool.Activity.NoticeboardActivity;
import com.gegosoft.demoschool.Activity.PastEventActivity;
import com.gegosoft.demoschool.Activity.RoomActivity;
import com.gegosoft.demoschool.Activity.SchoolInfoActivity;
import com.gegosoft.demoschool.Activity.StaffInfoActivity;
import com.gegosoft.demoschool.Activity.StartUpActivity;
import com.gegosoft.demoschool.Activity.TimeTableActivity;
import com.gegosoft.demoschool.Activity.WebActivity;
import com.gegosoft.demoschool.Fragments.ProgressDialogFragment;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.ClassNoticeBoardModel;
import com.gegosoft.demoschool.Model.LogOutModel;
import com.gegosoft.demoschool.Model.MyChildrenModel;
import com.gegosoft.demoschool.Model.MyChildrenDetailsModel;
import com.gegosoft.demoschool.Model.SchoolInfoModel;
import com.gegosoft.demoschool.Model.SchoolNoticeBoardModel;
import com.gegosoft.demoschool.Model.UpdateFCMTokenModel;
import com.gegosoft.demoschool.Receiver.NotificationReceiver;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.recyclerview.widget.RecyclerView.VERTICAL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView noticeboardlist;
    Api api;
    Toolbar toolbar;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    public DrawerLayout drawerLayout;
    ConstraintLayout accountlayout;
    List<MyChildrenModel.Datum> childrenData;
    AlertDialog alertDialog;
    TextView notificationindicator,feedbackindicator,eventindicator,homeworkindicator,assignmentindicator,disciplineindicator,magazineindicator,navusername,navclassname;
    NavigationView nav_view;
    TextView marquetext;
    ActionBarDrawerToggle actionBarDrawerToggle;
    CircleImageView navprofileimage,profileimage;
    TextView profilename,student_standard;
    SchoolNoticeAdapter schoolNoticeAdapter;
    ClassNoticeAdapter classNoticeAdapter;
    ConcatAdapter concatAdapter;
    CardView eventsgallerycardview,nodatacardview,feedbackcardview,timetablecardview,homeworkcardview,eventcardview,attendancecardview,notificationscardview,assignmentcardview,disciplinecardview,magazinecardview;
    LinearLayoutManager linearLayoutManager;
    TextView[] dots;
    LinearLayout dotslayout;
    List<SchoolNoticeBoardModel.Datum> schoolnoticelist;
    List<ClassNoticeBoardModel.Datum> classnoticelist;
    private NotificationReceiver notificationReceiver = new NotificationReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
            RemoteMessage message = intent.getParcelableExtra("MessageReceived");
            JSONObject jsonObject = new JSONObject(message.getData());
            String type = null;
            try {
                type = jsonObject.getString("type");
                if (type.equalsIgnoreCase("private message")){
                    userDetailsSharedPref.saveBoolean("isPrivateMessage",true);
                    notificationindicator.setVisibility(View.VISIBLE);
//
                }
                if (type.equalsIgnoreCase("homework")){
                    userDetailsSharedPref.saveBoolean("isHomeWork",true);
                    homeworkindicator.setVisibility(View.VISIBLE);
                }
                if (type.equalsIgnoreCase("assignment")){
                    userDetailsSharedPref.saveBoolean("isAssignment",true);
                    assignmentindicator.setVisibility(View.VISIBLE);
                }
                if (type.equalsIgnoreCase("discipline")){
                    userDetailsSharedPref.saveBoolean("isDiscipline",true);
                    disciplineindicator.setVisibility(View.VISIBLE);
                }
                if (type.equalsIgnoreCase("bulletin")){
                    userDetailsSharedPref.saveBoolean("isMagazine",true);
                    magazineindicator.setVisibility(View.VISIBLE);
                }
                if (type.equalsIgnoreCase("event")){
                    userDetailsSharedPref.saveBoolean("isEvent",true);
                    eventindicator.setVisibility(View.VISIBLE);
                }
                if (type.equalsIgnoreCase("conversation")){
                    userDetailsSharedPref.saveBoolean("isFeedback",true);
                    feedbackindicator.setVisibility(View.VISIBLE);
                }
                if (type.equalsIgnoreCase("video_room")){
                    userDetailsSharedPref.saveBoolean("isvideo_room",true);
//                    feedbackindicator.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }



        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = getWindow();
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
//        }
        setContentView(R.layout.activity_main);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(this);
        toolbar = findViewById(R.id.toolbar);
        accountlayout = findViewById(R.id.accountlayout);
      //  drawer_layout = findViewById(R.id.drawer_layout);
        notificationindicator = findViewById(R.id.notificationindicator);
        homeworkindicator= findViewById(R.id.homeworkindicator);
        eventindicator= findViewById(R.id.eventindicator);
        assignmentindicator =  findViewById(R.id.assignmentindicator);
        feedbackindicator =  findViewById(R.id.feedbackindicator);
        disciplineindicator =  findViewById(R.id.disciplineindicator);
        magazineindicator =  findViewById(R.id.magazineindicator);
        noticeboardlist= findViewById(R.id.noticeboardlist);
        feedbackcardview = findViewById(R.id.feedbackcardview);
        timetablecardview = findViewById(R.id.timetablecardview);
        homeworkcardview = findViewById(R.id.homeworkcardview);
        eventcardview = findViewById(R.id.eventcardview);
        linearLayoutManager=new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false);
        noticeboardlist.setLayoutManager(linearLayoutManager);
        SnapHelper snapHelper =  new PagerSnapHelper();
        snapHelper.attachToRecyclerView(noticeboardlist);
        noticeboardlist.setHasFixedSize(true);
        attendancecardview = findViewById(R.id.attendancecardview);
        notificationscardview = findViewById(R.id.notificationscardview);
        assignmentcardview = findViewById(R.id.assignmentcardview);
        disciplinecardview = findViewById(R.id.disciplinecardview);
        magazinecardview = findViewById(R.id.magazinecardview);
        nodatacardview = findViewById(R.id.nodatacardview);

        marquetext = findViewById(R.id.marque_texts);






        if (userDetailsSharedPref.getString("firebasetoken")!=null){
            String firebasetoken = FirebaseInstanceId.getInstance().getToken();
            if (!userDetailsSharedPref.getString("firebasetoken").equalsIgnoreCase(firebasetoken)){
                sendRegistrationToServer(firebasetoken);
            }
        }

        eventsgallerycardview = findViewById(R.id.eventsgallerycardview);
        nav_view = findViewById(R.id.nav_view);
        View hView =  nav_view.inflateHeaderView(R.layout.headerlayout);
        navprofileimage  =    hView.findViewById(R.id.navprofileimage);
        navusername =    hView.findViewById(R.id.navusername);
        navclassname  =    hView.findViewById(R.id.navclassname);
        dotslayout=findViewById(R.id.dotslayout);
        profileimage = findViewById(R.id.profileimage);
        profilename = findViewById(R.id.profilename);
        student_standard= findViewById(R.id.student_standard);
        feedbackcardview.setOnClickListener(this);
        timetablecardview.setOnClickListener(this);
        homeworkcardview.setOnClickListener(this);
        eventcardview.setOnClickListener(this);
        attendancecardview.setOnClickListener(this);
        notificationscardview.setOnClickListener(this);
        assignmentcardview.setOnClickListener(this);
        disciplinecardview.setOnClickListener(this);
        magazinecardview.setOnClickListener(this);
        accountlayout.setOnClickListener(this);
        eventsgallerycardview.setOnClickListener(this);
//        noticeboardlist.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false));
//        noticeboardlist.setHasFixedSize(true);
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        toolbar.setTitle("DASHBOARD");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu);

        Call<SchoolInfoModel> schoolInfoModelCall = api.getSchoolDetails(headermap);
        schoolInfoModelCall.enqueue(new Callback<SchoolInfoModel>() {
            @Override
            public void onResponse(Call<SchoolInfoModel> call, Response<SchoolInfoModel> response) {
                SchoolInfoModel.Data data =response.body().getData();
                marquetext.setText(data.getWebsite());
            }

            @Override
            public void onFailure(Call<SchoolInfoModel> call, Throwable t) {
                Toast.makeText(MainActivity.this, ""+t, Toast.LENGTH_SHORT).show();
            }
        });





//        noticeboardlist.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent=new Intent(MainActivity.this,NoticeboardActivity.class);
//                startActivity(intent);
//            }
//        });
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(MainActivity.this, NavigationActivity.class));
//            }
//        });
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);

        final Menu menu = nav_view.getMenu();
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
//                    drawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    drawerLayout.openDrawer(GravityCompat.END);
                }

            }
        });


        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.openDrawer, R.string.closeDrawer){

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
//
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
//                mainview.setTranslationX(slideOffset * drawerView.getWidth());
//                drawerLayout.bringChildToFront(drawerView);
//                drawerLayout.requestLayout();
            }

        };
        //calling sync state is necessay or else your hamburger icon wont show up
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                if (menuItem.isChecked())
                    menuItem.setChecked(false);
                else menuItem.setChecked(true);
                //Closing drawer on item click
                drawerLayout.closeDrawers();


                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    // For rest of the options we just show a toast on click
//
                    case R.id.nav_profile:
                       startActivity(new Intent(MainActivity.this, MyProfileActivity.class));
                      //  startActivity(new Intent(MainActivity.this, WebActivity.class));

                        return true;

//
                    case R.id.nav_changepassword:

                        startActivity(new Intent(MainActivity.this, ChangePasswordActivity.class));

                        return true;
//
                    case R.id.nav_school_info:

                        startActivity(new Intent(MainActivity.this, SchoolInfoActivity.class));

                        return true;
                    case R.id.nav_video_rooms:

                        startActivity(new Intent(MainActivity.this, RoomActivity.class));

                        return true;

                    case R.id.nav_media_files:
                        startActivity(new Intent(MainActivity.this, MediaFilesActivity.class));
                        return true;
                    case R.id.nav_notice:
                        startActivity(new Intent(MainActivity.this, NoticeboardActivity.class));
                        return true;
                    case R.id.nav_events:
                        userDetailsSharedPref.saveBoolean("isEvent",false);
                        startActivity(new Intent(MainActivity.this, EventsActivity.class));
                        return true;
                    case R.id.nav_magazine:
                        userDetailsSharedPref.saveBoolean("isMagazine",false);
                        startActivity(new Intent(MainActivity.this,MagazineActivity.class));
                        return true;
                    case R.id.nav_home_work:
                        userDetailsSharedPref.saveBoolean("isHomeWork",false);
                        startActivity(new Intent(MainActivity.this,HomeWorkActivity.class));
                        return true;
                    case R.id.nav_assignment:
                        userDetailsSharedPref.saveBoolean("isAssignment",false);
                        startActivity(new Intent(MainActivity.this,AssignmentActivity.class));
                        return true;
                    case R.id.nav_time_table:
                        startActivity(new Intent(MainActivity.this,TimeTableActivity.class));
                        return true;
                    case R.id.nav_class_events:
                        startActivity(new Intent(MainActivity.this, ClassRoomEventsActivity.class));
                        return true;
                    case R.id.nav_attendance:
                        startActivity(new Intent(MainActivity.this,AttendanceActivity.class));
                        return true;
                    case R.id.nav_discipline:
                        userDetailsSharedPref.saveBoolean("isDiscipline",false);
                        startActivity(new Intent(MainActivity.this,DisciplineActivity.class));
                        return true;
                    case R.id.nav_feedback:
                        userDetailsSharedPref.saveBoolean("isFeedback",false);
                        startActivity(new Intent(MainActivity.this, FeedbackActivity.class));
                        return true;
                    case R.id.nav_log_out:
                        signOut();
                        return true;
                    case R.id.nav_staffinfo:
                        startActivity(new Intent(MainActivity.this, StaffInfoActivity.class));
                        return true;
                    case R.id.nav_fee:
                        startActivity(new Intent(MainActivity.this, FeeActivity.class));
                        return true;
                    case R.id.nav_lessonplan:
                        startActivity(new Intent(MainActivity.this, LessonPlanActivity.class));
                        return true;
                    case R.id.nav_leave:
                        startActivity(new Intent(MainActivity.this, LeaveActivity.class));
                        return true;
                    case R.id.nav_exams:
                        startActivity(new Intent(MainActivity.this, ExamsActivity.class));
                        return true;
 //                    case R.id.account:
//                        frameLayout.setVisibility(View.VISIBLE);
//                        frameLayout.removeAllViews();
//                        ProfileFragment account = new ProfileFragment();
//                        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                        getSupportFragmentManager().beginTransaction().add(R.id.framelayout,account).addToBackStack(null).commit();
//                        return true;
//
//                    case R.id.transfer :
//                        framelayout.setVisibility(View.VISIBLE);
//                        framelayout.removeAllViews();
//
//                        Transfer transfer = new Transfer();
//                        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                        getSupportFragmentManager().beginTransaction().add(R.id.framelayout,transfer).addToBackStack(null).commit();
//                        return true;
//                    case R.id.history:
//                        frameLayout.setVisibility(View.VISIBLE);
//                        frameLayout.removeAllViews();
//
//                        TransactionsHistory history = new TransactionsHistory();
//                        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                        getSupportFragmentManager().beginTransaction().add(R.id.framelayout,history).addToBackStack(null).commit();
//                        return true;

                    default:
                        Toast.makeText(getApplicationContext(), "Under Developement", Toast.LENGTH_SHORT).show();
                        return true;

                }



            }


        });
        actionBarDrawerToggle.syncState();
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
//        Log.d("firebasetoken",userDetailsSharedPref.getString("firebasetoken"));
        if (CheckNetwork.isInternetAvailable(this)){
            getChildrenList();

        }
        noticeboardlist.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {


                }
                else if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int review_position = linearLayoutManager.findFirstVisibleItemPosition();

                    addDots(review_position);


                }

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });


       // getNot().subscribe();
       // setIndicator();
        //ShowNotication("Hello","Good Morning");
    }
    private void sendRegistrationToServer(final String refreshedToken){
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");


        String deviceID = Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);
        Call<UpdateFCMTokenModel> tokenModelCall = api.updatefcmtoken(headermap,refreshedToken);
        tokenModelCall.enqueue(new Callback<UpdateFCMTokenModel>() {
            @Override
            public void onResponse(Call<UpdateFCMTokenModel> call, Response<UpdateFCMTokenModel> response) {
                if (response.isSuccessful()){
                    //Toast.makeText(getBaseContext(),refreshedToken,Toast.LENGTH_LONG).show();
                }
                else {
                    if(response.code()==401){
                        AppUtils.SessionExpired(MainActivity.this);
                    }
                    if (response.code()==500){
                       // AppUtils.ServerError(getSupportFragmentManager());
                    }
                }
            }
            @Override
            public void onFailure(Call<UpdateFCMTokenModel> call, Throwable t) {
                AppUtils.APIFails(MainActivity.this,t);
            }
        });
    }
    private void setIndicator(){
        if(userDetailsSharedPref.getBoolean("isPrivateMessage")){
           notificationindicator.setVisibility(View.VISIBLE);

        }
        else {
            notificationindicator.setVisibility(View.GONE);
        }
        if(userDetailsSharedPref.getBoolean("isHomeWork")){
            homeworkindicator.setVisibility(View.VISIBLE);

        }
        else {
            homeworkindicator.setVisibility(View.GONE);
        }
        if(userDetailsSharedPref.getBoolean("isAssignment")){
            assignmentindicator.setVisibility(View.VISIBLE);

        }
        else {
            assignmentindicator.setVisibility(View.GONE);
        }
        if(userDetailsSharedPref.getBoolean("isMagazine")){
            magazineindicator.setVisibility(View.VISIBLE);

        }
        else {
            magazineindicator.setVisibility(View.GONE);
        }
        if(userDetailsSharedPref.getBoolean("isDiscipline")){
            disciplineindicator.setVisibility(View.VISIBLE);

        }
        else {
            disciplineindicator.setVisibility(View.GONE);
        }
        if(userDetailsSharedPref.getBoolean("isEvent")){
            eventindicator.setVisibility(View.VISIBLE);

        }
        else {
            eventindicator.setVisibility(View.GONE);
        }
        if(userDetailsSharedPref.getBoolean("isFeedback")){
            feedbackindicator.setVisibility(View.VISIBLE);

        }
        else {
            feedbackindicator.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(notificationReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setIndicator();
        registerReceiver(notificationReceiver,new IntentFilter(NotificationReceiver.ACTION_NOTIFICATION));
    //    ShowNotication("Hello","Good Morning");
    }

    private void getChildrenList(){
        Call<MyChildrenModel> myChildrenModelCall = api.getMychildren(headermap);
        myChildrenModelCall.enqueue(new Callback<MyChildrenModel>() {
            @Override
            public void onResponse(Call<MyChildrenModel> call, Response<MyChildrenModel> response) {
                if (response.isSuccessful()){
                    childrenData =  response.body().getData();
                    if (childrenData.get(0).getStudent_id()!=null){
                        userDetailsSharedPref.saveString("userId",childrenData.get(0).getStudent_id().toString());
                    }

                    getChildrenDetails();

                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(MainActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(Call<MyChildrenModel> call, Throwable t) {
                AppUtils.APIFails(MainActivity.this,t);
                Log.d("messi",t.getMessage());

            }
        });
     }
     private void getChildrenDetails(){
         Call<MyChildrenDetailsModel> profileModelCall = api.getChildrendetails(headermap,userDetailsSharedPref.getString("userId"));
         profileModelCall.enqueue(new Callback<MyChildrenDetailsModel>() {
             @Override
             public void onResponse(Call<MyChildrenDetailsModel> call, Response<MyChildrenDetailsModel> response) {
                 if (response.isSuccessful()){
                     MyChildrenDetailsModel.Data data = response.body().getData();
                     if(data.getStandard_id()!=null){
                         userDetailsSharedPref.saveString("standardId",data.getStandard_id().toString());
                     }
                     Glide.with(MainActivity.this).load(data.getAvatar()).into(navprofileimage);
                     Glide.with(MainActivity.this).load(data.getAvatar()).into(profileimage);
                     profilename.setText(data.getFullName());
                     student_standard.setText(data.getClass_());
                     navusername.setText(data.getFullName());
                     navclassname.setText(data.getClass_());
                     getSchoolNoticeBoard();

                 }
                 else {
                     if (response.code()==401){
                         AppUtils.SessionExpired(MainActivity.this);
                     }
                 }

             }

             @Override
             public void onFailure(Call<MyChildrenDetailsModel> call, Throwable t) {
                 AppUtils.APIFails(MainActivity.this,t);
             }
         });
     }
    private void getClassNoticeBoard(){
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        Call<ClassNoticeBoardModel> boardModelCall = api.getClassNotice(headermap,userDetailsSharedPref.getString("userId"));
        boardModelCall.enqueue(new Callback<ClassNoticeBoardModel>() {
            @Override
            public void onResponse(Call<ClassNoticeBoardModel> call, Response<ClassNoticeBoardModel> response) {
                if (response.isSuccessful()){
                    classnoticelist =response.body().getData();
                    if(classnoticelist.size()!=0){

                        noticeboardlist.setVisibility(View.VISIBLE);

                        classNoticeAdapter = new ClassNoticeAdapter(classnoticelist);
//                         noticeboardlist.setAdapter(classNoticeAdapter);
                        nodatacardview.setVisibility(View.GONE);
                        if(concatAdapter==null){


                            concatAdapter =  new ConcatAdapter(classNoticeAdapter);
                            noticeboardlist.setAdapter(classNoticeAdapter);
                        }
                        else{
                            concatAdapter.addAdapter(classNoticeAdapter);
                            concatAdapter.notifyDataSetChanged();
                        }
                        addDots(0);

                    }
                    else {
                        if (schoolnoticelist.size()!=0){
                            nodatacardview.setVisibility(View.GONE);
                            noticeboardlist.setVisibility(View.VISIBLE);
                        }
                        else {
                            nodatacardview.setVisibility(View.VISIBLE);
                            noticeboardlist.setVisibility(View.GONE);
                        }
//                         getSchoolNoticeBoard();
                    }


                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(MainActivity.this);
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<ClassNoticeBoardModel> call, Throwable t) {

                AppUtils.APIFails(MainActivity.this,t);
                progressDialogFragment.dismiss();
            }
        });

    }
    private  void getSchoolNoticeBoard(){
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getSupportFragmentManager(),"loading screen");
        Call<SchoolNoticeBoardModel> boardModelCall = api.getSchoolNotice(headermap);
        boardModelCall.enqueue(new Callback<SchoolNoticeBoardModel>() {
            @Override
            public void onResponse(Call<SchoolNoticeBoardModel> call, Response<SchoolNoticeBoardModel> response) {
                if (response.isSuccessful()){
                    schoolnoticelist = response.body().getData();

                    if(schoolnoticelist.size()!=0){

                        noticeboardlist.setVisibility(View.VISIBLE);
                        schoolNoticeAdapter = new SchoolNoticeAdapter(schoolnoticelist);
//                        noticeboardlist.setAdapter(schoolNoticeAdapter);
                        nodatacardview.setVisibility(View.GONE);
                        if(concatAdapter==null){
                            concatAdapter=new ConcatAdapter(schoolNoticeAdapter);
                            noticeboardlist.setAdapter(concatAdapter);
                        }
                        else{
                            concatAdapter.addAdapter(schoolNoticeAdapter);
                            concatAdapter.notifyDataSetChanged();
                        }
                        addDots(0);

                    }
                    else {
                        nodatacardview.setVisibility(View.VISIBLE);
                        noticeboardlist.setVisibility(View.GONE);
//                        if (concatAdapter.getAdapters().size()!=0){
//                            nodatacardview.setVisibility(View.GONE);
//                            noticeboardlist.setVisibility(View.VISIBLE);
//                        }
//                        else {
//                            nodatacardview.setVisibility(View.VISIBLE);
//                            noticeboardlist.setVisibility(View.GONE);
//                        }
                    }

                    getClassNoticeBoard();
                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(MainActivity.this);
                    }
                }

                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<SchoolNoticeBoardModel> call, Throwable t) {
                AppUtils.APIFails(MainActivity.this,t);
                progressDialogFragment.dismiss();
            }
        });

    }
    private void addDots(int position) {
        int count =0;
        if (schoolNoticeAdapter!=null){
            count = schoolNoticeAdapter.getItemCount();
        }
        if(classNoticeAdapter!=null){
            count = count+classNoticeAdapter.getItemCount();
        }

        dots = new TextView[count];
        dotslayout.removeAllViews();

        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("•"));
            dots[i].setTextSize(35);

            dotslayout.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[position].setTextColor(getResources().getColor(R.color.black));
        }

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.feedbackcardview :
                userDetailsSharedPref.saveBoolean("isFeedback",false);
                startActivity(new Intent(this, FeedbackActivity.class));
                break;
            case R.id.timetablecardview :
                startActivity(new Intent(this, TimeTableActivity.class));
                break;
            case R.id.attendancecardview :
                startActivity(new Intent(this, AttendanceActivity.class));
                break;
            case R.id.eventcardview :
                userDetailsSharedPref.saveBoolean("isEvent",false);
                startActivity(new Intent(this, EventsActivity.class));
                break;
            case R.id.homeworkcardview :
                userDetailsSharedPref.saveBoolean("isHomeWork",false);
                startActivity(new Intent(this, HomeWorkActivity.class));
                break;
            case R.id.notificationscardview :
                userDetailsSharedPref.saveBoolean("isPrivateMessage",false);
                startActivity(new Intent(this, NotificationActivity.class));
                break;
            case R.id.assignmentcardview :
                //Toast.makeText(this,"Under Developement",Toast.LENGTH_LONG).show();
                userDetailsSharedPref.saveBoolean("isAssignment",false);
                startActivity(new Intent(this, AssignmentActivity.class));
                break;
            case R.id.disciplinecardview :
//                Toast.makeText(this,"Under Developement",Toast.LENGTH_LONG).show();
                userDetailsSharedPref.saveBoolean("isDiscipline",false);
                startActivity(new Intent(this, DisciplineActivity.class));
                break;
            case R.id.magazinecardview :
                userDetailsSharedPref.saveBoolean("isMagazine",false);
                startActivity(new Intent(this, MagazineActivity.class));
                break;
            case R.id.eventsgallerycardview :
                startActivity(new Intent(this, PastEventActivity.class));
                break;

            case R.id.accountlayout :
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                LayoutInflater inflater = getLayoutInflater();

                View dialogLayout = inflater.inflate(R.layout.recyclerview, null);
                builder.setView(dialogLayout);
                builder.setTitle("Choose Account");

                RecyclerView recyclerView = dialogLayout.findViewById(R.id.recyclerview);
               // textnorecords = dialogLayout.findViewById(R.id.textnorecords);
                linearLayoutManager=new LinearLayoutManager(this, VERTICAL,false);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setHasFixedSize(true);
                DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(), VERTICAL);
                recyclerView.addItemDecoration(decoration);
                AccountAdapter accountAdapter = new AccountAdapter(childrenData);
                recyclerView.setAdapter(accountAdapter);
                alertDialog =  builder.show();
                break;
            default:
                return;
        }
    }
    private class ClassNoticeAdapter extends RecyclerView.Adapter<ClassNoticeAdapter.MyViewHolder>{
        List<ClassNoticeBoardModel.Datum> data;
        public ClassNoticeAdapter(List<ClassNoticeBoardModel.Datum> data) {
            this.data = data;

        }
        @NonNull
        @Override
        public ClassNoticeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.classnoticelist,parent,false);
            return new ClassNoticeAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ClassNoticeAdapter.MyViewHolder holder, int position) {
            holder.classtitle.setText(data.get(position).getTitle());
            holder.classtype.setText(data.get(position).getType());
          //  holder.classdescription.setText(Html.fromHtml(data.get(position).getDescription()));

            holder.classdescription.loadData(data.get(position).getDescription(),"text/html; chartset=UTF-8", null);



            Glide.with(MainActivity.this).load(data.get(position).getBackgroundimage()).into(holder.imageView);

            /*holder.classdescription.setText(data.get(position).getDescription());*/
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, NoticeboardActivity.class));
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView classtitle,classtype;
            WebView classdescription;
            ImageView imageView;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                classtitle = itemView.findViewById(R.id.classtitle);
                classdescription = itemView.findViewById(R.id.classdescription);


                classdescription.getSettings().setJavaScriptEnabled(true);
                classdescription.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                classdescription.setBackgroundColor(0);

                classtype=itemView.findViewById(R.id.classnoticeboardtype);
                imageView = itemView.findViewById(R.id.image_bg);
            }
        }
    }

    private class SchoolNoticeAdapter extends RecyclerView.Adapter<SchoolNoticeAdapter.MyViewHolder>{
        List<SchoolNoticeBoardModel.Datum> data;
        public SchoolNoticeAdapter(List<SchoolNoticeBoardModel.Datum> data) {
            this.data = data;

        }

        @NonNull
        @Override
        public SchoolNoticeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schoolnoticelist,parent,false);
            return new SchoolNoticeAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull SchoolNoticeAdapter.MyViewHolder holder, int position) {
            holder.title.setText(data.get(position).getTitle());
            holder.type.setText(data.get(position).getType());
         //   holder.description.setText(Html.fromHtml(data.get(position).getDescription()));
            holder.description.loadData(data.get(position).getDescription(),"text/html; chartset=UTF-8", null);

            Glide.with(MainActivity.this).load(data.get(position).getBackgroundimage()).into(holder.banner_image);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(MainActivity.this,NoticeboardActivity.class);
                            startActivity(intent);
                }
        });

        }
        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView title,type;
            WebView description;
            ImageView banner_image;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                title = itemView.findViewById(R.id.title);
//                date = itemView.findViewById(R.id.date);
                description = itemView.findViewById(R.id.description);

                description.getSettings().setJavaScriptEnabled(true);
                description.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                description.setBackgroundColor(0);

                 type=itemView.findViewById(R.id.noticeboardtype);
                banner_image=itemView.findViewById(R.id.banner_image);
            }
        }
    }

    private class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.MyViewHolder>{
        List<MyChildrenModel.Datum> data;

        public AccountAdapter(List<MyChildrenModel.Datum> childrenData) {
            this.data = childrenData;
        }

        @NonNull
        @Override
        public AccountAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.childrenaccount,parent,false);
            return new AccountAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull AccountAdapter.MyViewHolder holder, int position) {
            holder.name.setText(data.get(position).getName());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    userDetailsSharedPref.saveString("userId",data.get(position).getStudent_id().toString());
                    getChildrenDetails();
                }
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView name;
            CircleImageView image;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                image = itemView.findViewById(R.id.image);
                name = itemView.findViewById(R.id.name);
            }
        }
    }

//    private void ShowNotication( String title, String Msg){
//        int id=15;
//        userDetailsSharedPref.saveBoolean("isPrivateMessage",true);
//
//        Intent intent= new Intent(this, MainActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////        Bundle bundle = new Bundle();
////        bundle.putBoolean("notification",true);
////        intent.putExtras(bundle);
//        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 0);
//        Notification.Builder b = new Notification.Builder(this);
//
//        NotificationChannel mChannel = null;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                b.setAutoCancel(false)
//                        .setSmallIcon(R.drawable.ic_notication_icon)
//                        .setColor(getColor(R.color.colorPrimaryDark))
//
//                        .setContentTitle(title)
//                        .setTicker(title)
//                        .setContentText(Msg)
//                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
//                        .setContentIntent(contentIntent);
//            }
//        }
//
//
//
//
////        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
////            notification.setSmallIcon(R.mi.ic);
////            notification.setColor(getResources().getColor(R.color.notification_color));
////        } else {
////            notification.setSmallIcon(R.drawable.icon);
////        }
//
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            mChannel = new NotificationChannel("cid", "name", NotificationManager.IMPORTANCE_HIGH);
//            mChannel.setShowBadge(true);
//            b.setChannelId("cid");
//            mChannel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), new AudioAttributes.Builder()
//                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
//                    .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
//                    .build());
//        }
//
//        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            notificationManager.createNotificationChannel(mChannel);
//        }
//        b.setAutoCancel(true);
//        b.setOngoing(true);
//        notificationManager.notify(id, b.build());
//        Bundle bundle = new Bundle();
//        bundle.putString("type","test");
//        sendBroadcast(new Intent(NotificationReceiver.ACTION_NOTIFICATION).putExtra("MessageReceived",new RemoteMessage(bundle)));
//
//    }

    private void signOut(){
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        Call<LogOutModel> logOutModelCall = api.logOut(headermap);
        logOutModelCall.enqueue(new Callback<LogOutModel>() {
            @Override
            public void onResponse(Call<LogOutModel> call, Response<LogOutModel> response) {
                if(response.isSuccessful())
                {
                    new  AsyncTask<Void,Void,Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            try
                            {
                                FirebaseInstanceId.getInstance().deleteInstanceId();
                                String token =  FirebaseInstanceId.getInstance().getToken();
                            } catch (IOException e)
                            {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void result) {
                            // Call your Activity where you want to land after log out
                            // userDetailsSharedPref.saveString("login",null);
                            userDetailsSharedPref.logout();
                            userDetailsSharedPref.clearAllEntries();
                            Intent i = new Intent(MainActivity.this, StartUpActivity.class);
// set the new task and clear flags
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                    }.execute();

                }

                if (response.code()==401){
                    new  AsyncTask<Void,Void,Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            try
                            {
                                FirebaseInstanceId.getInstance().deleteInstanceId();
                                String token =  FirebaseInstanceId.getInstance().getToken();
                            } catch (IOException e)
                            {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void result) {
                            // Call your Activity where you want to land after log out
                            // userDetailsSharedPref.saveString("login",null);
                            userDetailsSharedPref.logout();
                            userDetailsSharedPref.clearAllEntries();
                            Intent i = new Intent(MainActivity.this, StartUpActivity.class);
// set the new task and clear flags
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                    }.execute();
                }

                else {
                    userDetailsSharedPref.logout();
                    userDetailsSharedPref.clearAllEntries();
                    Intent i = new Intent(MainActivity.this, StartUpActivity.class);
// set the new task and clear flags
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();

                }
            }

            @Override
            public void onFailure(Call<LogOutModel> call, Throwable t) {
                AppUtils.APIFails(MainActivity.this,t);
            }
        });
    }

}
