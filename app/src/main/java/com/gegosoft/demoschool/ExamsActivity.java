package com.gegosoft.demoschool;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gegosoft.demoschool.Fragments.PastExamFragment;
import com.gegosoft.demoschool.Fragments.UpcomingExamFragment;
import com.gegosoft.demoschool.Model.ExamsModel;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

public class ExamsActivity extends AppCompatActivity {
    List<ExamsModel.Datum> exDatumList;

    Toolbar toolbar;
    TabLayout examtablayout;
    ViewPager examviewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.examslayout);

        toolbar=findViewById(R.id.examstoolbar);
        toolbar.setTitle("EXAMS");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        examtablayout = findViewById(R.id.examtablayout);
        examviewPager = findViewById(R.id.examviewpager);
        examtablayout.addTab(examtablayout.newTab().setText("PAST"));
        examtablayout.addTab(examtablayout.newTab().setText("UPCOMING"));
        examtablayout.setTabTextColors(getResources().getColor(R.color.black),getResources().getColor(R.color.white));
        examtablayout.setTabGravity(TabLayout.GRAVITY_FILL);



        examviewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(examtablayout));
        examtablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                examviewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        ExamPagerAdapter examPagerAdapter = new ExamPagerAdapter(getSupportFragmentManager(), ExamsActivity.this, examtablayout.getTabCount());
        examviewPager.setAdapter(examPagerAdapter);


    }

    public class ExamPagerAdapter extends FragmentPagerAdapter {

        private Context myContext;
        int totalTabs;

        public ExamPagerAdapter(@NonNull FragmentManager fm, Context context, int totalTabs) {
            super(fm);
            myContext = context;
            this.totalTabs = totalTabs;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    PastExamFragment examFragment = new PastExamFragment();

                    return examFragment;
                case 1:
                    UpcomingExamFragment upcomingExamFragment = new UpcomingExamFragment();


                    return upcomingExamFragment;

                default:
                    return null;
            }

        }
        @Override
        public int getCount () {
            return totalTabs;
        }
    }

}
