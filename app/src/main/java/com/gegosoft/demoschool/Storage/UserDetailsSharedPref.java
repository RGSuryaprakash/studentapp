package com.gegosoft.demoschool.Storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

public class UserDetailsSharedPref {

    public static final String TAG = "Utils";
    public static final String PREFS = "prefs";
    private static final String IS_LOGIN = "IsLoggedIn";
    private static UserDetailsSharedPref instance = null;
    SharedPreferences.Editor editor;
    private boolean someValue = false;
    public SharedPreferences signup_pref;

    public SharedPreferences getPref(){
        return signup_pref;
    }

    public UserDetailsSharedPref(Context context)
    {
        this.signup_pref = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
    }

    public static UserDetailsSharedPref getInstance(Context context)
    {
        if(instance == null)
            instance = new UserDetailsSharedPref(context);
        return instance;
    }
    public void createLoginSession() {
        editor= signup_pref.edit();
        editor.putBoolean(IS_LOGIN, true);

        editor.commit();
    }
    public boolean isLoggedIn() {
        return signup_pref.getBoolean(IS_LOGIN, false);
    }
    public void logout() {

        editor= signup_pref.edit();
        editor.clear();
        editor.commit();
    }
    /**
     * Saves a key/value pair in preferences.
     * @param key
     * @param value
     */
    public void saveString(String key, String value)
    {
        this.signup_pref.edit().putString(key, value).commit();
    }


    /**
     * Gets the string value by its key.
     //     * @param key
     * @return the string value of the key or null otherwise
     */
    public String getString(String Key)
    {
        return this.signup_pref.getString(Key, null );
    }


    public void saveId(String key, String value){
        this.signup_pref.edit().putString(key, value).commit();
    }

    public void saveBoolean(String key, Boolean value)
    {
        this.signup_pref.edit().putBoolean(key, value).commit();
    }

    public boolean getBoolean(String Key)
    {
        return this.signup_pref.getBoolean(Key, false );
    }


    public void saveStringImage(String key, Uri value)
    {
        this.signup_pref.edit().putString(key, String.valueOf(value)).commit();
    }


    /**
     * Gets the string value by its key.
     //     * @param key
     * @return the string value of the key or null otherwise
     */
    public String getStringImage(String Key)
    {
        return this.signup_pref.getString(Key, null );
    }

    // Removes all entries from the shared preferences file.
    public boolean clearAllEntries()
    {
        return this.signup_pref.edit().clear().commit();
    }
}
