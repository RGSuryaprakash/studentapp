package com.gegosoft.demoschool.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

public class NotificationReceiver  extends BroadcastReceiver {
    UserDetailsSharedPref userDetailsSharedPref;

    public static String ACTION_NOTIFICATION = "com.gegosoft.demoschool.NotificationReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {
        userDetailsSharedPref = userDetailsSharedPref.getInstance(context);
        RemoteMessage message = intent.getParcelableExtra("MessageReceived");
        JSONObject jsonObject = new JSONObject(message.getData());
        String type = null;
        try {
            type = jsonObject.getString("type");
            if (type.equalsIgnoreCase("event")){
                userDetailsSharedPref.saveBoolean("isEventNotification",true);
//                    userDetailsSharedPref.saveBoolean("isUpcomingEventUpdate",false);
//                    userDetailsSharedPref.saveBoolean("isPastEventUpdate",false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}