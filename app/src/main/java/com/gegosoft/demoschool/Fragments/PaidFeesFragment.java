package com.gegosoft.demoschool.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.PaidFeesModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaidFeesFragment extends Fragment {
    RecyclerView recyclerView;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Api api;
    TextView textnodata;
    ProgressDialogFragment progressDialogFragment;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerview,container,false);
        recyclerView=view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(getContext());
        textnodata=view.findViewById(R.id.textnodata);
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        if (CheckNetwork.isInternetAvailable(getContext())){
            getdata();
        }
        return view;
    }

    private void getdata(){
        Call<PaidFeesModel> paidFeesModelCall = api.getPaidFeeList(headermap,userDetailsSharedPref.getString("userId"));
        progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getActivity().getSupportFragmentManager(),"loading screen");
        paidFeesModelCall.enqueue(new Callback<PaidFeesModel>() {
            @Override
            public void onResponse(Call<PaidFeesModel> call, Response<PaidFeesModel> response) {
                if (response.isSuccessful()){
                    List<PaidFeesModel.Datum> data = response.body().getData();
                    if (data!=null&&data.size()!=0){
                        FeeAdapter feeAdapter = new FeeAdapter(data);
                        recyclerView.setAdapter(feeAdapter);

                    }
                    else {
                        textnodata.setVisibility(View.VISIBLE);
                    }


                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(getActivity());
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<PaidFeesModel> call, Throwable t) {

            }
        });
    }

    private class FeeAdapter extends RecyclerView.Adapter<FeeAdapter.MyViewHolder>{
        List<PaidFeesModel.Datum> data;
        public FeeAdapter(List<PaidFeesModel.Datum> data) {
            this.data = data;


        }

        @NonNull
        @Override
        public FeeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_paid_fee,parent,false);
            return new FeeAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull FeeAdapter.MyViewHolder holder, int position) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
            try {
                Date date = simpleDateFormat.parse(data.get(position).getDueDate());
                Calendar cal = Calendar.getInstance();
                String[] monthName = {"January", "February",
                        "March", "April", "May", "June", "July",
                        "August", "September", "October", "November",
                        "December"};

                cal.setTime(date);
                String monthname = monthName[cal.get(Calendar.MONTH)];
             //   holder.assigneddate.setText(String.valueOf(cal.get(Calendar.DATE)));
                holder.due_date.setText(cal.get(Calendar.DATE)+"\n"+monthname);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.amount.setText("Amount : "+data.get(position).getAmount());
        //    holder.due_date.setText(data.get(position).getDueDate());
            holder.paid_date.setText("Paid on : "+data.get(position).getPaidOn());
            holder.first_term.setText(data.get(position).getName()+" - "+data.get(position).getTerm());


        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView paid_date,due_date,amount,first_term;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                paid_date = itemView.findViewById(R.id.paid_date);
                due_date = itemView.findViewById(R.id.due_date);
                amount = itemView.findViewById(R.id.amount);
                first_term = itemView.findViewById(R.id.first_term);

            }
        }
    }
}
