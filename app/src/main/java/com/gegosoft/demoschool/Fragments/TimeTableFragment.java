package com.gegosoft.demoschool.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.Model.TimeTableModel;
import com.gegosoft.demoschool.R;

import java.util.List;

public class TimeTableFragment extends Fragment {
    List<TimeTableModel.Datum.Array> timeTableModels;
    String dayname;
    RecyclerView timetablerecyclerview;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.timetablefragment,null);
        dayname = getArguments().getString("dayname");
        timeTableModels = (List<TimeTableModel.Datum.Array>) getArguments().getSerializable(dayname);
        timetablerecyclerview = view.findViewById(R.id.timetablerecyclerview);
        timetablerecyclerview.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));
        timetablerecyclerview.setHasFixedSize(true);
        TimeTableAdapter timeTableAdapter = new TimeTableAdapter();
        timetablerecyclerview.setAdapter(timeTableAdapter);
        return view;

    }
    public class TimeTableAdapter extends RecyclerView.Adapter<TimeTableAdapter.MyViewHolder>{

        @NonNull
        @Override
        public TimeTableAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.timetable_mon_layout,parent,false);

            return new TimeTableAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull TimeTableAdapter.MyViewHolder holder, int position) {
            if (position == 3){
                holder.breaktext.setVisibility(View.VISIBLE);
            }
            else {
                holder.breaktext.setVisibility(View.GONE);

            }
            holder.subjectname.setText(timeTableModels.get(position).getSubjectId());
            holder.teachername.setText(timeTableModels.get(position).getTeacher());
            holder.starttime.setText(timeTableModels.get(position).getStartTime());
            holder.endtime.setText(timeTableModels.get(position).getEndTime());

        }

        @Override
        public int getItemCount() {
            return timeTableModels.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView breaktext,subjectname,teachername,starttime,endtime;
            ConstraintLayout firstcontainer;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                breaktext = itemView.findViewById(R.id.breaktext);
                subjectname= itemView.findViewById(R.id.subjectname);
                firstcontainer = itemView.findViewById(R.id.firstcontainer);
                teachername = itemView.findViewById(R.id.timetableteachername);
                starttime = itemView.findViewById(R.id.ttstarttime);
                endtime = itemView.findViewById(R.id.ttendtime);
            }
        }
    }
}
