package com.gegosoft.demoschool.Fragments;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.MainActivity;
import com.gegosoft.demoschool.Model.ClassNoticeBoardModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClassNoticefragment extends Fragment {
    RecyclerView recyclerView;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Api api;
    TextView textnodata;
    //    SchoolNoticefragment.SchoolNoticeAdapter schoolNoticeAdapter;
    List<ClassNoticeBoardModel.Datum> data;
    ClassNoticeAdapter classNoticeAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.classnotice_layout,null);
        recyclerView=view.findViewById(R.id.recyclerview);
        textnodata=view.findViewById(R.id.textnodata);
        recyclerView=view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(getContext());
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        if (CheckNetwork.isInternetAvailable(getContext())){
            getClassnotice();
        }


        return view;

    }
    private void getClassnotice(){
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getActivity().getSupportFragmentManager(),"loading screen");
        Call<ClassNoticeBoardModel> modelCall=api.getClassNotice(headermap,userDetailsSharedPref.getString("userId"));
        modelCall.enqueue(new Callback<ClassNoticeBoardModel>() {
            @Override
            public void onResponse(Call<ClassNoticeBoardModel> call, Response<ClassNoticeBoardModel> response) {
                if(response.isSuccessful()){
                    data = response.body().getData();
                    if (data.size()!=0){
                        textnodata.setVisibility(View.GONE);
                        classNoticeAdapter = new ClassNoticeAdapter(data);
                        recyclerView.setAdapter(classNoticeAdapter);
                    }
                    else
                    {
                        textnodata.setVisibility(View.VISIBLE);
                    }

                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<ClassNoticeBoardModel> call, Throwable t) {
                progressDialogFragment.dismiss();
            }
        });
    }
    private class ClassNoticeAdapter extends RecyclerView.Adapter<ClassNoticeAdapter.MyViewHolder>{
        List<ClassNoticeBoardModel.Datum> datumList;
        public ClassNoticeAdapter(List<ClassNoticeBoardModel.Datum> datumList) {
            this.datumList = datumList;

        }
        @NonNull
        @Override
        public ClassNoticeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custome_classnoticeboard,parent,false);
            return new ClassNoticeAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ClassNoticeAdapter.MyViewHolder holder, int position) {

//            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//            Date convertedDate = new Date();
//            try {
//                convertedDate = dateFormat.parse(datumList.get(position).getExpireDate());
//                SimpleDateFormat sdfnewformat = new SimpleDateFormat("MMM dd , yyyy");
//                String finalDateString = sdfnewformat.format(convertedDate);
//                holder.date.setText(finalDateString);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
            holder.title.setText(Html.fromHtml(datumList.get(position).getTitle()));
            holder.description.loadData(datumList.get(position).getDescription(),"text/html; chartset=UTF-8", null);

            // holder.description.setText(Html.fromHtml(datumList.get(position).getDescription()));
         //   holder.type.setText(datumList.get(position).getType());

//            holder.fromdate.setText("From"+datumList.get(position).getPublishDate());
//            holder.todate.setText("To "+datumList.get(position).getExpireDate());
             Glide.with(getContext()).load(data.get(position).getBackgroundimage()).into(holder.imageView);


        }

        @Override
        public int getItemCount() {
            return datumList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView title,date,type;
            ImageView imageView;
            WebView description;
            TextView fromdate,todate;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                title = itemView.findViewById(R.id.classtitle);
//                date = itemView.findViewById(R.id.date);
                description = itemView.findViewById(R.id.classdescription);
                description.getSettings().setJavaScriptEnabled(true);
                description.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                description.setBackgroundColor(0);
             //   type=itemView.findViewById(R.id.classnoticeboardtype);
                    imageView = itemView.findViewById(R.id.bg_img_2);
                    fromdate = itemView.findViewById(R.id.fromdate_notice);
                    todate = itemView.findViewById(R.id.todate_notice);

            }
        }
    }
}
