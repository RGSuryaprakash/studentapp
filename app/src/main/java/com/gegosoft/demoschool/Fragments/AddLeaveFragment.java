package com.gegosoft.demoschool.Fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.AddLeaveModel;
import com.gegosoft.demoschool.Model.ReasonListModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddLeaveFragment extends Fragment {
    EditText txt_fromdate,txt_todate;
    EditText edtremarks;
    Spinner spinnerreason;
    Api api;
    public static final String TAG = "just run";
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String, String> headermap;
    ProgressDialogFragment progressDialogFragment;

    Button submit;
    String date;
    List<ReasonListModel.Datum> reasonList;
    String fromdate, todate, reasonid, remarks;
    int leaveid;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_addleave, container, false);
        spinnerreason = view.findViewById(R.id.spinnerreason);
        spinnerreason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                reasonid = reasonList.get(position).getId().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        userDetailsSharedPref = userDetailsSharedPref.getInstance(getContext());
        headermap = new HashMap<>();
        headermap.put("Authorization", "Bearer " + userDetailsSharedPref.getString("token"));
        headermap.put("Accept", "application/json");
        txt_fromdate = view.findViewById(R.id.txt_fromdate);
        txt_todate = view.findViewById(R.id.txt_todate);

        edtremarks = view.findViewById(R.id.edtremarks);


        txt_fromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFromDatePickerDialog(v);
            }
        });
        txt_todate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openToDatePickerDialog(v);
            }
        });

       // leaveid.setText(String.valueOf(bundle.getString("email")));

        submit = view.findViewById(R.id.btnsubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()){
//                    submit.setEnabled(false);
                    if (leaveid!=0)
                    {
                        UpdateLeave();
                    }
                    else {
                        AddLeave();
                    }

                }

            }
        });
        api = ApiClient.getClient().create(Api.class);
        if (CheckNetwork.isInternetAvailable(getContext())){
            getReasonList();

        }

        return view;
    }



    private void getReasonList(){
        Call<ReasonListModel> leaveListModelCall = api.getReason(headermap);
        leaveListModelCall.enqueue(new Callback<ReasonListModel>() {
            @Override
            public void onResponse(Call<ReasonListModel> call, Response<ReasonListModel> response) {
                if (response.isSuccessful()) {
                    reasonList = response.body().getData();
                    for (int i = 0; i < reasonList.size(); i++) {
                        String title = reasonList.get(i).getTitle();
                        Log.d(TAG, "onResponse: " + title);
                    }
                    CustomAdapter customAdapter = new CustomAdapter(reasonList, getContext());
                    spinnerreason.setAdapter(customAdapter);
                }
            }
            @Override
            public void onFailure(Call<ReasonListModel> call, Throwable t) {
                AppUtils.APIFails(getActivity(), t);
            }
        });
    }

    private boolean validate() {
        remarks= edtremarks.getText().toString();
        todate = txt_todate.getText().toString();
        fromdate = txt_fromdate.getText().toString();
        if (fromdate.isEmpty()){
            txt_fromdate.setError("Chosse From Date");
            txt_fromdate.requestFocus();
            return false;
        }
        if (todate.isEmpty()){
            txt_todate.setError("Chosse To Date");
            txt_todate.requestFocus();
            return false;
        }

        if (remarks.isEmpty()){
            edtremarks.setError("Enter remarks");
            edtremarks.requestFocus();
            return false;
        }

        return true;
    }

    private void UpdateLeave() {
        api = ApiClient.getClient().create(Api.class);

        Call<ResponseBody> addLeaveModelCall = api.updateleave(headermap, fromdate, todate, reasonid, remarks,leaveid);
        progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getActivity().getSupportFragmentManager(),"please wait..");
        addLeaveModelCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    Log.d(TAG, "onResponse: " + response);
                    txt_fromdate.setText(null);
                    txt_todate.setText(null);
                    edtremarks.setText(null);

                    submit.setText("Add Leave");
                    Toast.makeText(getContext(),"Leave Updated Successfully",Toast.LENGTH_SHORT).show();

                }
                else {
                    submit.setEnabled(true);
                    if (response.code()==401){
                        AppUtils.SessionExpired(getActivity());
                    }
                    if (response.code()==422){
                        try {
                            String s = response.errorBody().string();
                            JSONObject jsonObject = new JSONObject(s);
                            String error = jsonObject.getString("errors");
                            JSONObject jsonObject1 = new JSONObject(error);
                            Iterator keys = jsonObject1.keys();
                            while (keys.hasNext()) {
                                Object key = keys.next();
                                JSONArray value = jsonObject1.getJSONArray((String) key);
                                String errormessage = value.getString(0);
                                if (key.equals("remarks")){
                                    edtremarks.setError(errormessage);
                                    edtremarks.requestFocus();
                                }
                                else if (key.equals("from_date")){
                                    txt_fromdate.setError(errormessage);
                                    txt_fromdate.requestFocus();
                                }
                                else if (key.equals("to_date")){
                                    txt_todate.setError(errormessage);
                                    txt_todate.requestFocus();
                                }
                                else if (key.equals("reason_id")){
                                    TextView errorText = (TextView)spinnerreason.getSelectedView();
                                    errorText.setError("");
                                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                                    errorText.setText(errormessage);//changes the selected item text to this
                                }
                            }


                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }

                    }


                }
                progressDialogFragment.dismiss();

            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                AppUtils.APIFails(getContext(), t);
                progressDialogFragment.dismiss();
            }
        });

    }
    private void AddLeave() {
        api = ApiClient.getClient().create(Api.class);

        Call<AddLeaveModel> addLeaveModelCall = api.AddLeave(headermap, fromdate, todate, reasonid, remarks, userDetailsSharedPref.getString("userId"));
        progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getActivity().getSupportFragmentManager(),"please wait..");
        addLeaveModelCall.enqueue(new Callback<AddLeaveModel>() {
            @Override
            public void onResponse(Call<AddLeaveModel> call, Response<AddLeaveModel> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onResponse: " + response);
                    txt_fromdate.setText(null);
                    txt_todate.setText(null);
                    edtremarks.setText(null);

                }
                else {
                    submit.setEnabled(true);
                    if (response.code()==401){
                        AppUtils.SessionExpired(getActivity());
                    }
                    if (response.code()==422){
                        try {
                            String s = response.errorBody().string();
                            JSONObject jsonObject = new JSONObject(s);
                            String error = jsonObject.getString("errors");
                            JSONObject jsonObject1 = new JSONObject(error);
                            Iterator keys = jsonObject1.keys();
                            while (keys.hasNext()) {
                                Object key = keys.next();
                                JSONArray value = jsonObject1.getJSONArray((String) key);
                                String errormessage = value.getString(0);
                                if (key.equals("remarks")){
                                    edtremarks.setError(errormessage);
                                    edtremarks.requestFocus();
                                }
                                else if (key.equals("from_date")){
                                    txt_fromdate.setError(errormessage);
                                    txt_fromdate.requestFocus();
                                }
                                else if (key.equals("to_date")){
                                    txt_todate.setError(errormessage);
                                    txt_todate.requestFocus();
                                }
                                else if (key.equals("reason_id")){
                                    TextView errorText = (TextView)spinnerreason.getSelectedView();
                                    errorText.setError("");
                                    errorText.setTextColor(Color.RED);//just to highlight that this is an error
                                    errorText.setText(errormessage);//changes the selected item text to this
                                }
                            }

                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }

                    }


                }
                progressDialogFragment.dismiss();

            }
            @Override
            public void onFailure(Call<AddLeaveModel> call, Throwable t) {
                AppUtils.APIFails(getContext(), t);
                progressDialogFragment.dismiss();
            }
        });
    }
    public void openFromDatePickerDialog(final View v) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        date = date + " " + hourOfDay + ":" + minute + ":" + "00";

                        txt_fromdate.setText(date);

                        fromdate = txt_fromdate.getText().toString();

                    }
                }, mHour, mMinute, false);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        timePickerDialog.show();
                        date = String.valueOf(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
        // Launch Time Picker Dialog
    }
    public void openToDatePickerDialog(final View v) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        date = date + " " + hourOfDay + ":" + minute + ":" + "00";
                        txt_todate.setText(date);
                        todate = txt_todate.getText().toString();

                    }
                }, mHour, mMinute, false);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        timePickerDialog.show();
                        date = String.valueOf(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
        // Launch Time Picker Dialog
    }

    public void displayReceivedData(Bundle bundle) {
        if (bundle!=null){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss a");
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            try {
                Date convertedfromdate = simpleDateFormat.parse(bundle.getString("ed_fromdate"));
                Date convertedtodate = simpleDateFormat.parse(bundle.getString("ed_todate"));
                fromdate  = simpleDateFormat1.format(convertedfromdate);
                todate  = simpleDateFormat1.format(convertedtodate);

                txt_fromdate.setText(fromdate);
                txt_todate.setText(todate);
                edtremarks.setText(bundle.getString("ed_remarks"));
                leaveid = bundle.getInt("leaveid");
                submit.setText("Update");
            } catch (ParseException e) {
                e.printStackTrace();
            }


        }
    }

    public class CustomAdapter extends BaseAdapter {
        Context context;
        LayoutInflater layoutInflater;
        List<ReasonListModel.Datum> data;
        public CustomAdapter(List<ReasonListModel.Datum> data, Context context1) {
            this.data = data;
            this.context = context1;
            layoutInflater = (LayoutInflater.from(context));
        }
        @Override
        public int getCount() {
            return data.size();
        }
        @Override
        public Object getItem(int i) {
            return null;
        }
        @Override
        public long getItemId(int i) {
            return 0;
        }
        @Override
        public View getView(int i, View view, ViewGroup parent) {
            if (view == null)
                view = layoutInflater.inflate(R.layout.spinner_layout, null);
            TextView spinnertext = (TextView) view.findViewById(R.id.spinnertext);
            spinnertext.setText((CharSequence) data.get(i).getTitle());

            return view;
        }
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (leaveid!=0){
                leaveid = 0;
                getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            }

        }
    }
}
