package com.gegosoft.demoschool.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.Activity.PDFWebViewActivity;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.AssignmentModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompletedAssignment extends Fragment {
    RecyclerView recyclerView;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Api api;
    TextView textnodata;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerview,container,false);
        recyclerView=view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(getContext());
        textnodata = view.findViewById(R.id.textnodata);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        if (CheckNetwork.isInternetAvailable(getContext())){
            getdata();
        }
        return view;
    }
    private void getdata(){
        api = ApiClient.getClient().create(Api.class);
        Call<AssignmentModel> assignmentModelCall = api.getCompletedAssignment(headermap,userDetailsSharedPref.getString("userId"));
        assignmentModelCall.enqueue(new Callback<AssignmentModel>() {
            @Override
            public void onResponse(Call<AssignmentModel> call, Response<AssignmentModel> response) {
                if (response.isSuccessful()){
                    List<AssignmentModel.Datum> data = response.body().getData();
                    if (data.size()!=0){
                        CompletedAssigmentAdapter completedAssigmentAdapter = new CompletedAssigmentAdapter(data);
                        recyclerView.setAdapter(completedAssigmentAdapter);
                    }
                    else {
                        textnodata.setVisibility(View.VISIBLE);
                    }
                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<AssignmentModel> call, Throwable t) {
                AppUtils.APIFails(getActivity(),t);
            }
        });

    }
    private class CompletedAssigmentAdapter extends RecyclerView.Adapter<CompletedAssigmentAdapter.MyViewHolder>{
        List<AssignmentModel.Datum> data;
        public CompletedAssigmentAdapter(List<AssignmentModel.Datum> data) {

            this.data = data;
        }

        @NonNull
        @Override
        public CompletedAssigmentAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customongoinassignment,parent,false);
            return new CompletedAssigmentAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CompletedAssigmentAdapter.MyViewHolder holder, int position) {
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d MMM yyyy");
//            try {
//                Date date = simpleDateFormat.parse(data.get(position).getAssignedDate());
//                Calendar cal = Calendar.getInstance();
//                String[] monthName = {"January", "February",
//                        "March", "April", "May", "June", "July",
//                        "August", "September", "October", "November",
//                        "December"};
//                String monthname = monthName[cal.get(Calendar.MONTH)];
//                cal.setTime(date);
//                holder.assigneddate.setText(String.valueOf(cal.get(Calendar.DATE)));
//                holder.assignedmonth.setText(monthname);
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }

            if (data.get(position).getAttachment()!=null){
                holder.attachment.setVisibility(View.VISIBLE);
            }
            holder.title.setText(data.get(position).getTitle());
            holder.description.setText(data.get(position).getDescription());
            holder.submissiondate.setText(data.get(position).getSubmissionDate());
            holder.startdate.setText(data.get(position).getAssignedDate());
            holder.marks.setText("Marks "+data.get(position).getMarks().toString());
            holder.attachment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle extras = new Bundle();

                    extras.putString("url", String.valueOf(data.get(position).getAttachment()));
                    startActivity(new Intent(getContext(), PDFWebViewActivity.class).putExtras(extras));
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView startdate,submissiondate,title,description,marks;
            ImageView attachment;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                startdate = itemView.findViewById(R.id.startdate);
                submissiondate = itemView.findViewById(R.id.submissiondate);
                title = itemView.findViewById(R.id.title);
                description = itemView.findViewById(R.id.description);
                attachment = itemView.findViewById(R.id.attachment);
                marks = itemView.findViewById(R.id.marks);

            }
        }
    }
}
