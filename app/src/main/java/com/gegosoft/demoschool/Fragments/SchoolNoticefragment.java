package com.gegosoft.demoschool.Fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.MainActivity;
import com.gegosoft.demoschool.Model.SchoolNoticeBoardModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolNoticefragment extends Fragment implements Html.ImageGetter {
    RecyclerView recyclerView;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String, String> headermap;
    Api api;
    TextView textnodata;
    SchoolNoticeAdapter schoolNoticeAdapter;
    List<SchoolNoticeBoardModel.Datum> data;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.schoolnotice_layout, null);
        recyclerView = view.findViewById(R.id.recyclerview);
        textnodata = view.findViewById(R.id.textnodata);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(getContext());
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization", "Bearer " + userDetailsSharedPref.getString("token"));
        headermap.put("Accept", "application/json");
        if (CheckNetwork.isInternetAvailable(getContext())) {
            getSchoolnotice();
        }


        return view;

    }

    private void getSchoolnotice() {
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message", "Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getActivity().getSupportFragmentManager(), "loading screen");
        Call<SchoolNoticeBoardModel> modelCall = api.getSchoolNotice(headermap);
        modelCall.enqueue(new Callback<SchoolNoticeBoardModel>() {
            @Override
            public void onResponse(Call<SchoolNoticeBoardModel> call, Response<SchoolNoticeBoardModel> response) {
                if (response.isSuccessful()) {
                    data = response.body().getData();
                    if (data.size() != 0) {
                        schoolNoticeAdapter = new SchoolNoticeAdapter(data);
                        recyclerView.setAdapter(schoolNoticeAdapter);
                    } else {
                        textnodata.setVisibility(View.VISIBLE);
                    }

                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<SchoolNoticeBoardModel> call, Throwable t) {
                progressDialogFragment.dismiss();
            }
        });
    }

    @Override
    public Drawable getDrawable(String source) {
        LevelListDrawable d = new LevelListDrawable();
        Drawable empty = getResources().getDrawable(R.drawable.profile);
        d.addLevel(0, 0, empty);
        d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

//        new LoadImage().execute(source, d);

        return d;
    }


    public class SchoolNoticeAdapter extends RecyclerView.Adapter<SchoolNoticeAdapter.MyViewHolder> implements Html.ImageGetter {
        List<SchoolNoticeBoardModel.Datum> datumList;

        public SchoolNoticeAdapter(List<SchoolNoticeBoardModel.Datum> datumList) {
            this.datumList = datumList;

        }

        @NonNull
        @Override
        public SchoolNoticeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custome_schoolnoticeboard, parent, false);
            return new SchoolNoticeAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull SchoolNoticeAdapter.MyViewHolder holder, int position) {

//            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//            Date convertedDate = new Date();
//            try {
//                convertedDate = dateFormat.parse(datumList.get(position).getExpireDate());
//                SimpleDateFormat sdfnewformat = new SimpleDateFormat("MMM dd , yyyy");
//                String finalDateString = sdfnewformat.format(convertedDate);
//                holder.date.setText(finalDateString);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }

            //        Spannable html;

//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//                html = (Spannable) Html.fromHtml(getContext(), Html.FROM_HTML_MODE_LEGACY, holder.description.setText(datumList.get(position).getDescription()),null);
//            } else {
//                html = (Spannable) Html.fromHtml(getContext(), imageGetter, null);
//            }
//            holder.description.setText(html);

//            Spanned spanned = Html.fromHtml(datumList.get(position).getDescription(), this, null);
//
//           holder.description.setText(spanned);

//            holder.description.setText(Html.fromHtml(datumList.get(position).getDescription(), new Html.ImageGetter() {
//
//                @Override
//                public Drawable getDrawable(String source) {
//                    int resourceId = getResources().getIdentifier(source, "drawable",get());
//                    Drawable drawable = getResources().getDrawable(resourceId);
//                    drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
//                    return drawable;
//                }
//            }, null));


//holder.description.setText(Html.fromHtml(datumList.get(position).getDescription(), new Html.ImageGetter() {
//    @Override
//    public Drawable getDrawable(String source) {
//        Drawable drawable = null;
//        drawable = Drawable.createFromPath(datumList.get(position).getDescription());  // Or fetch it from the URL
//        // Important
//        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable
//                .getIntrinsicHeight());
//        return drawable;
//    }
//},null));
// Html.ImageGetter imageGetter = null;

//            Spannable html;
//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//                html = (Spannable) Html.fromHtml(datumList.get(position).getDescription(), Html.FROM_HTML_MODE_LEGACY, imageGetter, null);
//            } else {
//                html = (Spannable) Html.fromHtml(datumList.get(position).getDescription(), imageGetter, null);
//            }
//            holder.description.setText(html);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                holder.description.setText(Html.fromHtml(datumList.get(position).getDescription(), Html.FROM_HTML_MODE_LEGACY, new Html.ImageGetter() {
//                    @Override
//                    public Drawable getDrawable(String source) {
//                        source = datumList.get(position).getDescription();
//                Picasso.get().
//                        load(source)
//                        .placeholder(R.drawable.profile)
//                        .into((Target) holder.description);
//                return Drawable.createFromPath(source);
//                    }
//                },null));
//            }
//            else
//                holder.description.setText(Html.fromHtml(datumList.get(position).getDescription()));
//            holder.description.setText(Html.fromHtml(datumList.get(position).getDescription(), new Html.ImageGetter() {
//                @Override public Drawable getDrawable(String source) {
//                    Drawable drawFromPath;
//                    String path = (datumList.get(position).getBackgroundimage());
//                    drawFromPath =this.getDrawable(path);
//                    drawFromPath.setBounds(0, 0, drawFromPath.getIntrinsicWidth(), drawFromPath.getIntrinsicHeight());
//                    return drawFromPath;
//                }
//            }, null));so
            String source = datumList.get(position).getDescription();
            holder.title.setText(datumList.get(position).getTitle());
           // holder.description.setText(Html.fromHtml(datumList.get(position).getDescription()));
            holder.type.setText(datumList.get(position).getType());
         //   Spanned spanned = Html.fromHtml(source, this, null);
        //    holder.description.setText(spanned);
            holder.description.loadData(datumList.get(position).getDescription(),"text/html; chartset=UTF-8", null);
            Glide.with(getContext()).load(data.get(position).getBackgroundimage()).into(holder.backgroundimage);

        }

        @Override
        public int getItemCount() {
            return datumList.size();
        }



        @Override
        public Drawable getDrawable(String source) {
            LevelListDrawable d = new LevelListDrawable();
            Drawable empty = getResources().getDrawable(R.drawable.profile);
            d.addLevel(0, 0, empty);
            d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

            new LoadImage().execute(source, d);

            return d;
        }


//        public class PicassoImageGetter implements Html.ImageGetter {
//
//            private TextView textView = null;
//
//            public PicassoImageGetter() {
//
//            }
//
//            public PicassoImageGetter(TextView target) {
//                textView = target;
//            }
//
//            @Override
//            public Drawable getDrawable(String source) {
//                BitmapDrawablePlaceHolder drawable = new BitmapDrawablePlaceHolder();
//                Picasso.get().
//                        load(datumList.get(0).getDescription())
//                        .placeholder(R.drawable.profile)
//                        .into(drawable);
//                return drawable;
//            }
//
//            private class BitmapDrawablePlaceHolder extends BitmapDrawable implements Target {
//
//                protected Drawable drawable;
//
//                @Override
//                public void draw(final Canvas canvas) {
//                    if (drawable != null) {
//                        drawable.draw(canvas);
//                    }
//                }
//
//                public void setDrawable(Drawable drawable) {
//                    this.drawable = drawable;
//                    int width = drawable.getIntrinsicWidth();
//                    int height = drawable.getIntrinsicHeight();
//                    drawable.setBounds(0, 0, width, height);
//                    setBounds(0, 0, width, height);
//                    if (textView != null) {
//                        textView.setText(textView.getText());
//                    }
//                }
//
//                @Override
//                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                    setDrawable(new BitmapDrawable(getActivity().getResources(), bitmap));
//                }
//
//                @Override
//                public void onBitmapFailed(Exception e, Drawable errorDrawable) {
//
//                }
//
//
//                @Override
//                public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                }
//
//            }
//        }
//        @Override
//        public Drawable getDrawable(String source) {
//            LevelListDrawable d = new LevelListDrawable();
//            Drawable empty = getResources().getDrawable(R.drawable.profile);
//            d.addLevel(0, 0, empty);
//            d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());
//
//            new LoadImage().execute(source, d);
//
//            return d;
//        }
//
//
//       public class LoadImage extends AsyncTask<Object, Void, Bitmap> {
//
//            private LevelListDrawable mDrawable;
//
//            @Override
//            protected Bitmap doInBackground(Object... params) {
//                String source = (String) params[0];
//                mDrawable = (LevelListDrawable) params[1];
//                Log.d("tag", "doInBackground " + source);
//                try {
//                    InputStream is = new URL(source).openStream();
//                    return BitmapFactory.decodeStream(is);
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (MalformedURLException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(Bitmap bitmap) {
//                Log.d("TAG", "onPostExecute drawable " + mDrawable);
//                Log.d("TAG", "onPostExecute bitmap " + bitmap);
//                if (bitmap != null) {
//                    BitmapDrawable d = new BitmapDrawable(bitmap);
//                    mDrawable.addLevel(1, 1, d);
//                    mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
//                    mDrawable.setLevel(1);
//                    // i don't know yet a better way to refresh TextView
//                    // mTv.invalidate() doesn't work as expected
//                    CharSequence t = .getText();
//                    mTv.setText(t);
//                }
//            }
//        }
//
//
//


            public class MyViewHolder extends RecyclerView.ViewHolder {
              private TextView title, date, type;
              WebView description;
              ImageView backgroundimage;

                public MyViewHolder(@NonNull View itemView) {
                    super(itemView);
                    title = itemView.findViewById(R.id.title);
                    date = itemView.findViewById(R.id.date);
                    description = itemView.findViewById(R.id.description);
                    description.getSettings().setJavaScriptEnabled(true);
                    description.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                    description.setBackgroundColor(0);
                    type = itemView.findViewById(R.id.noticeboardtype);
                    backgroundimage = itemView.findViewById(R.id.bg_img);

                }


            }


        private class LoadImage extends AsyncTask<Object, Void, Bitmap> {

            private LevelListDrawable mDrawable;

            @Override
            protected Bitmap doInBackground(Object... params) {
                String source = (String) params[0];
                mDrawable = (LevelListDrawable) params[1];
                Log.d("TAG", "doInBackground " + source);
                try {
                    InputStream is = new URL(source).openStream();
                    return BitmapFactory.decodeStream(is);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                Log.d("TAG", "onPostExecute drawable " + mDrawable);
                Log.d("TAG", "onPostExecute bitmap " + bitmap);
                if (bitmap != null) {
                    BitmapDrawable d = new BitmapDrawable(bitmap);
                    mDrawable.addLevel(1, 1, d);
                    mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
                    mDrawable.setLevel(1);
                    // i don't know yet a better way to refresh TextView
                    // mTv.invalidate() doesn't work as expected
//                    CharSequence t = mTv.getText();
//                    mTv.setText(t);
                }
            }
        }
    }

    }


