package com.gegosoft.demoschool.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.gegosoft.demoschool.Activity.LoginActivity;
import com.gegosoft.demoschool.MainActivity;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreen extends Fragment {
    Timer RunSplash = new Timer();
    UserDetailsSharedPref userDetailsSharedPref;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.splashscreen,container,false);
        userDetailsSharedPref = userDetailsSharedPref.getInstance(getContext());
        TimerTask ShowSplash = new TimerTask() {

            @Override
            public void run() {
                if (userDetailsSharedPref.isLoggedIn()){
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                        getActivity(). overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                }
                else {

                    if (getActivity()!=null){
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                        getActivity(). overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

                    }
                }
//            Intent intent = new Intent(getActivity(), SignUpActivity.class);
//            startActivity(intent);
//            getActivity().finish();
//            getActivity(). overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);


//stuff that updates ui

            }

        };

        // Start the timer
        RunSplash.schedule(ShowSplash, 3000);

        return view;
    }
    private void launchHomeScreen() {

        startActivity(new Intent(getContext(), MainActivity.class));

    }
}