package com.gegosoft.demoschool.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.AllNotificationModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllNotificationFragment extends Fragment {
    RecyclerView recyclerView;
    TextView textnodata;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Api api;
 MyAdapter myAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.allnotification_layout,null);
        recyclerView=view.findViewById(R.id.allnotification_recyclerview);
        textnodata = view.findViewById(R.id.textnodata);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(getContext());
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        if (CheckNetwork.isInternetAvailable(getContext())){
            getdata();
        }

        return view;
    }
    private void getdata(){
        api= ApiClient.getClient().create(Api.class);
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getActivity().getSupportFragmentManager(),"loading screen");
        Call<AllNotificationModel> messageModelCall = api.getallnotification(headermap,userDetailsSharedPref.getString("userId"));
//        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString("message","Loading");
//        progressDialogFragment.setArguments(bundle);
//
//        progressDialogFragment.show(getFragmentManager(),"loading screen");
        messageModelCall.enqueue(new Callback<AllNotificationModel>() {
            @Override
            public void onResponse(Call<AllNotificationModel> call, Response<AllNotificationModel> response) {
                if (response.isSuccessful()){
                    List<AllNotificationModel.Datum> data =response.body().getData();
                    if (data.size()!=0){
                        myAdapter=new MyAdapter(data);
                        recyclerView.setAdapter(myAdapter);
                    }
                    else {
                        textnodata.setVisibility(View.VISIBLE);
                    }

                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(getActivity());
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<AllNotificationModel> call, Throwable t) {
                AppUtils.APIFails(getActivity(),t);
                progressDialogFragment.dismiss();
            }
        });

    }
    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{
        List<AllNotificationModel.Datum> data;
        // int[] drawable = {R.drawable.green,R.drawable.pink,R.drawable.red,R.drawable.skyblue,R.drawable.violet,R.drawable.yellow};
        public MyAdapter(List<AllNotificationModel.Datum> data){
            this.data=data;
        }

        @NonNull
        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.custome_allnotification,parent,false);
            return new MyAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
//            int background =new Random().nextInt(drawable.length);
//            holder.icons.setImageResource(drawable[background]);
//            holder.heading.setText(data.get(position).getSubject());
            holder.subheading.setText(data.get(position).getDataMessage());
            holder.time.setText(data.get(position).getCreatedAt());
//            holder.time.setText(data.get(position).getSentAt());
//            if (data.get(position).getReadStatus()==0){
//                holder.indicator.setVisibility(View.VISIBLE);
//            }
//            else {
//                holder.indicator.setVisibility(View.GONE);
//            }
//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    holder.subheading.setText(data.get(position).getMessage());
//                    holder.subheading.setMaxLines(Integer.MAX_VALUE);
//                    holder.subheading.setTextIsSelectable(true);
////                    if (data.get(position).getReadStatus()==0){
////                        if (CheckNetwork.isInternetAvailable(NotificationActivity.this)){
////                            UpdateReadStatus(data.get(position).getId(),data.get(position));
////                        }
////
////                    }
//
//
//                }
//            });
//            holder.subheading.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    ClipboardManager cm = (ClipboardManager)getBaseContext().getSystemService(Context.CLIPBOARD_SERVICE);
//                    cm.setText(holder.subheading.getText());
//                    Toast.makeText(getBaseContext(), "Copied to clipboard", Toast.LENGTH_SHORT).show();
//                }
//            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView icons;
            TextView heading,subheading,time,indicator;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
//                icons=itemView.findViewById(R.id.circularimage);
//                heading=itemView.findViewById(R.id.heading);
                subheading=itemView.findViewById(R.id.subheading);
                time=itemView.findViewById(R.id.notificationtime);
//                time=itemView.findViewById(R.id.notificationtime);
//                indicator=itemView.findViewById(R.id.indicator);

            }
        }
    }


}
