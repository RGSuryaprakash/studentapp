package com.gegosoft.demoschool.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.AppliedLeaveListModel;
import com.gegosoft.demoschool.Model.DeleteLeaveModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyLeavesFragment extends Fragment {
    public static final String TAG = "just run";
    RecyclerView recyclerView;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String, String> headermap;
    Api api;
    List<AppliedLeaveListModel.Datum> leaveListModels;
//    List<EditLeaveModel.EditLeave> editLeaves;
    String ed_fromdate, ed_todate, ed_remarks, ed_reason;
//    EditLeaveFragment editLeaveFragment;
    public static final int message_code = 01;
    LeaveAdapter recyclerAdapter;
    MyLeavesFragment MyLeavesFragment;
    int leaveid;
    SendMessage SM;
    TextView nodata;
    public interface SendMessage {
        void sendData(Bundle message);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerview, container, false);
        recyclerView = view.findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
     //   recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
        userDetailsSharedPref = userDetailsSharedPref.getInstance(getContext());
        headermap = new HashMap<>();
        headermap.put("Authorization", "Bearer " + userDetailsSharedPref.getString("token"));
        headermap.put("Accept", "application/json");
        api = ApiClient.getClient().create(Api.class);
        nodata=view.findViewById(R.id.textnodata);
        myLeaves();
        return view;
    }
    private void myLeaves()
    {
        Call<AppliedLeaveListModel> appliedLeaveListModelCall = api.getAppliedLeaveListModelCall(headermap, userDetailsSharedPref.getString("userId"));
        appliedLeaveListModelCall.enqueue(new Callback<AppliedLeaveListModel>() {
            @Override
            public void onResponse(Call<AppliedLeaveListModel> call, Response<AppliedLeaveListModel> response) {
                if (response.isSuccessful()) {
                    leaveListModels = response.body().getData();
                    if (leaveListModels!=null&&leaveListModels.size()!=0){
                        recyclerAdapter = new LeaveAdapter(leaveListModels, getContext());
                        recyclerView.setAdapter(recyclerAdapter);
                        nodata.setVisibility(View.GONE);
                    }
                    else {
                        nodata.setVisibility(View.VISIBLE);
                    }

                }else
                    Toast.makeText(getContext(), "adapter_failed", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(Call<AppliedLeaveListModel> call, Throwable t) {
                Log.e(TAG, "surya" + t.getLocalizedMessage());
            }
        });
    }
    public class LeaveAdapter extends RecyclerView.Adapter<LeaveAdapter.ViewHolder> {
        private List<AppliedLeaveListModel.Datum> data;
        private Context context;
        public LeaveAdapter(List<AppliedLeaveListModel.Datum> data, Context context) {
            this.data = data;
            this.context = context;
        }
        @NonNull
        @Override
        public LeaveAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_my_leaves, parent, false);
            return new LeaveAdapter.ViewHolder(view);
        }
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            AppliedLeaveListModel.Datum appliedLeaveListModel = data.get(position);
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DeleteleaveAlertDialog(position,data);


                    //  deletemethodcall();
                }
            });


            if (data.get(position).getStatus().contains("Approved")  )
            {
                holder.update.setVisibility(View.GONE);
            }else {
                holder.update.setVisibility(View.VISIBLE);
            }
            if (data.get(position).getStatus().contains("Cancelled"))
            {
                holder.update.setVisibility(View.GONE);

            }
            else {
                holder.update.setVisibility(View.VISIBLE);

            }



            holder.approval_by.setText(String.valueOf(appliedLeaveListModel.getApprovedBy()));
            holder.approval_on.setText(String.valueOf(appliedLeaveListModel.getApprovedOn()));
            holder.comments.setText(String.valueOf(appliedLeaveListModel.getComments()));
            holder.reason.setText(String.valueOf(appliedLeaveListModel.getReason()));
            holder.status.setText(String.valueOf(appliedLeaveListModel.getStatus()));
            holder.fromdate.setText(String.valueOf(appliedLeaveListModel.getFromDate()));
            holder.todate.setText(String.valueOf(appliedLeaveListModel.getToDate()));
            holder.remarks.setText(String.valueOf(appliedLeaveListModel.getRemarks()));
//            }
//            else
//            {
//
//                holder.reason.setText(String.valueOf(appliedLeaveListModel.getReason()));
//                holder.status.setText(String.valueOf(appliedLeaveListModel.getStatus()));
//                holder.fromdate.setText(String.valueOf(appliedLeaveListModel.getFromDate()));
//                holder.todate.setText(String.valueOf(appliedLeaveListModel.getToDate()));
//                holder.remarks.setText(String.valueOf(appliedLeaveListModel.getRemarks()));
//             }
            holder.update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ed_fromdate = holder.fromdate.getText().toString();
                    ed_todate = holder.todate.getText().toString();
                    ed_remarks = holder.remarks.getText().toString();


                    Bundle  bundle  = new Bundle();


                    bundle.putString("ed_fromdate",data.get(position).getFromDate());
                    bundle.putString("ed_todate",data.get(position).getToDate());
                    bundle.putString("ed_remarks",data.get(position).getRemarks());
                    bundle.putInt("leaveid",data.get(position).getId());
                    SM.sendData(bundle);
//                    ((LeaveActivity)getActivity()).editLeave(true,bundle);

                }
            });
        }
        @Override
        public int getItemCount() {
            return data.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView fromdate, todate, remarks, status, reason, approval_on, approval_by, comments;
            TextView delete, update;
            TextView fromdate_s, todate_s, remarks_s, stats_s, approval_on_s, approval_by_s, comments_s;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                fromdate = itemView.findViewById(R.id.fromdate_display);
                todate = itemView.findViewById(R.id.todate_display);
                remarks = itemView.findViewById(R.id.remarks_display);
                status = itemView.findViewById(R.id.status_shown);
                reason = itemView.findViewById(R.id.reasons_shown);
                approval_on = itemView.findViewById(R.id.approval_on_display);
                approval_by = itemView.findViewById(R.id.approval_by_display);
                comments = itemView.findViewById(R.id.comments_display);
                fromdate_s = itemView.findViewById(R.id.from_date_shown);
                todate_s = itemView.findViewById(R.id.to_date_shown);
                remarks_s = itemView.findViewById(R.id.remarks_shown);
                stats_s = itemView.findViewById(R.id.status_shown);
                approval_on_s = itemView.findViewById(R.id.approval_on_shown);
                approval_by_s = itemView.findViewById(R.id.approval_by_shown);
                comments_s = itemView.findViewById(R.id.comments_shown);
                delete = itemView.findViewById(R.id.deleteimg);
                update = itemView.findViewById(R.id.editimg);
            }
        }
    }

    private void DeleteleaveAlertDialog(int position, List<AppliedLeaveListModel.Datum> data) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());


                //Uncomment the below code to Set the message and title from the strings.xml file

                //Setting message manually and performing action on button click
                builder.setMessage("Do you want to cancel your leave request?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                Call<DeleteLeaveModel> deleteLeaveModelCall = api.getdDeleteLeaveModelCall(headermap, data.get(position).getId());
                                deleteLeaveModelCall.enqueue(new Callback<DeleteLeaveModel>() {
                                    @Override
                                    public void onResponse(Call<DeleteLeaveModel> call, Response<DeleteLeaveModel> response) {
                                        if (response.isSuccessful()) {
                                            data.remove(position);
                                            recyclerAdapter.notifyDataSetChanged();
                                            Toast.makeText(getContext(),"Leave Deleted",Toast.LENGTH_SHORT).show();
//
                                        }
                                    }
                                    @Override
                                    public void onFailure(Call<DeleteLeaveModel> call, Throwable t) {
                                        Toast.makeText(getContext(), "error", Toast.LENGTH_SHORT).show();
                                        //     Log.e(TAG, "onFailure: ",t.getLocalizedMessage());
                                    }
                                });

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();

                            }
                        });
                //Creating dialog box
                AlertDialog alert = builder.create();
                //Setting the title manually
                alert.show();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==message_code&&resultCode== Activity.RESULT_OK)
        {
            myLeaves();
        }
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

                getFragmentManager().beginTransaction().detach(this).attach(this).commit();


        }
    }
}