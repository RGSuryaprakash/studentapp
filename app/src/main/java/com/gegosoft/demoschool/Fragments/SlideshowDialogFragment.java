package com.gegosoft.demoschool.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gegosoft.demoschool.Model.GalleryModel;
import com.gegosoft.demoschool.R;

import org.json.JSONObject;

import java.util.List;

public class SlideshowDialogFragment  extends DialogFragment  {
    private String TAG = SlideshowDialogFragment.class.getSimpleName();
    List<GalleryModel.Datum.Eventgallery> eventimages;
   List<String> imageList ;
    //    List<GalleryDetailsModel.Datum> galleryimages;
    private ViewPager viewPager;

    private int selectedPosition = 0;
    ImageView backimage ;
    String from;
    public static SlideshowDialogFragment newInstance() {
        SlideshowDialogFragment f = new SlideshowDialogFragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_image_slider, container, false);
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        backimage =v.findViewById(R.id.backimage);
        from = getArguments().getString("from");
        if (from.equalsIgnoreCase("EventGallery")){

            eventimages = ( List<GalleryModel.Datum.Eventgallery> ) getArguments().getSerializable("images");
            EventGalleryViewPagerAdapter eventGalleryViewPagerAdapter = new EventGalleryViewPagerAdapter(getContext(),eventimages);
            viewPager.setAdapter(eventGalleryViewPagerAdapter);

            selectedPosition = getArguments().getInt("position");
            viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
            setCurrentItem(selectedPosition);
        }
        else if (from.equalsIgnoreCase("homeWork")){
            imageList = (List<String>) getArguments().getSerializable("images");
            HomeWorkAttachmentAdapter eventGalleryViewPagerAdapter = new HomeWorkAttachmentAdapter(getContext(),imageList);
            viewPager.setAdapter(eventGalleryViewPagerAdapter);

            selectedPosition = getArguments().getInt("position");
            viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
            setCurrentItem(selectedPosition);
        }
//        else if (from.equalsIgnoreCase("Gallery")){
//            galleryimages = ( List<GalleryDetailsModel.Datum> ) getArguments().getSerializable("images");
//            GalleryViewPagerAdapter galleryViewPagerAdapter = new GalleryViewPagerAdapter(getContext(),galleryimages);
//            viewPager.setAdapter(galleryViewPagerAdapter);
//
//            selectedPosition = getArguments().getInt("position");
//            viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
//            setCurrentItem(selectedPosition);
//        }




//        Log.e(TAG, "position: " + selectedPosition);
//        Log.e(TAG, "images size: " + images.size());





        backimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(SlideshowDialogFragment.this).commit();
                //getActivity().onBackPressed();
            }
        });
        return v;
    }

    private void setCurrentItem(int position) {
        viewPager.setCurrentItem(position, false);
        displayMetaInfo(selectedPosition);
    }

    //  page change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            displayMetaInfo(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void displayMetaInfo(int position) {
//        lblCount.setText((position + 1) + " of " + images.length());

//        String image = eventimages.get(position).getPath();
//        lblTitle.setText(image.getName());
//        lblDate.setText(image.getTimestamp());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }



    public class EventGalleryViewPagerAdapter extends PagerAdapter {

        private LayoutInflater layoutInflater;
        Context context;
        List<GalleryModel.Datum.Eventgallery> eventimages;
        public EventGalleryViewPagerAdapter(Context context, List<GalleryModel.Datum.Eventgallery> eventimages) {
            this.context = context;
            this.eventimages = eventimages;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.image_fullscreen_preview, container, false);

            ImageView imageViewPreview = (ImageView) view.findViewById(R.id.image_preview);
            TextView date = view.findViewById(R.id.date);

//            Image image = images.get(position);

            Glide.with(context).load(eventimages.get(position).getPath())
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageViewPreview);

            date.setText(eventimages.get(position).getUpdatedAt());

            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return eventimages.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == ((View) obj);
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    public class HomeWorkAttachmentAdapter extends PagerAdapter {

        private LayoutInflater layoutInflater;
        Context context;
        List<String> images;
        public HomeWorkAttachmentAdapter(Context context, List<String> images) {
            this.context = context;
            this.images = images;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.image_fullscreen_preview, container, false);

            ImageView imageViewPreview = (ImageView) view.findViewById(R.id.image_preview);
            TextView date = view.findViewById(R.id.date);

//            Image image = images.get(position);

            Glide.with(context).load(images.get(position))
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageViewPreview);


            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == ((View) obj);
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

}
