package com.gegosoft.demoschool.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.ExamsModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpcomingExamFragment extends Fragment {
    RecyclerView recyclerview;
    List<ExamsModel.Datum> datumList;
    Api api;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerview,container,false);
        recyclerview = view.findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false));
        recyclerview.setHasFixedSize(true);
        //for expanding purpore make recyclerview height to match parent
        ViewGroup.LayoutParams params=recyclerview.getLayoutParams();
        params.height= ViewGroup.LayoutParams.MATCH_PARENT;
        recyclerview.setLayoutParams(params);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(getContext());
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        if (CheckNetwork.isInternetAvailable(getContext())){
            getdata();
        }
        return view;
    }
    private void getdata(){
        Api api = ApiClient.getClient().create(Api.class);
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getActivity().getSupportFragmentManager(),"loading screen");
        Call<ExamsModel> examsModelCall = api.getExamModel(headermap,ApiClient.getClient().baseUrl()+"api/v2/exams/upcoming/"+userDetailsSharedPref.getString("userId"));
        examsModelCall.enqueue(new Callback<ExamsModel>() {
            @Override
            public void onResponse(Call<ExamsModel> call, Response<ExamsModel> response) {
                if (response.isSuccessful()){
                    datumList = response.body().getData();

//                    UpcomingExamsAdapter upcomingExamsAdapter = new UpcomingExamsAdapter(datumList);
//                    recyclerview.setAdapter(upcomingExamsAdapter);
                    List<Exam> examArrayList = new ArrayList<>();
                    for (int i= 0 ; i<datumList.size();i++){
                        examArrayList.add(new Exam(datumList.get(i).getName(),datumList.get(i).getSchedule()));
                    }
                    UpcomingExamAdapter genreAdapter = new UpcomingExamAdapter(examArrayList);
                    recyclerview.setAdapter(genreAdapter);

                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(getActivity());
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<ExamsModel> call, Throwable t) {

                AppUtils.APIFails(getActivity(),t);
                progressDialogFragment.dismiss();

            }
        });


    }
    public class Exam extends ExpandableGroup<ExamsModel.Datum.Schedule> {



        public Exam(String title, List<ExamsModel.Datum.Schedule> items) {
            super(title, items);
        }
    }
    public class UpcomingExamAdapter extends ExpandableRecyclerViewAdapter<UpcomingExamAdapter.ExamNameViewHolder, UpcomingExamAdapter.ExamListViewHolder> {


        public UpcomingExamAdapter(List<? extends ExpandableGroup> groups) {
            super(groups);
        }

        @Override
        public ExamNameViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customupcomingexam, parent, false);
            return new ExamNameViewHolder(view);

        }

        @Override
        public ExamListViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.examlist, parent, false);
            return new ExamListViewHolder(view);
        }

        @Override
        public void onBindChildViewHolder(ExamListViewHolder holder, int flatPflosition, ExpandableGroup group, int childIndex) {
             ExamsModel.Datum.Schedule schedule = (ExamsModel.Datum.Schedule) group.getItems().get(childIndex);

            holder.subjectname.setText(schedule.getSubject());
            holder.date.setText(schedule.getStartDate());
            holder.timeduration.setText( schedule.getDuration());
            if (childIndex==0){
                holder.headertablerow.setVisibility(View.VISIBLE);
            }
            else {
                holder.headertablerow.setVisibility(View.GONE);


            }


        }

        @Override
        public void onBindGroupViewHolder(ExamNameViewHolder holder, int flatPosition, ExpandableGroup group) {

            holder.examname.setText(group.getTitle());


        }

        public class ExamNameViewHolder extends GroupViewHolder {
            TextView examname;
            public ExamNameViewHolder(View itemView) {
                super(itemView);
                examname = itemView.findViewById(R.id.examname);
            }

            @Override
            public void expand() {
                super.expand();
            }

            @Override
            public void collapse() {
                super.collapse();
            }
        }

        public class ExamListViewHolder extends ChildViewHolder {
            TextView subjectname,date,timeduration;
            TableRow headertablerow;
            public ExamListViewHolder(View itemView) {
                super(itemView);
                subjectname=itemView.findViewById(R.id.subjectname);
                date=itemView.findViewById(R.id.date);
                timeduration=itemView.findViewById(R.id.timeduration);
                headertablerow=itemView.findViewById(R.id.headertablerow);
            }
        }

    }
//    public class UpcomingExamsAdapter extends RecyclerView.Adapter<UpcomingExamsAdapter.ExamsViewHolder>{
//        List<ExamsModel.Datum> modelArrayList;
//
//        public UpcomingExamsAdapter(List<ExamsModel.Datum> modelArrayList){
//            this.modelArrayList=modelArrayList;
//        }
//
//        @NonNull
//        @Override
//        public UpcomingExamsAdapter.ExamsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.customupcomingexam,parent,false);
//
//            return new ExamsViewHolder(view);
//        }
//
//        @Override
//        public void onBindViewHolder(@NonNull UpcomingExamsAdapter.ExamsViewHolder holder, final int position) {
//            holder.subjectname.setText(modelArrayList.get(position).getSchedule().get(position).getSubject());
//            holder.date.setText(modelArrayList.get(position).getSchedule().get(position).getStartDate());
//            holder.timeduration.setText("Duration :" +modelArrayList.get(position).getSchedule().get(position).getDuration());
//
//
//        }
//
//        @Override
//        public int getItemCount() {
//            return modelArrayList.size();
//        }
//
//        public class ExamsViewHolder extends RecyclerView.ViewHolder {
//            TextView subjectname,date,timeduration;
//
//            public ExamsViewHolder(@NonNull View itemView) {
//                super(itemView);
//                subjectname=itemView.findViewById(R.id.subjectname);
//                date=itemView.findViewById(R.id.date);
//                timeduration=itemView.findViewById(R.id.timeduration);
//            }
//        }
//    }

}
