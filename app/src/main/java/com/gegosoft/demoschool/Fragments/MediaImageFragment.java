package com.gegosoft.demoschool.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gegosoft.demoschool.Activity.GalleryActivity;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.GalleryModel;
import com.gegosoft.demoschool.Model.MediaImageModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class MediaImageFragment extends Fragment {
    String eventId,title;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Toolbar toolbar;
    Api api;
    RecyclerView gallerylist;
    StaggeredGridLayoutManager gridLayoutManager;
    TextView textnodata;
    List<MediaImageModel.Datum>data;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mediaimage_recyclerviewlayout, container, false);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(getContext());
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");

        textnodata = view.findViewById(R.id.textnodata);
        gallerylist=view.findViewById(R.id.recyclerview);
        gridLayoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        gallerylist.setLayoutManager(gridLayoutManager);
        gallerylist.setHasFixedSize(true);
        gallerylist.setItemAnimator(new DefaultItemAnimator());
        if (CheckNetwork.isInternetAvailable(getContext())){
            getGallery();
        }

        return view;
    }
    private void getGallery(){
        Call<MediaImageModel> modelCall = api.getmediaimage(headermap,userDetailsSharedPref.getString("userId"));
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getFragmentManager(),"loading screen");
        modelCall.enqueue(new Callback<MediaImageModel>() {
            @Override
            public void onResponse(Call<MediaImageModel> call, Response<MediaImageModel> response) {
                if (response.isSuccessful()){
                    data = response.body().getData();

                    if (data.size()!=0){
                        MediaImageAdapter mediaImageAdapter=new MediaImageAdapter();
                        gallerylist.setAdapter(mediaImageAdapter);
                    }
                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(getActivity());
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<MediaImageModel> call, Throwable t) {
                AppUtils.APIFails(getContext(),t);
                progressDialogFragment.dismiss();

            }
        });

    }
    private class MediaImageAdapter extends RecyclerView.Adapter<MediaImageAdapter.MediaImageViewHolder>{
         List<MediaImageModel.Datum>datumList=new ArrayList<>();
        @NonNull
        @Override
        public MediaImageAdapter.MediaImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_mediaimage_list,parent,false);
            return  new MediaImageAdapter.MediaImageViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MediaImageAdapter.MediaImageViewHolder holder, int position) {
            holder.galleryname.setText(data.get(position).getName());
//            holder.total.setVisibility(GONE);
            Glide.with(getActivity()).load(data.get(position).getUrl())
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.galleryimage);
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MediaImageViewHolder extends RecyclerView.ViewHolder {
            public ImageView galleryimage;
            TextView galleryname,total;
            public MediaImageViewHolder(@NonNull View itemView) {
                super(itemView);
                galleryname = (TextView) itemView.findViewById(R.id.galleryname);
                galleryimage = (ImageView) itemView.findViewById(R.id.galleryimage);
                total = (TextView) itemView.findViewById(R.id.total);
            }
        }
    }

}
