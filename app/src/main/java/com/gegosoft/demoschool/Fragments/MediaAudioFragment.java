package com.gegosoft.demoschool.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.Activity.VideoPlayerActivity;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.MediaAudioModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MediaAudioFragment extends Fragment {
    RecyclerView recyclerView;
    TextView textnorecords;
    Api apiInterface;

    LinearLayoutManager linearLayoutManager;
    AudioAdapter audioAdapter;
    private boolean loading = true;
    int page =1;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mediaaudio_recyclerviewlayout, container, false);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(getContext());
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        recyclerView =view.findViewById(R.id.recyclerview);

        linearLayoutManager = new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        audioAdapter = new AudioAdapter();
        recyclerView.setAdapter(audioAdapter);
        textnorecords = view.findViewById(R.id.textnodata);
        apiInterface = ApiClient.getClient().create(Api.class);

        if (CheckNetwork.isInternetAvailable(getContext())){
            getaudio();
        }
//        currentaudioname =view.findViewById(R.id.currentaudioname);
//        currentaudiodescription =view.findViewById(R.id.currentaudiodescription);
//        linearLayoutManager = new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
//        recyclerView.setLayoutManager(linearLayoutManager);
//        recyclerView.setHasFixedSize(true);
//        audioAdapter = new AudioAdapter();
//        recyclerView.setAdapter(audioAdapter);
//        textnorecords = view.findViewById(R.id.textnorecords);
//        playerView = view.findViewById(R.id.audioplayer);
//        apiInterface = ApiClient.getClient().create(Api.class);
//        audioManager = (AudioManager)getActivity().getSystemService(Context.AUDIO_SERVICE);
//
//        if (CheckNetwork.isInternetAvailable(getContext())){
//            getaudio();
//        }
        return view;

    }
    private void getaudio() {
        Call<MediaAudioModel> modelCall = apiInterface.getmediaaudio(headermap,userDetailsSharedPref.getString("userId"));
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getFragmentManager(),"loading screen");
        modelCall.enqueue(new Callback<MediaAudioModel>() {
            @Override
            public void onResponse(Call<MediaAudioModel> call, Response<MediaAudioModel> response) {
                if (response.isSuccessful())
                {
                    List<MediaAudioModel.Datum> datumList = response.body().getData();
                    if (datumList!=null&&datumList.size()!=0){

                        recyclerView.setVisibility(View.VISIBLE);
                        for (MediaAudioModel.Datum datum : datumList){
                            audioAdapter.addAudios(datum);
                        }
                        audioAdapter.notifyDataSetChanged();
                        loading =true;
                    }
                    else {
                        if (page==1){
                            recyclerView.setVisibility(View.GONE);
                            textnorecords.setVisibility(View.VISIBLE);
                        }
                        else{
                            recyclerView.setVisibility(View.VISIBLE);
                            textnorecords.setVisibility(View.GONE);
                        }
                        loading =false;

                    }


                    {
                        if (response.code()==401){
                            AppUtils.SessionExpired(getActivity());
                        }
                    }
                    progressDialogFragment.dismiss();
                }

            }

            @Override
            public void onFailure(Call<MediaAudioModel> call, Throwable t) {
                Toast.makeText(getContext(), "Failure", Toast.LENGTH_SHORT).show();
            }
        });

    }



    private class AudioAdapter extends RecyclerView.Adapter<AudioAdapter.MyViewHolder>{

        List<MediaAudioModel.Datum> datumList = new ArrayList<>();

//        public AudioAdapter(List<Guest_Audio_Model.Datum> datumList) {
//            this.datumList = datumList;
//        }

        @NonNull
        @Override
        public AudioAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_mediaaudio_list,parent,false);
            return  new AudioAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull AudioAdapter.MyViewHolder holder, int position) {

            holder.title.setText(datumList.get(position).getName());
            holder.description.setText(datumList.get(position).getDescription());
//            if (datumList.get(position).getMediaType().equalsIgnoreCase("audio")){
//                holder.watchvideotxt.setText("Play Audio");
            //}
//            holder.watchvideotxt.setText("play Audio");
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getContext(), VideoPlayerActivity.class).putExtra("videoUrl",datumList.get(position).getUrl()));

//                    player.seekToDefaultPosition();



//                    BottomSheetDialog(datumList.get(position).getUrl());



                }
            });
        }


        @Override
        public int getItemCount() {
            return datumList.size();
        }

        public void addAudios(MediaAudioModel.Datum datum) {
            datumList.add(datum);
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView title,description,watchvideotxt;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                title = itemView.findViewById(R.id.title);
                description = itemView.findViewById(R.id.description);
//                watchvideotxt = itemView.findViewById(R.id.watchvideotxt);

            }
        }
    }
    //    private void BottomSheetDialog(String url){
//        View view = getLayoutInflater().inflate(R.layout.mediaplayer, null);
//
//        BottomSheetDialog dialog = new BottomSheetDialog(getContext());
//        playerView = view.findViewById(R.id.audioplayer);
////        ImageView imageView = view.findViewById(R.id.music_gif);
////        Glide.with(getContext()).load(R.drawable.musci_gif).into(imageView);
////        imageView.setVisibility(View.GONE);
//        int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
//                AudioManager.AUDIOFOCUS_GAIN);
//
//        if(result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
//        {
//            // Play
//            hideSystemUi();
//
//            initializePlayer(url);
//        }
//
//        dialog.setContentView(view);
//        dialog.setCancelable(false);
//
//        dialog.show();
//    }

//
//
//        //  builder.setPositiveButton("OK", null);
//        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                releasePlayer();
//                dialog.cancel();
//
//            }
//        });
//        builder.setView(dialogLayout);
//
//        AlertDialog alertDialog = builder.show();
//        Window window = alertDialog.getWindow();
//        window.setGravity(Gravity.BOTTOM);
//
//        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//
//    }






}
