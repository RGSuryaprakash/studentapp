package com.gegosoft.demoschool.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.Activity.PDFWebViewActivity;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Helper.FileUtils;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.HomeworkModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeWorkPendingFragment extends Fragment {
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static final int PICKFILE_RESULT_CODE = 2;
    RecyclerView recyclerView;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Api api;
    TextView textnodata;
    List<MultipartBody.Part> parts = new ArrayList<>();
    String home_work_id;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.homework_all_fragment_layout,null);
        recyclerView=view.findViewById(R.id.homeworkallrecyclerview);
        textnodata=view.findViewById(R.id.textnodata);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(getContext());
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        if (CheckNetwork.isInternetAvailable(getContext())){
            getdata();
        }


        return view;


    }

    private void getdata(){
        api= ApiClient.getClient().create(Api.class);
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getActivity().getSupportFragmentManager(),"loading screen");
        Call<HomeworkModel> examsModelCall = api.getPendingHomeWork(headermap,userDetailsSharedPref.getString("userId"));
        examsModelCall.enqueue(new Callback<HomeworkModel>() {
            @Override
            public void onResponse(Call<HomeworkModel> call, Response<HomeworkModel> response) {
                if (response.isSuccessful()){
                    List<HomeworkModel.Datum> data = response.body().getData();
                    if (data.size()!=0){
                        AllAdapter allAdapter=new AllAdapter(data);
                        recyclerView.setAdapter(allAdapter);
                        allAdapter.notifyDataSetChanged();
                        textnodata.setVisibility(View.GONE);

                    }
                    else {
                        textnodata.setVisibility(View.VISIBLE);
                    }
                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(getActivity());
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<HomeworkModel> call, Throwable t) {
                AppUtils.APIFails(getActivity(),t);
                progressDialogFragment.dismiss();
            }
        });


    }
    public class AllAdapter extends RecyclerView.Adapter<AllAdapter.AllViewHolder>{
        List<HomeworkModel.Datum> homework_all_modelArrayList;
        public AllAdapter(List<HomeworkModel.Datum> homework_all_modelArrayList){
        this.homework_all_modelArrayList=homework_all_modelArrayList;
        }

        @NonNull
        @Override
        public AllAdapter.AllViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.pendinghomework,parent,false);

            return new AllViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull AllAdapter.AllViewHolder holder, int position) {
         //  holder.allheading.setText(homework_all_modelArrayList.get(position).getSubjectName());
            List<String> imagelist = new ArrayList<>();
            holder.delete_files.setVisibility(View.GONE);
            holder.allsubheading.setText(Html.fromHtml(homework_all_modelArrayList.get(position).getDescription()));
            holder.date.setText(homework_all_modelArrayList.get(position).getDate());

//            if (homework_all_modelArrayList.get(position).getStudentHomeworkStatus().equals("checked") )
//            {
//                holder.delete_files.setVisibility(View.GONE);
//            }
//            else {
//                holder.delete_files.setVisibility(View.VISIBLE);
//            }
//if (homework_all_modelArrayList.get(position).getStudentHomeworkStatus()==null)
//{
//    holder.checked_tv.setVisibility(View.VISIBLE);
//    holder.teacher_comments.setVisibility(View.VISIBLE);
//    holder.teacher_comm_display.setVisibility(View.VISIBLE);
//
//}
if (homework_all_modelArrayList.get(position).getTeacherComments() == null)
{
    holder.teacher_comments.setText("");

}else {
    holder.teacher_comments.setText(homework_all_modelArrayList.get(position).getTeacherComments().toString());

}

            holder.allheading.setText(homework_all_modelArrayList.get(position).getSubject());




            if (homework_all_modelArrayList.get(position).getStudentHomeworkId() ==null)
            {
                holder.add_homework.setVisibility(View.VISIBLE);
                holder.view_files.setVisibility(View.GONE);
                holder.delete_files.setVisibility(View.GONE);
                holder.teacher_comm_display.setVisibility(View.GONE);

            }
            else if (homework_all_modelArrayList.get(position).getStudentHomeworkStatus().equals("unchecked"))
            {
                holder.view_files.setVisibility(View.VISIBLE);
                holder.delete_files.setVisibility(View.VISIBLE);
                holder.add_homework.setVisibility(View.GONE);
                holder.teacher_comm_display.setVisibility(View.GONE);


            }
            else if (homework_all_modelArrayList.get(position).getStudentHomeworkStatus().equals("checked"))
            {
                holder.view_files.setVisibility(View.VISIBLE);
                holder.delete_files.setVisibility(View.GONE);
                holder.add_homework.setVisibility(View.GONE);
                holder.teacher_comm_display.setVisibility(View.VISIBLE);
                holder.teacher_comments.setVisibility(View.VISIBLE);
                holder.checked_tv.setVisibility(View.VISIBLE);
            }

//                if (homework_all_modelArrayList.get(position).getStudentHomeworkStatus()!=null && homework_all_modelArrayList.get(position).getStudentHomeworkStatus().equals("checked"))
//                {
//                   // holder.delete_files.setVisibility(View.GONE);
//                    holder.teacher_comm_display.setVisibility(View.GONE);
//                    holder.delete_files.setVisibility(View.GONE);
//
//                    holder.checked_tv.setText(homework_all_modelArrayList.get(position).getStudentHomeworkStatus().toString());
//                    if (homework_all_modelArrayList.get(position).getTeacherComments() == null)
//                    {
//                        holder.teacher_comments.setText("");
//                    }else {
//                        holder.teacher_comments.setText(homework_all_modelArrayList.get(position).getTeacherComments().toString());
//
//                    }
//                    holder.checked_tv.setVisibility(View.VISIBLE);
//                    holder.teacher_comments.setVisibility(View.VISIBLE);
//                    holder.teacher_comm_display.setVisibility(View.VISIBLE);
//                    holder.view_files.setVisibility(View.GONE);
//                    holder.delete_files.setVisibility(View.GONE);
//
//                }
//                else
//                {
//                    holder.checked_tv.setVisibility(View.GONE);
//                    holder.teacher_comments.setVisibility(View.GONE);
//                    holder.teacher_comm_display.setVisibility(View.GONE);
//                    holder.delete_files.setVisibility(View.VISIBLE);
//
//                }

            if (homework_all_modelArrayList.get(position).getAttachmentFile()!=null){
                Gson gson = new Gson();
                String files = gson.toJson(homework_all_modelArrayList.get(position).getAttachmentFile());
                try {
                     JSONObject jsonObject = new JSONObject(files);
                    Iterator keys = jsonObject.keys();
                    while (keys.hasNext()) {
                        Object key = keys.next();

                        String walletbalance = jsonObject.getString((String) key);
                        imagelist.add(walletbalance);

                    }
                  //  holder.files_count.setText( String.valueOf(jsonObject.length()));
//                    holder.delete_files.setVisibility(View.VISIBLE);
//                    holder.view_files.setVisibility(View.VISIBLE);
                    holder.add_homework.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else{
                holder.delete_files.setVisibility(View.GONE);
                holder.view_files.setVisibility(View.GONE);
                holder.add_homework.setVisibility(View.VISIBLE);
            }
            if (!homework_all_modelArrayList.get(position).getAttachment().equalsIgnoreCase("")){
                holder.attachmentlayout.setVisibility(View.VISIBLE);
            }
            else {
                holder.attachmentlayout.setVisibility(View.GONE);
            }
            holder.allsubheading.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.allsubheading.setText(Html.fromHtml(homework_all_modelArrayList.get(position).getDescription()));
                    holder.allsubheading.setMaxLines(Integer.MAX_VALUE);
                    holder.allsubheading.setTextIsSelectable(true);

                }
            });
            holder.view_files.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (imagelist!=null&&imagelist.size()!=0){
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("images", (Serializable) imagelist);
                        bundle.putString("from","homeWork");
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                        newFragment.setArguments(bundle);
                        newFragment.show(ft, "galleryslideshow");

                    }


                }
            });
            holder.add_homework.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                        if(checkPermissionExternalStorage()){
                            home_work_id = homework_all_modelArrayList.get(position).getId().toString();
                            seletFile();
                        }
                        else {
                            requestPermissionForExternalStorage();
                        }


                }
            });
            holder.delete_files.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                deleteFile(homework_all_modelArrayList.get(position).getStudentHomeworkId());

                }
            });


            holder.attachmentlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle extras = new Bundle();

                    extras.putString("url", String.valueOf(homework_all_modelArrayList.get(position).getAttachment()));
                    startActivity(new Intent(getContext(), PDFWebViewActivity.class).putExtras(extras));
                }
            });
        }

        @Override
        public int getItemCount() {
            return homework_all_modelArrayList.size();
        }

        public class AllViewHolder extends RecyclerView.ViewHolder {
            TextView allheading,allsubheading;
            TextView date,checked_tv,teacher_comments,teacher_comm_display;
            LinearLayout attachmentlayout;
            Button add_homework,view_files;
            ImageView delete_files;

            public AllViewHolder(@NonNull View itemView) {
                super(itemView);
                allheading=itemView.findViewById(R.id.homeworkallheading);
                allsubheading=itemView.findViewById(R.id.homeworkallsubheading);
                date=itemView.findViewById(R.id.date);
                attachmentlayout=itemView.findViewById(R.id.attachmentlayout);
              //  files_count=itemView.findViewById(R.id.files_count);
                add_homework=itemView.findViewById(R.id.add_homework);
                delete_files=itemView.findViewById(R.id.delete_files);
                view_files=itemView.findViewById(R.id.view_files);
                checked_tv = itemView.findViewById(R.id.checked_status_tv);
                teacher_comments = itemView.findViewById(R.id.teacher_comments);
                teacher_comm_display = itemView.findViewById(R.id.tec_com);
            }
        }
    }

    private void deleteFile(Integer studentHomeworkId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        //Setting message manually and performing action on button click
        builder.setMessage("Do you want to delete files ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Call<ResponseBody> bodyCall  = api.deleteHomeworkfiles(headermap,studentHomeworkId,userDetailsSharedPref.getString("userId"));
                        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("message","Loading");
                        progressDialogFragment.setArguments(bundle);
                        progressDialogFragment.show(getActivity().getSupportFragmentManager(),"please wait");
                        bodyCall.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()){
                                    try {
                                        String res = response.body().string();
                                        JSONObject  jsonObject =  new JSONObject(res);
                                        Toast.makeText(getContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();

                                        getdata();

                                    } catch (IOException | JSONException e) {

                                        e.printStackTrace();
                                    }
                                }
                                else {
                                    if (response.code() == 422) {
                                        try {
                                            String s = response.errorBody().string();
                                            JSONObject jsonObject = new JSONObject(s);
                                            String error = jsonObject.getString("errors");
                                            JSONObject jsonObject1 = new JSONObject(error);
                                            Iterator keys = jsonObject1.keys();
                                            while (keys.hasNext()) {
                                                Object key = keys.next();
                                                JSONArray value = jsonObject1.getJSONArray((String) key);
                                                String errormessage = value.getString(0);
                                                if (key.equals("errors")) {
                                                    Toast.makeText(getContext(), "File Extension Error", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        } catch (IOException | JSONException e) {
                                            e.printStackTrace();
                                        }
                                        progressDialogFragment.dismiss();
                                    }
                                }
                                progressDialogFragment.dismiss();
                                }
                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                AppUtils.APIFails(getActivity(),t);
                                progressDialogFragment.dismiss();
                            }
                        });

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually

        alert.show();
    }

    private boolean checkPermissionExternalStorage() {
        int externalStorage = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);

        return externalStorage == PackageManager.PERMISSION_GRANTED ;
    }

    private void requestPermissionForExternalStorage() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) ) {
//            Toast.makeText(this,
//                    R.string.permissions_needed,
//                    Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_EXTERNAL_STORAGE);


        } else {
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            // check whether storage permission granted or not.
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do what you want;
                seletFile();
            }
            else {
                requestPermissionForExternalStorage();
            }
        }
    }

    public void seletFile() {
        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
        String[] mimeTypes = {"image/*"};
        chooseFile.setType("*/*");
        chooseFile.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        chooseFile.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);

        chooseFile = Intent.createChooser(chooseFile, "Choose a file");
        startActivityForResult(chooseFile, PICKFILE_RESULT_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK) {

            if (data.getClipData() != null) {
                int count = data.getClipData().getItemCount(); //evaluate the count before the for loop --- otherwise, the count is evaluated every loop.
                for (int i = 0; i < count; i++) {
                    Uri uri = data.getClipData().getItemAt(i).getUri();

                   // Uri uri = data.getData();
                    String  filePath= FileUtils.getPath(getContext(),uri);
                    // filePath = uri.getPath();
                    Log.d("path",filePath);
                    ImageView imageView = new ImageView(getContext());
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(128,
                            128);
                    imageView.setLayoutParams(params);
                    imageView.setPadding(8,8,8,8);



//            imageviewlayout.addView(imageView);
                    imageView.setImageURI(uri);
                    File file = new File(filePath);
                    RequestBody requestFile = RequestBody.create(file, MultipartBody.FORM);

                    MultipartBody.Part body = MultipartBody.Part.createFormData("file[]",file.getName(), requestFile);
                    parts.add(body);

                }

            } else if (data.getData() != null) {
                Uri uri = data.getData();
                String  filePath= FileUtils.getPath(getContext(),uri);
                // filePath = uri.getPath();
                Log.d("path",filePath);
                ImageView imageView = new ImageView(getContext());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(128,
                        128);
                imageView.setLayoutParams(params);
                imageView.setPadding(8,8,8,8);



//            imageviewlayout.addView(imageView);
                imageView.setImageURI(uri);
                File file = new File(filePath);
                RequestBody requestFile = RequestBody.create(file, MultipartBody.FORM);
                MultipartBody.Part body = MultipartBody.Part.createFormData("file[]",file.getName(), requestFile);
                parts.add(body);
            }

            uploadHomeWork();

//
//            RequestBody reqpriorityid =
//                    RequestBody.create(priorityid,MultipartBody.FORM);
//            RequestBody reqcategoryid =
//                    RequestBody.create(categoryid,MultipartBody.FORM);
        }

    }

    private void uploadHomeWork() {
        Call<ResponseBody>  bodyCall = api.uploadHomeWorkFiles(headermap,home_work_id,userDetailsSharedPref.getString("userId"),parts);
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getActivity().getSupportFragmentManager(),"please wait");
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        String res = response.body().string();
                        JSONObject  jsonObject =  new JSONObject(res);
                        Toast.makeText(getContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                        parts.clear();
                        getdata();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    if (response.code()==401){
                        AppUtils.SessionExpired(getActivity());
                    }
                    if (response.code()== 422){
                        try {
                            String s = response.errorBody().string();
                            JSONObject jsonObject = new JSONObject(s);
                            String error = jsonObject.getString("errors");
                            JSONObject jsonObject1 = new JSONObject(error);
                            Iterator keys = jsonObject1.keys();
                            while (keys.hasNext()) {
                                Object key = keys.next();
                                JSONArray value = jsonObject1.getJSONArray((String) key);
                                String errormessage = value.getString(0);
                                if (key.equals("file")) {
                                 Toast.makeText(getContext(),errormessage,Toast.LENGTH_LONG).show();
                                }

                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                AppUtils.APIFails(getActivity(),t);
                progressDialogFragment.dismiss();

            }
        });
    }


}
