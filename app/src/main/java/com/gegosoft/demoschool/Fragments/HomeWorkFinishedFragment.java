package com.gegosoft.demoschool.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.gegosoft.demoschool.Activity.PDFWebViewActivity;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.HomeworkModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeWorkFinishedFragment extends Fragment {
    RecyclerView recyclerView;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Api api;
    TextView textnodata;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//
        View view=inflater.inflate(R.layout.homework_today_fragment_layout,null);
        recyclerView=view.findViewById(R.id.homeworktodayrecyclerview);
        textnodata = view.findViewById(R.id.textnodata);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(getContext());

        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        if (CheckNetwork.isInternetAvailable(getContext())){
            getdata();
        }

        return view;

    }
    private void getdata(){

        api= ApiClient.getClient().create(Api.class);
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getActivity().getSupportFragmentManager(),"loading screen");
        Call<HomeworkModel> examsModelCall = api.getFinishedHomeWork(headermap,userDetailsSharedPref.getString("userId"));
        examsModelCall.enqueue(new Callback<HomeworkModel>() {
            @Override
            public void onResponse(Call<HomeworkModel> call, Response<HomeworkModel> response) {
                if (response.isSuccessful()){
                    List<HomeworkModel.Datum> data = response.body().getData();
                    if (data.size()!=0){
                        MyAdapter myAdapter=new MyAdapter(data);
                        recyclerView.setAdapter(myAdapter);
                        textnodata.setVisibility(View.GONE);
                    }
                    else {
                        textnodata.setVisibility(View.VISIBLE);
                    }
                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(getActivity());
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<HomeworkModel> call, Throwable t) {
                AppUtils.APIFails(getActivity(),t);
                progressDialogFragment.dismiss();
            }
        });


    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{
        List<HomeworkModel.Datum> modelArrayList;
        public MyAdapter(List<HomeworkModel.Datum> modelArrayList){
             this.modelArrayList=modelArrayList;

        }

        @NonNull
        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.finishedhomework,parent,false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
        //  holder.todayheading.setText(modelArrayList.get(position).getSubjectName());
            holder.todaysubheading.setText(Html.fromHtml(modelArrayList.get(position).getDescription()));
            holder.date.setText((modelArrayList.get(position).getDate()));

            if (!modelArrayList.get(position).getAttachment().equalsIgnoreCase("")){
                holder.attachmentlayout.setVisibility(View.VISIBLE);
            }
            else {
                holder.attachmentlayout.setVisibility(View.GONE);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.todaysubheading.setText(modelArrayList.get(position).getDescription());
                    holder.todaysubheading.setMaxLines(Integer.MAX_VALUE);
                    holder.todaysubheading.setTextIsSelectable(true);



                }
            });
            holder.attachmentlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle extras = new Bundle();

                    extras.putString("url", String.valueOf(modelArrayList.get(position).getAttachment()));
                    startActivity(new Intent(getContext(), PDFWebViewActivity.class).putExtras(extras));
                }
            });
        }

        @Override
        public int getItemCount() {
            return modelArrayList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView todayheading,todaysubheading,date;
            LinearLayout attachmentlayout;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                todayheading=itemView.findViewById(R.id.homeworkallheading);
                todaysubheading=itemView.findViewById(R.id.homeworkallsubheading);
                date=itemView.findViewById(R.id.date);
                attachmentlayout=itemView.findViewById(R.id.attachmentlayout);
            }
        }
    }
}
