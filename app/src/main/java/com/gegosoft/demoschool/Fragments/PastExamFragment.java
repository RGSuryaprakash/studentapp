package com.gegosoft.demoschool.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.ExamResultMarksModel;
import com.gegosoft.demoschool.Model.ExamsModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class PastExamFragment extends Fragment {
    RecyclerView recyclerview;
    List<ExamsModel.Datum> datumList;
    Api api;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    List<PastExam> examArrayList ;
    PastExamAdapter examsAdapter;
    TextView textnodata;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerview,container,false);
        recyclerview = view.findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));
        recyclerview.setHasFixedSize(true);
        textnodata= view.findViewById(R.id.textnodata);
        //for expanding purpore make recyclerview height to match parent
        ViewGroup.LayoutParams params=recyclerview.getLayoutParams();
        params.height= ViewGroup.LayoutParams.MATCH_PARENT;
        recyclerview.setLayoutParams(params);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(getContext());
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        if (CheckNetwork.isInternetAvailable(getContext())){
           // getdata();
        }
        examArrayList =new ArrayList<>();

        Observable<ExamsModel> observable = api.getPastExamModel(headermap,ApiClient.getClient().baseUrl()+"api/v2/exams/past/"+userDetailsSharedPref.getString("userId"));
        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .concatMap(new Function<ExamsModel, ObservableSource<?>>() {
                    @Override
                    public ObservableSource<?> apply(ExamsModel examsModel) throws Exception {
                        Log.d("TESTING","KJJbdjd");
                        datumList = examsModel.getData();
                        if (datumList.size()==0){
                            textnodata.setVisibility(View.VISIBLE);
                        }
                        return Observable.fromIterable(examsModel.getData())
                                .concatMap(new Function<ExamsModel.Datum, ObservableSource<?>>() {
                                    @Override
                                    public ObservableSource<?> apply(ExamsModel.Datum datum) throws Exception {
                                        return api.getExamMarksList(headermap,userDetailsSharedPref.getString("userId"),datum.getId().toString())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribeOn(Schedulers.io())
                                                .map(new Function<ExamResultMarksModel, ObservableSource<?>>() {
                                                    @Override
                                                    public ObservableSource<?> apply(ExamResultMarksModel examResultMarksModel) throws Exception {
                                                        Log.d("TESTING","1");

                                                        // if (examResultMarksModel.getData().size()!=0)


                                                        examArrayList.add(new PastExam(datum.getName(),examResultMarksModel.getData().getMarks()));
                                                        examsAdapter = new PastExamAdapter(examArrayList);
                                                        recyclerview.setAdapter(examsAdapter);
                                                        //    examsAdapter.updateExamList(examArrayList);


//


                                                        return Observable.fromIterable(examResultMarksModel.getData().getMarks());

                                                    }
                                                });
                                    }
                                });
                    }
                }).subscribe();

//        Observable<ExamsModel> observable = api.getPastExamModel(headermap,ApiClient.getClient().baseUrl()+"api/v2/exams/past/"+userDetailsSharedPref.getString("userId"));
//        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
//                .concatMap(new Function<ExamsModel, ObservableSource<?>>() {
//                    @Override
//                    public ObservableSource<?> apply(ExamsModel examsModel) throws Exception {
//                        Log.d("TESTING","KJJbdjd");
//                         datumList = examsModel.getData();
//                         if (datumList.size()==0){
//                             textnodata.setVisibility(View.VISIBLE);
//                         }
////                        return Observable.fromIterable(examsModel.getData())
////                            .concatMap(new Function<ExamsModel.Datum, ObservableSource<?>>() {
////                            @Override
////                            public ObservableSource<?> apply(ExamsModel.Datum datum) throws Exception {
////                                return api.getExamMarksList(headermap,userDetailsSharedPref.getString("userId"),datum.getId().toString())
////                                        .observeOn(AndroidSchedulers.mainThread())
////                                        .subscribeOn(Schedulers.io())
////                                        .map(new Function<ExamResultMarksModel.Data, ObservableSource<?>>() {
////                                            @Override
//////                                            public ObservableSource<?> apply(@io.reactivex.annotations.NonNull ExamResultMarksModel.Data data) throws Exception {
//////                                                Log.d("TESTING","1");
//////                                                // if (examResultMarksModel.getData().size()!=0)
//////                                                examArrayList.add(new PastExam(datum.getName(),data));
//////                                                examsAdapter = new PastExamAdapter(examArrayList);
//////                                                recyclerview.setAdapter(examsAdapter);
//////                                                //    examsAdapter.updateExamList(examArrayList);
//////                                                return Observable.fromIterable(data.getData());
//////                                            }
////
//////                                            @Override
//////                                            public ObservableSource<?> apply(ExamResultMarksModel examResultMarksModel) throws Exception {
//////                                                Log.d("TESTING","1");
//////                                               // if (examResultMarksModel.getData().size()!=0)
//////                                                examArrayList.add(new PastExam(datum.getName(),examResultMarksModel.getData()));
//////                                                examsAdapter = new PastExamAdapter(examArrayList);
//////                                                recyclerview.setAdapter(examsAdapter);
//////                                           //    examsAdapter.updateExamList(examArrayList);
//////                                                return Observable.fromIterable(examResultMarksModel.getData());
//////
//////                                            }
////                                        });
////                            }
////                        });
//                        return null;
//                    }
//                }).subscribe();
//
        return view;

    }

//    private Observable<ExamsModel> getExamList(){
//
//        return api.getPastExamModel(headermap,ApiClient.getClient().baseUrl()+"api/v2/exams/past/"+userDetailsSharedPref.getString("userId"))
//            .observeOn(Schedulers.io())
//                .subscribeOn(AndroidSchedulers.mainThread())
//                .flatMap(new Function<ExamsModel, ObservableSource<ExamsModel>>() {
//                    @Override
//                    public ObservableSource<ExamsModel> apply(ExamsModel examsModel) throws Exception {
//                        return Observable.fromIterable(examsModel.getData()).flatMap(new Function<ExamsModel.Datum, ObservableSource<?>>() {
//                            @Override
//                            public ObservableSource<ExamsModel.Datum> apply(ExamsModel.Datum datum) throws Exception {
//                            }
//                        });
//                    }
//                });
//    }
    public class PastExam extends ExpandableGroup<ExamResultMarksModel.Data.Mark> {



        public PastExam(String title, List<ExamResultMarksModel.Data.Mark> items) {
            super(title, items);
        }
    }

    private class PastExamAdapter extends ExpandableRecyclerViewAdapter<PastExamAdapter.ExamNameHolder,PastExamAdapter.MarksHolder>{
        List<? extends ExpandableGroup> groups;
        int total,average;

//        List<PastExam> pastExams =new ArrayList<>();


//        public PastExamAdapter(List<PastExam> groups){
//            super(groups);
//            this.groups = groups;
//
//        }
        public PastExamAdapter(List<? extends ExpandableGroup> groups) {
            super(groups);
            this.groups = groups;
        }



        @Override
        public ExamNameHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custompastexams, parent, false);
            return new PastExamAdapter.ExamNameHolder(view);
        }

        @Override
        public MarksHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.exammarkslayout, parent, false);
            return new PastExamAdapter.MarksHolder(view);
        }

        @Override
        public void onBindChildViewHolder(MarksHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
            ExamResultMarksModel.Data.Mark datum = (ExamResultMarksModel.Data.Mark) group.getItems().get(childIndex);
            if (datum!=null){
                holder.subjectname.setText(datum.getSubject());
                holder.marks.setText(datum.getObtainedMarks());
                holder.comments.setText( datum.getTeacherComments());
                if (childIndex==0){
                    holder.headertablerow.setVisibility(View.VISIBLE);
                }
                else {
                    holder.headertablerow.setVisibility(View.GONE);
                }

            }


        }

        @Override
        public void onBindGroupViewHolder(ExamNameHolder holder, int flatPosition, ExpandableGroup group) {

            holder.examname.setText(group.getTitle());
            total=0;
            average=0;
            List<ExamResultMarksModel.Data.Mark> datum = (List<ExamResultMarksModel.Data.Mark>) groups.get(flatPosition).getItems();
            if (datum.size()!=0){
              //  holder.average.setText("Average :"+datum.get(flatPosition).getAverage());
//                for (int i = 0 ; i<datum.size();i++){
//                    total = total+Integer.parseInt(datum.get(i).getObtainedMarks());
//
//                }
               // holder.totalscore.setText("Total Score : "+datum.get(flatPosition).getTotal());
//                holder.average.setText("Average : "+String.valueOf(total/datum.size()));
            }
            else {
                holder.totalscore.setText("Mark will be uploaded soon");
                holder.totalscore.setTextColor(getResources().getColor(R.color.lightgreen));
                holder.average.setText("");
            }

        }

        public class ExamNameHolder extends GroupViewHolder {
            TextView examname,totalscore,average;
            public ExamNameHolder(View itemView) {
                super(itemView);
                examname = itemView.findViewById(R.id.examname);
                totalscore=itemView.findViewById(R.id.totalscore);
                average=itemView.findViewById(R.id.average);
            }
        }

        public class MarksHolder extends ChildViewHolder {
            TextView subjectname,marks,comments,status;
            TableRow headertablerow;
            public MarksHolder(View itemView) {
                super(itemView);
                subjectname=itemView.findViewById(R.id.subjectname);
                marks=itemView.findViewById(R.id.marks);
                comments=itemView.findViewById(R.id.comments);
//                status=itemView.findViewById(R.id.status);
                headertablerow=itemView.findViewById(R.id.headertablerow);

            }
        }
        public void updateExamList(List<PastExam> pastExam){
            this.groups = pastExam;
            notifyDataSetChanged();

        }
    }

//    private void getdata(){
//        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString("message","Loading");
//        progressDialogFragment.setArguments(bundle);
//        progressDialogFragment.show(getActivity().getSupportFragmentManager(),"loading screen");
//        Call<ExamsModel> examsModelCall = api.getExamModel(headermap,ApiClient.getClient().baseUrl()+"api/v2/exams/past/"+userDetailsSharedPref.getString("userId"));
//        examsModelCall.enqueue(new Callback<ExamsModel>() {
//            @Override
//            public void onResponse(Call<ExamsModel> call, Response<ExamsModel> response) {
//                if (response.isSuccessful()){
//                    datumList = response.body().getData();
//                    PastExamsAdapter examsAdapter = new PastExamsAdapter(datumList);
//
//                    recyclerview.setAdapter(examsAdapter);
//
//                }
//                else {
//                    if (response.code()==401){
//                        AppUtils.SessionExpired(getActivity());
//                    }
//                }
//                progressDialogFragment.dismiss();
//            }
//
//            @Override
//            public void onFailure(Call<ExamsModel> call, Throwable t) {
//                AppUtils.APIFails(getActivity(),t);
//                progressDialogFragment.dismiss();
//
//            }
//        });
//
//
//    }

//    public class PastExamsAdapter extends RecyclerView.Adapter<PastExamsAdapter.ExamsViewHolder>{
//        List<ExamsModel.Datum> modelArrayList;
//
//        public PastExamsAdapter(List<ExamsModel.Datum> modelArrayList){
//            this.modelArrayList=modelArrayList;
//        }
//
//        @NonNull
//        @Override
//        public PastExamsAdapter.ExamsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.custompastexams,parent,false);
//
//            return new ExamsViewHolder(view);
//        }
//
//        @Override
//        public void onBindViewHolder(@NonNull PastExamsAdapter.ExamsViewHolder holder, final int position) {
//            holder.heading.setText(modelArrayList.get(position).getName());
////            holder.result.setText(modelArrayList.get(position).getResult());
////            holder.passfail.setText(modelArrayList.get(position).getPassfail());
//            holder.rightarrow.setImageDrawable(getResources().getDrawable(R.drawable.right_arrow));
//
//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    startActivity(new Intent(getActivity(), ExamResultActivity.class).putExtra("examid",modelArrayList.get(position).getId()));
//                }
//            });
//        }
//
//        @Override
//        public int getItemCount() {
//            return modelArrayList.size();
//        }
//
//        public class ExamsViewHolder extends RecyclerView.ViewHolder {
//            TextView heading,result,passfail;
//            ImageView rightarrow;
//            public ExamsViewHolder(@NonNull View itemView) {
//                super(itemView);
//                heading=itemView.findViewById(R.id.examheading);
//                result=itemView.findViewById(R.id.resulttext);
//                passfail=itemView.findViewById(R.id.passfailtxt);
//                rightarrow=itemView.findViewById(R.id.rightarrow);
//            }
//        }
//    }
}
