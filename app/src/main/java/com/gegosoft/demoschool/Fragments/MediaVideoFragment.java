package com.gegosoft.demoschool.Fragments;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bumptech.glide.Glide;
import com.gegosoft.demoschool.Activity.VideoPlayerActivity;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.MediaVideoModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MediaVideoFragment extends Fragment {
    RecyclerView recyclerView;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Api api;
    // Using ArrayList to store images data

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View   view = inflater.inflate(R.layout.recyclerview, container,false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(getContext());
        api = ApiClient.getClient().create(Api.class);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        // Setting the layout as Staggered Grid for vertical orientation
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);

        // Sending reference and data to Adapter

        if (CheckNetwork.isInternetAvailable(getContext())){
            getVideo();
        }
        return view;
    }
    private void getVideo(){
        Call<MediaVideoModel> modelCall = api.getmediavideo(headermap,userDetailsSharedPref.getString("userId"));
        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("message","Loading");
        progressDialogFragment.setArguments(bundle);
        progressDialogFragment.show(getFragmentManager(),"loading screen");
        modelCall.enqueue(new Callback<MediaVideoModel>() {
            @Override
            public void onResponse(Call<MediaVideoModel> call, Response<MediaVideoModel> response) {
                if (response.isSuccessful()){
                  List<MediaVideoModel.Datum> data = response.body().getData();

                    if (data.size()!=0){
                        Adapter adapter = new Adapter( data);

                        // Setting Adapter to RecyclerView
                        recyclerView.setAdapter(adapter);
                    }

                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(getActivity());
                    }
                }
                progressDialogFragment.dismiss();
            }

            @Override
            public void onFailure(Call<MediaVideoModel> call, Throwable t) {
                AppUtils.APIFails(getContext(),t);
                progressDialogFragment.dismiss();

            }
        });

    }

    public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
        List<MediaVideoModel.Datum> data;

        // Constructor for initialization


        public Adapter(List<MediaVideoModel.Datum> data) {
            this.data = data;
        }

        @NonNull
        @Override
        public Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // Inflating the Layout(Instantiates list_item.xml layout file into View object)
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_media_videolist, parent, false);

            // Passing view to ViewHolder
            Adapter.ViewHolder viewHolder = new Adapter.ViewHolder(view);
            return viewHolder;
        }

        // Binding data to the into specified position
        @Override
        public void onBindViewHolder(@NonNull Adapter.ViewHolder holder, int position) {

            Glide.with(getContext()).load(data.get(position).getThumbFile()).into(holder.images);

            holder.title.setText(data.get(position).getName());
            holder.desc.setText(data.get(position).getDescription());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (data.get(position).getMediaType().equalsIgnoreCase("url")){
                        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(data.get(position).getUrl()));
                        try {
                          getActivity().startActivity(webIntent);
                        } catch (ActivityNotFoundException ex) {
                        }
                    }
                    else if(data.get(position).getMediaType().equalsIgnoreCase("upload")) {
                        startActivity(new Intent(getContext(), VideoPlayerActivity.class).putExtra("videoUrl",data.get(position).getUrl()));

                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            // Returns number of items currently available in Adapter
            return data.size();
        }

        // Initializing the Views
        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView images;
            TextView title,desc;

            public ViewHolder(View view) {
                super(view);
                images = (ImageView) view.findViewById(R.id.thumbnail);
                title = view.findViewById(R.id.title_media_file);
                desc = view.findViewById(R.id.desc_media_file);
            }
        }
    }
}
