package com.gegosoft.demoschool.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gegosoft.demoschool.Activity.MagazineActivity;
import com.gegosoft.demoschool.Activity.PDFWebViewActivity;
import com.gegosoft.demoschool.ApiClient;
import com.gegosoft.demoschool.Helper.AppUtils;
import com.gegosoft.demoschool.Helper.CheckNetwork;
import com.gegosoft.demoschool.Helper.FileUtils;
import com.gegosoft.demoschool.Interface.Api;
import com.gegosoft.demoschool.Model.AssignmentModel;
import com.gegosoft.demoschool.R;
import com.gegosoft.demoschool.Storage.UserDetailsSharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OngoingAssignmentFragment extends Fragment {
    RecyclerView recyclerView;
    UserDetailsSharedPref userDetailsSharedPref;
    Map<String,String> headermap;
    Api api;
    TextView textnodata;
  //  List<MultipartBody.Part> parts = new ArrayList<>();

    String assignment_id;

    Uri selectedfile;

    MultipartBody.Part requestFile  = null;

    private static final int PICK_PDF_FILE = 2;

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static final int PICKFILE_RESULT_CODE = 2;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerview,container,false);
        recyclerView=view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        userDetailsSharedPref = UserDetailsSharedPref.getInstance(getContext());
        textnodata=view.findViewById(R.id.textnodata);
        headermap = new HashMap<>();
        headermap.put("Authorization","Bearer "+userDetailsSharedPref.getString("token"));
        headermap.put("Accept","application/json");
        if (CheckNetwork.isInternetAvailable(getContext())){
            getdata();
        }

        return view;
    }
    private void getdata(){
        api = ApiClient.getClient().create(Api.class);
        Call<AssignmentModel> assignmentModelCall = api.getOngoingAssignment(headermap,userDetailsSharedPref.getString("userId"));
        assignmentModelCall.enqueue(new Callback<AssignmentModel>() {
            @Override
            public void onResponse(Call<AssignmentModel> call, Response<AssignmentModel> response) {
                if (response.isSuccessful()){
                    List<AssignmentModel.Datum> data = response.body().getData();
                    if (data.size()!=0){

                        OngoingAssigmentAdapter ongoingAssigmentAdapter = new OngoingAssigmentAdapter(data);
                        recyclerView.setAdapter(ongoingAssigmentAdapter);
                        ongoingAssigmentAdapter.notifyDataSetChanged();
                    }
                    else {
                        textnodata.setVisibility(View.VISIBLE);
                    }
                }
                else {
                    if (response.code()==401){
                        AppUtils.SessionExpired(getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<AssignmentModel> call, Throwable t) {
                AppUtils.APIFails(getActivity(),t);
            }
        });

    }
    private class OngoingAssigmentAdapter extends RecyclerView.Adapter<OngoingAssigmentAdapter.MyViewHolder> {
        List<AssignmentModel.Datum> data;

        public OngoingAssigmentAdapter(List<AssignmentModel.Datum> data) {

            this.data = data;
        }

        @NonNull
        @Override
        public OngoingAssigmentAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customongoinassignment, parent, false);
            return new OngoingAssigmentAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull OngoingAssigmentAdapter.MyViewHolder holder, int position) {
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d MMM yyyy");
//            try {
//                Date date = simpleDateFormat.parse(data.get(position).getAssignedDate());
//                Calendar cal = Calendar.getInstance();
//                String[] monthName = {"January", "February",
//                        "March", "April", "May", "June", "July",
//                        "August", "September", "October", "November",
//                        "December"};
//                String monthname = monthName[cal.get(Calendar.MONTH)];
//                cal.setTime(date);
//                holder.assigneddate.setText(String.valueOf(cal.get(Calendar.DATE)));
//                holder.assignedmonth.setText(monthname);
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }

            holder.delete_doc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteFile(data.get(position).getStudentAssignmentId());
                }
            });


            if (data.get(position).getStudentAssignmentId()==null)
            {
//                holder.view_doc.setVisibility(View.VISIBLE);
//                holder.delete_doc.setVisibility(View.VISIBLE);
//                holder.upload_doc.setVisibility(View.GONE);
                holder.upload_doc.setVisibility(View.VISIBLE);
                holder.view_doc.setVisibility(View.GONE);
                holder.delete_doc.setVisibility(View.GONE);

            }
            else if (data.get(position).getStudentAssignmentStatus().equals("submitted")){
                holder.view_doc.setVisibility(View.VISIBLE);
                holder.delete_doc.setVisibility(View.VISIBLE);
                holder.upload_doc.setVisibility(View.GONE);
            }
            else if (data.get(position).getStudentAssignmentStatus().equals("completed"))
            {
                holder.comments_shown.setText(data.get(position).getTeacherComments().toString());
                holder.comment_status.setText("Obtained Mark "+data.get(position).getObtainedMarks().toString().trim());


                holder.comments_tv.setVisibility(View.VISIBLE);
                holder.comments_shown.setVisibility(View.VISIBLE);
                holder.comment_status.setVisibility(View.VISIBLE);
                holder.upload_doc.setVisibility(View.GONE);
                holder.delete_doc.setVisibility(View.GONE);
                holder.view_doc.setVisibility(View.VISIBLE);

            }


//            else



//            if (data.get(position).getAssignmentFile() == null && data.get(position).getStudentAssignmentStatus() != null)
//            {
//                holder.view_doc.setVisibility(View.GONE);
//                holder.delete_doc.setVisibility(View.GONE);
//
//
//            }else {
//                holder.view_doc.setVisibility(View.VISIBLE);
//                holder.delete_doc.setVisibility(View.VISIBLE);
//                holder.upload_doc.setVisibility(View.GONE);
//            }
//
//








            holder.view_doc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle extras = new Bundle();

                    extras.putString("url", String.valueOf(data.get(position).getAssignmentFile()));
                    startActivity(new Intent(getContext(),PDFWebViewActivity.class).putExtras(extras));
                }
            });

            holder.upload_doc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkPermissionExternalStorage()) {
                        assignment_id = data.get(position).getId().toString();
                        openFile();

                       // uploadassignment();

                    } else {
                        requestPermissionForExternalStorage();
                    }


                }
            });

            if (data.get(position).getAttachment() != null) {
                holder.attachment.setVisibility(View.VISIBLE);
                holder.txtattachment.setVisibility(View.VISIBLE);
            }
            holder.title.setText(data.get(position).getTitle());
            holder.description.setText(data.get(position).getDescription());
            holder.submissiondate.setText(data.get(position).getSubmissionDate());
            holder.startdate.setText(data.get(position).getAssignedDate());
            if (!data.get(position).getAttachment().equalsIgnoreCase("")) {
                holder.attachment.setVisibility(View.VISIBLE);
                holder.txtattachment.setVisibility(View.VISIBLE);
            } else {
                holder.attachment.setVisibility(View.GONE);
                holder.txtattachment.setVisibility(View.GONE);
            }
            holder.marks.setText("Marks " + data.get(position).getMarks());
            if (data.get(position).getMarks() == null) {
                holder.marks.setVisibility(View.GONE);
            } else {
                holder.marks.setVisibility(View.VISIBLE);
            }
            holder.attachment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (data.get(position).getAttachment().isEmpty() && data.get(position).getAttachment() == "null")
                    {
                        holder.attachment.setVisibility(View.GONE);
                    }else {
                        Bundle extras = new Bundle();
                        extras.putString("url", String.valueOf(data.get(position).getAttachment()));
                        startActivity(new Intent(getContext(), PDFWebViewActivity.class).putExtras(extras));
                    }
                }
            });
        }


        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView startdate, submissiondate, title, description, marks, txtattachment;
            ImageView attachment, delete_doc;
            Button upload_doc, view_doc,submit;
            TextView comments_tv,comments_shown,comment_status;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                startdate = itemView.findViewById(R.id.startdate);
                submissiondate = itemView.findViewById(R.id.submissiondate);
                title = itemView.findViewById(R.id.title);
                description = itemView.findViewById(R.id.description);
                attachment = itemView.findViewById(R.id.attachment);
                marks = itemView.findViewById(R.id.marks);
                txtattachment = itemView.findViewById(R.id.attachmenttxt);
                upload_doc = itemView.findViewById(R.id.upload_btn_assignment);
                view_doc = itemView.findViewById(R.id.view_doc);
                delete_doc = itemView.findViewById(R.id.delete_doc);
                submit = itemView.findViewById(R.id.submit_method);

                comment_status = itemView.findViewById(R.id.status_comments_for_student_in_assignment);
                comments_shown = itemView.findViewById(R.id.comments_shown_by_assignment);
                comments_tv = itemView.findViewById(R.id.comment_text);

            }
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }
    }
    private void deleteFile(Integer studentassignment_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        //Setting message manually and performing action on button click
        builder.setMessage("Do you want to delete files ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Call<ResponseBody> bodyCall  = api.deleteassignmentbystudent(headermap,studentassignment_id,userDetailsSharedPref.getString("userId"));
                        ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("message","Loading");
                        progressDialogFragment.setArguments(bundle);
                        progressDialogFragment.show(getActivity().getSupportFragmentManager(),"please wait");
                        bodyCall.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()){
                                    try {
                                        String res = response.body().string();
                                        JSONObject  jsonObject =  new JSONObject(res);
                                        Toast.makeText(getContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();

                                        getdata();

                                    } catch (IOException | JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                else {
                                    if (response.code()==401){
                                        AppUtils.SessionExpired(getActivity());
                                    }
                                }
                                progressDialogFragment.dismiss();
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                AppUtils.APIFails(getActivity(),t);
                                progressDialogFragment.dismiss();
                            }
                        });

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();


        alert.show();
    }


        private boolean checkPermissionExternalStorage() {
            int externalStorage = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);

            return externalStorage == PackageManager.PERMISSION_GRANTED ;
        }


        private void requestPermissionForExternalStorage() {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) ) {
//            Toast.makeText(this,
//                    R.string.permissions_needed,
//                    Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(
                        getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_EXTERNAL_STORAGE);

            } else {
                ActivityCompat.requestPermissions(
                        getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_EXTERNAL_STORAGE);
            }
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            if (grantResults.length > 0 && permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // check whether storage permission granted or not.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // do what you want;
                    openFile();
                } else {
                    requestPermissionForExternalStorage();
                }
            }
        }


    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (requestCode == PICK_PDF_FILE){
            selectedfile = resultData.getData();
            uploadFile(selectedfile);
          //  uploadassignment();
        }

    }

    private void openFile() {
//        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
//        intent.setType("application/pdf");
//
//        // Optionally, specify a URI for the file that should appear in the
//        // system file picker when it loads.
//       // intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, pickerInitialUri);
//
//        startActivityForResult(intent, PICK_PDF_FILE);
//        Toast.makeText(this,"File added",Toast.LENGTH_SHORT).show();
        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
//        String[] mimeTypes = {"image/*", "application/pdf"};
        String[] mimeTypes = {"application/pdf"};
        chooseFile.setType("*/*");
        chooseFile.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        chooseFile = Intent.createChooser(chooseFile, "Choose a file");
        startActivityForResult(chooseFile, PICK_PDF_FILE);
    }
    private void uploadFile(Uri fileUri) {
        //creating a file
        String path = FileUtils.getPath(getContext(),fileUri);
        File file = new File(path);
        //creating request body for file
        RequestBody reqFile = RequestBody.create(MediaType.parse("pdf/*"), file);
//        RequestBody body = RequestBody.create(MediaType.parse(getContentResolver().getType(fileUri)),file);
        requestFile = MultipartBody.Part.createFormData("assignment_file", file.getName(), reqFile);
//        filename.setText(file.getName());
//        filename.setVisibility(View.VISIBLE);
        Toast.makeText(getContext(),"PDF file added successfully",Toast.LENGTH_SHORT).show();
        uploadassignment();
    }


        private void uploadassignment() {
            Call<ResponseBody>  bodyCall = api.addassignmentbystudent(headermap,assignment_id,userDetailsSharedPref.getString("userId"),requestFile);
            ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putString("message","Loading");
            progressDialogFragment.setArguments(bundle);
            progressDialogFragment.show(getActivity().getSupportFragmentManager(),"please wait");
            bodyCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful())
                    {
                        Toast.makeText(getContext(), "uploaded successfully", Toast.LENGTH_LONG).show();
                        try {
                            String res = response.body().string();
                            JSONObject jsonObject =  new JSONObject(res);
                            Toast.makeText(getContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                          //  parts.clear();
                            getdata();
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else{
                        if (response.code()==401){
                            AppUtils.SessionExpired(getActivity());
                        }
                        if (response.code()== 422){
                            try {
                                String s = response.errorBody().string();
                                JSONObject jsonObject = new JSONObject(s);
                                String error = jsonObject.getString("errors");
                                JSONObject jsonObject1 = new JSONObject(error);
                                Iterator keys = jsonObject1.keys();
                                while (keys.hasNext()) {
                                    Object key = keys.next();
                                    JSONArray value = jsonObject1.getJSONArray((String) key);
                                    String errormessage = value.getString(0);
                                    if (key.equals("file")) {
                                        Toast.makeText(getContext(),errormessage,Toast.LENGTH_LONG).show();
                                    }

                                }


                            } catch (IOException | JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }
                    progressDialogFragment.dismiss();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    AppUtils.APIFails(getActivity(),t);
                    progressDialogFragment.dismiss();

                }
            });
        }


    }




