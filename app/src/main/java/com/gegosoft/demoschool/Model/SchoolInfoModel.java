package com.gegosoft.demoschool.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SchoolInfoModel {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("schoolName")
        @Expose
        private String schoolName;
        @SerializedName("schoolLogo")
        @Expose
        private String schoolLogo;
        @SerializedName("moto")
        @Expose
        private String moto;
        @SerializedName("affiliatedBy")
        @Expose
        private String affiliatedBy;
        @SerializedName("affiliationNo")
        @Expose
        private String affiliationNo;
        @SerializedName("dateOfEstablishment")
        @Expose
        private String dateOfEstablishment;
        @SerializedName("board")
        @Expose
        private String board;
        @SerializedName("landlineNo")
        @Expose
        private String landlineNo;
        @SerializedName("aboutUs")
        @Expose
        private String aboutUs;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("pincode")
        @Expose
        private String pincode;


        @SerializedName("website")
        @Expose
        private String website;

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getSchoolName() {
            return schoolName;
        }

        public void setSchoolName(String schoolName) {
            this.schoolName = schoolName;
        }

        public String getSchoolLogo() {
            return schoolLogo;
        }

        public void setSchoolLogo(String schoolLogo) {
            this.schoolLogo = schoolLogo;
        }

        public String getMoto() {
            return moto;
        }

        public void setMoto(String moto) {
            this.moto = moto;
        }

        public String getAffiliatedBy() {
            return affiliatedBy;
        }

        public void setAffiliatedBy(String affiliatedBy) {
            this.affiliatedBy = affiliatedBy;
        }

        public String getAffiliationNo() {
            return affiliationNo;
        }

        public void setAffiliationNo(String affiliationNo) {
            this.affiliationNo = affiliationNo;
        }

        public String getDateOfEstablishment() {
            return dateOfEstablishment;
        }

        public void setDateOfEstablishment(String dateOfEstablishment) {
            this.dateOfEstablishment = dateOfEstablishment;
        }

        public String getBoard() {
            return board;
        }

        public void setBoard(String board) {
            this.board = board;
        }

        public String getLandlineNo() {
            return landlineNo;
        }

        public void setLandlineNo(String landlineNo) {
            this.landlineNo = landlineNo;
        }

        public String getAboutUs() {
            return aboutUs;
        }

        public void setAboutUs(String aboutUs) {
            this.aboutUs = aboutUs;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

    }
}
