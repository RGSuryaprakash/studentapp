package com.gegosoft.demoschool.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FeedBackModel {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("feedback_id")
        @Expose
        private Integer feedbackId;
        @SerializedName("messages")
        @Expose
        private List<Message> messages = null;
        @SerializedName("category")
        @Expose
        private String category;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created_on")
        @Expose
        private String createdOn;
        @SerializedName("last_reply_by")
        @Expose
        private String lastReplyBy;
        @SerializedName("last_reply_on")
        @Expose
        private String lastReplyOn;

        public Integer getFeedbackId() {
            return feedbackId;
        }

        public void setFeedbackId(Integer feedbackId) {
            this.feedbackId = feedbackId;
        }

        public List<Message> getMessages() {
            return messages;
        }

        public void setMessages(List<Message> messages) {
            this.messages = messages;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getLastReplyBy() {
            return lastReplyBy;
        }

        public void setLastReplyBy(String lastReplyBy) {
            this.lastReplyBy = lastReplyBy;
        }

        public String getLastReplyOn() {
            return lastReplyOn;
        }

        public void setLastReplyOn(String lastReplyOn) {
            this.lastReplyOn = lastReplyOn;
        }
        public class Message {

            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("time")
            @Expose
            private String time;
            @SerializedName("message")
            @Expose
            private String message;
            @SerializedName("file")
            @Expose
            private List<String> file = null;

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public List<String> getFile() {
                return file;
            }

            public void setFile(List<String> file) {
                this.file = file;
            }
        }
    }
}
