package com.gegosoft.demoschool.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ExamResultMarksModel {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
//    @Valid
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public class Data {

        @SerializedName("total")
        @Expose
        private String total;
        @SerializedName("average")
        @Expose
        private String average;
        @SerializedName("marks")
        @Expose
//        @Valid
        private List<Mark> marks = null;

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getAverage() {
            return average;
        }

        public void setAverage(String average) {
            this.average = average;
        }

        public List<Mark> getMarks() {
            return marks;
        }

        public void setMarks(List<Mark> marks) {
            this.marks = marks;
        }



        public class Mark implements Parcelable {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("subject")
            @Expose
            private String subject;
            @SerializedName("obtainedMarks")
            @Expose
            private String obtainedMarks;
            @SerializedName("teacherComments")
            @Expose
            private String teacherComments;

            protected Mark(Parcel in) {
                if (in.readByte() == 0) {
                    id = null;
                } else {
                    id = in.readInt();
                }
                subject = in.readString();
                obtainedMarks = in.readString();
                teacherComments = in.readString();
            }

            public final Creator<Mark> CREATOR = new Creator<Mark>() {
                @Override
                public Mark createFromParcel(Parcel in) {
                    return new Mark(in);
                }

                @Override
                public Mark[] newArray(int size) {
                    return new Mark[size];
                }
            };

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getSubject() {
                return subject;
            }

            public void setSubject(String subject) {
                this.subject = subject;
            }

            public String getObtainedMarks() {
                return obtainedMarks;
            }

            public void setObtainedMarks(String obtainedMarks) {
                this.obtainedMarks = obtainedMarks;
            }

            public String getTeacherComments() {
                return teacherComments;
            }

            public void setTeacherComments(String teacherComments) {
                this.teacherComments = teacherComments;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel parcel, int i) {
                if (id == null) {
                    parcel.writeByte((byte) 0);
                } else {
                    parcel.writeByte((byte) 1);
                    parcel.writeInt(id);
                }
                parcel.writeString(subject);
                parcel.writeString(obtainedMarks);
                parcel.writeString(teacherComments);
            }
        }
    }

}
