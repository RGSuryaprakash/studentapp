package com.gegosoft.demoschool.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeworkModel {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("class")
        @Expose
        private String _class;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("attachment")
        @Expose
        private String attachment;
        @SerializedName("studentHomeworkStatus")
        @Expose
        private String studentHomeworkStatus;
        @SerializedName("attachmentFile")
        @Expose
        private Object attachmentFile;
        @SerializedName("studentHomeworkId")
        @Expose
        private Integer studentHomeworkId;
        @SerializedName("teacher_comments")
        @Expose
        private Object teacherComments;

        public Object getTeacherComments() {
            return teacherComments;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        @SerializedName("subject")
        @Expose
        private String subject;

        public void setTeacherComments(Object teacherComments) {
            this.teacherComments = teacherComments;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getClass_() {
            return _class;
        }

        public void setClass_(String _class) {
            this._class = _class;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getAttachment() {
            return attachment;
        }

        public void setAttachment(String attachment) {
            this.attachment = attachment;
        }

        public String getStudentHomeworkStatus() {
            return studentHomeworkStatus;
        }

        public void setStudentHomeworkStatus(String studentHomeworkStatus) {
            this.studentHomeworkStatus = studentHomeworkStatus;
        }

        public Object getAttachmentFile() {
            return attachmentFile;
        }

        public void setAttachmentFile(Object attachmentFile) {
            this.attachmentFile = attachmentFile;
        }

        public Integer getStudentHomeworkId() {
            return studentHomeworkId;
        }

        public void setStudentHomeworkId(Integer studentHomeworkId) {
            this.studentHomeworkId = studentHomeworkId;
        }


    }

}

