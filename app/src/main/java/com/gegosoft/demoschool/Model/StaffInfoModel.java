package com.gegosoft.demoschool.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StaffInfoModel {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public class Data {

        @SerializedName("teachers")
        @Expose
        private List<Teacher> teachers = null;
        @SerializedName("classTeacher")
        @Expose
        private ClassTeacher classTeacher;

        public List<Teacher> getTeachers() {
            return teachers;
        }

        public void setTeachers(List<Teacher> teachers) {
            this.teachers = teachers;
        }

        public ClassTeacher getClassTeacher() {
            return classTeacher;
        }

        public void setClassTeacher(ClassTeacher classTeacher) {
            this.classTeacher = classTeacher;
        }
        public class ClassTeacher {

            @SerializedName("Fullname")
            @Expose
            private String fullname;
            @SerializedName("Avatar")
            @Expose
            private String avatar;

            public String getFullname() {
                return fullname;
            }

            public void setFullname(String fullname) {
                this.fullname = fullname;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

        }
        public class Teacher {

            @SerializedName("teacherAvatar")
            @Expose
            private String teacherAvatar;
            @SerializedName("teacherFullname")
            @Expose
            private String teacherFullname;
            @SerializedName("subjectName")
            @Expose
            private String subjectName;

            public String getTeacherAvatar() {
                return teacherAvatar;
            }

            public void setTeacherAvatar(String teacherAvatar) {
                this.teacherAvatar = teacherAvatar;
            }

            public String getTeacherFullname() {
                return teacherFullname;
            }

            public void setTeacherFullname(String teacherFullname) {
                this.teacherFullname = teacherFullname;
            }

            public String getSubjectName() {
                return subjectName;
            }

            public void setSubjectName(String subjectName) {
                this.subjectName = subjectName;
            }

        }
    }
}
