package com.gegosoft.demoschool.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AssignmentModel {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("class")
        @Expose
        private String _class;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("assignedDate")
        @Expose
        private String assignedDate;
        @SerializedName("submissionDate")
        @Expose
        private String submissionDate;
        @SerializedName("attachment")
        @Expose
        private String attachment;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("marks")
        @Expose
        private Integer marks;
        @SerializedName("studentAssignmentStatus")
        @Expose
        private Object studentAssignmentStatus;
        @SerializedName("assignmentFile")
        @Expose
        private Object assignmentFile;
        @SerializedName("submittedOn")
        @Expose
        private Object submittedOn;
        @SerializedName("obtainedMarks")
        @Expose
        private Object obtainedMarks;
        @SerializedName("teacherComments")
        @Expose
        private Object teacherComments;

        @SerializedName("studentAssignmentId")
        @Expose
        private Integer studentAssignmentId;

        public Integer getStudentAssignmentId() {
            return studentAssignmentId;
        }

        public void setStudentAssignmentId(Integer studentAssignmentId) {
            this.studentAssignmentId = studentAssignmentId;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getClass_() {
            return _class;
        }

        public void setClass_(String _class) {
            this._class = _class;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAssignedDate() {
            return assignedDate;
        }

        public void setAssignedDate(String assignedDate) {
            this.assignedDate = assignedDate;
        }

        public String getSubmissionDate() {
            return submissionDate;
        }

        public void setSubmissionDate(String submissionDate) {
            this.submissionDate = submissionDate;
        }

        public String getAttachment() {
            return attachment;
        }

        public void setAttachment(String attachment) {
            this.attachment = attachment;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Integer getMarks() {
            return marks;
        }

        public void setMarks(Integer marks) {
            this.marks = marks;
        }

        public Object getStudentAssignmentStatus() {
            return studentAssignmentStatus;
        }

        public void setStudentAssignmentStatus(Object studentAssignmentStatus) {
            this.studentAssignmentStatus = studentAssignmentStatus;
        }

        public Object getAssignmentFile() {
            return assignmentFile;
        }

        public void setAssignmentFile(Object assignmentFile) {
            this.assignmentFile = assignmentFile;
        }

        public Object getSubmittedOn() {
            return submittedOn;
        }

        public void setSubmittedOn(Object submittedOn) {
            this.submittedOn = submittedOn;
        }

        public Object getObtainedMarks() {
            return obtainedMarks;
        }

        public void setObtainedMarks(Object obtainedMarks) {
            this.obtainedMarks = obtainedMarks;
        }

        public Object getTeacherComments() {
            return teacherComments;
        }

        public void setTeacherComments(Object teacherComments) {
            this.teacherComments = teacherComments;
        }

    }
}