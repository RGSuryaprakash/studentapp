package com.gegosoft.demoschool.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DisciplineModel {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("incidentDate")
        @Expose
        private String incidentDate;
        @SerializedName("teacher")
        @Expose
        private String teacher;
        @SerializedName("incidentDetail")
        @Expose
        private String incidentDetail;
        @SerializedName("response")
        @Expose
        private Object response;
        @SerializedName("action_taken")
        @Expose
        private Integer actionTaken;
        @SerializedName("attachments")
        @Expose
        private Object attachments;
        @SerializedName("notifyParents")
        @Expose
        private Integer notifyParents;
        @SerializedName("is_seen")
        @Expose
        private Integer isSeen;

        public String getIncidentDate() {
            return incidentDate;
        }

        public void setIncidentDate(String incidentDate) {
            this.incidentDate = incidentDate;
        }

        public String getTeacher() {
            return teacher;
        }

        public void setTeacher(String teacher) {
            this.teacher = teacher;
        }

        public String getIncidentDetail() {
            return incidentDetail;
        }

        public void setIncidentDetail(String incidentDetail) {
            this.incidentDetail = incidentDetail;
        }

        public Object getResponse() {
            return response;
        }

        public void setResponse(Object response) {
            this.response = response;
        }

        public Integer getActionTaken() {
            return actionTaken;
        }

        public void setActionTaken(Integer actionTaken) {
            this.actionTaken = actionTaken;
        }

        public Object getAttachments() {
            return attachments;
        }

        public void setAttachments(Object attachments) {
            this.attachments = attachments;
        }

        public Integer getNotifyParents() {
            return notifyParents;
        }

        public void setNotifyParents(Integer notifyParents) {
            this.notifyParents = notifyParents;
        }

        public Integer getIsSeen() {
            return isSeen;
        }

        public void setIsSeen(Integer isSeen) {
            this.isSeen = isSeen;
        }

    }
}
