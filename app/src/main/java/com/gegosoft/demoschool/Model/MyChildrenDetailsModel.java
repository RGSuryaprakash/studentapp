package com.gegosoft.demoschool.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyChildrenDetailsModel {



        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private Data data;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {

            @SerializedName("schoolName")
            @Expose
            private String schoolName;
            @SerializedName("fullName")
            @Expose
            private String fullName;
            @SerializedName("gender")
            @Expose
            private String gender;
            @SerializedName("dateOfBirth")
            @Expose
            private String dateOfBirth;
            @SerializedName("bloodGroup")
            @Expose
            private String bloodGroup;
            @SerializedName("address")
            @Expose
            private String address;
            @SerializedName("city")
            @Expose
            private Integer city;
            @SerializedName("state")
            @Expose
            private Integer state;
            @SerializedName("country")
            @Expose
            private Integer country;
            @SerializedName("pincode")
            @Expose
            private String pincode;
            @SerializedName("birthPlace")
            @Expose
            private String birthPlace;
            @SerializedName("nativePlace")
            @Expose
            private String nativePlace;
            @SerializedName("motherTongue")
            @Expose
            private String motherTongue;
            @SerializedName("caste")
            @Expose
            private String caste;
            @SerializedName("aadharNumber")
            @Expose
            private Object aadharNumber;
            @SerializedName("EMISNumber")
            @Expose
            private Object eMISNumber;
            @SerializedName("emailId")
            @Expose
            private String emailId;
            @SerializedName("mobileNo")
            @Expose
            private String mobileNo;
            @SerializedName("notes")
            @Expose
            private Object notes;
            @SerializedName("avatar")
            @Expose
            private String avatar;
            @SerializedName("class")
            @Expose
            private String _class;
            @SerializedName("standard_id")
            @Expose
            private Integer standard_id;
            @SerializedName("joiningDate")
            @Expose
            private String joiningDate;
            @SerializedName("registrationNumber")
            @Expose
            private String registrationNumber;
            @SerializedName("academicYear")
            @Expose
            private String academicYear;
            @SerializedName("rollNumber")
            @Expose
            private String rollNumber;
            @SerializedName("idCardNumber")
            @Expose
            private String idCardNumber;
            @SerializedName("boardRegistrationNumber")
            @Expose
            private Object boardRegistrationNumber;
            @SerializedName("modeOfTransport")
            @Expose
            private String modeOfTransport;
            @SerializedName("transportDetails")
            @Expose
            private TransportDetails transportDetails;
            @SerializedName("siblings")
            @Expose
            private String siblings;
            @SerializedName("siblingRelation")
            @Expose
            private List<SiblingRelation> siblingRelation = null;

            public String getSchoolName() {
                return schoolName;
            }

            public void setSchoolName(String schoolName) {
                this.schoolName = schoolName;
            }

            public String getFullName() {
                return fullName;
            }

            public void setFullName(String fullName) {
                this.fullName = fullName;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getDateOfBirth() {
                return dateOfBirth;
            }

            public void setDateOfBirth(String dateOfBirth) {
                this.dateOfBirth = dateOfBirth;
            }

            public String getBloodGroup() {
                return bloodGroup;
            }

            public void setBloodGroup(String bloodGroup) {
                this.bloodGroup = bloodGroup;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public Integer getCity() {
                return city;
            }

            public void setCity(Integer city) {
                this.city = city;
            }

            public Integer getState() {
                return state;
            }

            public void setState(Integer state) {
                this.state = state;
            }

            public Integer getCountry() {
                return country;
            }

            public void setCountry(Integer country) {
                this.country = country;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }

            public String getBirthPlace() {
                return birthPlace;
            }

            public void setBirthPlace(String birthPlace) {
                this.birthPlace = birthPlace;
            }

            public String getNativePlace() {
                return nativePlace;
            }

            public void setNativePlace(String nativePlace) {
                this.nativePlace = nativePlace;
            }

            public String getMotherTongue() {
                return motherTongue;
            }

            public void setMotherTongue(String motherTongue) {
                this.motherTongue = motherTongue;
            }

            public String getCaste() {
                return caste;
            }

            public void setCaste(String caste) {
                this.caste = caste;
            }

            public Object getAadharNumber() {
                return aadharNumber;
            }

            public void setAadharNumber(Object aadharNumber) {
                this.aadharNumber = aadharNumber;
            }

            public Object getEMISNumber() {
                return eMISNumber;
            }

            public void setEMISNumber(Object eMISNumber) {
                this.eMISNumber = eMISNumber;
            }

            public String getEmailId() {
                return emailId;
            }

            public void setEmailId(String emailId) {
                this.emailId = emailId;
            }

            public String getMobileNo() {
                return mobileNo;
            }

            public void setMobileNo(String mobileNo) {
                this.mobileNo = mobileNo;
            }

            public Object getNotes() {
                return notes;
            }

            public void setNotes(Object notes) {
                this.notes = notes;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getClass_() {
                return _class;
            }

            public void setClass_(String _class) {
                this._class = _class;
            }

            public Integer getStandard_id() {
                return standard_id;
            }

            public void setStandard_id(Integer standard_id) {
                this.standard_id = standard_id;
            }

            public String getJoiningDate() {
                return joiningDate;
            }

            public void setJoiningDate(String joiningDate) {
                this.joiningDate = joiningDate;
            }

            public String getRegistrationNumber() {
                return registrationNumber;
            }

            public void setRegistrationNumber(String registrationNumber) {
                this.registrationNumber = registrationNumber;
            }

            public String getAcademicYear() {
                return academicYear;
            }

            public void setAcademicYear(String academicYear) {
                this.academicYear = academicYear;
            }

            public String getRollNumber() {
                return rollNumber;
            }

            public void setRollNumber(String rollNumber) {
                this.rollNumber = rollNumber;
            }

            public String getIdCardNumber() {
                return idCardNumber;
            }

            public void setIdCardNumber(String idCardNumber) {
                this.idCardNumber = idCardNumber;
            }

            public Object getBoardRegistrationNumber() {
                return boardRegistrationNumber;
            }

            public void setBoardRegistrationNumber(Object boardRegistrationNumber) {
                this.boardRegistrationNumber = boardRegistrationNumber;
            }

            public String getModeOfTransport() {
                return modeOfTransport;
            }

            public void setModeOfTransport(String modeOfTransport) {
                this.modeOfTransport = modeOfTransport;
            }

            public TransportDetails getTransportDetails() {
                return transportDetails;
            }

            public void setTransportDetails(TransportDetails transportDetails) {
                this.transportDetails = transportDetails;
            }

            public String getSiblings() {
                return siblings;
            }

            public void setSiblings(String siblings) {
                this.siblings = siblings;
            }

            public List<SiblingRelation> getSiblingRelation() {
                return siblingRelation;
            }

            public void setSiblingRelation(List<SiblingRelation> siblingRelation) {
                this.siblingRelation = siblingRelation;
            }
            public class SiblingRelation {

                @SerializedName("sibling_relation")
                @Expose
                private String siblingRelation;
                @SerializedName("sibling_name")
                @Expose
                private String siblingName;
                @SerializedName("sibling_date_of_birth")
                @Expose
                private SiblingDateOfBirth siblingDateOfBirth;
                @SerializedName("sibling_standard")
                @Expose
                private Integer siblingStandard;

                public String getSiblingRelation() {
                    return siblingRelation;
                }

                public void setSiblingRelation(String siblingRelation) {
                    this.siblingRelation = siblingRelation;
                }

                public String getSiblingName() {
                    return siblingName;
                }

                public void setSiblingName(String siblingName) {
                    this.siblingName = siblingName;
                }

                public SiblingDateOfBirth getSiblingDateOfBirth() {
                    return siblingDateOfBirth;
                }

                public void setSiblingDateOfBirth(SiblingDateOfBirth siblingDateOfBirth) {
                    this.siblingDateOfBirth = siblingDateOfBirth;
                }

                public Integer getSiblingStandard() {
                    return siblingStandard;
                }

                public void setSiblingStandard(Integer siblingStandard) {
                    this.siblingStandard = siblingStandard;
                }
                public class SiblingDateOfBirth {

                    @SerializedName("date")
                    @Expose
                    private String date;
                    @SerializedName("timezone_type")
                    @Expose
                    private Integer timezoneType;
                    @SerializedName("timezone")
                    @Expose
                    private String timezone;

                    public String getDate() {
                        return date;
                    }

                    public void setDate(String date) {
                        this.date = date;
                    }

                    public Integer getTimezoneType() {
                        return timezoneType;
                    }

                    public void setTimezoneType(Integer timezoneType) {
                        this.timezoneType = timezoneType;
                    }

                    public String getTimezone() {
                        return timezone;
                    }

                    public void setTimezone(String timezone) {
                        this.timezone = timezone;
                    }

                }
            }
            public class TransportDetails {

                @SerializedName("driver_name")
                @Expose
                private String driverName;
                @SerializedName("driver_contact_number")
                @Expose
                private String driverContactNumber;

                public String getDriverName() {
                    return driverName;
                }

                public void setDriverName(String driverName) {
                    this.driverName = driverName;
                }

                public String getDriverContactNumber() {
                    return driverContactNumber;
                }

                public void setDriverContactNumber(String driverContactNumber) {
                    this.driverContactNumber = driverContactNumber;
                }

            }
        }


}
