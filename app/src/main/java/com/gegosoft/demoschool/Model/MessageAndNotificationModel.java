package com.gegosoft.demoschool.Model;

public class MessageAndNotificationModel {
    int icon;
    String heading;
    String subheading;
    String time;


    public MessageAndNotificationModel() {
    }

    public MessageAndNotificationModel(int icon, String heading, String subheading, String time) {
        this.icon = icon;
        this.heading = heading;
        this.subheading = subheading;
        this.time = time;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getSubheading() {
        return subheading;
    }

    public void setSubheading(String subheading) {
        this.subheading = subheading;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
