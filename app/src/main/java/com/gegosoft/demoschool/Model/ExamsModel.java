package com.gegosoft.demoschool.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ExamsModel {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public  class Datum  {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("type")
        @Expose
        private Object type;
        @SerializedName("schedule")
        @Expose
        private List<Schedule> schedule = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getType() {
            return type;
        }

        public void setType(Object type) {
            this.type = type;
        }

        public List<Schedule> getSchedule() {
            return schedule;
        }

        public void setSchedule(List<Schedule> schedule) {
            this.schedule = schedule;
        }
        public  class Schedule implements Parcelable {

            @SerializedName("subject")
            @Expose
            private String subject;
            @SerializedName("startDate")
            @Expose
            private String startDate;
            @SerializedName("duration")
            @Expose
            private String duration;

            protected Schedule(Parcel in) {
                subject = in.readString();
                startDate = in.readString();
                duration = in.readString();
            }

            public final Creator<Schedule> CREATOR = new Creator<Schedule>() {
                @Override
                public Schedule createFromParcel(Parcel in) {
                    return new Schedule(in);
                }

                @Override
                public Schedule[] newArray(int size) {
                    return new Schedule[size];
                }
            };

            public String getSubject() {
                return subject;
            }

            public void setSubject(String subject) {
                this.subject = subject;
            }

            public String getStartDate() {
                return startDate;
            }

            public void setStartDate(String startDate) {
                this.startDate = startDate;
            }

            public String getDuration() {
                return duration;
            }

            public void setDuration(String duration) {
                this.duration = duration;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(subject);
                dest.writeString(startDate);
                dest.writeString(duration);
            }
        }
    }
}
